﻿using System.Drawing;

namespace PoE_Trade_Overlay
{
    // What a colorful life
    public static class Constants
    {
        public static Color Normal = Color.FromArgb(200, 200, 200);
        public static Color Magic = Color.FromArgb(136, 136, 255);
        public static Color Rare = Color.FromArgb(255, 255, 119);
        public static Color Unique = Color.FromArgb(175, 96, 37);
        public static Color Gem = Color.FromArgb(27, 162, 155);
        public static Color Prophecy = Color.FromArgb(181, 56, 172);
        public static Color Relic = Color.FromArgb(27, 162, 155);
        public static Color Divination = Color.FromArgb(108, 127, 108);
        public static Color Currency = Color.FromArgb(170, 158, 130);
        public static Color HoverEnter = Color.FromArgb(25, 25, 25);
        public static Color HoverLeave = Color.FromArgb(8, 8, 8);
    }
}