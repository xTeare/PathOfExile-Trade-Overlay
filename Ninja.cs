﻿using PoE_Trade_Overlay.Queries;
using System;
using System.Data;
using System.IO;
using System.Net;

namespace PoE_Trade_Overlay
{
    public static class Ninja
    {
        public static poeNinja ninja;
        private const string url = "https://poe.ninja/api/Data/GetCurrencyOverview?league=";
        public static bool loaded;
        /// <summary>
        /// Load Currency Data from poe.ninja and save it into (DataTable)Data.ninja
        /// </summary>
        ///
        public static void Load(string league)
        {
            poeNinja nin = new poeNinja();
            try
            {

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url + league);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0";

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var jayson = streamReader.ReadToEnd();
                    Console.WriteLine(jayson);
                    nin = poeNinja.FromJson(jayson);
                }
                ninja = nin;

                DataTable dt = new DataTable();
                dt.Columns.Add("currencyTypeName");
                dt.Columns.Add("chaosEquivalent");

                foreach (Line l in ninja.Lines)
                {
                    dt.Rows.Add(l.CurrencyTypeName, l.ChaosEquivalent);
                }
                loaded = true;
                Data.ninja = dt;
            }
            catch (Exception ex)
            {
                Console.WriteLine("poeNinja.Load Exception : " + ex.Message);
                loaded = false;
            }
        }
    }
}