﻿using System.IO;

namespace PoE_Trade_Overlay
{
    internal class Config
    {
        public static int windowOpacity = 100;

        public static int mainX = 100;
        public static int mainY = 100;

        public static int searchX = 100;
        public static int searchY = 100;
        public static int searchHeight = 960;
        public static int searchWidth = 960;

        public static int settingsX = 100;
        public static int settingsY = 100;

        public static int updateX = 100;
        public static int updateY = 100;

        public static bool hideSocketsOnHover = true;
        public static bool onlyRenderIfPoeIsActive = false;
        public static bool filterAFKPlayers = false;
        public static bool hideLegacyMaps = true;
        public static string currentTheme = "Dark";
        public static string version = "0.1";
        public static string league = "Standard";
        public static string atlasVersion = "War for the Atlas";
        public static bool loaded;

        public static void Load()
        {
            if (!File.Exists("config.ini"))
            {
                Save();
            }
            if (!loaded)
            {
                IniFile config = new IniFile("config.ini");
                version = config.Read("version");
                league = config.Read("league");
                atlasVersion = config.Read("atlasVersion");
                hideSocketsOnHover = config.Read("hideSocketsOnHover").ToBoolean();
                windowOpacity = config.Read("WindowOpacity").ToInt();
                mainX = config.Read("mainX").ToInt();
                mainY = config.Read("mainY").ToInt();
                searchX = config.Read("searchX").ToInt();
                searchY = config.Read("searchY").ToInt();
                settingsX = config.Read("settingsX").ToInt();
                settingsY = config.Read("settingsY").ToInt();
                updateX = config.Read("updateX").ToInt();
                updateY = config.Read("updateY").ToInt();
                searchHeight = config.Read("searchHeight").ToInt();
                searchWidth = config.Read("searchWidth").ToInt();
                onlyRenderIfPoeIsActive = config.Read("onlyRenderIfPoeIsActive").ToBoolean();
                filterAFKPlayers = config.Read("filterAFKPlayers").ToBoolean();
                hideLegacyMaps = config.Read("hideLegacyMaps").ToBoolean();
                currentTheme = config.Read("currentTheme");

                loaded = true;
            }
        }

        public static void Save()
        {
            IniFile config = new IniFile("config.ini");
            config.Write("version", version);
            config.Write("league", league);
            config.Write("atlasVersion", atlasVersion);
            config.Write("hideSocketsOnHover", hideSocketsOnHover.ToString());
            config.Write("WindowOpacity", windowOpacity.ToString());

            config.Write("mainX", mainX.ToString());
            config.Write("mainY", mainY.ToString());
            config.Write("searchX", searchX.ToString());
            config.Write("searchY", searchY.ToString());
            config.Write("settingsX", settingsX.ToString());
            config.Write("settingsY", settingsY.ToString());
            config.Write("updateX", updateX.ToString());
            config.Write("updateY", updateY.ToString());
            config.Write("searchHeight", searchHeight.ToString());
            config.Write("searchWidth", searchWidth.ToString());
            config.Write("onlyRenderIfPoeIsActive", onlyRenderIfPoeIsActive.ToString());
            config.Write("filterAFKPlayers", filterAFKPlayers.ToString());
            config.Write("hideLegacyMaps", hideLegacyMaps.ToString());
            config.Write("currentTheme", currentTheme);
        }
    }
}