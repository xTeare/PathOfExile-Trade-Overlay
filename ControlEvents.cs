﻿using PoE_Trade_Overlay.Forms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PoE_Trade_Overlay
{
    public static class ControlEvents
    {

        private static bool clicked = false;
        private static int iOldX;
        private static int iOldY;
        private static int iClickX;
        private static int iClickY;
        public static Control panel;


        public static void PanelMove_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point p = new Point(e.X, e.Y);
                iOldX = p.X;
                iOldY = p.Y;
                iClickX = e.X;
                iClickY = e.Y;
                clicked = true;
            }
        }
        
        public static void PanelMove_MouseMove(object sender, MouseEventArgs e)
        {
            Control ctrl = (Control)sender;
            switch (ctrl.Name)
            {
                case "pb_drag":
                    panel = Form_Main.instance;
                    break;
                case "pb_title":
                case "lbl_header":
                    panel = Form_Search.instance;
                    break;
                case "lbl_header_Update":
                    panel = Form_Update.instance;
                    break;

            }

            panel = ctrl.TopLevelControl;

            if (clicked)
            {
                Point p = new Point(); // New Coordinate
                p.X = e.X + panel.Left;
                p.Y = e.Y + panel.Top;
                panel.Left = p.X - iClickX;
                panel.Top = p.Y - iClickY;
            }
        }

        public static void Eventasdasds(Control c)
        {

        }

        public static void AssignMoveEvents(this Control ctrl)
        {
            ctrl.MouseMove += PanelMove_MouseMove;
            ctrl.MouseDown += PanelMove_MouseDown;
            ctrl.MouseUp += PanelMove_MouseUp;
        }

        public static void PanelMove_MouseUp(object sender, MouseEventArgs e)
        {
            clicked = false;

            switch (panel.Name)
            {
                case "panel_main":
                    Config.mainX = panel.Location.X;
                    Config.mainY = panel.Location.Y;
                    break;
                case "pb_title":
                case "lbl_header":
                    Config.searchX = panel.Location.X;
                    Config.searchY = panel.Location.Y;
                    break;
                case "lbl_header_Update":
                    Config.updateX = panel.Location.X;
                    Config.updateY = panel.Location.Y;
                    break;
            }
            Config.Save();

        }
    }
}
