﻿using Newtonsoft.Json;
using PoE_Trade_Overlay.Queries;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace PoE_Trade_Overlay.Controls
{
    public partial class SearchResult_Minimalistic : UserControl
    {
        private Result _result;

        public Result result
        {
            get { return _result; }
            set
            {
                _result = value;
                SetControl();
            }
        }

        public SearchQuery sq;
        public bool sortModDesc;
        public bool sortGemProgressDesc;
        public bool sortPriceDesc;
        public TabPage parent;
        public string pseudo;
        public string[] eventProps = new string[] { "Item Level", "Quality", "Physical Damage", "Critical Strike Chance", "Attacks per Second", "Energy Shield", "Armour", "Evasion Rating", "Chance to Block" };

        public string modToSort;

        public ToolTip tip = new ToolTip();

        public Color ForeColor_Button;
        public Color ForeColor_Online;
        public Color ForeColor_Offline;
        public Color ForeColor_Afk;
        public Color BackColor_Button;
        public Color BackColor_Online;
        public Color BackColor_Offline;
        public Color BackColor_Afk;
        public Color BackColor_ModHover;

        public Color Normal;
        public Color Magic;
        public Color Rare;
        public Color Unique;
        public Color Gem;
        public Color Prophecy;
        public Color Relic;
        public Color Divination;
        public Color Currency;
        public Color Pseudo;
        public Color Corrupted;
        public Color Crafted;
        public Color Veiled;
        public Color Unidentified;
        public Color Bar;
        public Color Separator;

        public SearchResult_Minimalistic()
        {
            InitializeComponent();

            tip.SetToolTip(lbl_PiC, "Displays the asking currency in the equivalent amount of chaos.\nThis uses poe.ninja's currency course");
            tip.SetToolTip(lbl_ign, "Name of the last played Character\nClick to execute /whois");
            tip.SetToolTip(lbl_acc, "Name of the sellers account\nClick to open his profile");
            tip.SetToolTip(btn_whisper, "Whisper this seller. If you have PoE running, it will directly send this message.\nIf not, it's put in your Clipboard");
        }

        public string GetListedString(DateTimeOffset listed)
        {
            DateTime dt = listed.DateTime.ToUniversalTime();
            DateTime now = DateTime.Now.ToUniversalTime();
            TimeSpan span = now.Subtract(dt);
            string newS = "";
            newS += dt.Month + "/";
            newS += dt.Day + "/";
            newS += (dt.Year - 2000) + " (";

            if (span.Days != 0)
                newS += span.Days + "d,";
            if (span.Hours != 0)
                newS += span.Hours + "hrs,";
            if (span.Minutes != 0)
                newS += span.Minutes + "min,";
            if (span.Seconds != 0)
                newS += span.Seconds + "sec";

            newS += " ago)";
            return newS;

        }

        public void SetControl()
        {

            lbl_acc.BackColor = BackColor;
            lbl_acc.ForeColor = ForeColor;

            lbl_ign.BackColor = BackColor;
            lbl_ign.ForeColor = ForeColor;

            lbl_PiC.BackColor = BackColor;
            lbl_PiC.ForeColor = ForeColor;

            lbl_price.BackColor = BackColor;
            lbl_price.ForeColor = ForeColor;

            lbl_priceAmount.BackColor = BackColor;
            lbl_priceAmount.ForeColor = ForeColor;

            lbl_verified.ForeColor = ForeColor;
            lbl_verified.BackColor = BackColor;

            pb_itemArt.BackColor = BackColor;
            btn_whisper.ForeColor = ForeColor_Button;
            btn_whisper.BackColor = BackColor_Button;

            if (result == null)
                return;

            Item i = result.Item;
            Listing l = result.Listing;

            Color itemNameColor = Normal;
            switch (i.FrameType)
            {
                case 1: /// MAGIC ITEM
                    itemNameColor = Magic;
                    break;

                case 2: /// RARE ITEM
                    itemNameColor = Rare;
                    break;

                case 3: /// UNIQUE ITEM
                    itemNameColor = Unique;
                    break;

                case 4: /// SKILL GEM
                    itemNameColor = Gem;
                    break;

                case 5: /// ESSENCE
                    itemNameColor = Currency;
                    break;

                case 6: /// DIVINATION CARD
                    itemNameColor = Divination;
                    break;

                case 8: /// Prophecy
                    itemNameColor = Prophecy;
                    break;

                case 9: /// Relic / Foil
                    itemNameColor = Relic;
                    break;
            }

            int augTop = 3;

            string listed = GetListedString(l.Indexed);

            Size size_Listed = TextRenderer.MeasureText(listed, new Font(new FontFamily("Consolas"), 8));
            Label lbl_listed = CreateLabel("listed", listed, size_Listed.Width, this.ForeColor, this, new Point(this.Width - size_Listed.Width, augTop), false, 8);
            augTop = 21;
            if (i.Extended != null)
            {
                if (i.Extended.DpsAug == true)
                {
                    Size s = TextRenderer.MeasureText("DPS at +20% Quality: " + i.Extended.Dps, new Font(new FontFamily("Consolas"), 9));
                    CreateLabel("lbl", "DPS at +20% Quality: " + i.Extended.Dps, s.Width, this.ForeColor, this, new Point(this.Width - s.Width, augTop), true, 9);
                    augTop += 13;
                }
                if (i.Extended.PdpsAug == true)
                {
                    Size s = TextRenderer.MeasureText("Physical DPS at +20% Quality: " + i.Extended.Pdps, new Font(new FontFamily("Consolas"), 9));
                    CreateLabel("lbl", "Physical DPS at +20% Quality: " + i.Extended.Pdps, s.Width, this.ForeColor, this, new Point(this.Width - s.Width, augTop), true, 9);
                    augTop += 13;
                }
                if (i.Extended.Edps != null && i.Extended.Edps != 0)
                {
                    Size s = TextRenderer.MeasureText("Elemental DPS: " + i.Extended.Edps, new Font(new FontFamily("Consolas"), 9));
                    CreateLabel("lbl", "Elemental DPS: " + i.Extended.Edps, s.Width, this.ForeColor, this, new Point(this.Width - s.Width, augTop), true, 9);
                    augTop += 13;
                }
            }

            Label lbl_itemName = CreateLabel("lbl_itemName", i.Name + ((i.Name != string.Empty) ? " " : "") + i.TypeLine, flp.Width - 6, itemNameColor, new Padding(3, 0, 3, 6), flp, false, 9);

            if (i.FlavourText != null)
            {
                if (i.FlavourText.Count > 0)
                {
                    string flav = "";
                    foreach (string s in i.FlavourText)
                        flav += s;

                    tip.SetToolTip(lbl_itemName, flav);
                }
            }

            if (i.Properties != null)
            {
                foreach (Property p in i.Properties)
                {
                    string name = p.Name;

                    string add = "";
                    if (i.TypeLine.Contains("Flask"))
                    {
                        for (int x = 0; x < p.Values.Count; x++)
                            for (int y = 0; y < p.Values[x].Count; y++)
                                name = name.Replace("%" + x, p.Values[x][y].String);
                    }
                    else
                    {
                        name = p.Name;
                        if (p.Values.Count > 0)
                        {
                            foreach (List<Value> val in p.Values)
                            {
                                foreach (Value val2 in val)
                                {
                                    if (val2.String == null)
                                        continue;

                                    if (val2.String != string.Empty)
                                        add += ": " + val2.String.Replace(":", "");
                                }
                            }
                        }
                    }

                    bool hasEvents = false;
                    foreach (string s in eventProps)
                        if (p.Name == s)
                            hasEvents = true;

                    CreateLabel(p.Name + ";" + add, name + add, flp.Width - 6, this.ForeColor, flp, hasEvents, 9, hasEvents);
                }
            }

            CreateSeparator(1, Separator, flp);

            if (i.Ilvl != 0)
                CreateLabel("lbl_ilvl", "Item Level: " + i.Ilvl, flp.Width - 6, this.ForeColor, flp, true, 9, true);

            if (i.Requirements != null)
            {
                string t = "";
                foreach (Property req in i.Requirements)
                {
                    if (req.Name == "Level")
                    {
                        t = "Requires Level ";
                        foreach (List<Value> req2 in req.Values)
                        {
                            foreach (Value req3 in req2)
                            {
                                if (req3.String == null)
                                    continue;
                                t += req3.String;
                            }
                        }
                    }
                    else
                    {
                        foreach (List<Value> req2 in req.Values)
                        {
                            foreach (Value req3 in req2)
                            {
                                if (req3.String == null)
                                    continue;
                                if (t == string.Empty) // Check if it is the first Requirement
                                {
                                    t += req3.String + " " + req.Name;
                                }
                                else
                                {
                                    t += ", " + req3.String + " " + req.Name;
                                }
                            }
                        }
                    }
                }

                bool hasEvents = false;

                string[] eventProps = new string[] { "Item Level" };
                foreach (string s in eventProps)
                    if (t.Contains(s))
                        hasEvents = true;

                CreateLabel("lbl_ilvl", t, flp.Width - 6, this.ForeColor, flp, hasEvents, 9, hasEvents);
            }

            CreateSeparator(1, Separator, flp);
            
            /// ENCHANTS
            /// + separator if there are any
            if (i.EnchantMods != null)
            {
                foreach (string s in i.EnchantMods)
                {
                    /// Important to set label name as we need it to determine whether it's an implicit or not
                    CreateLabel("lbl_enchant", s, flp.Width - 6, Magic, flp, true, 9, true);
                }

                CreateSeparator(1, Separator, flp);
            }

            /// IMPLICITS
            /// + separator if there are any
            if (i.ImplicitMods != null)
            {
                foreach (string s in i.ImplicitMods)
                {
                    /// Important to set label name as we need it to determine whether it's an implicit or not
                    //CreateLabel("lbl_implicit", s, flp.Width - 6, this.ForeColor, flp, true, 9, true);
                    CreateModLabel("lbl_implicit", new string[] { s }, new string[] { Normal.ToShortColor() }, flp.Width - 6, flp, true, 9, true, GetModInfo(Data.GetMod(s, true)));
                }

                CreateSeparator(1, Separator, flp);
            }

            /// EXPLICITS
            ///
            if (i.ExplicitMods != null)
            {
                foreach (string s in i.ExplicitMods)
                {
                    CreateModLabel("lbl_explicit", new string[] { s }, new string[] { Normal.ToShortColor() }, flp.Width - 6, flp, true, 9, true, GetModInfo(Data.GetMod(s)));
                }
            }

            /// VEILED MODS
            ///
            if (i.CraftMods != null)
            {
                foreach (string s in i.CraftMods)
                {
                    CreateModLabel("lbl_Crafted", new string[] { s }, new string[] { Crafted.ToShortColor() }, flp.Width - 6, flp, true, 9, true, GetModInfo(Data.GetMod(s)));
                }
            }

            /// VEILED MODS
            ///
            if (i.VeiledMods != null)
            {
                foreach (string s in i.VeiledMods)
                {
                    CreateModLabel("lbl_Veiled", new string[] { string.Format("Veiled {0} Mod",s.Contains("Prefix")? "Prefix" : "Suffix") }, new string[] { Veiled.ToShortColor() }, flp.Width - 6, flp, true, 9, true, GetModInfo(Data.GetMod(s)));
                }
            }


            if (result.Item.PseudoMods != null)
            {
                foreach (string pseudo in result.Item.PseudoMods)
                {
                    CreateLabel("lbl_pseudo", pseudo, flp.Width - 6, Pseudo, flp, true, 9, true);
                }
            }

            if (i.Corrupted)
                CreateLabel("lbl_corrupted", "Corrupted", flp.Width - 6, Color.Red, flp, false, 9, false);

            if (!i.Identified)
                CreateLabel("lbl_unidentified", "Unidentified", flp.Width - 6, Color.Red, flp, false, 9, false);

            CreateSeparator(1, Separator, flp);

            if (i.FrameType == 4)
            {
                if (i.AdditionalProperties != null)
                {
                    if (i.AdditionalProperties.Count > 0)
                    {
                        foreach (AdditionalProperty add in i.AdditionalProperties)
                        {
                            if (add.DisplayMode == 2) // Display EXP bar
                            {
                                // Create Panel, add 2 pb's (bar + fill) and label for numbers
                                Panel panel = new Panel();
                                panel.Name = "GemExp";
                                panel.Size = new Size(flp.Width - 6, 14);
                                flp.Controls.Add(panel);

                                PictureBox frame = new PictureBox();
                                panel.Controls.Add(frame);
                                frame.Name = "Frame";

                                PictureBox fill = new PictureBox();
                                frame.Controls.Add(fill);
                                fill.Name = "Fill";

                                frame.Size = new Size(212, 12);
                                fill.Size = new Size((int)((frame.Width - 6) * add.Progress), 5);

                                frame.Location = new Point(3, 1);
                                fill.Location = new Point(3, 4);

                                frame.Image = Properties.Resources.ItemsExperienceBar;
                                fill.BackColor = Bar;

                                Label lbl = new Label();
                                lbl.Text = add.Values[0][0].String;
                                lbl.Font = new Font("Consolas", 9);
                                lbl.ForeColor = Constants.Normal;
                                lbl.Size = new Size(TextRenderer.MeasureText(lbl.Text, lbl.Font).Width, 12);

                                panel.Controls.Add(lbl);
                                lbl.Location = new Point(frame.Right + 3, 1);

                                panel.MouseEnter += expEnter;
                                frame.MouseEnter += expEnter;
                                fill.MouseEnter += expEnter;
                                lbl.MouseEnter += expEnter;

                                panel.MouseLeave += expLeave;
                                frame.MouseLeave += expLeave;
                                fill.MouseLeave += expLeave;
                                lbl.MouseLeave += expLeave;

                                panel.Click += SortGemEXP_Click;
                                frame.Click += SortGemEXP_Click;
                                fill.Click += SortGemEXP_Click;
                                lbl.Click += SortGemEXP_Click;
                            }
                        }
                    }
                }
            }

            if (l.Price == null)
            {
                lbl_price.Text = "No Price Set";
                lbl_price.Location = new Point(pb_price.Left, lbl_price.Top);
                lbl_priceAmount.Visible = false;
                pb_price.Visible = false;
            }
            else
            {
                lbl_price.Text = (result.Listing.Price.Type == "~b/o") ? "" : "(exact price)";
                string currencyName = result.Listing.Price.Currency.GetFullCurrencyName();
                lbl_priceAmount.Text = string.Format("x{0}", result.Listing.Price.Amount);

                lbl_priceAmount.MouseEnter += eEnter;
                lbl_priceAmount.MouseLeave += eLeave;
                lbl_priceAmount.Click += SortableLabel_Click;

                /// LOAD PRICE IN CHAOS
                ///

                if (currencyName != "Chaos Orb")
                {
                    if (Ninja.loaded)
                    {
                        lbl_PiC.Text = "PiC : " + Extensions.LoadPriceInChaos(currencyName, result.Listing.Price.Amount);
                        lbl_PiC.Visible = true;
                    }
                }
                else
                    lbl_PiC.Visible = false;

                pb_price.LoadAsync("https://" + result.Listing.Price.Currency.GetCurrencyURL());
            }

            if (result.Listing.Account.Online != null)
            {
                // Is Logged in. Now check if afk
                if (result.Listing.Account.Online.Status != null)
                {
                    lbl_accStatus.Text = "afk";
                    lbl_accStatus.BackColor = BackColor_Afk;
                    lbl_accStatus.ForeColor = ForeColor_Afk;
                }
                else
                {
                    lbl_accStatus.Text = "online";
                    lbl_accStatus.BackColor = BackColor_Online;
                    lbl_accStatus.ForeColor = ForeColor_Online;
                }
            }
            else
            {
                lbl_accStatus.Text = "offline";
                lbl_accStatus.BackColor = BackColor_Offline;
                lbl_accStatus.ForeColor = ForeColor_Offline;
            }

            lbl_acc.Text = "acc.: " + result.Listing.Account.Name;
            lbl_ign.Text = "char: " + result.Listing.Account.LastCharacterName;

            panel.Location = new Point(panel.Left, flp.Top + flp.Height);

            // Load item art and resize the BetterPictureBox. Also move the verified label

            LoadItemArtAndResize(i.Icon);
        }

        private PictureBox CreateSeparator(int height, Color color, Control parent)
        {
            PictureBox pb = new PictureBox()
            {
                Size = new Size(parent.Width - 6, height),
                BackColor = color
            };
            parent.Controls.Add(pb);
            return pb;
        }

        private void SortGemEXP_Click(object sender, EventArgs e)
        {
            Console.WriteLine(sortGemProgressDesc);
            Control c = (Control)sender;

            sq.Sort = new Sort();
            sq.Sort.Price = null;

            sq.Sort.stat = (sortGemProgressDesc) ? "asc" : "desc";

            var jsonResolver = new PropertyRenameAndIgnoreSerializerContractResolver();
            jsonResolver.RenameProperty(typeof(Sort), "stat", "gem_level_progress");

            var serializerSettings = new JsonSerializerSettings();
            serializerSettings.ContractResolver = jsonResolver;

            var json = JsonConvert.SerializeObject(sq, serializerSettings);

            sortGemProgressDesc = !sortGemProgressDesc;

            Form_Search.instance.GetTradeData(json, sq, parent, pseudo, "", sortGemProgressDesc);
            Form_Search.instance.headers[Form_Search.instance.tc.TabPages.IndexOf(parent)] = string.Format(" Sort : Gem Level Progress - {0}", (sortGemProgressDesc) ? "desc ▼" : "asc ▲");
            Form_Search.instance.SetHeader();

        }

        private void SortableLabel_Click(object sender, EventArgs e)
        {
            Label lbl = (Label)sender;

            if (lbl.Name == "lbl_priceAmount")
            {

                sq.Sort = new Sort();
                sq.Sort.stat = null;
                sq.Sort.Price = (sortPriceDesc) ? "asc" : "desc";

                sortPriceDesc = !sortPriceDesc;

                var json = sq.ToJson();

                Form_Search.instance.GetTradeData(json, sq, parent, pseudo, "", sortPriceDesc);
                Form_Search.instance.headers[Form_Search.instance.tc.TabPages.IndexOf(parent)] = string.Format(" Sort : Price - {0}", (sortPriceDesc) ? "desc ▼" : "asc ▲");
                Form_Search.instance.SetHeader();
            }

            string lblText = lbl.Text;
            string modId = "";
            string header = "";
            bool sortProperty = false;
            bool sortSum = false;
            foreach (string s in eventProps)
            {
                if (lblText.Contains(s))
                {
                    sortProperty = true;
                    lblText = s;
                    header = s;
                }
            }

            if (lblText.Contains("Sum:"))
                sortSum = true;

            if (sortProperty)
            {
                modId = Data.GetPropertyAbb(lblText);
            }
            else if (sortSum)
            {
                // Get index in pseudo list
                for (int i = 0; i < result.Item.PseudoMods.Count; i++)
                {
                    if (lblText == result.Item.PseudoMods[i])
                    {
                        modId = result.Item.Extended.Hashes.Pseudo[i][0];
                    }
                }
            }
            else
            {
                header = Data.RemoveSpecialChars(lblText, true);
                modId = Data.GetMod(lblText, lbl.Name.Contains("implicit"));
            }

            if (modId != string.Empty)
            {
                if (modToSort == modId)
                {
                    sortModDesc = !sortModDesc;
                }

                sq.Sort = new Sort();
                sq.Sort.stat = (sortModDesc) ? "asc" : "desc";
                sq.Sort.Price = null;

                var jsonResolver = new PropertyRenameAndIgnoreSerializerContractResolver();
                jsonResolver.RenameProperty(typeof(Sort), "stat", (sortProperty || sortSum) ? modId : "stat." + modId);

                var serializerSettings = new JsonSerializerSettings();
                serializerSettings.ContractResolver = jsonResolver;

                var json = JsonConvert.SerializeObject(sq, serializerSettings);

                Console.WriteLine(json);

                modToSort = modId;

                Form_Search.instance.GetTradeData(json, sq, parent, pseudo, modToSort, sortModDesc);
                Form_Search.instance.headers[Form_Search.instance.tc.TabPages.IndexOf(parent)] = string.Format(" Sort : {0} ({1})", header, (sortModDesc) ? "asc ▲ " : "desc ▼");
                Form_Search.instance.SetHeader();
            }
        }

        private void LoadItemArtAndResize(string path)
        {
            pb_itemArt.LoadCompleted += Pb_itemArt_LoadCompleted;
            pb_itemArt.LoadAsync(path);
        }

        private void Pb_itemArt_LoadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            pb_itemArt.Size = new Size(pb_itemArt.Image.Width, pb_itemArt.Image.Height);
            pb_itemArt.Location = new Point((94 - pb_itemArt.Width) / 2, (this.Height / 2) - (pb_itemArt.Image.Height / 2));

            CreateSockets();
        }

        private void CreateSockets()
        {
            if (result.Item.Sockets == null)
                return;

            Label lbl = new Label();
            lbl.Location = new Point(0, 0);
            lbl.Size = new Size(pb_itemArt.Width, pb_itemArt.Height);
            lbl.BackColor = Color.Transparent;
            lbl.MouseEnter += pEnter;
            if (pb_itemArt.Image.Width <= 47)
            {
                // Since one handed weapons are special we just need vertical links
                if (result.Item.Sockets.Count >= 2)
                {
                    if (result.Item.Sockets[0].Group == result.Item.Sockets[1].Group)
                    {
                        BetterPictureBox bpb_linked = new BetterPictureBox();
                        pb_itemArt.Controls.Add(bpb_linked);
                        bpb_linked.Size = new Size(16, 38);
                        bpb_linked.BackColor = Color.Transparent;
                        bpb_linked.Location = new Point(16, 28);
                        bpb_linked.Image = Properties.Resources.link_Vertical;
                    }
                }

                if (result.Item.Sockets.Count >= 3)
                {
                    if (result.Item.Sockets[1].Group == result.Item.Sockets[2].Group)
                    {
                        BetterPictureBox bpb_linked = new BetterPictureBox();
                        pb_itemArt.Controls.Add(bpb_linked);
                        bpb_linked.Size = new Size(16, 38);
                        bpb_linked.BackColor = Color.Transparent;
                        bpb_linked.Location = new Point(16, 76);
                        bpb_linked.Image = Properties.Resources.link_Vertical;
                    }

                }

                for (int i = 0; i < result.Item.Sockets.Count; i++)
                {
                    BetterPictureBox bpb = new BetterPictureBox();
                    pb_itemArt.Controls.Add(bpb);
                    bpb.BackColor = Color.Transparent;
                    bpb.Size = new Size(47, 47);
                    bpb.Location = new Point(0, 0 + (48 * i));

                    switch (result.Item.Sockets[i].SColour)
                    {
                        case "R":
                            bpb.Image = Properties.Resources.str;
                            break;

                        case "G":
                            bpb.Image = Properties.Resources.dex;
                            break;

                        case "B":
                            bpb.Image = Properties.Resources._int;
                            break;

                        case "W":
                            bpb.Image = Properties.Resources.gen;
                            break;
                    }
                }
            }
            else
            {
                // Check if there are any Links and if there are create Lonk images
                // We need to do that first in order to have the Lonk images above the sockets.
                if (result.Item.Sockets.Count >= 2)
                {
                    if (result.Item.Sockets[0].Group == result.Item.Sockets[1].Group)
                    {
                        BetterPictureBox bpb_linked = new BetterPictureBox();
                        pb_itemArt.Controls.Add(bpb_linked);
                        bpb_linked.Size = new Size(38, 16);
                        bpb_linked.BackColor = Color.Transparent;
                        bpb_linked.Location = new Point(28, 16);
                        bpb_linked.Image = Properties.Resources.link_horizontal;
                    }
                }

                if (result.Item.Sockets.Count >= 3)
                {
                    if (result.Item.Sockets[1].Group == result.Item.Sockets[2].Group)
                    {
                        BetterPictureBox bpb_linked = new BetterPictureBox();
                        pb_itemArt.Controls.Add(bpb_linked);
                        bpb_linked.Size = new Size(16, 38);
                        bpb_linked.BackColor = Color.Transparent;
                        bpb_linked.Location = new Point(63, 28);
                        bpb_linked.Image = Properties.Resources.link_Vertical;
                    }
                }

                if (result.Item.Sockets.Count >= 4)
                {
                    if (result.Item.Sockets[2].Group == result.Item.Sockets[3].Group)
                    {
                        BetterPictureBox bpb_linked = new BetterPictureBox();
                        pb_itemArt.Controls.Add(bpb_linked);
                        bpb_linked.Size = new Size(38, 16);
                        bpb_linked.BackColor = Color.Transparent;
                        bpb_linked.Location = new Point(28, 63);
                        bpb_linked.Image = Properties.Resources.link_horizontal;
                    }
                }

                if (result.Item.Sockets.Count >= 5)
                {
                    if (result.Item.Sockets[3].Group == result.Item.Sockets[4].Group)
                    {
                        BetterPictureBox bpb_linked = new BetterPictureBox();
                        pb_itemArt.Controls.Add(bpb_linked);
                        bpb_linked.Size = new Size(16, 38);
                        bpb_linked.BackColor = Color.Transparent;
                        bpb_linked.Location = new Point(16, 78);
                        bpb_linked.Image = Properties.Resources.link_Vertical;
                    }
                }

                if (result.Item.Sockets.Count >= 6)
                {
                    if (result.Item.Sockets[4].Group == result.Item.Sockets[5].Group)
                    {
                        BetterPictureBox bpb_linked = new BetterPictureBox();
                        pb_itemArt.Controls.Add(bpb_linked);
                        bpb_linked.Size = new Size(38, 16);
                        bpb_linked.BackColor = Color.Transparent;
                        bpb_linked.Location = new Point(28, 110);
                        bpb_linked.Image = Properties.Resources.link_horizontal;
                    }
                }

                for (int i = 0; i < result.Item.Sockets.Count; i++)
                {
                    BetterPictureBox bpb = new BetterPictureBox();
                    pb_itemArt.Controls.Add(bpb);
                    bpb.BackColor = Color.Transparent;
                    bpb.Size = new Size(47, 47);
                    bpb.Location = new Point(Data.socketPositions.Rows[i]["x"].ToString().ToInt(), Data.socketPositions.Rows[i]["y"].ToString().ToInt());

                    switch (result.Item.Sockets[i].SColour)
                    {
                        case "R":
                            bpb.Image = Properties.Resources.str;
                            break;

                        case "G":
                            bpb.Image = Properties.Resources.dex;
                            break;

                        case "B":
                            bpb.Image = Properties.Resources._int;
                            break;

                        case "W":
                            bpb.Image = Properties.Resources.gen;
                            break;
                    }
                }
            }
        }

        private string GetModInfo(string modID)
        {
            string mags = "";
            if (result.Item.Extended != null && result.Item.Extended.Mods != null && result.Item.Extended.Mods.Explicit != null)
            {
                foreach (ExplicitClass ex in result.Item.Extended.Mods.Explicit)
                {
                    if (ex.Magnitudes != null)
                    {
                        if (ex.Magnitudes[0].Hash == modID)
                        {
                            if (ex.Name != string.Empty)
                                mags += string.Format("({0}) ", ex.Name);

                            if (ex.Tier != string.Empty)
                                mags += string.Format("{0} ", ex.Tier);

                            Console.WriteLine(ex.Name);
                            Console.WriteLine(ex.Tier);
                            if (ex.Magnitudes.Count == 1)
                            {
                                mags = string.Format("[{0}-{1}]", ex.Magnitudes[0].Min, ex.Magnitudes[0].Max);
                            }
                        }
                    }
                }
            }
            return mags;
        }

        #region CreateLabel
        private ModLabel CreateModLabel(string name, string[] text, string[] colors, int width, Control parent = null, bool hasHoverEvents = false, int fontSize = 8, bool isSortable = false, string modInfo = "")
        {
            ModLabel lbl = new ModLabel()
            {
                Name = name,
                Margin = new Padding(3, 0, 3, 0),
                AutoSize = false,
                ForeColor = Normal,
                modInfo = modInfo,
                Size = new Size(width, 14 * (int)((text.Length / 70) + 1)),
                Font = new Font(new FontFamily("Consolas"), fontSize)
            };

            if (hasHoverEvents)
            {
                lbl.MouseEnter += eEnter;
                lbl.MouseLeave += eLeave;
            }

            for(int i = 0; i < text.Length; i++)
            {
                string c = "200,200,200";
                if (colors.Length >= i)
                    c = colors[i];
                lbl.AddText(text[i], c);
            }
            

            if (isSortable)
                lbl.Click += SortableLabel_Click;

            if (parent != null)
                parent.Controls.Add(lbl);

            return lbl;
        }


        /// <summary>
        /// Creates a label and adds it to the parent, if set.
        /// </summary>
        /// <returns></returns>
        private Label CreateLabel(string name, string text, int width, Color foreColor, Padding margin, Control parent = null, bool hasHoverEvents = false, int fontSize = 8, bool isSortable = false)
        {
            Label lbl = new Label()
            {
                Name = name,
                Text = text,
                Margin = margin,
                ForeColor = foreColor,
                AutoSize = false,
                Size = new Size(width, 14 * (int)((text.Length / 70) + 1)),
                Font = new Font(new FontFamily("Consolas"), fontSize)
            };

            if (hasHoverEvents)
            {
                lbl.MouseEnter += eEnter;
                lbl.MouseLeave += eLeave;
            }

            if (parent != null)
                parent.Controls.Add(lbl);
            if (isSortable)
                lbl.Click += SortableLabel_Click;

            return lbl;
        }

        /// <summary>
        /// Creates a label and adds it to the parent
        /// </summary>
        /// <returns></returns>
        private Label CreateLabel(string name, string text, int width, Color foreColor, Control parent, Point location, bool hasHoverEvents = false, int fontSize = 8, bool isSortable = false)
        {
            Label lbl = new Label()
            {
                Name = name,
                Text = text,
                Location = location,
                Margin = new Padding(3, 0, 3, 0),
                ForeColor = foreColor,
                AutoSize = false,
                Size = new Size(width, 14 * (int)((text.Length / 70) + 1)),
                Font = new Font(new FontFamily("Consolas"), fontSize)
            };

            if (hasHoverEvents)
            {
                lbl.MouseEnter += eEnter;
                lbl.MouseLeave += eLeave;
            }

            if (isSortable)
                lbl.Click += SortableLabel_Click;

            parent.Controls.Add(lbl);
            lbl.BringToFront();

            return lbl;
        }

        private Label CreateLabel(string name, string text, int width, Color foreColor, Control parent = null, bool hasHoverEvents = false, int fontSize = 8, bool isSortable = false)
        {
            Label lbl = new Label()
            {
                Name = name,
                Text = text,
                Margin = new Padding(3, 0, 3, 0),
                ForeColor = foreColor,
                AutoSize = false,
                Size = new Size(width, 14 * (int)((text.Length / 70) + 1)),
                Font = new Font(new FontFamily("Consolas"), fontSize)
            };

            if (hasHoverEvents)
            {
                lbl.MouseEnter += eEnter;
                lbl.MouseLeave += eLeave;
            }

            if (isSortable)
                lbl.Click += SortableLabel_Click;

            if (parent != null)
                parent.Controls.Add(lbl);

            return lbl;
        }

        #endregion CreateLabel

        #region Hover events

        private void expEnter(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            Control c1 = c;

            if (c.Name != "GemExp")
            {
                c1 = c.Parent;
                if (c.Name == "Fill")
                    c1 = c.Parent.Parent;
            }
            c1.BackColor = BackColor_ModHover;
        }

        private void expLeave(object sender, EventArgs e)
        {
            // Check if we hit another control than the panel. If so, we assign the control's parent as the "root" (the panel to change the bg)
            Control c = (Control)sender;
            Control c1 = c;

            if (c.Name != "GemExp")
            {
                c1 = c.Parent;
                if (c.Name == "Fill")
                    c1 = c.Parent.Parent;
            }
            c1.BackColor = BackColor;
        }

        private void pEnter(object sender, EventArgs e)
        {
            Console.WriteLine("EVENT");
            Control c = (Control)sender;
            Console.WriteLine(c.Name);
            foreach (Control c1 in c.Controls)
            {
                if (Config.hideSocketsOnHover)
                    c1.Visible = false;
            }
        }

        private void pLeave(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            foreach (Control c1 in c.Controls)
            {
                c1.Visible = true;
            }
        }

        private void eEnter(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            c.BackColor = BackColor_ModHover;
        }

        private void eLeave(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            c.BackColor = BackColor;
        }

        #endregion Hover events

        private void btn_whisper_Click(object sender, EventArgs e)
        {
            if (ChatHelper.IsPoeOpen())
            {
                ChatHelper.SendChatMessage(result.Listing.Whisper);
            }
            else
            {
                Clipboard.SetText(result.Listing.Whisper);
            }
        }
    }
}