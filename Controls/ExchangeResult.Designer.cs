﻿namespace PoE_Trade_Overlay.Controls
{
    partial class ExchangeResult
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel = new System.Windows.Forms.Panel();
            this.lbl_lastChar = new System.Windows.Forms.Label();
            this.lbl_sellerName = new System.Windows.Forms.Label();
            this.lbl_online = new System.Windows.Forms.Label();
            this.lbl_stock = new System.Windows.Forms.Label();
            this.btn_visitHideout = new System.Windows.Forms.Button();
            this.lbl_payGet = new System.Windows.Forms.Label();
            this.lbl_message = new System.Windows.Forms.Label();
            this.lbl_stockWarning = new System.Windows.Forms.Label();
            this.pb_worth1 = new System.Windows.Forms.PictureBox();
            this.lbl_worth1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbl_getAmountTrack = new System.Windows.Forms.Label();
            this.lbl_payAmountTrack = new System.Windows.Forms.Label();
            this.pb_get = new System.Windows.Forms.PictureBox();
            this.lbl_getType = new System.Windows.Forms.Label();
            this.lbl_getAmount = new System.Windows.Forms.Label();
            this.lbl_payAmount = new System.Windows.Forms.Label();
            this.pb_pay = new System.Windows.Forms.PictureBox();
            this.lbl_payType = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pb_worth2 = new System.Windows.Forms.PictureBox();
            this.pb_worth4 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lbl_worth2 = new System.Windows.Forms.Label();
            this.pb_worth3 = new System.Windows.Forms.PictureBox();
            this.tip = new System.Windows.Forms.ToolTip(this.components);
            this.dragBar1 = new PoE_Trade_Overlay.Controls.DragBar();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_worth1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_get)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_pay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_worth2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_worth4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_worth3)).BeginInit();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.panel.Controls.Add(this.lbl_lastChar);
            this.panel.Controls.Add(this.lbl_sellerName);
            this.panel.Controls.Add(this.lbl_online);
            this.panel.Controls.Add(this.lbl_stock);
            this.panel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel.Location = new System.Drawing.Point(0, 142);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(590, 30);
            this.panel.TabIndex = 14;
            // 
            // lbl_lastChar
            // 
            this.lbl_lastChar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_lastChar.AutoSize = true;
            this.lbl_lastChar.Font = new System.Drawing.Font("Consolas", 8.25F);
            this.lbl_lastChar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_lastChar.Location = new System.Drawing.Point(234, 5);
            this.lbl_lastChar.MinimumSize = new System.Drawing.Size(0, 20);
            this.lbl_lastChar.Name = "lbl_lastChar";
            this.lbl_lastChar.Size = new System.Drawing.Size(115, 20);
            this.lbl_lastChar.TabIndex = 17;
            this.lbl_lastChar.Text = "verylongsellername";
            this.lbl_lastChar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_lastChar.Click += new System.EventHandler(this.lbl_lastChar_Click);
            this.lbl_lastChar.MouseEnter += new System.EventHandler(this.LabelEnter);
            this.lbl_lastChar.MouseLeave += new System.EventHandler(this.LabelLeave);
            // 
            // lbl_sellerName
            // 
            this.lbl_sellerName.AutoSize = true;
            this.lbl_sellerName.Font = new System.Drawing.Font("Consolas", 8.25F);
            this.lbl_sellerName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_sellerName.Location = new System.Drawing.Point(69, 5);
            this.lbl_sellerName.MinimumSize = new System.Drawing.Size(0, 20);
            this.lbl_sellerName.Name = "lbl_sellerName";
            this.lbl_sellerName.Size = new System.Drawing.Size(115, 20);
            this.lbl_sellerName.TabIndex = 16;
            this.lbl_sellerName.Text = "verylongsellername";
            this.lbl_sellerName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_sellerName.Click += new System.EventHandler(this.lbl_sellerName_Click);
            this.lbl_sellerName.MouseEnter += new System.EventHandler(this.LabelEnter);
            this.lbl_sellerName.MouseLeave += new System.EventHandler(this.LabelLeave);
            // 
            // lbl_online
            // 
            this.lbl_online.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(150)))), ((int)(((byte)(48)))));
            this.lbl_online.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold);
            this.lbl_online.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_online.Location = new System.Drawing.Point(3, 5);
            this.lbl_online.Name = "lbl_online";
            this.lbl_online.Size = new System.Drawing.Size(60, 20);
            this.lbl_online.TabIndex = 11;
            this.lbl_online.Text = "Online";
            this.lbl_online.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_stock
            // 
            this.lbl_stock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_stock.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_stock.Location = new System.Drawing.Point(316, 3);
            this.lbl_stock.MinimumSize = new System.Drawing.Size(0, 24);
            this.lbl_stock.Name = "lbl_stock";
            this.lbl_stock.Size = new System.Drawing.Size(271, 24);
            this.lbl_stock.TabIndex = 44;
            this.lbl_stock.Text = " Stock: 7190 Orb of Augmentation";
            this.lbl_stock.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btn_visitHideout
            // 
            this.btn_visitHideout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.btn_visitHideout.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_visitHideout.FlatAppearance.BorderSize = 0;
            this.btn_visitHideout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_visitHideout.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_visitHideout.Image = global::PoE_Trade_Overlay.Properties.Resources.hideout;
            this.btn_visitHideout.Location = new System.Drawing.Point(3, 25);
            this.btn_visitHideout.Name = "btn_visitHideout";
            this.btn_visitHideout.Size = new System.Drawing.Size(26, 26);
            this.btn_visitHideout.TabIndex = 19;
            this.btn_visitHideout.UseVisualStyleBackColor = false;
            // 
            // lbl_payGet
            // 
            this.lbl_payGet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.lbl_payGet.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_payGet.Font = new System.Drawing.Font("Consolas", 10F);
            this.lbl_payGet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_payGet.Location = new System.Drawing.Point(0, 0);
            this.lbl_payGet.MinimumSize = new System.Drawing.Size(0, 22);
            this.lbl_payGet.Name = "lbl_payGet";
            this.lbl_payGet.Size = new System.Drawing.Size(590, 22);
            this.lbl_payGet.TabIndex = 15;
            this.lbl_payGet.Text = "What you get           What you pay";
            this.lbl_payGet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_message
            // 
            this.lbl_message.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_message.AutoSize = true;
            this.lbl_message.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.lbl_message.Font = new System.Drawing.Font("Consolas", 8F);
            this.lbl_message.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_message.Location = new System.Drawing.Point(38, 88);
            this.lbl_message.MinimumSize = new System.Drawing.Size(0, 23);
            this.lbl_message.Name = "lbl_message";
            this.lbl_message.Size = new System.Drawing.Size(541, 23);
            this.lbl_message.TabIndex = 18;
            this.lbl_message.Text = "@JanuszWasacz Hi, I\'d like to buy your 2500 Chaos Orb for my 7450 Orb of Fusing i" +
    "n Delve.";
            this.lbl_message.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_message.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lbl_message_MouseClick);
            this.lbl_message.MouseEnter += new System.EventHandler(this.LabelEnter);
            this.lbl_message.MouseLeave += new System.EventHandler(this.LabelLeave);
            // 
            // lbl_stockWarning
            // 
            this.lbl_stockWarning.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_stockWarning.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold);
            this.lbl_stockWarning.ForeColor = System.Drawing.Color.Red;
            this.lbl_stockWarning.Location = new System.Drawing.Point(3, 112);
            this.lbl_stockWarning.Name = "lbl_stockWarning";
            this.lbl_stockWarning.Size = new System.Drawing.Size(590, 23);
            this.lbl_stockWarning.TabIndex = 19;
            this.lbl_stockWarning.Text = "Not enough stock !";
            this.lbl_stockWarning.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pb_worth1
            // 
            this.pb_worth1.Location = new System.Drawing.Point(27, 113);
            this.pb_worth1.Name = "pb_worth1";
            this.pb_worth1.Size = new System.Drawing.Size(24, 24);
            this.pb_worth1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_worth1.TabIndex = 20;
            this.pb_worth1.TabStop = false;
            // 
            // lbl_worth1
            // 
            this.lbl_worth1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_worth1.AutoSize = true;
            this.lbl_worth1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_worth1.Location = new System.Drawing.Point(63, 113);
            this.lbl_worth1.MinimumSize = new System.Drawing.Size(0, 24);
            this.lbl_worth1.Name = "lbl_worth1";
            this.lbl_worth1.Size = new System.Drawing.Size(91, 24);
            this.lbl_worth1.TabIndex = 22;
            this.lbl_worth1.Text = "is worth {0} x";
            this.lbl_worth1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.label3.Location = new System.Drawing.Point(3, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 24);
            this.label3.TabIndex = 24;
            this.label3.Text = "1x";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_getAmountTrack
            // 
            this.lbl_getAmountTrack.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_getAmountTrack.AutoSize = true;
            this.lbl_getAmountTrack.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl_getAmountTrack.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_getAmountTrack.Location = new System.Drawing.Point(103, 53);
            this.lbl_getAmountTrack.MinimumSize = new System.Drawing.Size(0, 32);
            this.lbl_getAmountTrack.Name = "lbl_getAmountTrack";
            this.lbl_getAmountTrack.Size = new System.Drawing.Size(44, 32);
            this.lbl_getAmountTrack.TabIndex = 26;
            this.lbl_getAmountTrack.Text = "x 1000";
            this.lbl_getAmountTrack.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_payAmountTrack
            // 
            this.lbl_payAmountTrack.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_payAmountTrack.AutoSize = true;
            this.lbl_payAmountTrack.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lbl_payAmountTrack.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_payAmountTrack.Location = new System.Drawing.Point(449, 53);
            this.lbl_payAmountTrack.MinimumSize = new System.Drawing.Size(0, 32);
            this.lbl_payAmountTrack.Name = "lbl_payAmountTrack";
            this.lbl_payAmountTrack.Size = new System.Drawing.Size(44, 32);
            this.lbl_payAmountTrack.TabIndex = 27;
            this.lbl_payAmountTrack.Text = "1000 x";
            this.lbl_payAmountTrack.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pb_get
            // 
            this.pb_get.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.pb_get.Location = new System.Drawing.Point(192, 27);
            this.pb_get.Name = "pb_get";
            this.pb_get.Size = new System.Drawing.Size(32, 32);
            this.pb_get.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_get.TabIndex = 28;
            this.pb_get.TabStop = false;
            // 
            // lbl_getType
            // 
            this.lbl_getType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_getType.AutoSize = true;
            this.lbl_getType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.lbl_getType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_getType.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbl_getType.Location = new System.Drawing.Point(129, 27);
            this.lbl_getType.MinimumSize = new System.Drawing.Size(0, 32);
            this.lbl_getType.Name = "lbl_getType";
            this.lbl_getType.Size = new System.Drawing.Size(61, 32);
            this.lbl_getType.TabIndex = 30;
            this.lbl_getType.Text = "Chaos Orb";
            this.lbl_getType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_getAmount
            // 
            this.lbl_getAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_getAmount.AutoSize = true;
            this.lbl_getAmount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.lbl_getAmount.Font = new System.Drawing.Font("Consolas", 9F);
            this.lbl_getAmount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_getAmount.Location = new System.Drawing.Point(234, 27);
            this.lbl_getAmount.MinimumSize = new System.Drawing.Size(0, 32);
            this.lbl_getAmount.Name = "lbl_getAmount";
            this.lbl_getAmount.Size = new System.Drawing.Size(42, 32);
            this.lbl_getAmount.TabIndex = 34;
            this.lbl_getAmount.Text = "x1000";
            this.lbl_getAmount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_payAmount
            // 
            this.lbl_payAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_payAmount.Font = new System.Drawing.Font("Consolas", 9F);
            this.lbl_payAmount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_payAmount.Location = new System.Drawing.Point(313, 27);
            this.lbl_payAmount.Name = "lbl_payAmount";
            this.lbl_payAmount.Size = new System.Drawing.Size(43, 32);
            this.lbl_payAmount.TabIndex = 35;
            this.lbl_payAmount.Text = "x1000";
            this.lbl_payAmount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pb_pay
            // 
            this.pb_pay.Location = new System.Drawing.Point(362, 27);
            this.pb_pay.Name = "pb_pay";
            this.pb_pay.Size = new System.Drawing.Size(32, 32);
            this.pb_pay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_pay.TabIndex = 36;
            this.pb_pay.TabStop = false;
            // 
            // lbl_payType
            // 
            this.lbl_payType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_payType.AutoSize = true;
            this.lbl_payType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_payType.Location = new System.Drawing.Point(400, 27);
            this.lbl_payType.MinimumSize = new System.Drawing.Size(0, 32);
            this.lbl_payType.Name = "lbl_payType";
            this.lbl_payType.Size = new System.Drawing.Size(61, 32);
            this.lbl_payType.TabIndex = 37;
            this.lbl_payType.Text = "Chaos Orb";
            this.lbl_payType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.label10.Location = new System.Drawing.Point(283, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 32);
            this.label10.TabIndex = 38;
            this.label10.Text = "<-";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pb_worth2
            // 
            this.pb_worth2.Location = new System.Drawing.Point(137, 113);
            this.pb_worth2.Name = "pb_worth2";
            this.pb_worth2.Size = new System.Drawing.Size(24, 24);
            this.pb_worth2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_worth2.TabIndex = 39;
            this.pb_worth2.TabStop = false;
            // 
            // pb_worth4
            // 
            this.pb_worth4.Location = new System.Drawing.Point(563, 113);
            this.pb_worth4.Name = "pb_worth4";
            this.pb_worth4.Size = new System.Drawing.Size(24, 24);
            this.pb_worth4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_worth4.TabIndex = 43;
            this.pb_worth4.TabStop = false;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.label8.Location = new System.Drawing.Point(429, 113);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 24);
            this.label8.TabIndex = 42;
            this.label8.Text = "1x";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_worth2
            // 
            this.lbl_worth2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_worth2.AutoSize = true;
            this.lbl_worth2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_worth2.Location = new System.Drawing.Point(489, 113);
            this.lbl_worth2.MinimumSize = new System.Drawing.Size(0, 24);
            this.lbl_worth2.Name = "lbl_worth2";
            this.lbl_worth2.Size = new System.Drawing.Size(91, 24);
            this.lbl_worth2.TabIndex = 41;
            this.lbl_worth2.Text = "is worth {0} x";
            this.lbl_worth2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pb_worth3
            // 
            this.pb_worth3.Location = new System.Drawing.Point(459, 113);
            this.pb_worth3.Name = "pb_worth3";
            this.pb_worth3.Size = new System.Drawing.Size(24, 24);
            this.pb_worth3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_worth3.TabIndex = 40;
            this.pb_worth3.TabStop = false;
            // 
            // dragBar1
            // 
            this.dragBar1.BarShape = PoE_Trade_Overlay.Controls.DragBarShape.Rounded;
            this.dragBar1.BarSize = new System.Drawing.Size(280, 8);
            this.dragBar1.BorderRadius = 2;
            this.dragBar1.BorderWidth = 1F;
            this.dragBar1.Caption = "";
            this.dragBar1.ColorBar = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.dragBar1.ColorBarFill = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.dragBar1.ColorBorder = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.dragBar1.ColorKnob = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.dragBar1.drawValue = false;
            this.dragBar1.KnobShape = PoE_Trade_Overlay.Controls.DragKnobShape.Box;
            this.dragBar1.KnobSize = new System.Drawing.Size(8, 16);
            this.dragBar1.Location = new System.Drawing.Point(153, 63);
            this.dragBar1.Maximum = 20;
            this.dragBar1.Minimum = 1;
            this.dragBar1.Name = "dragBar1";
            this.dragBar1.Size = new System.Drawing.Size(290, 25);
            this.dragBar1.TabIndex = 44;
            this.dragBar1.Value = 1;
            this.dragBar1.ValueChanged += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // ExchangeResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.Controls.Add(this.dragBar1);
            this.Controls.Add(this.btn_visitHideout);
            this.Controls.Add(this.lbl_payGet);
            this.Controls.Add(this.pb_worth4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lbl_worth2);
            this.Controls.Add(this.pb_worth3);
            this.Controls.Add(this.pb_worth2);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lbl_payType);
            this.Controls.Add(this.pb_pay);
            this.Controls.Add(this.lbl_payAmount);
            this.Controls.Add(this.lbl_getAmount);
            this.Controls.Add(this.lbl_getType);
            this.Controls.Add(this.pb_get);
            this.Controls.Add(this.lbl_payAmountTrack);
            this.Controls.Add(this.lbl_getAmountTrack);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbl_worth1);
            this.Controls.Add(this.pb_worth1);
            this.Controls.Add(this.lbl_stockWarning);
            this.Controls.Add(this.lbl_message);
            this.Controls.Add(this.panel);
            this.Font = new System.Drawing.Font("Consolas", 8.25F);
            this.Name = "ExchangeResult";
            this.Size = new System.Drawing.Size(590, 172);
            this.Load += new System.EventHandler(this.ExchangeResult_Load);
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_worth1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_get)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_pay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_worth2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_worth4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_worth3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Button btn_visitHideout;
        private System.Windows.Forms.Label lbl_lastChar;
        private System.Windows.Forms.Label lbl_sellerName;
        private System.Windows.Forms.Label lbl_online;
        private System.Windows.Forms.Label lbl_payGet;
        private System.Windows.Forms.Label lbl_message;
        private System.Windows.Forms.Label lbl_stockWarning;
        private System.Windows.Forms.PictureBox pb_worth1;
        private System.Windows.Forms.Label lbl_worth1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbl_getAmountTrack;
        private System.Windows.Forms.Label lbl_payAmountTrack;
        private System.Windows.Forms.PictureBox pb_get;
        private System.Windows.Forms.Label lbl_getType;
        private System.Windows.Forms.Label lbl_getAmount;
        private System.Windows.Forms.Label lbl_payAmount;
        private System.Windows.Forms.PictureBox pb_pay;
        private System.Windows.Forms.Label lbl_payType;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pb_worth2;
        private System.Windows.Forms.PictureBox pb_worth4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbl_worth2;
        private System.Windows.Forms.PictureBox pb_worth3;
        private System.Windows.Forms.Label lbl_stock;
        private System.Windows.Forms.ToolTip tip;
        private DragBar dragBar1;
    }
}
