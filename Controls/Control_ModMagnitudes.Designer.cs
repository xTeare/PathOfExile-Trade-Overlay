﻿namespace PoE_Trade_Overlay.Controls
{
    partial class Control_ModMagnitudes
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_modTier = new System.Windows.Forms.Label();
            this.lbl_modMagnitudes = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_modTier
            // 
            this.lbl_modTier.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lbl_modTier.BackColor = System.Drawing.Color.Transparent;
            this.lbl_modTier.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_modTier.Location = new System.Drawing.Point(0, 0);
            this.lbl_modTier.Name = "lbl_modTier";
            this.lbl_modTier.Size = new System.Drawing.Size(100, 17);
            this.lbl_modTier.TabIndex = 0;
            this.lbl_modTier.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_modMagnitudes
            // 
            this.lbl_modMagnitudes.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lbl_modMagnitudes.BackColor = System.Drawing.Color.Transparent;
            this.lbl_modMagnitudes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_modMagnitudes.Location = new System.Drawing.Point(340, 0);
            this.lbl_modMagnitudes.Name = "lbl_modMagnitudes";
            this.lbl_modMagnitudes.Size = new System.Drawing.Size(100, 17);
            this.lbl_modMagnitudes.TabIndex = 1;
            this.lbl_modMagnitudes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Control_ModMagnitudes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.lbl_modMagnitudes);
            this.Controls.Add(this.lbl_modTier);
            this.Name = "Control_ModMagnitudes";
            this.Size = new System.Drawing.Size(440, 17);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_modTier;
        private System.Windows.Forms.Label lbl_modMagnitudes;
    }
}
