﻿namespace PoE_Trade_Overlay.Controls
{
    partial class ModSearch
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_mod = new System.Windows.Forms.TextBox();
            this.tb_mod_min = new System.Windows.Forms.TextBox();
            this.tb_mod_max = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lb_mods = new System.Windows.Forms.ListBox();
            this.tb_weight = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tb_mod
            // 
            this.tb_mod.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.tb_mod.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_mod.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(1)), true);
            this.tb_mod.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.tb_mod.Location = new System.Drawing.Point(3, 2);
            this.tb_mod.Multiline = true;
            this.tb_mod.Name = "tb_mod";
            this.tb_mod.Size = new System.Drawing.Size(302, 16);
            this.tb_mod.TabIndex = 0;
            this.tb_mod.TextChanged += new System.EventHandler(this.tb_mod_TextChanged);
            this.tb_mod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_mod_KeyDown);
            this.tb_mod.Leave += new System.EventHandler(this.tb_mod_Leave);
            // 
            // tb_mod_min
            // 
            this.tb_mod_min.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.tb_mod_min.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_mod_min.Font = new System.Drawing.Font("Consolas", 8F);
            this.tb_mod_min.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.tb_mod_min.Location = new System.Drawing.Point(367, 2);
            this.tb_mod_min.Multiline = true;
            this.tb_mod_min.Name = "tb_mod_min";
            this.tb_mod_min.Size = new System.Drawing.Size(40, 16);
            this.tb_mod_min.TabIndex = 1;
            this.tb_mod_min.Text = "min";
            this.tb_mod_min.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_mod_min.TextChanged += new System.EventHandler(this.tb_mod_min_TextChanged);
            // 
            // tb_mod_max
            // 
            this.tb_mod_max.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.tb_mod_max.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_mod_max.Font = new System.Drawing.Font("Consolas", 8F);
            this.tb_mod_max.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.tb_mod_max.Location = new System.Drawing.Point(413, 2);
            this.tb_mod_max.Multiline = true;
            this.tb_mod_max.Name = "tb_mod_max";
            this.tb_mod_max.Size = new System.Drawing.Size(40, 16);
            this.tb_mod_max.TabIndex = 2;
            this.tb_mod_max.Text = "max";
            this.tb_mod_max.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_mod_max.TextChanged += new System.EventHandler(this.tb_mod_max_TextChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Consolas", 8F);
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.button1.Location = new System.Drawing.Point(459, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(40, 17);
            this.button1.TabIndex = 3;
            this.button1.Text = "X";
            this.button1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lb_mods
            // 
            this.lb_mods.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.lb_mods.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lb_mods.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(1)), true);
            this.lb_mods.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lb_mods.FormattingEnabled = true;
            this.lb_mods.ItemHeight = 17;
            this.lb_mods.Location = new System.Drawing.Point(3, 18);
            this.lb_mods.Name = "lb_mods";
            this.lb_mods.Size = new System.Drawing.Size(302, 170);
            this.lb_mods.TabIndex = 4;
            this.lb_mods.Visible = false;
            this.lb_mods.VisibleChanged += new System.EventHandler(this.lb_mods_VisibleChanged);
            this.lb_mods.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lb_mods_MouseDown);
            // 
            // tb_weight
            // 
            this.tb_weight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.tb_weight.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_weight.Font = new System.Drawing.Font("Consolas", 8F);
            this.tb_weight.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.tb_weight.Location = new System.Drawing.Point(311, 2);
            this.tb_weight.Multiline = true;
            this.tb_weight.Name = "tb_weight";
            this.tb_weight.Size = new System.Drawing.Size(50, 16);
            this.tb_weight.TabIndex = 5;
            this.tb_weight.Text = "wheight";
            this.tb_weight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_weight.TextChanged += new System.EventHandler(this.tb_weight_TextChanged);
            // 
            // ModSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.tb_weight);
            this.Controls.Add(this.lb_mods);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tb_mod_max);
            this.Controls.Add(this.tb_mod_min);
            this.Controls.Add(this.tb_mod);
            this.Name = "ModSearch";
            this.Size = new System.Drawing.Size(505, 20);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_mod;
        private System.Windows.Forms.TextBox tb_mod_min;
        private System.Windows.Forms.TextBox tb_mod_max;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox lb_mods;
        private System.Windows.Forms.TextBox tb_weight;
    }
}
