﻿
namespace PoE_Trade_Overlay.Controls
{
    partial class Filter_DropDown
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_FilterName = new System.Windows.Forms.Label();
            this.combo = new PoE_Trade_Overlay.Controls.FlattenCombo();
            this.SuspendLayout();
            // 
            // lbl_FilterName
            // 
            this.lbl_FilterName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.lbl_FilterName.Font = new System.Drawing.Font("Consolas", 9F);
            this.lbl_FilterName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_FilterName.Location = new System.Drawing.Point(3, 2);
            this.lbl_FilterName.Name = "lbl_FilterName";
            this.lbl_FilterName.Size = new System.Drawing.Size(200, 22);
            this.lbl_FilterName.TabIndex = 1;
            this.lbl_FilterName.Text = "label1";
            this.lbl_FilterName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_FilterName.Click += new System.EventHandler(this.lbl_FilterName_Click);
            // 
            // combo
            // 
            this.combo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.combo.AutoComp = false;
            this.combo.BorderColor = System.Drawing.Color.Black;
            this.combo.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.combo.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.combo.Data = null;
            this.combo.DropDownColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo.Font = new System.Drawing.Font("Consolas", 8F);
            this.combo.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.combo.ForeColorDrop = System.Drawing.Color.Empty;
            this.combo.FormattingEnabled = true;
            this.combo.Items.AddRange(new object[] {
            "Either",
            "Yes",
            "No"});
            this.combo.Location = new System.Drawing.Point(206, 3);
            this.combo.Name = "combo";
            this.combo.Size = new System.Drawing.Size(83, 21);
            this.combo.TabIndex = 2;
            // 
            // Filter_DropDown
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.Controls.Add(this.combo);
            this.Controls.Add(this.lbl_FilterName);
            this.Name = "Filter_DropDown";
            this.Size = new System.Drawing.Size(292, 26);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_FilterName;
        private FlattenCombo combo;
    }
}
