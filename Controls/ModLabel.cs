﻿using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System;

namespace PoE_Trade_Overlay.Controls
{
    class ModLabel : Label
    {

        public List<string> content = new List<string>();
        public List<string> colors = new List<string>();

        private string _modInfo;
        public string modInfo
        {
            get { return _modInfo; }
            set
            {
                _modInfo = value;
                SetEvents();
            }
        }

        public bool isHovering;
        public bool alwaysDrawInfo;
        private bool eventsSet = false;
        protected override void OnPaint(PaintEventArgs e)
        {
            //base.OnPaint(e);
            Graphics g = e.Graphics;

            Point oldP = new Point();

            for(int i = 0; i < content.Count; i++)
            {
                Brush b = new SolidBrush(colors[i].ToColor());

                g.DrawString(content[i], this.Font, b, oldP.X, 0);
                Size s = TextRenderer.MeasureText(content[i], this.Font);
                oldP.X += s.Width;
            }
            if (alwaysDrawInfo)
                isHovering = alwaysDrawInfo;
            if (isHovering && modInfo != string.Empty)
            {
                Size s = TextRenderer.MeasureText(modInfo, this.Font);
                Point pos = new Point(this.Width - s.Width, 0);
                g.DrawString(modInfo, this.Font, new SolidBrush(this.ForeColor), pos);
            }
        }



        public void AddText(string text, string color)
        {
            content.Add(text);
            colors.Add(color);
        }

        private void SetEvents()
        {
            if (!eventsSet)
            {
                this.MouseEnter += new System.EventHandler(this.ModLabel_MouseEnter);
                this.MouseLeave += new System.EventHandler(this.ModLabel_MouseLeave);
                eventsSet = true;
            }
        }

        private void ModLabel_MouseEnter(object sender, System.EventArgs e)
        {
            isHovering = true;
            this.Invalidate();
        }

        private void ModLabel_MouseLeave(object sender, System.EventArgs e)
        {
            if(!alwaysDrawInfo)
                isHovering = false;

            this.Invalidate();
        }
    }
}
