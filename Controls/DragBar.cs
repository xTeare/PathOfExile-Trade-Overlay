﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace PoE_Trade_Overlay.Controls
{
    public partial class DragBar : UserControl
    {
        [Category("Knob"), Description("KnobSize")]
        public Size KnobSize { get; set; } = new Size(8, 16);

        [Category("Knob"), Description("ColorKnob")]
        public Color ColorKnob { get; set; } = Color.FromArgb(34, 31, 31);

        [Category("Knob"), Description("VertAlign")]
        public DragKnobShape KnobShape { get; set; } = DragKnobShape.Box;

        [Category("BarColors"), Description("ColorBar")]
        public Color ColorBar { get; set; } = Color.FromArgb(16, 16, 16);

        [Category("BarColors"), Description("ColorBarFill")]
        public Color ColorBarFill { get; set; } = Color.FromArgb(40, 40, 40);

        [Category("BarColors"), Description("ColorBorder")]
        public Color ColorBorder { get; set; } = Color.FromArgb(35, 35, 35);

        [Category("BarSettings"), Description("BarShape")]
        public DragBarShape BarShape { get; set; } = DragBarShape.Rounded;

        [Category("BarSettings"), Description("BarSize")]
        public Size BarSize { get; set; } = new Size(150, 8);

        [Category("BarSettings"), Description("BorderRadius")]
        public int BorderRadius { get; set; } = 2;

        [Category("BarSettings"), Description("BorderWidth")]
        public float BorderWidth { get; set; } = 1f;

        public event EventHandler ValueChanged;

        [Category("BarSettings"), Description("Draw Value")]
        public bool drawValue { get; set; } = true;

        private void onValueChanged()
        {
            if (this.ValueChanged != null)
                ValueChanged(this, new EventArgs());
        }

        private int _value;
        [Category("BarValues"), Description("Value")]
        public int Value
        {
            get { return _value; }
            set
            {
                _value = value;
                this.Invalidate();
                onValueChanged();
            }
        }

        [Category("BarValues"), Description("Minimum")]
        public int Minimum { get; set; } = 75;

        [Category("BarValues"), Description("Maximum")]
        public int Maximum { get; set; } = 100;

        [Category("BarValues"), Description("Caption")]
        public string Caption { get; set; } = "Value :";

        public DragBar()
        {
            InitializeComponent();
            Console.WriteLine(((80 - Minimum) * 100) / (Maximum - Minimum));

        }

        private int x;
        private int y;
        private int kX = 0;
        private bool isDragging;

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            SolidBrush brush_ColorBar = new SolidBrush(ColorBar);
            SolidBrush brush_ColorBarFill = new SolidBrush(ColorBarFill);
            SolidBrush brush_ColorBorder = new SolidBrush(ColorBorder);
            SolidBrush foreColor = new SolidBrush(ForeColor);
            SolidBrush knob = new SolidBrush(ColorKnob);

            if (BarShape == DragBarShape.Rounded)
            {
                x = (this.Width - BarSize.Width) / 2;
                y = 5;

                Rectangle rect_Fill = new Rectangle(x, y, BarSize.Width, BarSize.Height);
                Rectangle rect_Border = new Rectangle(x, y, BarSize.Width, BarSize.Height);
                GraphicExtensions.FillRoundedRectangle(g, brush_ColorBar, rect_Fill, BorderRadius);
                GraphicExtensions.DrawRoundedRectangle(g, new Pen(ColorBorder, BorderWidth), rect_Border, BorderRadius);

                if (drawValue)
                {
                    string s3 = Caption + Value;
                    Size s = TextRenderer.MeasureText(s3, this.Font);
                    g.DrawString(s3, this.Font, foreColor, (this.Width - s.Width) / 2, y + s.Height + 3);
                }


                float percent = ((Value - Minimum) * 100) / (Maximum - Minimum);
                kX = x + (int)(BarSize.Width * (percent / 100)) - (KnobSize.Width / 2);

                Rectangle rect_Fill2 = new Rectangle(x, y, (int)(BarSize.Width * (percent / 100)), BarSize.Height);
                GraphicExtensions.FillRoundedRectangle(g, brush_ColorBarFill, rect_Fill2, BorderRadius);

                if (KnobShape == DragKnobShape.Box)
                {
                    g.FillRectangle(knob, kX, y - ((KnobSize.Height - BarSize.Height) / 2), KnobSize.Width, KnobSize.Height);
                }

            }
        }

        private void DragBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Y > y && e.Y < (y + BarSize.Height))
            {
                if (e.X > kX && e.X < (kX + KnobSize.Width))
                {
                    isDragging = true;
                }
                else if (e.X > x && e.X < (x + BarSize.Width))
                {
                    float percent = ((float)e.X - (float)x) / (float)BarSize.Width;
                    Value = (int)(((percent * 100) * (Maximum - Minimum) / 100) + Minimum);
                    isDragging = true;
                }
            }
        }
        

        private void DragBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                float percent = ((float)e.X - (float)x) / (float)BarSize.Width;
                if (percent >= 1f)
                    percent = 1;
                if (percent <= 0)
                    percent = 0;

                Value = (int)(((percent * 100) * (Maximum - Minimum) / 100) + Minimum);
            }
        }

        private void DragBar_MouseUp(object sender, MouseEventArgs e)
        {
            isDragging = false;
        }
    }

    public enum DragBarShape
    {
        Rounded,
        Box
    }

    public enum DragKnobShape
    {
        Image,
        Box,
        RoundedBox,
        Circle,
        Arrow
    }
}
 