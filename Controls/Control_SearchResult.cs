﻿using Newtonsoft.Json;
using PoE_Trade_Overlay.Queries;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Input;

namespace PoE_Trade_Overlay.Controls
{
    public partial class Control_SearchResult : UserControl
    {
        private const int offset_itemName = 8;
        private const int width_itemName = 34;
        private const int height_lbl = 18;
        private const int height_itemName = 44;
        private const int height_separator = 8;

        private string msg;
        private Image separatorImage = null;

        private BetterPictureBox div;

        public Result result;

        public SearchQuery sq;

        public TabPage parent;
        public Stopwatch whisperDelayWatch = new Stopwatch();
        public int whisperDelay = 5000;
        public bool sortPriceAsc = true;
        public bool sortModAsc;
        public bool sortGemProgressAsc;
        public string pseudo;
        public string header;

        public Control_SearchResult()
        {
            InitializeComponent();
            pb_itemArt.MouseEnter += new EventHandler(HideSockets);
            pb_itemArt.MouseLeave += new EventHandler(ShowSockets);
            whisperDelayWatch.Start();
        }

        private void CreateHeader(string text, Color foreColor, Bitmap headerLeft, Bitmap headerCenter, Bitmap headerRight, Bitmap separator, int size = 450)
        {
            pb_header_left.Size = new Size(headerLeft.Width, headerLeft.Height);
            pb_header_left.Image = headerLeft;

            pb_header_center.Location = new Point(pb_header_left.Bounds.Right, pb_header_left.Bounds.Top);
            pb_header_center.Size = new Size(size, headerCenter.Height);
            pb_header_center.BackgroundImage = headerCenter;

            pb_header_right.Location = new Point(pb_header_center.Bounds.Right, pb_header_center.Bounds.Top);
            pb_header_right.Size = new Size(headerRight.Width, headerRight.Height);
            pb_header_right.Image = headerRight;

            lbl_itemName.Size = new Size(pb_header_center.Size.Width, width_itemName);
            lbl_itemName.Location = new Point(0, pb_header_left.Bounds.Top + offset_itemName);
            lbl_itemName.Text = text;
            lbl_itemName.ForeColor = foreColor;

            separatorImage = Properties.Resources.ItemsSeparatorWhite;
        }

        public void SetControl()
        {
            msg = result.Listing.Whisper;

            switch (result.Item.FrameType)
            {
                case 0: /// NORMAL ITEM
                    CreateHeader(result.Item.TypeLine, Constants.Normal, Properties.Resources.ItemsHeaderWhiteLeft, Properties.Resources.ItemsHeaderWhiteMiddle, Properties.Resources.ItemsHeaderWhiteRight, Properties.Resources.ItemsSeparatorWhite, 480);
                    break;

                case 1: /// MAGIC ITEM
                    CreateHeader(result.Item.TypeLine, Constants.Magic, Properties.Resources.ItemsHeaderMagicLeft, Properties.Resources.ItemsHeaderMagicMiddle, Properties.Resources.ItemsHeaderMagicRight, Properties.Resources.ItemsSeparatorMagic, 480);
                    break;

                case 2: /// RARE ITEM
                    CreateHeader(result.Item.Name + "\n" + result.Item.TypeLine, Constants.Rare, Properties.Resources.ItemsHeaderRareLeft, Properties.Resources.ItemsHeaderRareMiddle, Properties.Resources.ItemsHeaderRareRight, Properties.Resources.ItemsSeparatorRare);
                    break;

                case 3: /// UNIQUE ITEM
                    CreateHeader(result.Item.Name + "\n" + result.Item.TypeLine, Constants.Unique, Properties.Resources.ItemsHeaderUniqueLeft, Properties.Resources.ItemsHeaderUniqueMiddle, Properties.Resources.ItemsHeaderUniqueRight, Properties.Resources.ItemsSeparatorUnique);
                    btn_wiki.Visible = true;

                    break;

                case 4: /// SKILL GEM
                    CreateHeader(result.Item.TypeLine, Constants.Gem, Properties.Resources.ItemsHeaderGemLeft, Properties.Resources.ItemsHeaderGemMiddle, Properties.Resources.ItemsHeaderGemRight, Properties.Resources.ItemsSeparatorGem, 480);
                    btn_wiki.Visible = true;
                    break;

                case 5: /// ESSENCE
                    CreateHeader(result.Item.TypeLine, Constants.Currency, Properties.Resources.ItemsHeaderCurrencyLeft, Properties.Resources.ItemsHeaderCurrencyMiddle, Properties.Resources.ItemsHeaderCurrencyRight, Properties.Resources.ItemsSeparatorCurrency, 480);
                    btn_wiki.Visible = true;
                    break;

                case 6: /// DIVINATION CARD

                    #region DIV

                    pb_header_left.Visible = false;
                    pb_header_center.Visible = false;
                    pb_header_right.Visible = false;
                    flp_item.Visible = false;

                    BetterPictureBox bpb_frame = new BetterPictureBox();
                    bpb_frame.Image = Properties.Resources.DivinationCard;
                    bpb_frame.Size = new Size((int)(bpb_frame.Image.Width * 0.75), (int)(bpb_frame.Image.Height * 0.75));
                    bpb_frame.SizeMode = PictureBoxSizeMode.StretchImage;
                    bpb_frame.Location = new Point((this.Width / 2) - (bpb_frame.Size.Width / 2), 0);

                    BetterPictureBox bpb_art = new BetterPictureBox();

                    bpb_art.LoadAsync("https://www.pathofexile.com/image/gen/divination_cards/" + result.Item.ArtFilename + ".png");

                    bpb_art.Size = new Size((int)(237 * 0.75), (int)(170 * 0.75));
                    bpb_art.Location = new Point(bpb_frame.Location.X + 15, bpb_frame.Location.Y + 30);
                    bpb_art.SizeMode = PictureBoxSizeMode.StretchImage;

                    Label lbl_name = new Label();
                    lbl_name.Location = new Point(0, 1);
                    lbl_name.Size = new Size(bpb_frame.Width, 32);
                    lbl_name.Font = new Font(lbl_name.Font.FontFamily, 9, FontStyle.Regular);
                    lbl_name.ForeColor = Color.Black;
                    lbl_name.BackColor = Color.Transparent;
                    lbl_name.Text = result.Item.TypeLine;
                    lbl_name.TextAlign = ContentAlignment.MiddleCenter;
                    bpb_frame.Controls.Add(lbl_name);

                    Label lbl_stack = new Label();
                    bpb_frame.Controls.Add(lbl_stack);
                    lbl_stack.Location = new Point(15, 140);
                    lbl_stack.Size = new Size(43, 22);
                    lbl_stack.TextAlign = ContentAlignment.MiddleCenter;
                    lbl_stack.ForeColor = Color.White;
                    lbl_stack.BackColor = Color.Transparent;
                    lbl_stack.Text = string.Format("{0}/{1}", result.Item.StackSize, result.Item.MaxStackSize);

                    Label lbl_flav = new Label();
                    bpb_frame.Controls.Add(lbl_flav);
                    lbl_flav.Location = new Point(7, 225);
                    lbl_flav.Size = new Size(bpb_frame.Width - 14, 300);
                    lbl_flav.TextAlign = ContentAlignment.TopCenter;
                    lbl_flav.Font = new Font(lbl_flav.Font.FontFamily, 8, FontStyle.Italic);

                    lbl_flav.ForeColor = Color.FromArgb(175, 96, 37);
                    lbl_flav.BackColor = Color.Transparent;

                    string flav = "";
                    foreach (string s in result.Item.FlavourText)
                    {
                        string str = s;
                        str = str.RemoveFromTo("<", ">");
                        str = str.Replace("{", "");
                        str = str.Replace("}", "");
                        flav += str.Replace("\r", "") + "\n";
                    }

                    lbl_flav.Text = flav;

                    if (result.Item.ExplicitMods != null)
                    {
                        Label lbl_item = new Label();
                        bpb_frame.Controls.Add(lbl_item);
                        lbl_item.Location = new Point(10, 157);
                        lbl_item.Size = new Size(bpb_frame.Width - 20, 44);
                        lbl_item.TextAlign = ContentAlignment.MiddleCenter;
                        lbl_item.Font = new Font(lbl_item.Font.FontFamily, 8, FontStyle.Bold);
                        lbl_item.ForeColor = Color.White;
                        lbl_item.BackColor = Color.Transparent;

                        string t = result.Item.ExplicitMods[0];

                        if (t.Contains("<whiteitem>"))
                        {
                            lbl_item.ForeColor = Constants.Normal;
                            t = t.Replace("<whiteitem>", "");
                        }
                        else if (t.Contains("<magicitem>"))
                        {
                            lbl_item.ForeColor = Constants.Magic;
                            t = t.Replace("<magicitem>", "");
                        }
                        else if (t.Contains("<rareitem>"))
                        {
                            lbl_item.ForeColor = Constants.Rare;
                            t = t.Replace("<rareitem>", "");
                        }
                        else if (t.Contains("<uniqueitem>"))
                        {
                            lbl_item.ForeColor = Constants.Unique;
                            t = t.Replace("<uniqueitem>", "");
                        }
                        else if (t.Contains("<gemitem>"))
                        {
                            lbl_item.ForeColor = Constants.Gem;
                            t = t.Replace("<gemitem>", "");
                        }
                        else if (t.Contains("<prophecy>"))
                        {
                            lbl_item.ForeColor = Constants.Prophecy;
                            t = t.Replace("<prophecy>", "");
                        }
                        else if (t.Contains("<divination>"))
                        {
                            lbl_item.ForeColor = Constants.Divination;
                            t = t.Replace("<divination>", "");
                        }
                        else if (t.Contains("<currencyitem>"))
                        {
                            lbl_item.ForeColor = Constants.Currency;
                            t = t.Replace("<currencyitem>", "");
                        }

                        t = t.Replace("{", "");
                        t = t.Replace("}", "");
                        t = t.Replace("\r", "");
                        t = t.Replace("<corrupted>", "");
                        t = t.Replace("<default>", "");
                        t = t.Replace("<normal>", "");
                        t = t.RemoveFromTo("<", ">");

                        lbl_item.Text = t;
                    }

                    div = bpb_frame;
                    this.Controls.Add(bpb_frame);
                    this.Controls.Add(bpb_art);

                    #endregion DIV
                    btn_wiki.Visible = true;
                    break;

                case 8: /// PROPHECY
                    CreateHeader(result.Item.TypeLine, Constants.Prophecy, Properties.Resources.ItemsHeaderProphecyLeft, Properties.Resources.ItemsHeaderProphecyMiddle, Properties.Resources.ItemsHeaderProphecyRight, Properties.Resources.ItemsSeparatorProphecy, 480);
                    btn_wiki.Visible = true;
                    break;

                case 9: /// FOIL
                    CreateHeader(result.Item.Name + "\n" + result.Item.TypeLine, Constants.Relic, Properties.Resources.ItemsHeaderFoilLeft, Properties.Resources.ItemsHeaderFoilMiddle, Properties.Resources.ItemsHeaderFoilRight, Properties.Resources.ItemsSeparatorFoil);
                    btn_wiki.Visible = true;
                    break;
            }

            flp_item.Location = new Point(pb_header_left.Bounds.Left, pb_header_left.Bounds.Bottom);
            flp_item.Size = new Size(pb_header_left.Width + pb_header_center.Width + pb_header_right.Width, 20);

            if (result.Item.FrameType == 8)
            {
                if (result.Item.FlavourText != null )
                {
                    foreach (string flav in result.Item.FlavourText)
                    {
                        Label lbl_flav = new Label();
                        lbl_flav.Text = flav;
                        lbl_flav.ForeColor = Color.FromArgb(255, 175, 86, 25);
                        lbl_flav.Size = new Size(flp_item.Width - 10, height_lbl);
                        lbl_flav.TextAlign = ContentAlignment.MiddleCenter;
                        lbl_flav.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Italic, GraphicsUnit.Point, ((byte)(1)), true);
                        flp_item.Controls.Add(lbl_flav);
                        flp_item.Size = new Size(flp_item.Width, flp_item.Height + height_lbl);
                    }

                    PictureBox separator4 = new PictureBox();
                    separator4.SizeMode = PictureBoxSizeMode.CenterImage;
                    separator4.Size = new Size(flp_item.Width - 10, height_separator);
                    separator4.Image = separatorImage;
                    flp_item.Controls.Add(separator4);
                    flp_item.Size = new Size(flp_item.Width, flp_item.Height + height_separator);
                }
                if (result.Item.ProphecyText != null)
                {
                    Label lbl = new Label();
                    lbl.Text = result.Item.ProphecyText;
                    lbl.ForeColor = Color.FromArgb(255, 255, 255, 255);
                    lbl.Size = new Size(flp_item.Width - 10, height_lbl);
                    lbl.TextAlign = ContentAlignment.MiddleCenter;
                    lbl.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Italic, GraphicsUnit.Point, ((byte)(1)), true);
                    flp_item.Controls.Add(lbl);
                    flp_item.Size = new Size(flp_item.Width, flp_item.Height + height_lbl);
                }
            }
            else
            {
                lbl_verified.Visible = result.Item.Verified;

                if (result.Item.Properties != null)
                {
                    foreach (Property prop in result.Item.Properties)
                    {
                        Label lbl_prop = new Label();
                        lbl_prop.ForeColor = Color.FromArgb(255, 200, 200, 200);
                        lbl_prop.Size = new Size(flp_item.Width - 10, height_lbl);
                        lbl_prop.TextAlign = ContentAlignment.MiddleCenter;
                        lbl_prop.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Regular, GraphicsUnit.Point, ((byte)(1)), true);
                        flp_item.Controls.Add(lbl_prop);
                        flp_item.Size = new Size(flp_item.Width, flp_item.Height + height_lbl);

                        if (result.Item.TypeLine.Contains("Flask"))
                        {
                            string t = prop.Name;

                            for (int i = 0; i < prop.Values.Count; i++)
                                for (int j = 0; j < prop.Values[i].Count; j++)
                                    t = t.Replace("%" + i, prop.Values[i][j].String);

                            lbl_prop.Text = t;
                        }
                        else
                        {
                            lbl_prop.Text = prop.Name;
                            if (prop.Values.Count > 0)
                            {
                                foreach (List<Value> val in prop.Values)
                                {
                                    foreach (Value val2 in val)
                                    {
                                        if (val2.String == null)
                                            continue;

                                        if (val2.String != string.Empty)
                                            lbl_prop.Text += ": " + val2.String.Replace(":", "");
                                    }
                                }
                            }
                        }
                    }
                    PictureBox separator = new PictureBox();
                    separator.SizeMode = PictureBoxSizeMode.CenterImage;
                    separator.Size = new Size(flp_item.Width - 10, height_separator);
                    separator.Image = separatorImage;
                    flp_item.Controls.Add(separator);
                }

                if (result.Item.Ilvl != 0)
                {
                    // Add ItemLevel
                    Label lbl_iLvl = new Label();
                    lbl_iLvl.Text = "Item Level: " + result.Item.Ilvl;
                    lbl_iLvl.ForeColor = Color.FromArgb(255, 200, 200, 200);
                    lbl_iLvl.Size = new Size(flp_item.Width - 10, height_lbl);
                    lbl_iLvl.TextAlign = ContentAlignment.MiddleCenter;
                    lbl_iLvl.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Regular, GraphicsUnit.Point, ((byte)(1)), true);
                    flp_item.Controls.Add(lbl_iLvl);
                    flp_item.Size = new Size(flp_item.Width, flp_item.Height + height_lbl);
                }

                if (result.Item.Requirements != null)
                {
                    Label lbl_req = new Label();
                    lbl_req.ForeColor = Color.FromArgb(255, 200, 200, 200);
                    lbl_req.Size = new Size(flp_item.Width - 10, height_lbl);
                    lbl_req.TextAlign = ContentAlignment.MiddleCenter;
                    lbl_req.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Regular, GraphicsUnit.Point, ((byte)(1)), true);
                    flp_item.Controls.Add(lbl_req);
                    flp_item.Size = new Size(flp_item.Width, flp_item.Height + height_lbl);

                    foreach (Property req in result.Item.Requirements)
                    {
                        if (req.Name == "Level")
                        {
                            lbl_req.Text = "Requires Level ";
                            foreach (List<Value> req2 in req.Values)
                            {
                                foreach (Value req3 in req2)
                                {
                                    if (req3.String == null)
                                        continue;
                                    lbl_req.Text += req3.String;
                                }
                            }
                        }
                        else
                        {
                            foreach (List<Value> req2 in req.Values)
                            {
                                foreach (Value req3 in req2)
                                {
                                    if (req3.String == null)
                                        continue;
                                    if (lbl_req.Text == string.Empty)
                                    {
                                        lbl_req.Text += req3.String + " " + req.Name;
                                    }
                                    else
                                    {
                                        lbl_req.Text += ", " + req3.String + " " + req.Name;
                                    }
                                }
                            }
                        }
                    }

                    PictureBox separator2 = new PictureBox();
                    separator2.SizeMode = PictureBoxSizeMode.CenterImage;
                    separator2.Size = new Size(flp_item.Width - 10, height_separator);
                    separator2.Image = separatorImage;
                    flp_item.Controls.Add(separator2);
                    flp_item.Size = new Size(flp_item.Width, flp_item.Height + 12);
                }

                if (result.Item.ImplicitMods != null)
                {
                    foreach (string imp in result.Item.ImplicitMods)
                    {
                        Label lbl_imp = new Label();
                        lbl_imp.Text = imp;
                        lbl_imp.ForeColor = Constants.Magic;
                        lbl_imp.Size = new Size(flp_item.Width - 10, height_lbl);
                        lbl_imp.TextAlign = ContentAlignment.MiddleCenter;
                        lbl_imp.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Regular, GraphicsUnit.Point, ((byte)(1)), true);
                        flp_item.Controls.Add(lbl_imp);
                        flp_item.Size = new Size(flp_item.Width, flp_item.Height + height_lbl + 2);

                        lbl_imp.Click += new EventHandler(LabelImp_Click);
                        lbl_imp.MouseEnter += new EventHandler(lbl_exp_MouseEnter);
                        lbl_imp.MouseLeave += new EventHandler(lbl_exp_MouseLeave);

                        lbl_imp.MouseEnter += new EventHandler(Label_ModEnter);
                        lbl_imp.MouseLeave += new EventHandler(Label_ModLeave);

                        Control_ModMagnitudes magnitudes = new Control_ModMagnitudes();
                        
                        magnitudes.Location = new Point(0, 0);
                        magnitudes.Width = 400;

                        /// Find Magnitudes for explicit mods
                        /// Set Control_Magnitudes Labels
                        if (result.Item.Extended != null && result.Item.Extended.Mods != null && result.Item.Extended.Mods.Explicit != null)
                        {
                            string modId = Data.GetModIdByText(Regex.Replace(imp, @"\d", "#").Replace("##", "#").Replace("+", "").Replace("-", ""));
                            foreach (ExplicitClass ex in result.Item.Extended.Mods.Explicit)
                            {
                                if (ex.Magnitudes != null)
                                {
                                    if (ex.Magnitudes[0].Hash == modId)
                                    {
                                        magnitudes.SetTier(ex.Tier);
                                        if (ex.Magnitudes.Count == 1)
                                        {
                                            string mags = string.Format("{0} - {1}", ex.Magnitudes[0].Min, ex.Magnitudes[0].Max);
                                            magnitudes.SetMag(mags);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    PictureBox separator3 = new PictureBox();
                    separator3.SizeMode = PictureBoxSizeMode.CenterImage;
                    separator3.Size = new Size(flp_item.Width - 10, height_separator);
                    separator3.Image = separatorImage;
                    flp_item.Controls.Add(separator3);
                    flp_item.Size = new Size(flp_item.Width, flp_item.Height + height_separator);
                }

                if (result.Item.ExplicitMods != null)
                {
                    foreach (string exp in result.Item.ExplicitMods)
                    {
                        Label lbl_exp = new Label();
                        lbl_exp.Text = exp;
                        lbl_exp.ForeColor = Constants.Magic;
                        lbl_exp.Size = new Size(flp_item.Width - 10, height_lbl);
                        lbl_exp.TextAlign = ContentAlignment.MiddleCenter;
                        lbl_exp.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Regular, GraphicsUnit.Point, ((byte)(1)), true);
                        //lbl_exp.MouseClick += new System.Windows.Forms.MouseEventHandler(SortBy);
                        flp_item.Controls.Add(lbl_exp);
                        flp_item.Size = new Size(flp_item.Width, flp_item.Height + height_lbl + 2);

                        lbl_exp.MouseEnter += new EventHandler(lbl_exp_MouseEnter);
                        lbl_exp.MouseLeave += new EventHandler(lbl_exp_MouseLeave);
                        lbl_exp.Click += new EventHandler(LabelMod_Click);

                        Label lbl_modTier = new Label();
                        Label lbl_modValues = new Label();
                        lbl_exp.Controls.Add(lbl_modTier);
                        lbl_exp.Controls.Add(lbl_modValues);
                        
                        lbl_modTier.Size = new Size(25, lbl_modTier.Height);

                        lbl_modValues.TextAlign = ContentAlignment.MiddleRight;
                        lbl_modValues.AutoSize = true;

                        lbl_modTier.Location = new Point(0, 0);
                        lbl_modValues.Location = new Point(lbl_exp.Right - lbl_modValues.Width, 0);

                        /// Find Magnitudes for explicit mods
                        ///
                        if (result.Item.Extended != null && result.Item.Extended.Mods != null && result.Item.Extended.Mods.Explicit != null)
                        {
                            string modId = Data.GetModIdByText(Regex.Replace(exp, @"\d", "#").Replace("##", "#").Replace("+", "").Replace("-", ""));

                            lbl_exp.MouseEnter += new EventHandler(Label_ModEnter);
                            lbl_exp.MouseLeave += new EventHandler(Label_ModLeave);
                            foreach (ExplicitClass ex in result.Item.Extended.Mods.Explicit)
                            {
                                if (ex.Magnitudes != null)
                                {
                                    if (ex.Magnitudes[0].Hash == modId)
                                    {
                                        lbl_modTier.Text = ex.Tier;
                                        if (ex.Magnitudes.Count == 1)
                                        {
                                            string mags = string.Format("{0} - {1}", ex.Magnitudes[0].Min, ex.Magnitudes[0].Max);
                                            lbl_modValues.Text = mags;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (result.Item.PseudoMods != null)
                {
                    foreach (string pseudo in result.Item.PseudoMods)
                    {
                        Label lbl_pseudo = new Label();
                        lbl_pseudo.Text = pseudo;
                        lbl_pseudo.ForeColor = Color.FromArgb(200, 200, 200);
                        lbl_pseudo.Size = new Size(flp_item.Width - 10, height_lbl);
                        lbl_pseudo.TextAlign = ContentAlignment.MiddleCenter;
                        lbl_pseudo.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Regular, GraphicsUnit.Point, ((byte)(1)), true);
                        //lbl_exp.MouseClick += new System.Windows.Forms.MouseEventHandler(SortBy);
                        flp_item.Controls.Add(lbl_pseudo);
                        flp_item.Size = new Size(flp_item.Width, flp_item.Height + height_lbl + 2);

                        lbl_pseudo.MouseEnter += new EventHandler(lbl_exp_MouseEnter);
                        lbl_pseudo.MouseLeave += new EventHandler(lbl_exp_MouseLeave);
                        lbl_pseudo.Click += new EventHandler(LabelMod_Click);

                        /*
                        Label lbl_modTier = new Label();
                        Label lbl_modValues = new Label();
                        lbl_exp.Controls.Add(lbl_modTier);
                        lbl_exp.Controls.Add(lbl_modValues);

                        lbl_modTier.Visible = Config.alwaysShowModInfo;
                        lbl_modValues.Visible = Config.alwaysShowModInfo;
                        lbl_modTier.Size = new Size(25, lbl_modTier.Height);

                        lbl_modValues.TextAlign = ContentAlignment.MiddleRight;
                        lbl_modValues.AutoSize = true;

                        lbl_modTier.Location = new Point(0, 0);
                        lbl_modValues.Location = new Point(lbl_exp.Right - lbl_modValues.Width, 0);

                        /// Find Magnitudes for explicit mods
                        ///
                        if (result.Item.Extended != null && result.Item.Extended.Mods != null && result.Item.Extended.Mods.Explicit != null)
                        {
                            string modId = Data.GetModIdByText(Regex.Replace(exp, @"\d", "#").Replace("##", "#").Replace("+", "").Replace("-", ""));

                            lbl_exp.MouseEnter += new EventHandler(Label_ModEnter);
                            lbl_exp.MouseLeave += new EventHandler(Label_ModLeave);
                            foreach (ExplicitClass ex in result.Item.Extended.Mods.Explicit)
                            {
                                if (ex.Magnitudes != null)
                                {
                                    if (ex.Magnitudes[0].Hash == modId)
                                    {
                                        lbl_modTier.Text = ex.Tier;
                                        if (ex.Magnitudes.Count == 1)
                                        {
                                            string mags = string.Format("{0} - {1}", ex.Magnitudes[0].Min, ex.Magnitudes[0].Max);
                                            lbl_modValues.Text = mags;
                                        }
                                    }
                                }
                            }
                        }
                        */
                    }
                }

                /// Add EXP Bar if possible
                if (result.Item.FrameType == 4)
                {
                    if (result.Item.AdditionalProperties != null)
                    {
                        if (result.Item.AdditionalProperties.Count > 0)
                        {
                            foreach (AdditionalProperty add in result.Item.AdditionalProperties)
                            {
                                if (add.DisplayMode == 2) // Display EXP bar
                                {
                                    Label progress = new Label();
                                    if (add.Values.Count > 0)
                                    {
                                        if (add.Values[0].Count > 0)
                                            progress.Text = add.Values[0][0].String;
                                    }

                                    progress.ForeColor = Color.FromArgb(200, 200, 200);
                                    progress.TextAlign = ContentAlignment.MiddleCenter;
                                    progress.AutoSize = false;
                                    progress.Size = new Size(flp_item.Width - 10, 37);
                                    flp_item.Controls.Add(progress);
                                    flp_item.Size = new Size(flp_item.Width, flp_item.Height + progress.Height + 2);

                                    PictureBox frame = new PictureBox();
                                    progress.Controls.Add(frame);
                                    frame.Name = "Frame";

                                    PictureBox fill = new PictureBox();
                                    frame.Controls.Add(fill);
                                    fill.Name = "Fill";

                                    frame.Size = new Size(212, 13);
                                    fill.Size = new Size((int)((frame.Width - 6) * add.Progress), 7);

                                    frame.Location = new Point((progress.Width - frame.Width) / 2, 25);
                                    fill.Location = new Point(4, 4);

                                    frame.Image = Properties.Resources.ItemsExperienceBar;
                                    fill.BackColor = Color.FromArgb(100, 255, 255, 0);

                                    progress.MouseEnter += Label_ModEnter;
                                    progress.MouseLeave += Label_ModLeave;
                                    progress.MouseClick += LabelGem_Click;

                                    frame.MouseEnter += GemLevel_Enter;
                                    frame.MouseLeave += GemLevel_Leave;
                                    frame.MouseClick += GemImage_Click;

                                    fill.MouseEnter += GemLevel_Enter;
                                    fill.MouseLeave += GemLevel_Leave;
                                    fill.MouseClick += GemImage_Click;
                                }
                            }
                        }
                    }
                }

                if (result.Item.Duplicated | result.Item.Corrupted)
                {
                    if (result.Item.FrameType != 0)
                    {
                        PictureBox separator = new PictureBox();
                        separator.SizeMode = PictureBoxSizeMode.CenterImage;
                        separator.Size = new Size(flp_item.Width - 10, height_separator);
                        separator.Image = separatorImage;
                        flp_item.Controls.Add(separator);
                        flp_item.Size = new Size(flp_item.Width, flp_item.Height + height_separator);
                    }

                    if (result.Item.Duplicated)
                    {
                        Label lbl_mirrored = new Label();
                        lbl_mirrored.Text = "Mirrored";
                        lbl_mirrored.ForeColor = Color.FromArgb(255, 200, 200, 200);
                        lbl_mirrored.Size = new Size(flp_item.Width - 10, height_lbl);
                        lbl_mirrored.TextAlign = ContentAlignment.MiddleCenter;
                        lbl_mirrored.Font = new Font(lbl_mirrored.Font.FontFamily, 9f, FontStyle.Regular, GraphicsUnit.Point, ((byte)(1)), true);
                        flp_item.Controls.Add(lbl_mirrored);
                        flp_item.Size = new Size(flp_item.Width, flp_item.Height + height_lbl);
                    }
                    if (result.Item.Corrupted)
                    {
                        Label lbl_corrupted = new Label();
                        lbl_corrupted.Text = "Corrupted";
                        lbl_corrupted.ForeColor = Color.FromArgb(255, 255, 0, 0);
                        lbl_corrupted.Size = new Size(flp_item.Width - 10, height_lbl);
                        lbl_corrupted.TextAlign = ContentAlignment.MiddleCenter;
                        lbl_corrupted.Font = new Font(lbl_corrupted.Font.FontFamily, 9f, FontStyle.Bold, GraphicsUnit.Point, ((byte)(1)), true);
                        flp_item.Controls.Add(lbl_corrupted);
                        flp_item.Size = new Size(flp_item.Width, flp_item.Height + height_lbl);
                    }
                }

                if (!result.Item.Identified)
                {
                    Label lbl_ided = new Label();
                    lbl_ided.Text = "Unidentified";
                    lbl_ided.ForeColor = Color.FromArgb(255, 255, 0, 0);
                    lbl_ided.Size = new Size(flp_item.Width - 10, height_lbl);
                    lbl_ided.TextAlign = ContentAlignment.MiddleCenter;
                    lbl_ided.Font = new Font(lbl_ided.Font.FontFamily, 9f, FontStyle.Bold, GraphicsUnit.Point, ((byte)(1)), true);
                    flp_item.Controls.Add(lbl_ided);
                    flp_item.Size = new Size(flp_item.Width, flp_item.Height + height_lbl);
                }

                if (result.Item.FlavourText != null )
                {
                    PictureBox separator4 = new PictureBox();
                    separator4.SizeMode = PictureBoxSizeMode.CenterImage;
                    separator4.Size = new Size(flp_item.Width - 10, height_separator);
                    separator4.Image = separatorImage;
                    flp_item.Controls.Add(separator4);
                    flp_item.Size = new Size(flp_item.Width, flp_item.Height + height_separator);

                    foreach (string flav in result.Item.FlavourText)
                    {
                        Label lbl_flav = new Label();
                        lbl_flav.Text = flav;
                        lbl_flav.ForeColor = Color.FromArgb(255, 175, 86, 25);
                        lbl_flav.Size = new Size(flp_item.Width - 10, height_lbl);
                        lbl_flav.TextAlign = ContentAlignment.MiddleCenter;
                        lbl_flav.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Italic, GraphicsUnit.Point, ((byte)(1)), true);
                        flp_item.Controls.Add(lbl_flav);
                        flp_item.Size = new Size(flp_item.Width, flp_item.Height + height_lbl);
                    }
                }
            }

            /// LOAD PRICE DATA
            ///

            if (result.Listing.Price == null)
            {
                lbl_priceType.Text = "No Price Set";
                pb_currency.Visible = false;
            }
            else
            {
                lbl_priceType.Text = (result.Listing.Price.Type == "~b/o") ? "Asking Price:" : "Exact Price :";
                string currencyName = result.Listing.Price.Currency.GetFullCurrencyName();
                lbl_currencyType.Text = string.Format("{0}x {1}", result.Listing.Price.Amount, currencyName);

                /// LOAD PRICE IN CHAOS
                ///

                if (currencyName != "Chaos Orb")
                {
                    LoadPriceInChaos(currencyName, result.Listing.Price.Amount);
                    lbl_priceInC.Visible = true;
                    lbl_priceInChaos.Visible = true;
                }
                else
                {
                    lbl_priceInC.Visible = false;
                    lbl_priceInChaos.Visible = false;
                }

                pb_currency.LoadAsync("https://" + result.Listing.Price.Currency.GetCurrencyURL());
            }

            /// LOAD ACCOUNT STATUS
            ///

            if (result.Listing.Account.Online != null)
            {
                // Is Logged in. Now check if afk
                if (result.Listing.Account.Online.Status != null)
                {
                    lbl_online.Text = "afk";
                    lbl_online.BackColor = Color.FromArgb(196, 95, 0);
                }
                else
                {
                    lbl_online.Text = "online";
                    lbl_online.BackColor = Color.FromArgb(48, 150, 48);
                }
            }
            else
            {
                lbl_online.Text = "offline";
                lbl_online.BackColor = Color.FromArgb(217, 83, 79);
            }
            lbl_sellerName.Text = "Acc : " + result.Listing.Account.Name;
            lbl_lastChar.Text = "Char : " + result.Listing.Account.LastCharacterName;

            /// Set Price Panels position
            // If the item's a div card we need some other spacing
            if (result.Item.FrameType == 6)
            {
                // Place Prices Panel under flp_item
                panel_prices.Location = new Point(1, div.Bottom);
                panel_prices.Size = new Size(this.Width, panel_prices.Height);

                // Place Separator under prices panel
                pb_panelSeparator.Location = new Point(1, panel_prices.Bottom);
                pb_panelSeparator.Size = new Size(this.Width, pb_panelSeparator.Height);

                // Place Name panel under separator
                panel_name.Location = new Point(1, pb_panelSeparator.Bottom);
                panel_name.Size = new Size(this.Width, panel_name.Height);

                // Place ItemArt
                pb_itemArt.Location = new Point(pb_itemArt.Left, (flp_item.Bottom - (flp_item.Height / 2)) - (pb_itemArt.Height / 2));

                // Place verified text
                lbl_verified.Location = new Point(pb_itemArt.Left, pb_itemArt.Bottom);
            }
            else
            {
                // Place Prices Panel under flp_item
                panel_prices.Location = new Point(flp_item.Left, flp_item.Bottom);
                panel_prices.Size = new Size(flp_item.Width, panel_prices.Height);

                // Place Separator under prices panel
                pb_panelSeparator.Location = new Point(flp_item.Left, panel_prices.Bottom);
                pb_panelSeparator.Size = new Size(flp_item.Width, pb_panelSeparator.Height);

                // Place Name panel under separator
                panel_name.Location = new Point(flp_item.Left, pb_panelSeparator.Bottom);
                panel_name.Size = new Size(flp_item.Width, panel_name.Height);

                // Place ItemArt
                pb_itemArt.Location = new Point(pb_itemArt.Left, (flp_item.Bottom - (flp_item.Height / 2)) - (pb_itemArt.Height / 2));

                // Place verified text
                lbl_verified.Location = new Point(pb_itemArt.Left, pb_itemArt.Bottom);

                // Place Copy button
                btn_copyItem.Location = new Point(btn_copyItem.Left, lbl_verified.Bottom);

                // Place Wiki button
                btn_wiki.Location = new Point(btn_wiki.Left, lbl_verified.Bottom);
            }


            
        }

        public void TryLoadItemArt(string url)
        {
            try
            {
                pb_itemArt.LoadAsync(url);
            }
            catch (Exception e)
            {
                Console.WriteLine("Control_SearchResult::TryLoadItemArt Exception : " + e.Message);
            }
        }

        public void SetSockets()
        {
            lbl_itemName.Parent = pb_header_center;

            if (result.Item.Sockets == null)
                return;

            // Check if there are any Links and if there are create Lonk images
            // We need to do that first in order to have the Lonk images above the sockets.
            if (result.Item.Sockets.Count >= 2)
            {
                if (result.Item.Sockets[0].Group == result.Item.Sockets[1].Group)
                {
                    BetterPictureBox bpb_linked = new BetterPictureBox();
                    pb_itemArt.Controls.Add(bpb_linked);
                    bpb_linked.Size = new Size(38, 16);
                    bpb_linked.BackColor = Color.Transparent;
                    bpb_linked.Location = new Point(28, 31);
                    bpb_linked.Image = Properties.Resources.link_horizontal;
                }
            }

            if (result.Item.Sockets.Count >= 3)
            {
                if (result.Item.Sockets[1].Group == result.Item.Sockets[2].Group)
                {
                    BetterPictureBox bpb_linked = new BetterPictureBox();
                    pb_itemArt.Controls.Add(bpb_linked);
                    bpb_linked.Size = new Size(16, 38);
                    bpb_linked.BackColor = Color.Transparent;
                    bpb_linked.Location = new Point(63, 43);
                    bpb_linked.Image = Properties.Resources.link_Vertical;
                }
            }

            if (result.Item.Sockets.Count >= 4)
            {
                if (result.Item.Sockets[2].Group == result.Item.Sockets[3].Group)
                {
                    BetterPictureBox bpb_linked = new BetterPictureBox();
                    pb_itemArt.Controls.Add(bpb_linked);
                    bpb_linked.Size = new Size(38, 16);
                    bpb_linked.BackColor = Color.Transparent;
                    bpb_linked.Location = new Point(28, 78);
                    bpb_linked.Image = Properties.Resources.link_horizontal;
                }
            }

            if (result.Item.Sockets.Count >= 5)
            {
                if (result.Item.Sockets[3].Group == result.Item.Sockets[4].Group)
                {
                    BetterPictureBox bpb_linked = new BetterPictureBox();
                    pb_itemArt.Controls.Add(bpb_linked);
                    bpb_linked.Size = new Size(16, 38);
                    bpb_linked.BackColor = Color.Transparent;
                    bpb_linked.Location = new Point(16, 90);
                    bpb_linked.Image = Properties.Resources.link_Vertical;
                }
            }

            if (result.Item.Sockets.Count >= 6)
            {
                if (result.Item.Sockets[4].Group == result.Item.Sockets[5].Group)
                {
                    BetterPictureBox bpb_linked = new BetterPictureBox();
                    pb_itemArt.Controls.Add(bpb_linked);
                    bpb_linked.Size = new Size(38, 16);
                    bpb_linked.BackColor = Color.Transparent;
                    bpb_linked.Location = new Point(28, 125);
                    bpb_linked.Image = Properties.Resources.link_horizontal;
                }
            }

            for (int i = 0; i < result.Item.Sockets.Count; i++)
            {
                BetterPictureBox bpb = new BetterPictureBox();
                pb_itemArt.Controls.Add(bpb);
                bpb.BackColor = Color.Transparent;
                bpb.Size = new Size(47, 47);
                bpb.Location = new Point(Data.socketPositions.Rows[i]["x"].ToString().ToInt(), Data.socketPositions.Rows[i]["y"].ToString().ToInt());

                switch (result.Item.Sockets[i].SColour)
                {
                    case "R":
                        bpb.Image = Properties.Resources.str;
                        break;

                    case "G":
                        bpb.Image = Properties.Resources.dex;
                        break;

                    case "B":
                        bpb.Image = Properties.Resources._int;
                        break;

                    case "W":
                        bpb.Image = Properties.Resources.gen;
                        break;
                }
            }
        }

        private void HideSockets(object sender, EventArgs e)
        {
            if (Config.hideSocketsOnHover)
            {
                foreach (Control c in pb_itemArt.Controls)
                {
                    c.Visible = false;
                }
            }
        }

        private void ShowSockets(object sender, EventArgs e)
        {
            if (Config.hideSocketsOnHover)
            {
                foreach (Control c in pb_itemArt.Controls)
                {
                    c.Visible = true;
                }
            }
        }

        private float LoadPriceInChaos(string origPrice, float amount)
        {
            string priceString = Data.GetChaosEquivalent(origPrice);
            try
            {
                float price = float.Parse(priceString);
                lbl_priceInC.Text = (price * amount).ToString();
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("Control_SearchResult::LoadPriceInChaos exception : {0}, Input String {1}", e.Message, origPrice));
            }

            return 0.0f;
        }



        #region ENTER LEAVE EVENTS

        private void LabelEnter(object sender, EventArgs e)
        {
            Label lbl = (Label)sender;
            //lbl.BackColor = ThemeLoader.LoadColor(ThemeLoader.theme_Results, "PriceLabel", "Hover");
            //lbl.ForeColor = ThemeLoader.LoadColor(ThemeLoader.theme_Results, "PriceLabel", "HoverForeColor");
        }

        private void LabelLeave(object sender, EventArgs e)
        {
            Label lbl = (Label)sender;
            //lbl.BackColor = ThemeLoader.LoadColor(ThemeLoader.theme_Results, "PriceLabel", "BackColor");
            //lbl.ForeColor = ThemeLoader.LoadColor(ThemeLoader.theme_Results, "PriceLabel", "ForeColor");
        }

        private void lbl_exp_MouseEnter(object sender, EventArgs e)
        {
            Label lbl = (Label)sender;
            if (lbl.Controls.Count != 0)
                lbl.Controls[0].Visible = true;
        }

        // Pass Event to Parent Label
        private void GemLevel_Enter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            if (pb.Name == "Frame")
                Label_ModEnter(pb.Parent, e);
            
            else if (pb.Name == "Fill")
                Label_ModEnter(pb.Parent.Parent, e);
                
        }

        // Pass Event to Parent Label
        private void GemLevel_Leave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            if (pb.Name == "Frame")
                Label_ModLeave(pb.Parent, e);
            else if (pb.Name == "Fill")
                Label_ModLeave(pb.Parent.Parent, e);
        }

        private void Label_ModEnter(object sender, EventArgs e)
        {
            if (sender.GetType().Name == "Label")
            {
                Label lbl = (Label)sender;
                //lbl.BackColor = ThemeLoader.LoadColor(ThemeLoader.theme_Results, "Mod", "Hover");
            }
        }

        private void Label_ModLeave(object sender, EventArgs e)
        {
            if (sender.GetType().Name == "Label")
            {
                Label lbl = (Label)sender;
                //lbl.BackColor = ThemeLoader.LoadColor(ThemeLoader.theme_Results, "Mod", "BackColor");
            }
        }

        private void lbl_exp_MouseLeave(object sender, EventArgs e)
        {
            Label lbl = (Label)sender;
            if (lbl.Controls.Count != 0)
                lbl.Controls[0].Visible = false;
        }

        #endregion ENTER LEAVE EVENTS

        #region Control Events

        private void GemImage_Click(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;

            if (pb.Name == "Frame")
                LabelGem_Click(pb.Parent, e);
            else if (pb.Name == "Fill")
                LabelGem_Click(pb.Parent.Parent, e);
        }

        private void LabelGem_Click(object sender, EventArgs e)
        {
            Label lbl = (Label)sender;

            sq.Sort = new Sort();
            sq.Sort.Price = null;

            sq.Sort.stat = (sortGemProgressAsc == true) ? "desc" : "asc";

            var jsonResolver = new PropertyRenameAndIgnoreSerializerContractResolver();
            jsonResolver.RenameProperty(typeof(Sort), "stat", "gem_level_progress");

            var serializerSettings = new JsonSerializerSettings();
            serializerSettings.ContractResolver = jsonResolver;

            var json = JsonConvert.SerializeObject(sq, serializerSettings);

            Form_Search.instance.GetTradeData(json, sq, parent, pseudo);
            string header = string.Format(" Sort : Gem Level Progress - {0}", (sortGemProgressAsc == true) ? "asc" : "desc");
            //Form_Search.instance.SetHeader(Form_Search.instance.defaultHeader + header);

            sortGemProgressAsc = !sortGemProgressAsc;
        }

        private void lbl_currencyType_Click(object sender, EventArgs e)
        {
            Label lbl = (Label)sender;

            sq.Sort = new Sort();
            sq.Sort.stat = null;
            sq.Sort.Price = (sortPriceAsc == true) ? "desc" : "asc";

            Form_Search.instance.GetTradeData(sq.ToJson(), sq, parent);
            header = string.Format(" Sort : {0} - {1}", " Price", (sortPriceAsc == true) ? "desc" : "asc");
            //Form_Search.instance.SetHeader(Form_Search.instance.defaultHeader + header);
            sortPriceAsc = !sortPriceAsc;
        }

        private void LabelImp_Click(object sender, EventArgs e)
        {
            Label lbl = (Label)sender;

            string lblText = lbl.Text;
            string mod;
            string modId;

            string removedNumbers = Regex.Replace(lblText, @"\d", "#"); // Replace all numbers with #
            removedNumbers = Regex.Replace(removedNumbers, @"#+", "#"); // Replace all # with just one #

            /// Replace reduced with increased on some mods
            if (lblText.Contains("reduced"))
                if (!Data.modsWithReduced.Contains(removedNumbers))
                    removedNumbers = removedNumbers.Replace("reduced", "increased");

            mod = removedNumbers.Replace("+", "");
            modId = Data.GetModIdByText(removedNumbers.Replace("+", ""), true);


            string priceSort = sq.Sort.Price;

            sq.Sort = new Sort();
            sq.Sort.stat = (sortModAsc == true) ? "desc" : "asc";
            sq.Sort.Price = null;

            var jsonResolver = new PropertyRenameAndIgnoreSerializerContractResolver();
            jsonResolver.RenameProperty(typeof(Sort), "stat", "stat." + modId);

            var serializerSettings = new JsonSerializerSettings();
            serializerSettings.ContractResolver = jsonResolver;

            var json = JsonConvert.SerializeObject(sq, serializerSettings);

            Form_Search.instance.GetTradeData(json, sq, parent, pseudo);
            header = string.Format(" Sort : {0} - {1}", mod, (sortModAsc == true) ? "asc" : "desc");
            //Form_Search.instance.SetHeader(Form_Search.instance.defaultHeader + header);
            sortModAsc = !sortModAsc;


        }

        /// <summary>
        ///  Parse Mod and create a new Search Query
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LabelMod_Click(object sender, EventArgs e)
        {
            Label lbl = (Label)sender;

            string lblText = lbl.Text;
            string mod;
            string modId;

            string removedNumbers = Regex.Replace(lblText, @"\d", "#"); // Replace all numbers with #
            removedNumbers = Regex.Replace(removedNumbers, @"#+", "#"); // Replace all # with just one #

            /// Replace reduced with increased on some mods
            if (lblText.Contains("reduced"))
                if (!Data.modsWithReduced.Contains(removedNumbers))
                    removedNumbers = removedNumbers.Replace("reduced", "increased");

            if (lbl.Text.Contains("(pseudo)"))
            {
                mod = removedNumbers.Replace("(pseudo) ", "");
                modId = Data.GetModIdByText(removedNumbers.Replace("(pseudo) ", ""));
            }
            else
            {
                mod = removedNumbers.Replace("+", "");
                modId = Data.GetModIdByText(removedNumbers.Replace("+", ""));
            }

            string priceSort = sq.Sort.Price;

            sq.Sort = new Sort();
            sq.Sort.stat = (sortModAsc == true) ? "asc" : "desc";
            sq.Sort.Price = null;

            var jsonResolver = new PropertyRenameAndIgnoreSerializerContractResolver();
            jsonResolver.RenameProperty(typeof(Sort), "stat", "stat." + modId);

            var serializerSettings = new JsonSerializerSettings();
            serializerSettings.ContractResolver = jsonResolver;

            var json = JsonConvert.SerializeObject(sq, serializerSettings);

            Form_Search.instance.GetTradeData(json, sq, parent, pseudo);
            header = string.Format(" Sort : {0} - {1}", mod, (sortModAsc == true) ? "asc" : "desc");
            //Form_Search.instance.SetHeader(Form_Search.instance.defaultHeader + header);
            sortModAsc = !sortModAsc;
        }

        private void btn_Whisper_Click(object sender, EventArgs e)
        {
            if (whisperDelayWatch.ElapsedMilliseconds > whisperDelay)
            {
                if (Keyboard.IsKeyDown(Key.LeftCtrl))
                {
                    Form_Search.instance.hide = true;
                    Form_Search.instance.Hide();
                }

                ChatHelper.SendChatMessage(msg);
                whisperDelayWatch.Restart();
            }
        }

        private void lbl_sellerName_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.pathofexile.com/account/view-profile/" + result.Listing.Account.Name);
        }

        private void lbl_lastChar_Click(object sender, EventArgs e)
        {
            ChatHelper.SendChatMessage("/whois  " + result.Listing.Account.LastCharacterName);
        }

        private void btn_copyItem_Click(object sender, EventArgs e)
        {
            string item = "";
            string rarity = "";

            switch (result.Item.FrameType)
            {
                case 0: /// NORMAL ITEM
                    rarity = "Normal";
                    break;

                case 1: /// MAGIC ITEM
                    rarity = "Magic";
                    break;

                case 2: /// RARE ITEM
                    rarity = "Rare";
                    break;

                case 3: /// UNIQUE ITEM
                    rarity = "Unique";
                    break;

                case 4: /// SKILL GEM
                    rarity = "Gem";
                    break;

                case 5: /// ESSENCE
                    rarity = "Currency";
                    break;
            }

            item += string.Format("Rarity: {0}\n", rarity);

            if (result.Item.Name != "")
                item += result.Item.Name + "\n";

            if (result.Item.TypeLine != "")
                item += result.Item.TypeLine + "\n";

            item += "--------\n";

            if (result.Item.StackSize != 0)
            {
                item += string.Format("Stack Size: {0}/{1}\n", result.Item.StackSize, result.Item.MaxStackSize);
            }

            Clipboard.SetText(item);
        }

        private void btn_visitHideout_Click(object sender, EventArgs e)
        {
            ChatHelper.SendChatMessage("/hideout  " + result.Listing.Account.LastCharacterName);
        }

        private void btn_wiki_Click(object sender, EventArgs e)
        {
            string url = "http://pathofexile.gamepedia.com/";

            switch (result.Item.FrameType)
            {
                case 3:
                    url += result.Item.Name;
                    break;

                case 4:
                case 5:
                case 6:
                    url += result.Item.TypeLine;
                    break;
            }

            Process.Start(url);
        }

        #endregion Control Events


    }
}