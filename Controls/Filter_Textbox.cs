﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PoE_Trade_Overlay.Controls
{
    public partial class Filter_Textbox : UserControl
    {
        public string Header
        {
            get { return lbl_FilterName.Text; }
            set { lbl_FilterName.Text = value; }
        }
        
        public Color ForeColor_Label;
        public Color ForeColor_Textbox;

        public Color BackColor_Label;
        public Color BackColor_Textbox;

        public Filter_Textbox()
        {
            InitializeComponent();
        }

        public void RefreshControls()
        {
            lbl_FilterName.ForeColor = ForeColor_Label;
            lbl_FilterName.BackColor = BackColor_Label;

            textBox1.ForeColor = ForeColor_Textbox;
            textBox1.BackColor = BackColor_Textbox;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }

        public void Reset()
        {
            textBox1.Text = string.Empty;
        }

        public string GetText()
        {
            return textBox1.Text;
        }

        public bool IsEmpty()
        {
            return (textBox1.Text.Length == 0) ? true : false;
        }
    }
}