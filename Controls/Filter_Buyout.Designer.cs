﻿namespace PoE_Trade_Overlay.Controls
{
    partial class Filter_Buyout
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_max = new System.Windows.Forms.TextBox();
            this.tb_min = new System.Windows.Forms.TextBox();
            this.lbl_FilterName = new System.Windows.Forms.Label();
            this.combo = new PoE_Trade_Overlay.Controls.FlattenCombo();
            this.SuspendLayout();
            // 
            // tb_max
            // 
            this.tb_max.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.tb_max.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_max.Font = new System.Drawing.Font("Consolas", 8F);
            this.tb_max.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.tb_max.Location = new System.Drawing.Point(547, 5);
            this.tb_max.Multiline = true;
            this.tb_max.Name = "tb_max";
            this.tb_max.Size = new System.Drawing.Size(40, 17);
            this.tb_max.TabIndex = 4;
            this.tb_max.Text = "max";
            this.tb_max.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_min
            // 
            this.tb_min.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.tb_min.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_min.Font = new System.Drawing.Font("Consolas", 9F);
            this.tb_min.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.tb_min.Location = new System.Drawing.Point(504, 5);
            this.tb_min.Multiline = true;
            this.tb_min.Name = "tb_min";
            this.tb_min.Size = new System.Drawing.Size(40, 17);
            this.tb_min.TabIndex = 3;
            this.tb_min.Text = "min";
            this.tb_min.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_FilterName
            // 
            this.lbl_FilterName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.lbl_FilterName.Font = new System.Drawing.Font("Consolas", 9F);
            this.lbl_FilterName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_FilterName.Location = new System.Drawing.Point(3, 3);
            this.lbl_FilterName.Name = "lbl_FilterName";
            this.lbl_FilterName.Size = new System.Drawing.Size(200, 21);
            this.lbl_FilterName.TabIndex = 5;
            this.lbl_FilterName.Text = "Label";
            this.lbl_FilterName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // combo
            // 
            this.combo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.combo.AutoComp = false;
            this.combo.BackColor = System.Drawing.Color.Red;
            this.combo.BorderColor = System.Drawing.Color.Black;
            this.combo.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.combo.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.combo.Data = null;
            this.combo.DropDownColor = System.Drawing.Color.Red;
            this.combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo.Font = new System.Drawing.Font("Consolas", 8F);
            this.combo.ForeColor = System.Drawing.Color.Blue;
            this.combo.ForeColorDrop = System.Drawing.Color.Blue;
            this.combo.FormattingEnabled = true;
            this.combo.Items.AddRange(new object[] {
            "Chaos Orb Equivalent",
            "Blessed Orb",
            "Cartographer\'s Chisel",
            "Chaos Orb",
            "Chromatic Orb",
            "Divine Orb",
            "Exalted Orb",
            "Gemcutter\'s Prism",
            "Jeweller\'s Orb",
            "Orb of Scouring",
            "Orb of Regret",
            "Orb of Fusing",
            "Orb of Chance",
            "Orb of Alteration",
            "Orb of Alchemy",
            "Regal Orb",
            "Vaal Orb"});
            this.combo.Location = new System.Drawing.Point(209, 3);
            this.combo.Name = "combo";
            this.combo.Size = new System.Drawing.Size(289, 21);
            this.combo.TabIndex = 6;
            // 
            // Filter_Buyout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.Controls.Add(this.combo);
            this.Controls.Add(this.lbl_FilterName);
            this.Controls.Add(this.tb_max);
            this.Controls.Add(this.tb_min);
            this.Name = "Filter_Buyout";
            this.Size = new System.Drawing.Size(590, 26);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_max;
        private System.Windows.Forms.TextBox tb_min;
        private System.Windows.Forms.Label lbl_FilterName;
        private FlattenCombo combo;
    }
}
