﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace PoE_Trade_Overlay.Controls
{
    public partial class Filter_DropDown : UserControl
    {
        public string Header
        {
            get { return lbl_FilterName.Text; }
            set { lbl_FilterName.Text = value; }
        }

        public string[] Items
        {
            get
            {
                List<string> str = new List<string>();
                foreach (object obj in combo.Items)
                {
                    str.Add(obj.ToString());
                }
                return str.ToArray();
            }

            set
            {
                combo.Items.Clear();
                foreach (string st in value)
                {
                    combo.Items.Add(st);
                }
                if (combo.Items.Count != 0)
                    combo.SelectedIndex = 0;
            }
        }
        
        public Color ForeColor_Label;
        public Color ForeColor_Dropdown;

        public Color BackColor_Label;
        public Color BackColor_DropDown;
        public Color BackColor_Button;
        public Color Border;
        public Color Arrow_Active;
        public Color Arrow_Inactive;
        public Filter_DropDown()
        {
            InitializeComponent();
            combo.Items.Clear();
            foreach (string str in Items)
            {
                combo.Items.Add(str);
            }
            if (combo.Items.Count != 0)
                combo.SelectedIndex = 0;
        }

        public void RefreshControls()
        {
            lbl_FilterName.ForeColor = ForeColor_Label;
            lbl_FilterName.BackColor = BackColor_Label;
            combo.DropDownColor = BackColor_DropDown;
            combo.BackColor = BackColor_DropDown;
            combo.ForeColor = ForeColor_Dropdown;
            combo.ForeColorDrop = ForeColor_Dropdown;
            combo.ButtonColor = BackColor_Button;
            combo.Border = Border;
            combo.Arrow_Active = Arrow_Active;
            combo.Arrow_Inactive = Arrow_Inactive;
        }

        private void lbl_FilterName_Click(object sender, EventArgs e)
        {
            combo.SelectedIndex = (combo.SelectedIndex == combo.Items.Count - 1) ? 0 : combo.SelectedIndex + 1;
        }

        public void Reset()
        {
            combo.SelectedIndex = 0;
        }

        public bool IsEmpty()
        {
            return (combo.SelectedIndex != 0) ? false : true;
        }

        public string GetTrueFalse()
        {
            return (combo.Items[combo.SelectedIndex].ToString() == "Yes") ? "true" : "false";
        }

        public int GetIndex()
        {
            return combo.SelectedIndex;
        }
    }
}