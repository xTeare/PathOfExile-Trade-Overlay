﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PoE_Trade_Overlay.Controls
{
    public partial class ModGroup : UserControl
    {
        private string _type;
        public string type
        {
            get { return _type; }
            set
            {
                _type = value;
                ShowHideControls();
                lbl_typ.Text = "Mod Group : " + value;
            }
        }

        public string min;
        public string max;

        public Color ForeColor_Textbox;
        public Color ForeColor_Header;
        public Color ForeColor_Button;
        public Color ForeColor_Placeholder;
        public Color BackColor_Header;
        public Color BackColor_Button;
        public Color BackColor_Textbox;

        public ModGroup()
        {
            InitializeComponent();
            tb_min.KeyDown += new KeyEventHandler(TextboxEvents.PreventEnter);
            tb_min.LostFocus += new EventHandler(TextboxEvents.PlaceMinMax);
            tb_min.GotFocus += new EventHandler(TextboxEvents.RemovePlaceholder);

            tb_max.KeyDown += new KeyEventHandler(TextboxEvents.PreventEnter);
            tb_max.LostFocus += new EventHandler(TextboxEvents.PlaceMinMax);
            tb_max.GotFocus += new EventHandler(TextboxEvents.RemovePlaceholder);
        }

        public void RefreshControls()
        {
            tb_min.ForeColor = ForeColor_Placeholder;
            tb_max.ForeColor = ForeColor_Placeholder;
            btn_addMod.ForeColor = ForeColor_Button;
            btn_addMod.BackColor = BackColor_Button;

            lbl_typ.ForeColor = ForeColor_Header;
            lbl_typ.BackColor = BackColor_Header;
            
        }

        public List<ModSearch> GetMods()
        {
            List<ModSearch> cntrls = new List<ModSearch>();
            foreach(Control c in flp.Controls)
            {
                if (c.GetType() == typeof(ModSearch))
                    cntrls.Add((ModSearch)c);
            }
            return cntrls;
        }

        public void RemoveModControl(ModSearch c)
        {
            flp.Controls.Remove(c);
            Form_Search.instance.CalculateECMod();
        }

        public void AddSearchResults(ListBox lb, ModSearch cms)
        {
            flp.Controls.Add(lb);
            flp.Controls.SetChildIndex(lb, flp.Controls.GetChildIndex(cms) + 1);
            Form_Search.instance.RepositionModGroups();
            Form_Search.instance.CalculateECMod();
        }

        private void ShowHideControls()
        {
            if(type == "count" || type == "weight")
            {
                tb_min.Visible = true;
                tb_max.Visible = true;
            }
            else
            {
                tb_min.Visible = false;
                tb_max.Visible = false;
            }
        }

        public bool isMinMaxEmpty()
        {
            bool isEmpty = true;

            if (tb_min.Text != "min")
                if (tb_min.Text.ToLong() != 0)
                    isEmpty = false;

            if (tb_max.Text != "max")
                if (tb_max.Text.ToLong() != 0)
                    isEmpty = false;

            return isEmpty;
        }

        private void btn_addMod_Click(object sender, EventArgs e)
        {
            AddSearch();
        }

        public void AddSearch()
        {
            ModSearch cms = new ModSearch();
            cms.parent = this;

            if (type == "weight")
                cms.SetWeighted();

            flp.Controls.Add(cms);
            ThemeLoader.ApplyTheme(new List<Control>() { cms });
            Form_Search.instance.RepositionModGroups();
            Form_Search.instance.CalculateECMod();
        }

        private void btn_addModGroup_Click(object sender, EventArgs e)
        {
            Form_Search.instance.RemoveModGroup(this);
        }

        private void tb_min_TextChanged(object sender, EventArgs e)
        {
            min = tb_min.Text;
        }

        private void tb_max_TextChanged(object sender, EventArgs e)
        {
            max = tb_max.Text;
        }
    }
}
