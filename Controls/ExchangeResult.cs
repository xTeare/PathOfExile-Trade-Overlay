﻿using PoE_Trade_Overlay.Queries;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace PoE_Trade_Overlay.Controls
{
    public partial class ExchangeResult : UserControl
    {
        public Result result;
        public BulkQuery bq;
        public TabPage page;

        public WebClient wc;

        public Stopwatch whisperDelayWatch = new Stopwatch();
        public int whisperDelay = 5000;

        public Color ForeColor_Exchange;
        public Color ForeColor_ExchangeHover;
        public Color ForeColor_Online;
        public Color ForeColor_Offline;
        public Color ForeColor_Afk;
        public Color ForeColor_Warning;
        public Color BackColor_Top;
        public Color BackColor_Bottom;
        public Color BackColor_Online;
        public Color BackColor_Offline;
        public Color BackColor_Afk;
        public Color BackColor_Exchange;
        public Color BackColor_ExchangeHover;
        public Color BackColor_ModHover;


        public ExchangeResult()
        {
            InitializeComponent();
            whisperDelayWatch.Start();
            tip.SetToolTip(lbl_lastChar, "Name of the last played Character\nClick to execute /whois");
            tip.SetToolTip(lbl_sellerName, "Name of the sellers account\nClick to open his profile");
        }

        public void LoadResult()
        {
            lbl_stockWarning.Visible = false;
            lbl_stockWarning.ForeColor = ForeColor_Warning;
            lbl_stockWarning.BackColor = BackColor;

            lbl_message.BackColor = BackColor_Exchange;
            lbl_message.ForeColor = ForeColor_Exchange;

            lbl_payGet.ForeColor = ForeColor;
            lbl_payGet.BackColor = BackColor_Top;
            panel.BackColor = BackColor_Bottom;

            lbl_getType.Text = result.Item.TypeLine;
            lbl_getType.ForeColor = ForeColor;
            lbl_getType.BackColor = BackColor;


            string payType = Data.GetEntryFromRow(Data.currency, 1, "id", result.Listing.Price.Exchange.Currency);
            lbl_payType.Text = (payType != string.Empty) ? payType : System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(result.Listing.Price.Exchange.Currency.Replace("-", " ").ToLower());
            lbl_payType.ForeColor = ForeColor;
            lbl_payType.BackColor = BackColor;


            lbl_getAmount.Text = "x" + result.Listing.Price.Item.Amount.ToString();
            lbl_getAmount.ForeColor = ForeColor;
            lbl_getAmount.BackColor = BackColor;

            lbl_payAmount.Text = result.Listing.Price.Exchange.Amount.ToString() + "x";
            lbl_payAmount.ForeColor = ForeColor;
            lbl_payAmount.BackColor = BackColor;

            dragBar1.Minimum = 1;
            dragBar1.Maximum = 50;
            dragBar1.Value = 1;

            lbl_message.Text = string.Format(result.Listing.Whisper, result.Listing.Price.Item.Amount * dragBar1.Value, result.Listing.Price.Exchange.Amount * dragBar1.Value);

            lbl_stock.Text = string.Format("Stock: {0} {1}", result.Listing.Price.Item.Stock, result.Item.TypeLine);
            lbl_stock.ForeColor = ForeColor;
            lbl_stock.BackColor = BackColor_Bottom;

            lbl_getAmountTrack.Text = "x " + result.Listing.Price.Item.Amount * dragBar1.Value;
            lbl_getAmountTrack.ForeColor = ForeColor;
            lbl_getAmountTrack.BackColor = BackColor;

            lbl_payAmountTrack.Text = result.Listing.Price.Exchange.Amount * dragBar1.Value + " x";
            lbl_payAmountTrack.ForeColor = ForeColor;
            lbl_payAmountTrack.BackColor = BackColor;

            lbl_sellerName.Text = "acc.: " + result.Listing.Account.Name;
            lbl_sellerName.ForeColor = ForeColor;
            lbl_sellerName.BackColor = BackColor_Bottom;

            lbl_lastChar.Text = "char: " + result.Listing.Account.LastCharacterName;
            lbl_lastChar.ForeColor = ForeColor;
            lbl_lastChar.BackColor = BackColor_Bottom;

            lbl_worth1.Text = string.Format("is worth {0} x", result.Listing.Price.Item.Amount);
            lbl_worth1.ForeColor = ForeColor;
            lbl_worth1.BackColor = BackColor;

            lbl_worth2.Text = string.Format("is worth {0} x", ((decimal)result.Listing.Price.Exchange.Amount / (decimal)result.Listing.Price.Item.Amount).ToString("#.####"));
            lbl_worth2.ForeColor = ForeColor;
            lbl_worth2.BackColor = BackColor;

            lbl_worth1.Location = new Point((pb_worth1.Left + pb_worth1.Width) + 2, lbl_worth1.Top);
            lbl_worth2.Location = new Point((pb_worth4.Left - lbl_worth2.Width) - 6, lbl_worth2.Top);

            pb_worth2.Location = new Point((lbl_worth1.Left + lbl_worth1.Width) + 2, pb_worth2.Top);
            pb_worth3.Location = new Point((lbl_worth2.Left - pb_worth3.Width) - 2, pb_worth3.Top);

            label8.Location = new Point((pb_worth3.Left - label8.Width) - 2, label8.Top);

            lbl_message.Location = new Point((this.Width / 2) - (lbl_message.Width / 2), lbl_message.Top);
            lbl_payGet.Location = new Point((this.Width / 2) - (lbl_payGet.Width / 2), lbl_payGet.Top);

            pb_get.Location = new Point((lbl_getAmount.Left - pb_get.Width) - 6, pb_get.Top);
            pb_pay.Location = new Point((lbl_payAmount.Left + lbl_payAmount.Width) + 6, pb_pay.Top);

            lbl_getType.Location = new Point((pb_get.Left - lbl_getType.Width) - 6, lbl_getType.Top);
            lbl_payType.Location = new Point((pb_pay.Left + pb_pay.Width) + 6, lbl_payType.Top);

            lbl_lastChar.Location = new Point((lbl_sellerName.Left + lbl_sellerName.Width) + 6, lbl_lastChar.Top);

            string getImageLocation = Application.StartupPath + "/images/" + result.Listing.Price.Item.Currency + ".png";
            string payImageLocation = Application.StartupPath + "/images/" + result.Listing.Price.Exchange.Currency + ".png";

            if (File.Exists(getImageLocation))
            {
                pb_get.LoadAsync(getImageLocation);
                pb_get.BackColor = BackColor;
                pb_worth2.LoadAsync(getImageLocation);
                pb_worth2.BackColor = BackColor;
                pb_worth3.LoadAsync(getImageLocation);
                pb_worth3.BackColor = BackColor;
            }

            if (File.Exists(payImageLocation))
            {
                pb_pay.LoadAsync(payImageLocation);
                pb_pay.BackColor = BackColor;
                pb_worth1.LoadAsync(payImageLocation);
                pb_worth1.BackColor = BackColor;
                pb_worth4.LoadAsync(payImageLocation);
                pb_worth4.BackColor = BackColor;
            }
            else
            {
                pb_pay.Visible = false;
                lbl_payType.Location = new Point((lbl_payAmount.Left + lbl_payAmount.Width) + 6, lbl_payType.Top);
            }

            if (result.Listing.Account.Online != null)
            {
                // Is Logged in. Now check if afk
                if (result.Listing.Account.Online.Status != null)
                {
                    lbl_online.Text = "afk";
                    lbl_online.BackColor = BackColor_Afk;
                    lbl_online.ForeColor = ForeColor_Afk;
                }
                else
                {
                    lbl_online.Text = "online";
                    lbl_online.BackColor = BackColor_Online;
                    lbl_online.ForeColor = ForeColor_Online;
                }
            }
            else
            {
                lbl_online.Text = "offline";
                lbl_online.BackColor = BackColor_Offline;
                lbl_online.ForeColor = ForeColor_Offline;
            }
        }

        private void LabelEnter(object sender, EventArgs e)
        {
            Label lbl = (Label)sender;
            if (lbl.Name == "lbl_message")
            {
                lbl.BackColor = BackColor_ExchangeHover;
                lbl.ForeColor = ForeColor_ExchangeHover;
            }
            else
            {
                lbl.BackColor = BackColor_ModHover;
                lbl.ForeColor = ForeColor;
            }
        }

        private void LabelLeave(object sender, EventArgs e)
        {
            Label lbl = (Label)sender;
            if(lbl.Name == "lbl_message")
            {
                lbl.BackColor = BackColor_Exchange;
                lbl.ForeColor = ForeColor_Exchange;
            }
            else
            {
                lbl.BackColor = BackColor_Bottom;
                lbl.ForeColor = ForeColor;
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            lbl_message.Text = string.Format(result.Listing.Whisper, result.Listing.Price.Item.Amount * dragBar1.Value, result.Listing.Price.Exchange.Amount * dragBar1.Value);

            if (result.Listing.Price.Item.Amount * dragBar1.Value > result.Listing.Price.Item.Stock)
                lbl_stockWarning.Visible = true;
            else
                lbl_stockWarning.Visible = false;

            lbl_message.Location = new Point((this.Width / 2) - (lbl_message.Width / 2), lbl_message.Top);

            lbl_getAmountTrack.Text = "x " + result.Listing.Price.Item.Amount * dragBar1.Value;
            lbl_payAmountTrack.Text = result.Listing.Price.Exchange.Amount * dragBar1.Value + " x";
        }

        private void lbl_sellerName_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.pathofexile.com/account/view-profile/" + result.Listing.Account.Name);
        }

        private void lbl_lastChar_Click(object sender, EventArgs e)
        {
            ChatHelper.SendChatMessage("/whois  " + result.Listing.Account.LastCharacterName);
        }

        private void ExchangeResult_Load(object sender, EventArgs e)
        {
        }

        private void lbl_message_MouseClick(object sender, MouseEventArgs e)
        {
            if (whisperDelayWatch.ElapsedMilliseconds > whisperDelay)
            {
                if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) || e.Button == MouseButtons.Right)
                {
                    Form_Search.instance.hide = true;
                    Form_Search.instance.Hide();
                }
                if (ChatHelper.IsPoeOpen())
                {
                    ChatHelper.SendChatMessage(lbl_message.Text);
                }
                else
                {
                    Clipboard.SetText(lbl_message.Text);
                }
                whisperDelayWatch.Restart();
            }
         }
    }
}