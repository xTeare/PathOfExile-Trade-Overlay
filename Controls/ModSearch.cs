﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace PoE_Trade_Overlay.Controls
{
    public partial class ModSearch : UserControl
    {
        public string mod;
        public string min;
        public string max;
        public string weight;
        
        public ExpandCollapse p;
        public FlowLayoutPanel flp;

        public ModGroup parent;
        public bool isWeighted;

        public Color ForeColor_Textbox;
        public Color ForeColor_Listbox;
        public Color ForeColor_Button;
        public Color ForeColor_Placeholder;

        public Color BackColor_Textbox;
        public Color BackColor_Listbox;
        public Color BackColor_Button;

        public ModSearch()
        {
            InitializeComponent();
            

            tb_mod.KeyDown += new KeyEventHandler(TextboxEvents.PreventOnlyEnter);
            tb_mod_min.KeyDown += new KeyEventHandler(TextboxEvents.PreventEnter);
            tb_mod_max.KeyDown += new KeyEventHandler(TextboxEvents.PreventEnter);
            tb_weight.KeyDown += new KeyEventHandler(TextboxEvents.PreventEnter);

            foreach (Control control in this.Controls)
            {
                if (control.GetType() == typeof(TextBox))
                {
                    if (control.Name == "tb_weight")
                        control.LostFocus += new EventHandler(TextboxEvents.PlaceWeight);
                    else
                        control.LostFocus += new EventHandler(TextboxEvents.PlaceMinMax);

                    control.GotFocus += new EventHandler(TextboxEvents.RemovePlaceholder);
                }
            }


            tb_mod.Size = new Size(358, 16);
            tb_weight.Visible = isWeighted;


        }

        public bool isMinMaxEmpty()
        {
            bool isEmpty = true;

            if (tb_mod_min.Text != "min")
                if (tb_mod_min.Text.ToLong() != 0)
                    isEmpty = false;

            if (tb_mod_max.Text != "max")
                if (tb_mod_max.Text.ToLong() != 0)
                    isEmpty = false;

            return isEmpty;
        }

        private int height;

        public void RefreshControls()
        {
            tb_mod.BackColor = BackColor_Textbox;
            tb_mod_min.BackColor = BackColor_Textbox;
            tb_mod_max.BackColor = BackColor_Textbox;

            tb_mod.ForeColor = ForeColor_Textbox;
            tb_mod_min.ForeColor = ForeColor_Placeholder;
            tb_mod_max.ForeColor = ForeColor_Placeholder;

            lb_mods.ForeColor = ForeColor_Listbox;
            lb_mods.BackColor = BackColor_Listbox;
            button1.ForeColor = ForeColor_Button;
            button1.BackColor = BackColor_Button;

        }

        public void SetWeighted()
        {
            isWeighted = true;
            tb_mod.Size = new Size(302, 16);
            tb_weight.Visible = isWeighted;
        }

        private void tb_mod_TextChanged(object sender, EventArgs e)
        {
            FlowLayoutPanel flp = (FlowLayoutPanel)this.Parent;

            // Only search if we have more than 2 letters to reduce input lag
            if (tb_mod.Text.Length < 3)
            {
                lb_mods.Visible = false;
                flp.Refresh();
                return;
            }
            parent.AddSearchResults(lb_mods, this);
            

            List<string> results = new List<string>();
            results = TextboxEvents.SearchDataTableMods(Data.mods, 1, tb_mod.Text);

            lb_mods.Items.Clear();

            foreach (string result in results)
                lb_mods.Items.Add(result);

            if (lb_mods.Items.Count > 0)
                lb_mods.SelectedIndex = 0;



            if (results.Count > 14)
                lb_mods.Size = new Size(this.Width, 15 * lb_mods.ItemHeight);
            else
                lb_mods.Size = new Size(this.Width, (lb_mods.Items.Count + 1) * lb_mods.ItemHeight);


            lb_mods.Visible = (results.Count > 0) ? true : false;
            height = lb_mods.Height;

            mod = tb_mod.Text;
        }

        private void lb_mods_MouseDown(object sender, MouseEventArgs e)
        {
            tb_mod.Text = lb_mods.Items[lb_mods.SelectedIndex].ToString();
            lb_mods.Visible = false;
            this.Size = new Size(544, 18);
        }

        private void tb_mod_Leave(object sender, EventArgs e)
        {
            if (!lb_mods.Focused)
            {
                lb_mods.Visible = false;
                this.Size = new Size(544, 18);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            parent.RemoveModControl(this);
        }

        private void tb_mod_min_TextChanged(object sender, EventArgs e)
        {
            min = tb_mod_min.Text;
        }

        private void tb_mod_max_TextChanged(object sender, EventArgs e)
        {
            max = tb_mod_max.Text;
        }

        private void lb_mods_VisibleChanged(object sender, EventArgs e)
        {
            Form_Search.instance.RepositionModGroups();
            Form_Search.instance.CalculateECMod();
        }

        private void tb_mod_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                if (lb_mods.Visible)
                {
                    if (lb_mods.SelectedIndex < lb_mods.Items.Count)
                        tb_mod.Text = lb_mods.Items[lb_mods.SelectedIndex].ToString();
                }
            }
        }

        private void tb_weight_TextChanged(object sender, EventArgs e)
        {
            weight = tb_weight.Text;
        }
    }
}