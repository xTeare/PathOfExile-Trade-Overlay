﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace PoE_Trade_Overlay.Controls
{
    public partial class BulkButton : UserControl
    {
        [Description("Header Text"), Category("Data")]
        public string Header
        {
            get { return button1.Text; }
            set
            {
                button1.Text = value;
                button1.AutoSize = false;
                this.Size = new Size(150, 24);
                button1.Size = new Size(150, 24);
            }
        }

        [Description("Image"), Category("Data")]
        public Image Bitmap
        {
            get { return button1.BackgroundImage; }
            set
            {
                button1.BackgroundImage = value;
                this.Size = new Size(40, 40);
                button1.Size = new Size(40, 40);
                button1.BackgroundImageLayout = ImageLayout.Zoom;
            }
        }

        public bool isWanted;
        public string currencyId;
        public string currencyName;
        public bool isSelected;
        private ToolTip tool;


        private bool _isHighlighted;
        public bool isHighlighted
        {
            get { return _isHighlighted; }
            set
            {
                _isHighlighted = value;
                SetBorder();
            }
        }

        public BulkButton()
        {
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            InitializeComponent();


            tool = new ToolTip();
        }

        private void SetBorder()
        {
            button1.FlatAppearance.BorderColor = (isHighlighted) ? Color.FromArgb(255, 255, 255) : Color.FromArgb(25, 25, 25);
        }


        private void button1_Click(object sender, EventArgs e)
        {
            isSelected = !isSelected;
            SetColor();
        }

        private void SetColor()
        {
            button1.BackColor = (isSelected == true) ? Color.FromArgb(35, 35, 35) : Color.FromArgb(5, 5, 5);
        }

        public void SetTip(string s)
        {
            tool.SetToolTip(button1, s);
        }
    }
}