﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PoE_Trade_Overlay.Controls
{
    public partial class Filter_Buyout : UserControl
    {
        public string Header
        {
            get { return lbl_FilterName.Text; }
            set { lbl_FilterName.Text = value; }
        }

        public Color ForeColor_Label;
        public Color ForeColor_Dropdown;
        public Color ForeColor_Textbox;
        public Color ForeColor_Placeholder;
        public Color BackColor_Textbox;
        public Color BackColor_Label;
        public Color BackColor_DropDown;
        public Color BackColor_Button;
        public Color Border;
        public Color Arrow_Active;
        public Color Arrow_Inactive;

        public Filter_Buyout()
        {
            InitializeComponent();

            tb_min.LostFocus += new EventHandler(TextboxEvents.PlaceMinMax);
            tb_min.GotFocus += new EventHandler(TextboxEvents.RemovePlaceholder);
            tb_min.KeyDown += new KeyEventHandler(TextboxEvents.PreventEnter);
            tb_max.LostFocus += new EventHandler(TextboxEvents.PlaceMinMax);
            tb_max.GotFocus += new EventHandler(TextboxEvents.RemovePlaceholder);
            tb_max.KeyDown += new KeyEventHandler(TextboxEvents.PreventEnter);

            if (combo.Items.Count != 0)
                combo.SelectedIndex = 0;
        }

        public void RefreshControls()
        {
            lbl_FilterName.ForeColor = ForeColor;
            lbl_FilterName.BackColor = BackColor_Label;
            
            tb_min.BackColor = BackColor_Textbox;
            tb_max.BackColor = BackColor_Textbox;

            combo.ForeColor = ForeColor_Dropdown;
            combo.ForeColorDrop = ForeColor_Dropdown;
            combo.BackColor = BackColor_DropDown;
            combo.Border = Border;
            combo.ButtonColor = BackColor_Button;
            combo.Arrow_Active = Arrow_Active;
            combo.Arrow_Inactive = Arrow_Inactive;
            combo.DropDownColor = BackColor_DropDown;
            combo.Invalidate();
        }

        public void Reset()
        {
            tb_min.Text = "min";
            tb_max.Text = "max";
            combo.SelectedIndex = 0;
        }

        public string GetMin()
        {
            if (tb_min.Text != string.Empty)
            {
                if (tb_min.Text != "min")
                    if (tb_min.Text != "0")
                        return tb_min.Text;
            }
            return null;
        }

        public string GetMax()
        {
            if (tb_max.Text != string.Empty)
            {
                if (tb_max.Text != "max")
                    if (tb_max.Text != "0")
                        return tb_max.Text;
            }
            return null;
        }

        public int GetIndex()
        {
            return combo.SelectedIndex;
        }

        public string GetSelectedItem()
        {
            return combo.Items[combo.SelectedIndex].ToString();
        }

        public bool IsEmpty()
        {
            bool isEmpty = true;

            if (combo.SelectedIndex != 0)
                isEmpty = false;

            if (tb_min.Text != "min" && tb_min.Text != "0" && tb_min.Text != string.Empty)
                isEmpty = false;

            if (tb_max.Text != "max" && tb_max.Text != "0" && tb_max.Text != string.Empty)
                isEmpty = false;

            return isEmpty;
        }
    }
}