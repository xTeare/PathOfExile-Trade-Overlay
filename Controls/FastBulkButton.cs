﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace PoE_Trade_Overlay.Controls
{
    public partial class FastBulkButton : UserControl
    {
        [Description("Header Text"), Category("Data")]
        private string _text = "";

        public string text
        {
            get { return this._text; }
            set
            {
                this._text = value;
                if (_text != string.Empty)
                {
                    Size size = TextRenderer.MeasureText(_text, this.Font, Size.Empty, TextFormatFlags.SingleLine);
                    size.Width += _text.Length / 2;
                    this.Size = new Size(155, 22);
                }
                
            }
        }

        public string tip
        {
            set
            {
                SetTip(value);
            }
        }

        private Image _image;

        public Image image
        {
            get { return _image; }
            set
            {
                _image = value;
                this.Size = new Size(40, 40);
            }
        }

        public string id; // Used to generate the bulkquery

        private bool isHovering = false;

        public bool isSelected;

        public bool isWanted;

        private bool _isHighlighted;

        public string itemName;

        public Color Border;
        public Color Border_Selected;
        public Color Selected;
        public Color Active;

        public Color ForeColor_Selected;
        public Color ForeColor_Hover;
        public Color BackColor_Selected;

        public bool isHighlighted
        {
            get { return _isHighlighted; }
            set
            {
                _isHighlighted = value;
                this.Refresh();
            }
        }
        

        public FastBulkButton()
        {
            InitializeComponent();

            this.MouseEnter += mouseEnter;
            this.MouseLeave += mouseLeave;
            this.MouseClick += mouseClick;
        }

        public void SetTip(string text)
        {
            ToolTip t = new ToolTip();
            t.AutomaticDelay = 0;
            t.InitialDelay = 0;
            t.ReshowDelay = 0;
            t.UseAnimation = false;
            t.UseFading = false;
            t.SetToolTip(this, text);
        }

        private void mouseClick(object sender, MouseEventArgs e)
        {
            isSelected = !isSelected;
            this.Refresh();
        }

        private void mouseEnter(object sender, EventArgs e)
        {
            isHovering = true;
            this.Refresh();
        }

        private void mouseLeave(object sender, EventArgs e)
        {
            isHovering = false;
            this.Refresh();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.Clear(BackColor);

            // Create pens for border
            Pen linePen = new Pen(new SolidBrush(Border));
            Pen linePenHighlighted = new Pen(new SolidBrush(Border_Selected));

            // Draw border
            if (isHighlighted)
                e.Graphics.DrawRectangle(linePenHighlighted, new Rectangle(0, 0, this.Width - 2, this.Height - 2));
            else
                e.Graphics.DrawRectangle(linePen, new Rectangle(0, 0, this.Width - 2, this.Height - 2));

            // Draw String box
            if (image == null)
            {
                if (this.isSelected)
                {
                    e.Graphics.FillRectangle(new SolidBrush(BackColor_Selected), new Rectangle(1, 1, this.Width - 3, this.Height - 3));
                    e.Graphics.DrawString(this.text, this.Font, new SolidBrush(ForeColor_Selected), new PointF(0, 3));
                }
                else
                    e.Graphics.DrawString(this.text, this.Font, new SolidBrush((this.isHovering) ? ForeColor_Hover : ForeColor), new PointF(0, 3));
            }
            else // Draw Image
            {
                if (this.isSelected)
                    e.Graphics.FillRectangle(new SolidBrush(BackColor_Selected), new Rectangle(1, 1, this.Width - 3, this.Height - 3));
                
                e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Bilinear;
                e.Graphics.DrawImage(image, 4, 4, 32, 32);
                
            }

            linePen.Dispose();
            linePenHighlighted.Dispose();
            e.Graphics.Dispose();
        }
    }
}