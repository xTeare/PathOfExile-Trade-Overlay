﻿namespace PoE_Trade_Overlay.Controls
{
    partial class SearchResult_Minimalistic
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_verified = new System.Windows.Forms.Label();
            this.flp = new System.Windows.Forms.FlowLayoutPanel();
            this.panel = new System.Windows.Forms.Panel();
            this.lbl_PiC = new System.Windows.Forms.Label();
            this.lbl_priceAmount = new System.Windows.Forms.Label();
            this.lbl_acc = new System.Windows.Forms.Label();
            this.lbl_ign = new System.Windows.Forms.Label();
            this.pb_price = new System.Windows.Forms.PictureBox();
            this.lbl_price = new System.Windows.Forms.Label();
            this.btn_whisper = new System.Windows.Forms.Button();
            this.lbl_accStatus = new System.Windows.Forms.Label();
            this.pb_itemArt = new PoE_Trade_Overlay.Controls.BetterPictureBox();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_price)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_itemArt)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_verified
            // 
            this.lbl_verified.Font = new System.Drawing.Font("Consolas", 7F);
            this.lbl_verified.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_verified.Location = new System.Drawing.Point(367, 5);
            this.lbl_verified.Name = "lbl_verified";
            this.lbl_verified.Size = new System.Drawing.Size(54, 15);
            this.lbl_verified.TabIndex = 11;
            this.lbl_verified.Text = "verified";
            this.lbl_verified.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flp
            // 
            this.flp.AutoSize = true;
            this.flp.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flp.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flp.Location = new System.Drawing.Point(100, 3);
            this.flp.MinimumSize = new System.Drawing.Size(490, 20);
            this.flp.Name = "flp";
            this.flp.Size = new System.Drawing.Size(490, 20);
            this.flp.TabIndex = 12;
            // 
            // panel
            // 
            this.panel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel.Controls.Add(this.lbl_verified);
            this.panel.Controls.Add(this.lbl_acc);
            this.panel.Controls.Add(this.lbl_ign);
            this.panel.Controls.Add(this.pb_price);
            this.panel.Controls.Add(this.lbl_price);
            this.panel.Controls.Add(this.btn_whisper);
            this.panel.Controls.Add(this.lbl_accStatus);
            this.panel.Controls.Add(this.lbl_PiC);
            this.panel.Controls.Add(this.lbl_priceAmount);
            this.panel.Location = new System.Drawing.Point(100, 94);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(490, 38);
            this.panel.TabIndex = 13;
            // 
            // lbl_PiC
            // 
            this.lbl_PiC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.lbl_PiC.Font = new System.Drawing.Font("Consolas", 8F);
            this.lbl_PiC.ForeColor = System.Drawing.Color.White;
            this.lbl_PiC.Location = new System.Drawing.Point(219, 4);
            this.lbl_PiC.Name = "lbl_PiC";
            this.lbl_PiC.Size = new System.Drawing.Size(142, 18);
            this.lbl_PiC.TabIndex = 19;
            this.lbl_PiC.Text = "PiC : 3000.231";
            this.lbl_PiC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl_priceAmount
            // 
            this.lbl_priceAmount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.lbl_priceAmount.Font = new System.Drawing.Font("Consolas", 10F);
            this.lbl_priceAmount.ForeColor = System.Drawing.Color.White;
            this.lbl_priceAmount.Location = new System.Drawing.Point(87, 2);
            this.lbl_priceAmount.Name = "lbl_priceAmount";
            this.lbl_priceAmount.Size = new System.Drawing.Size(155, 18);
            this.lbl_priceAmount.TabIndex = 18;
            this.lbl_priceAmount.Text = "x";
            this.lbl_priceAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_acc
            // 
            this.lbl_acc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_acc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.lbl_acc.Font = new System.Drawing.Font("Consolas", 8F);
            this.lbl_acc.ForeColor = System.Drawing.Color.White;
            this.lbl_acc.Location = new System.Drawing.Point(308, 23);
            this.lbl_acc.Name = "lbl_acc";
            this.lbl_acc.Size = new System.Drawing.Size(179, 13);
            this.lbl_acc.TabIndex = 17;
            this.lbl_acc.Text = "Acc.: Some very long account name";
            this.lbl_acc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl_ign
            // 
            this.lbl_ign.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.lbl_ign.Font = new System.Drawing.Font("Consolas", 8F);
            this.lbl_ign.ForeColor = System.Drawing.Color.White;
            this.lbl_ign.Location = new System.Drawing.Point(3, 23);
            this.lbl_ign.Name = "lbl_ign";
            this.lbl_ign.Size = new System.Drawing.Size(179, 13);
            this.lbl_ign.TabIndex = 16;
            this.lbl_ign.Text = "Acc.: Some very long account name";
            this.lbl_ign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pb_price
            // 
            this.pb_price.Location = new System.Drawing.Point(58, 0);
            this.pb_price.Name = "pb_price";
            this.pb_price.Size = new System.Drawing.Size(23, 23);
            this.pb_price.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_price.TabIndex = 14;
            this.pb_price.TabStop = false;
            // 
            // lbl_price
            // 
            this.lbl_price.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.lbl_price.Font = new System.Drawing.Font("Consolas", 8F);
            this.lbl_price.ForeColor = System.Drawing.Color.White;
            this.lbl_price.Location = new System.Drawing.Point(157, 7);
            this.lbl_price.Name = "lbl_price";
            this.lbl_price.Size = new System.Drawing.Size(85, 13);
            this.lbl_price.TabIndex = 15;
            this.lbl_price.Text = "(exact price)";
            this.lbl_price.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_whisper
            // 
            this.btn_whisper.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_whisper.FlatAppearance.BorderSize = 0;
            this.btn_whisper.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_whisper.Font = new System.Drawing.Font("Consolas", 8F);
            this.btn_whisper.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_whisper.Location = new System.Drawing.Point(427, 2);
            this.btn_whisper.Name = "btn_whisper";
            this.btn_whisper.Size = new System.Drawing.Size(60, 19);
            this.btn_whisper.TabIndex = 14;
            this.btn_whisper.Text = "whisper";
            this.btn_whisper.UseVisualStyleBackColor = false;
            this.btn_whisper.Click += new System.EventHandler(this.btn_whisper_Click);
            // 
            // lbl_accStatus
            // 
            this.lbl_accStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(150)))), ((int)(((byte)(48)))));
            this.lbl_accStatus.Font = new System.Drawing.Font("Consolas", 8F);
            this.lbl_accStatus.ForeColor = System.Drawing.Color.White;
            this.lbl_accStatus.Location = new System.Drawing.Point(3, 5);
            this.lbl_accStatus.Name = "lbl_accStatus";
            this.lbl_accStatus.Size = new System.Drawing.Size(49, 13);
            this.lbl_accStatus.TabIndex = 14;
            this.lbl_accStatus.Text = "online";
            this.lbl_accStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pb_itemArt
            // 
            this.pb_itemArt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.pb_itemArt.Location = new System.Drawing.Point(0, 0);
            this.pb_itemArt.Name = "pb_itemArt";
            this.pb_itemArt.Size = new System.Drawing.Size(94, 188);
            this.pb_itemArt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pb_itemArt.TabIndex = 2;
            this.pb_itemArt.TabStop = false;
            this.pb_itemArt.MouseEnter += new System.EventHandler(this.pEnter);
            this.pb_itemArt.MouseLeave += new System.EventHandler(this.pLeave);
            // 
            // SearchResult_Minimalistic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.Controls.Add(this.panel);
            this.Controls.Add(this.pb_itemArt);
            this.Controls.Add(this.flp);
            this.MinimumSize = new System.Drawing.Size(590, 0);
            this.Name = "SearchResult_Minimalistic";
            this.Size = new System.Drawing.Size(593, 191);
            this.panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb_price)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_itemArt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbl_verified;
        private System.Windows.Forms.FlowLayoutPanel flp;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Label lbl_accStatus;
        private System.Windows.Forms.Button btn_whisper;
        private System.Windows.Forms.Label lbl_price;
        private System.Windows.Forms.Label lbl_ign;
        private System.Windows.Forms.PictureBox pb_price;
        private System.Windows.Forms.Label lbl_priceAmount;
        private System.Windows.Forms.Label lbl_acc;
        private System.Windows.Forms.Label lbl_PiC;
        private BetterPictureBox pb_itemArt;
    }
}
