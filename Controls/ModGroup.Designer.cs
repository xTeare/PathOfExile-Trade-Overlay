﻿namespace PoE_Trade_Overlay.Controls
{
    partial class ModGroup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flp = new System.Windows.Forms.FlowLayoutPanel();
            this.lbl_typ = new System.Windows.Forms.Label();
            this.tb_min = new System.Windows.Forms.TextBox();
            this.tb_max = new System.Windows.Forms.TextBox();
            this.btn_addModGroup = new System.Windows.Forms.Button();
            this.btn_addMod = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // flp
            // 
            this.flp.AutoSize = true;
            this.flp.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flp.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flp.Location = new System.Drawing.Point(0, 30);
            this.flp.MinimumSize = new System.Drawing.Size(500, 25);
            this.flp.Name = "flp";
            this.flp.Size = new System.Drawing.Size(500, 25);
            this.flp.TabIndex = 0;
            // 
            // lbl_typ
            // 
            this.lbl_typ.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_typ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.lbl_typ.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_typ.Location = new System.Drawing.Point(0, 0);
            this.lbl_typ.Name = "lbl_typ";
            this.lbl_typ.Size = new System.Drawing.Size(514, 27);
            this.lbl_typ.TabIndex = 1;
            this.lbl_typ.Text = "Mod Group : and";
            this.lbl_typ.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tb_min
            // 
            this.tb_min.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.tb_min.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_min.Font = new System.Drawing.Font("Consolas", 9F);
            this.tb_min.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.tb_min.Location = new System.Drawing.Point(326, 6);
            this.tb_min.Multiline = true;
            this.tb_min.Name = "tb_min";
            this.tb_min.Size = new System.Drawing.Size(40, 16);
            this.tb_min.TabIndex = 2;
            this.tb_min.Text = "min";
            this.tb_min.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_min.TextChanged += new System.EventHandler(this.tb_min_TextChanged);
            // 
            // tb_max
            // 
            this.tb_max.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.tb_max.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_max.Font = new System.Drawing.Font("Consolas", 9F);
            this.tb_max.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.tb_max.Location = new System.Drawing.Point(372, 6);
            this.tb_max.Multiline = true;
            this.tb_max.Name = "tb_max";
            this.tb_max.Size = new System.Drawing.Size(40, 16);
            this.tb_max.TabIndex = 3;
            this.tb_max.Text = "max";
            this.tb_max.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_max.TextChanged += new System.EventHandler(this.tb_max_TextChanged);
            // 
            // btn_addModGroup
            // 
            this.btn_addModGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_addModGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.btn_addModGroup.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_addModGroup.FlatAppearance.BorderSize = 0;
            this.btn_addModGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_addModGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold);
            this.btn_addModGroup.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_addModGroup.Location = new System.Drawing.Point(490, 3);
            this.btn_addModGroup.Name = "btn_addModGroup";
            this.btn_addModGroup.Size = new System.Drawing.Size(21, 21);
            this.btn_addModGroup.TabIndex = 4;
            this.btn_addModGroup.Text = "X";
            this.btn_addModGroup.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_addModGroup.UseVisualStyleBackColor = false;
            this.btn_addModGroup.Click += new System.EventHandler(this.btn_addModGroup_Click);
            // 
            // btn_addMod
            // 
            this.btn_addMod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_addMod.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.btn_addMod.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_addMod.FlatAppearance.BorderSize = 0;
            this.btn_addMod.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_addMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.btn_addMod.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_addMod.Location = new System.Drawing.Point(418, 3);
            this.btn_addMod.Name = "btn_addMod";
            this.btn_addMod.Size = new System.Drawing.Size(66, 21);
            this.btn_addMod.TabIndex = 5;
            this.btn_addMod.Text = "Add Mod";
            this.btn_addMod.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_addMod.UseVisualStyleBackColor = false;
            this.btn_addMod.Click += new System.EventHandler(this.btn_addMod_Click);
            // 
            // ModGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.Controls.Add(this.btn_addMod);
            this.Controls.Add(this.btn_addModGroup);
            this.Controls.Add(this.tb_max);
            this.Controls.Add(this.tb_min);
            this.Controls.Add(this.lbl_typ);
            this.Controls.Add(this.flp);
            this.Font = new System.Drawing.Font("Consolas", 8.25F);
            this.MaximumSize = new System.Drawing.Size(514, 0);
            this.MinimumSize = new System.Drawing.Size(514, 65);
            this.Name = "ModGroup";
            this.Size = new System.Drawing.Size(514, 65);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flp;
        private System.Windows.Forms.Label lbl_typ;
        private System.Windows.Forms.TextBox tb_min;
        private System.Windows.Forms.TextBox tb_max;
        private System.Windows.Forms.Button btn_addModGroup;
        private System.Windows.Forms.Button btn_addMod;
    }
}
