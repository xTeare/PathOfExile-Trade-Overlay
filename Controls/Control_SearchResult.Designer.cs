﻿namespace PoE_Trade_Overlay.Controls
{
    partial class Control_SearchResult
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_itemName = new System.Windows.Forms.Label();
            this.flp_item = new System.Windows.Forms.FlowLayoutPanel();
            this.lbl_verified = new System.Windows.Forms.Label();
            this.lbl_online = new System.Windows.Forms.Label();
            this.panel_prices = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_priceInChaos = new System.Windows.Forms.Label();
            this.lbl_priceInC = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lbl_priceType = new System.Windows.Forms.Label();
            this.lbl_currencyType = new System.Windows.Forms.Label();
            this.panel_name = new System.Windows.Forms.Panel();
            this.lbl_lastChar = new System.Windows.Forms.Label();
            this.lbl_sellerName = new System.Windows.Forms.Label();
            this.btn_whisper = new System.Windows.Forms.Button();
            this.btn_wiki = new System.Windows.Forms.Button();
            this.btn_copyItem = new System.Windows.Forms.Button();
            this.pb_panelSeparator = new System.Windows.Forms.PictureBox();
            this.btn_visitHideout = new System.Windows.Forms.Button();
            this.pb_currency = new System.Windows.Forms.PictureBox();
            this.pb_header_right = new System.Windows.Forms.PictureBox();
            this.pb_header_left = new System.Windows.Forms.PictureBox();
            this.pb_itemArt = new PoE_Trade_Overlay.Controls.BetterPictureBox();
            this.pb_header_center = new System.Windows.Forms.PictureBox();
            this.panel_prices.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel_name.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_panelSeparator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_currency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_header_right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_header_left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_itemArt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_header_center)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_itemName
            // 
            this.lbl_itemName.BackColor = System.Drawing.Color.Transparent;
            this.lbl_itemName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.lbl_itemName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_itemName.Location = new System.Drawing.Point(328, 6);
            this.lbl_itemName.Name = "lbl_itemName";
            this.lbl_itemName.Size = new System.Drawing.Size(92, 26);
            this.lbl_itemName.TabIndex = 3;
            this.lbl_itemName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // flp_item
            // 
            this.flp_item.BackColor = System.Drawing.Color.Black;
            this.flp_item.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flp_item.Location = new System.Drawing.Point(103, 37);
            this.flp_item.Name = "flp_item";
            this.flp_item.Size = new System.Drawing.Size(476, 168);
            this.flp_item.TabIndex = 9;
            // 
            // lbl_verified
            // 
            this.lbl_verified.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_verified.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_verified.Location = new System.Drawing.Point(3, 190);
            this.lbl_verified.Name = "lbl_verified";
            this.lbl_verified.Size = new System.Drawing.Size(94, 15);
            this.lbl_verified.TabIndex = 10;
            this.lbl_verified.Text = "verified";
            this.lbl_verified.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_verified.Visible = false;
            // 
            // lbl_online
            // 
            this.lbl_online.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(150)))), ((int)(((byte)(48)))));
            this.lbl_online.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_online.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_online.Location = new System.Drawing.Point(3, 5);
            this.lbl_online.Name = "lbl_online";
            this.lbl_online.Size = new System.Drawing.Size(88, 20);
            this.lbl_online.TabIndex = 11;
            this.lbl_online.Text = "Online";
            this.lbl_online.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_prices
            // 
            this.panel_prices.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(12)))), ((int)(((byte)(12)))));
            this.panel_prices.Controls.Add(this.panel1);
            this.panel_prices.Controls.Add(this.panel3);
            this.panel_prices.Location = new System.Drawing.Point(97, 211);
            this.panel_prices.Name = "panel_prices";
            this.panel_prices.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.panel_prices.Size = new System.Drawing.Size(483, 30);
            this.panel_prices.TabIndex = 12;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.lbl_priceInChaos);
            this.panel1.Controls.Add(this.lbl_priceInC);
            this.panel1.Location = new System.Drawing.Point(295, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(188, 30);
            this.panel1.TabIndex = 19;
            // 
            // lbl_priceInChaos
            // 
            this.lbl_priceInChaos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_priceInChaos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_priceInChaos.Location = new System.Drawing.Point(0, 5);
            this.lbl_priceInChaos.Margin = new System.Windows.Forms.Padding(3);
            this.lbl_priceInChaos.Name = "lbl_priceInChaos";
            this.lbl_priceInChaos.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.lbl_priceInChaos.Size = new System.Drawing.Size(101, 20);
            this.lbl_priceInChaos.TabIndex = 17;
            this.lbl_priceInChaos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_priceInC
            // 
            this.lbl_priceInC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(12)))), ((int)(((byte)(12)))));
            this.lbl_priceInC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_priceInC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_priceInC.Location = new System.Drawing.Point(107, 5);
            this.lbl_priceInC.Margin = new System.Windows.Forms.Padding(3);
            this.lbl_priceInC.Name = "lbl_priceInC";
            this.lbl_priceInC.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.lbl_priceInC.Size = new System.Drawing.Size(81, 20);
            this.lbl_priceInC.TabIndex = 18;
            this.lbl_priceInC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lbl_priceType);
            this.panel3.Controls.Add(this.pb_currency);
            this.panel3.Controls.Add(this.lbl_currencyType);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(266, 30);
            this.panel3.TabIndex = 20;
            // 
            // lbl_priceType
            // 
            this.lbl_priceType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_priceType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_priceType.Location = new System.Drawing.Point(3, 5);
            this.lbl_priceType.Margin = new System.Windows.Forms.Padding(3);
            this.lbl_priceType.Name = "lbl_priceType";
            this.lbl_priceType.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.lbl_priceType.Size = new System.Drawing.Size(93, 20);
            this.lbl_priceType.TabIndex = 13;
            this.lbl_priceType.Text = "Asking Price:";
            this.lbl_priceType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_currencyType
            // 
            this.lbl_currencyType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_currencyType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_currencyType.Location = new System.Drawing.Point(132, 5);
            this.lbl_currencyType.Name = "lbl_currencyType";
            this.lbl_currencyType.Size = new System.Drawing.Size(131, 23);
            this.lbl_currencyType.TabIndex = 14;
            this.lbl_currencyType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_currencyType.Click += new System.EventHandler(this.lbl_currencyType_Click);
            this.lbl_currencyType.MouseEnter += new System.EventHandler(this.LabelEnter);
            this.lbl_currencyType.MouseLeave += new System.EventHandler(this.LabelLeave);
            // 
            // panel_name
            // 
            this.panel_name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(12)))), ((int)(((byte)(12)))));
            this.panel_name.Controls.Add(this.btn_visitHideout);
            this.panel_name.Controls.Add(this.lbl_lastChar);
            this.panel_name.Controls.Add(this.lbl_sellerName);
            this.panel_name.Controls.Add(this.lbl_online);
            this.panel_name.Controls.Add(this.btn_whisper);
            this.panel_name.Location = new System.Drawing.Point(97, 247);
            this.panel_name.Name = "panel_name";
            this.panel_name.Size = new System.Drawing.Size(483, 30);
            this.panel_name.TabIndex = 13;
            // 
            // lbl_lastChar
            // 
            this.lbl_lastChar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_lastChar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lastChar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_lastChar.Location = new System.Drawing.Point(259, 5);
            this.lbl_lastChar.Name = "lbl_lastChar";
            this.lbl_lastChar.Size = new System.Drawing.Size(127, 20);
            this.lbl_lastChar.TabIndex = 17;
            this.lbl_lastChar.Text = "verylongsellername";
            this.lbl_lastChar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbl_lastChar.Click += new System.EventHandler(this.lbl_lastChar_Click);
            this.lbl_lastChar.MouseEnter += new System.EventHandler(this.LabelEnter);
            this.lbl_lastChar.MouseLeave += new System.EventHandler(this.LabelLeave);
            // 
            // lbl_sellerName
            // 
            this.lbl_sellerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_sellerName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_sellerName.Location = new System.Drawing.Point(97, 5);
            this.lbl_sellerName.Name = "lbl_sellerName";
            this.lbl_sellerName.Size = new System.Drawing.Size(124, 20);
            this.lbl_sellerName.TabIndex = 16;
            this.lbl_sellerName.Text = "verylongsellername";
            this.lbl_sellerName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_sellerName.Click += new System.EventHandler(this.lbl_sellerName_Click);
            this.lbl_sellerName.MouseEnter += new System.EventHandler(this.LabelEnter);
            this.lbl_sellerName.MouseLeave += new System.EventHandler(this.LabelLeave);
            // 
            // btn_whisper
            // 
            this.btn_whisper.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_whisper.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.btn_whisper.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_whisper.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_whisper.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_whisper.Location = new System.Drawing.Point(392, 3);
            this.btn_whisper.Name = "btn_whisper";
            this.btn_whisper.Size = new System.Drawing.Size(88, 24);
            this.btn_whisper.TabIndex = 14;
            this.btn_whisper.Text = "Whisper";
            this.btn_whisper.UseVisualStyleBackColor = false;
            this.btn_whisper.Click += new System.EventHandler(this.btn_Whisper_Click);
            // 
            // btn_wiki
            // 
            this.btn_wiki.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.btn_wiki.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_wiki.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_wiki.FlatAppearance.BorderSize = 0;
            this.btn_wiki.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_wiki.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_wiki.Image = global::PoE_Trade_Overlay.Properties.Resources.wiki;
            this.btn_wiki.Location = new System.Drawing.Point(35, 208);
            this.btn_wiki.Name = "btn_wiki";
            this.btn_wiki.Size = new System.Drawing.Size(26, 26);
            this.btn_wiki.TabIndex = 19;
            this.btn_wiki.UseVisualStyleBackColor = false;
            this.btn_wiki.Visible = false;
            this.btn_wiki.Click += new System.EventHandler(this.btn_wiki_Click);
            // 
            // btn_copyItem
            // 
            this.btn_copyItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.btn_copyItem.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_copyItem.FlatAppearance.BorderSize = 0;
            this.btn_copyItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_copyItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_copyItem.Image = global::PoE_Trade_Overlay.Properties.Resources.copyitem;
            this.btn_copyItem.Location = new System.Drawing.Point(3, 208);
            this.btn_copyItem.Name = "btn_copyItem";
            this.btn_copyItem.Size = new System.Drawing.Size(26, 26);
            this.btn_copyItem.TabIndex = 18;
            this.btn_copyItem.UseVisualStyleBackColor = false;
            this.btn_copyItem.Click += new System.EventHandler(this.btn_copyItem_Click);
            // 
            // pb_panelSeparator
            // 
            this.pb_panelSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.pb_panelSeparator.Location = new System.Drawing.Point(97, 283);
            this.pb_panelSeparator.Name = "pb_panelSeparator";
            this.pb_panelSeparator.Size = new System.Drawing.Size(100, 3);
            this.pb_panelSeparator.TabIndex = 14;
            this.pb_panelSeparator.TabStop = false;
            // 
            // btn_visitHideout
            // 
            this.btn_visitHideout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(12)))), ((int)(((byte)(12)))));
            this.btn_visitHideout.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_visitHideout.FlatAppearance.BorderSize = 0;
            this.btn_visitHideout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_visitHideout.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_visitHideout.Image = global::PoE_Trade_Overlay.Properties.Resources.hideout;
            this.btn_visitHideout.Location = new System.Drawing.Point(227, 2);
            this.btn_visitHideout.Name = "btn_visitHideout";
            this.btn_visitHideout.Size = new System.Drawing.Size(26, 26);
            this.btn_visitHideout.TabIndex = 19;
            this.btn_visitHideout.UseVisualStyleBackColor = false;
            this.btn_visitHideout.Click += new System.EventHandler(this.btn_visitHideout_Click);
            // 
            // pb_currency
            // 
            this.pb_currency.Location = new System.Drawing.Point(102, 3);
            this.pb_currency.Name = "pb_currency";
            this.pb_currency.Size = new System.Drawing.Size(24, 24);
            this.pb_currency.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_currency.TabIndex = 15;
            this.pb_currency.TabStop = false;
            // 
            // pb_header_right
            // 
            this.pb_header_right.Image = global::PoE_Trade_Overlay.Properties.Resources.ItemsHeaderWhiteRight;
            this.pb_header_right.Location = new System.Drawing.Point(551, 3);
            this.pb_header_right.Name = "pb_header_right";
            this.pb_header_right.Size = new System.Drawing.Size(29, 34);
            this.pb_header_right.TabIndex = 6;
            this.pb_header_right.TabStop = false;
            // 
            // pb_header_left
            // 
            this.pb_header_left.Image = global::PoE_Trade_Overlay.Properties.Resources.ItemsHeaderWhiteLeft;
            this.pb_header_left.Location = new System.Drawing.Point(103, 3);
            this.pb_header_left.Name = "pb_header_left";
            this.pb_header_left.Size = new System.Drawing.Size(29, 34);
            this.pb_header_left.TabIndex = 4;
            this.pb_header_left.TabStop = false;
            // 
            // pb_itemArt
            // 
            this.pb_itemArt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.pb_itemArt.Location = new System.Drawing.Point(3, 2);
            this.pb_itemArt.Name = "pb_itemArt";
            this.pb_itemArt.Size = new System.Drawing.Size(94, 188);
            this.pb_itemArt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pb_itemArt.TabIndex = 1;
            this.pb_itemArt.TabStop = false;
            this.pb_itemArt.MouseEnter += new System.EventHandler(this.HideSockets);
            this.pb_itemArt.MouseLeave += new System.EventHandler(this.ShowSockets);
            // 
            // pb_header_center
            // 
            this.pb_header_center.BackgroundImage = global::PoE_Trade_Overlay.Properties.Resources.ItemsHeaderWhiteMiddle;
            this.pb_header_center.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_header_center.Location = new System.Drawing.Point(132, 3);
            this.pb_header_center.Name = "pb_header_center";
            this.pb_header_center.Size = new System.Drawing.Size(419, 34);
            this.pb_header_center.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_header_center.TabIndex = 5;
            this.pb_header_center.TabStop = false;
            // 
            // Control_SearchResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.Controls.Add(this.btn_wiki);
            this.Controls.Add(this.btn_copyItem);
            this.Controls.Add(this.pb_panelSeparator);
            this.Controls.Add(this.lbl_verified);
            this.Controls.Add(this.panel_name);
            this.Controls.Add(this.panel_prices);
            this.Controls.Add(this.pb_header_right);
            this.Controls.Add(this.pb_header_left);
            this.Controls.Add(this.lbl_itemName);
            this.Controls.Add(this.pb_itemArt);
            this.Controls.Add(this.pb_header_center);
            this.Controls.Add(this.flp_item);
            this.Name = "Control_SearchResult";
            this.Size = new System.Drawing.Size(590, 301);
            this.panel_prices.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel_name.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb_panelSeparator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_currency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_header_right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_header_left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_itemArt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_header_center)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbl_itemName;
        private System.Windows.Forms.PictureBox pb_header_left;
        private System.Windows.Forms.PictureBox pb_header_center;
        private System.Windows.Forms.PictureBox pb_header_right;
        private System.Windows.Forms.FlowLayoutPanel flp_item;
        private System.Windows.Forms.Label lbl_verified;
        private System.Windows.Forms.Label lbl_online;
        private System.Windows.Forms.Panel panel_prices;
        private System.Windows.Forms.PictureBox pb_currency;
        private System.Windows.Forms.Label lbl_priceType;
        private System.Windows.Forms.Label lbl_currencyType;
        private System.Windows.Forms.Panel panel_name;
        private System.Windows.Forms.Button btn_whisper;
        private System.Windows.Forms.Label lbl_sellerName;
        private System.Windows.Forms.Label lbl_priceInChaos;
        private System.Windows.Forms.Label lbl_priceInC;
        public BetterPictureBox pb_itemArt;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pb_panelSeparator;
        private System.Windows.Forms.Label lbl_lastChar;
        private System.Windows.Forms.Button btn_copyItem;
        private System.Windows.Forms.Button btn_visitHideout;
        private System.Windows.Forms.Button btn_wiki;
    }
}
