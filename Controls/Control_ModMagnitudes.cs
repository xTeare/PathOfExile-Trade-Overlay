﻿using System.Windows.Forms;

namespace PoE_Trade_Overlay.Controls
{
    public partial class Control_ModMagnitudes : UserControl
    {
        public Control_ModMagnitudes()
        {
            InitializeComponent();
        }

        public void SetTier(string tier)
        {
            lbl_modTier.Text = tier;
        }

        public void SetMag(string mag)
        {
            lbl_modMagnitudes.Text = mag;
        }
    }
}