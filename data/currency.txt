﻿alt:Orb of Alteration:web.poecdn.com/image/Art/2DItems/Currency/CurrencyRerollMagic.png?v=6d9520174f6643e502da336e76b730d3&scaleIndex=0
fuse:Orb of Fusing:web.poecdn.com/image/Art/2DItems/Currency/CurrencyRerollSocketLinks.png?v=0ad7134a62e5c45e4f8bc8a44b95540f&scaleIndex=0
alch:Orb of Alchemy:web.poecdn.com/image/Art/2DItems/Currency/CurrencyUpgradeToRare.png?v=89c110be97333995522c7b2c29cae728&scaleIndex=0
chaos:Chaos Orb:web.poecdn.com/image/Art/2DItems/Currency/CurrencyRerollRare.png?v=c60aa876dd6bab31174df91b1da1b4f9&scaleIndex=0
gcp:Gemcutter's Prism:web.poecdn.com/image/Art/2DItems/Currency/CurrencyGemQuality.png?v=f11792b6dbd2f5f869351151bc3a4539&scaleIndex=0
exa:Exalted Orb:web.poecdn.com/image/Art/2DItems/Currency/CurrencyAddModToRare.png?v=1745ebafbd533b6f91bccf588ab5efc5&scaleIndex=0
chrom:Chromatic Orb:web.poecdn.com/image/Art/2DItems/Currency/CurrencyRerollSocketColours.png?v=9d377f2cf04a16a39aac7b14abc9d7c3&scaleIndex=0
jew:Jeweller's Orb:web.poecdn.com/image/Art/2DItems/Currency/CurrencyRerollSocketNumbers.png?v=2946b0825af70f796b8f15051d75164d&scaleIndex=0
engineers-orb:Engineer's Orb:web.poecdn.com/image/Art/2DItems/Currency/EngineersOrb.png?v=4b291fd7d6b9022a2d35b34c43c08e87&scaleIndex=0
chance:Orb of Chance:web.poecdn.com/image/Art/2DItems/Currency/CurrencyUpgradeRandomly.png?v=e4049939b9cd61291562f94364ee0f00&scaleIndex=0
chisel:Cartographer's Chisel:web.poecdn.com/image/Art/2DItems/Currency/CurrencyMapQuality.png?v=f46e0a1af7223e2d4cae52bc3f9f7a1f&scaleIndex=0
scour:Orb of Scouring:web.poecdn.com/image/Art/2DItems/Currency/CurrencyConvertToNormal.png?v=15e3ef97f04a39ae284359309697ef7d&scaleIndex=0
blessed:Blessed Orb:web.poecdn.com/image/Art/2DItems/Currency/CurrencyImplicitMod.png?v=472eeef04846d8a25d65b3d4f9ceecc8&scaleIndex=0
regret:Orb of Regret:web.poecdn.com/image/Art/2DItems/Currency/CurrencyPassiveSkillRefund.png?v=1de687952ce56385b74ac450f97fcc33&scaleIndex=0
regal:Regal Orb:web.poecdn.com/image/Art/2DItems/Currency/CurrencyUpgradeMagicToRare.png?v=1187a8511b47b35815bd75698de1fa2a&scaleIndex=0
divine:Divine Orb:web.poecdn.com/image/Art/2DItems/Currency/CurrencyModValues.png?v=0ad99d4a2b0356a60fa8194910d80f6b&scaleIndex=0
vaal:Vaal Orb:web.poecdn.com/image/Art/2DItems/Currency/CurrencyVaal.png?v=64114709d67069cd665f8f1a918cd12a&scaleIndex=0
orb-of-annulment:Orb of Annulment:web.poecdn.com/image/Art/2DItems/Currency/AnnullOrb.png?v=f9a0f8b21515c8abf517e9648cfc7455&scaleIndex=0
orb-of-binding:Orb of Binding:web.poecdn.com/image/Art/2DItems/Currency/BindingOrb.png?v=6ee0528156592a01c2931262d024f842&scaleIndex=0
ancient-orb:Ancient Orb:web.poecdn.com/image/Art/2DItems/Currency/AncientOrb.png?v=3edb14b53b9b05e176124814aba86f94&scaleIndex=0
orb-of-horizons:Orb of Horizons:web.poecdn.com/image/Art/2DItems/Currency/HorizonOrb.png?v=f3b3343dc61c60e667003bbdbbdb2374&scaleIndex=0
harbingers-orb:Harbinger's Orb:web.poecdn.com/image/Art/2DItems/Currency/HarbingerOrb.png?v=a61bf3add692a2fe74bb5f39213f4d93&scaleIndex=0
wis:Scroll of Wisdom:web.poecdn.com/image/Art/2DItems/Currency/CurrencyIdentification.png?v=1b9b38c45be95c59d8900f91b2afd58b&scaleIndex=0
port:Portal Scroll:web.poecdn.com/image/Art/2DItems/Currency/CurrencyPortal.png?v=728696ea10d4fb1e789039debc5d8c3c&scaleIndex=0
scr:Armourer's Scrap:web.poecdn.com/image/Art/2DItems/Currency/CurrencyArmourQuality.png?v=251e204e4ec325f75ce8ef75b2dfbeb8&scaleIndex=0
whe:Blacksmith's Whetstone:web.poecdn.com/image/Art/2DItems/Currency/CurrencyWeaponQuality.png?v=d2ce9167e23a74cef5d8465433e86482&scaleIndex=0
ba:Glassblower's Bauble:web.poecdn.com/image/Art/2DItems/Currency/CurrencyFlaskQuality.png?v=ca8bd0dd43d2adf8b021578a398eb9de&scaleIndex=0
tra:Orb of Transmutation:web.poecdn.com/image/Art/2DItems/Currency/CurrencyUpgradeToMagic.png?v=333b8b5e28b73c62972fc66e7634c5c8&scaleIndex=0
aug:Orb of Augmentation:web.poecdn.com/image/Art/2DItems/Currency/CurrencyAddModToMagic.png?v=97e63b85807f2419f4208482fd0b4859&scaleIndex=0
mir:Mirror of Kalandra:web.poecdn.com/image/Art/2DItems/Currency/CurrencyDuplicate.png?v=6fd68c1a5c4292c05b97770e83aa22bc&scaleIndex=0
ete:Eternal Orb:web.poecdn.com/image/Art/2DItems/Currency/CurrencyImprintOrb.png?v=0483ded9ac1f08c320fc21d5ddc208c0&scaleIndex=0
p:Perandus Coin:web.poecdn.com/image/Art/2DItems/Currency/CurrencyCoin.png?v=b971d7d9ea1ad32f16cce8ee99c897cf&scaleIndex=0
silver:Silver Coin:web.poecdn.com/image/Art/2DItems/Currency/SilverObol.png?v=93c1b204ec2736a2fe5aabbb99510bcf&scaleIndex=0
annulment-shard:Annulment Shard:web.poecdn.com/image/Art/2DItems/Currency/AnnullShard.png?v=502496c41d3da750bcecbb194d0aa3f4&scaleIndex=0
mirror-shard:Mirror Shard:web.poecdn.com/image/Art/2DItems/Currency/MirrorShard.png?v=b5b677eece4ae7e8450452e3944f121d&scaleIndex=0
exalted-shard:Exalted Shard:web.poecdn.com/image/Art/2DItems/Currency/ExaltedShard.png?v=4945fe24b79868be79870f8ad01f20b0&scaleIndex=0
binding-shard:Binding Shard:web.poecdn.com/image/Art/2DItems/Currency/BindingShard.png?v=b00f27dee8ad94d7f187133a09b872f9&scaleIndex=0
horizon-shard:Horizon Shard:web.poecdn.com/image/Art/2DItems/Currency/HorizonShard.png?v=6d0e5b4c1bbaeb06c8cd10c269ad8121&scaleIndex=0
harbingers-shard:Harbinger's Shard:web.poecdn.com/image/Art/2DItems/Currency/HarbingerShard.png?v=2274561da4960ca41bb6742952edc74d&scaleIndex=0
engineers-shard:Engineer's Shard:web.poecdn.com/image/Art/2DItems/Currency/EngineersShard.png?v=8b69b76b1f42120f72cf7febb76b6a7a&scaleIndex=0
ancient-shard:Ancient Shard:web.poecdn.com/image/Art/2DItems/Currency/AncientShard.png?v=e1eb08f64822576bd6d47029cd72d1a9&scaleIndex=0
chaos-shard:Chaos Shard:web.poecdn.com/image/Art/2DItems/Currency/ChaosShard.png?v=c206269aeda3a6a7b5a8ac110045afca&scaleIndex=0
regal-shard:Regal Shard:web.poecdn.com/image/Art/2DItems/Currency/RegalShard.png?v=e10a1590d421e4305d8a19ff8c4d9948&scaleIndex=0
apprentice-sextant:Apprentice Cartographer's Sextant:web.poecdn.com/image/Art/2DItems/Currency/AtlasRadiusWhite.png?v=f6512c12b7ee4fc9d4fbaf51d43b31f6&scaleIndex=0
journeyman-sextant:Journeyman Cartographer's Sextant:web.poecdn.com/image/Art/2DItems/Currency/AtlasRadiusYellow.png?v=924e154198125996497d01d1effb28a7&scaleIndex=0
master-sextant:Master Cartographer's Sextant:web.poecdn.com/image/Art/2DItems/Currency/AtlasRadiusRed.png?v=ebadcbe85c704dba334a65389fc31d2a&scaleIndex=0
blessing-xoph:Blessing of Xoph:web.poecdn.com/image/Art/2DItems/Currency/Breach/BreachUpgraderFire.png?v=6c7efd3a8fc9150d4b293d5a5917fd04&scaleIndex=0
blessing-tul:Blessing of Tul:web.poecdn.com/image/Art/2DItems/Currency/Breach/BreachUpgraderCold.png?v=c25b6df9be1fd051d99616e4bb78066e&scaleIndex=0
blessing-esh:Blessing of Esh:web.poecdn.com/image/Art/2DItems/Currency/Breach/BreachUpgraderLightning.png?v=ed7bb0e73f5ca907ef62592e48d442dc&scaleIndex=0
blessing-uul-netol:Blessing of Uul-Netol:web.poecdn.com/image/Art/2DItems/Currency/Breach/BreachUpgraderPhysical.png?v=cf3d245db6b6bb796112a952d74543e0&scaleIndex=0
blessing-chayula:Blessing of Chayula:web.poecdn.com/image/Art/2DItems/Currency/Breach/BreachUpgraderChaos.png?v=841360bbd5f81619761a0d16f8c36793&scaleIndex=0
splinter-xoph:Splinter of Xoph:web.poecdn.com/image/Art/2DItems/Currency/Breach/BreachShardFire.png?v=4635e0847323cf1d62c8b4e8101351bf&scaleIndex=0
splinter-tul:Splinter of Tul:web.poecdn.com/image/Art/2DItems/Currency/Breach/BreachShardCold.png?v=4a7652d6ff5de2493d37768e993c9411&scaleIndex=0
splinter-esh:Splinter of Esh:web.poecdn.com/image/Art/2DItems/Currency/Breach/BreachShardLightning.png?v=11cd23560f7aa0dda27cc323fa97cb96&scaleIndex=0
splinter-uul:Splinter of Uul-Netol:web.poecdn.com/image/Art/2DItems/Currency/Breach/BreachShardPhysical.png?v=25f31f4a5e1ba4540cb7bfa03b82c1e8&scaleIndex=0
splinter-chayula:Splinter of Chayula:web.poecdn.com/image/Art/2DItems/Currency/Breach/BreachShardChaos.png?v=2c18cbe5384375bbe4643cd8b83ea32d&scaleIndex=0
dusk:Sacrifice at Dusk:web.poecdn.com/image/Art/2DItems/Maps/Vaal04.png?v=72207b7e72a4ed4fafd3e72f7b0b2a93&scaleIndex=0
mid:Sacrifice at Midnight:web.poecdn.com/image/Art/2DItems/Maps/Vaal01.png?v=3b21ce0cd4c0b9e8cf5db6257daf831a&scaleIndex=0
dawn:Sacrifice at Dawn:web.poecdn.com/image/Art/2DItems/Maps/Vaal02.png?v=3ead6455599ec6c303f54ba98d6f8eb2&scaleIndex=0
noon:Sacrifice at Noon:web.poecdn.com/image/Art/2DItems/Maps/Vaal03.png?v=ba374d543316349b87de121039c3cc6f&scaleIndex=0
grie:Mortal Grief:web.poecdn.com/image/Art/2DItems/Maps/UberVaal04.png?v=735839ceae0fc45d15ec69555e931413&scaleIndex=0
rage:Mortal Rage:web.poecdn.com/image/Art/2DItems/Maps/UberVaal01.png?v=9336df5d7d0befd5963b71e7a68479ce&scaleIndex=0
hope:Mortal Hope:web.poecdn.com/image/Art/2DItems/Maps/UberVaal02.png?v=db5b529a8425bd2b9fd7bee9fca2e018&scaleIndex=0
ign:Mortal Ignorance:web.poecdn.com/image/Art/2DItems/Maps/UberVaal03.png?v=9fb218dad337a4627a59f74bfa2d6c86&scaleIndex=0
eber:Eber's Key:web.poecdn.com/image/Art/2DItems/Maps/PaleCourt01.png?v=044cbdae1e06e621585eaa627c2162db&scaleIndex=0
yriel:Yriel's Key:web.poecdn.com/image/Art/2DItems/Maps/PaleCourt02.png?v=757829336b7239c4b1e398c203f0cca0&scaleIndex=0
inya:Inya's Key:web.poecdn.com/image/Art/2DItems/Maps/PaleCourt03.png?v=6e43b636847d46b560ef0518869a7294&scaleIndex=0
volkuur:Volkuur's Key:web.poecdn.com/image/Art/2DItems/Maps/PaleCourt04.png?v=5c3afc6bad631a50f9fe5ccb570aeb36&scaleIndex=0
hydra:Fragment of the Hydra:web.poecdn.com/image/Art/2DItems/Maps/AtlasMaps/FragmentHydra.png?v=fd37e4be7672c0db8b549a1b16ad489d&scaleIndex=0
phoenix:Fragment of the Phoenix:web.poecdn.com/image/Art/2DItems/Maps/AtlasMaps/FragmentPhoenix.png?v=8f76720ab06bb40a6d6f75730f92e4a7&scaleIndex=0
minot:Fragment of the Minotaur:web.poecdn.com/image/Art/2DItems/Maps/AtlasMaps/FragmentMinotaur.png?v=e0e3f5e7daf32736d63fc3df1ba98122&scaleIndex=0
chimer:Fragment of the Chimera:web.poecdn.com/image/Art/2DItems/Maps/AtlasMaps/FragmentChimera.png?v=15bd6ba80e1853c22ae3acf40abf6428&scaleIndex=0
sacrifice-set:Sacrifice Set:web.poecdn.com/image/Art/2DItems/Maps/VaalComplete.png?v=220c72c1470274a14f24b3c77e2272fd&scaleIndex=0
mortal-set:Mortal Set:web.poecdn.com/image/Art/2DItems/Maps/UberVaalComplete.png?v=abcf6327fdc6ff5a740ad8203fe2b9e7&scaleIndex=0
pale-court-set:Pale Court Set:web.poecdn.com/image/Art/2DItems/Maps/PaleCourtComplete.png?v=becaec4bfa49e2c8c6512557e7c20de5&scaleIndex=0
shaper-set:Shaper Set:web.poecdn.com/image/Art/2DItems/Maps/ShaperComplete.png?v=1d7beb22351d0872d188d6682d694222&scaleIndex=0
xophs-breachstone:Xoph's Breachstone:web.poecdn.com/image/Art/2DItems/Currency/Breach/BreachFragmentsFire.png?v=f9c679d9bd76b1b6c40f2a352b8bbea5&scaleIndex=0
tuls-breachstone:Tul's Breachstone:web.poecdn.com/image/Art/2DItems/Currency/Breach/BreachFragmentsCold.png?v=8698bfa03dcc9fca7fdde5c763839bd0&scaleIndex=0
eshs-breachstone:Esh's Breachstone:web.poecdn.com/image/Art/2DItems/Currency/Breach/BreachFragmentsLightning.png?v=01c1ec0220d90a59ebdbb1847a915710&scaleIndex=0
uul-breachstone:Uul-Netol's Breachstone:web.poecdn.com/image/Art/2DItems/Currency/Breach/BreachFragmentsPhysical.png?v=d3df204b32e1891bb1f6fb0d2376480d&scaleIndex=0
chayulas-breachstone:Chayula's Breachstone:web.poecdn.com/image/Art/2DItems/Currency/Breach/BreachFragmentsChaos.png?v=fabeba4157753d08666137bb482ce18c&scaleIndex=0
offer:Offering to the Goddess:web.poecdn.com/image/Art/2DItems/Maps/Labyrinth.png?v=ef005aef5d2f9135d6922f4b1b912f78&scaleIndex=0
divine-vessel:Divine Vessel:web.poecdn.com/image/Art/2DItems/Maps/SinFlaskEmpty.png?v=8b9f566d35bc00387e43f8ec4eefad31&scaleIndex=0
ancient-reliquary-key:Ancient Reliquary Key:web.poecdn.com/image/Art/2DItems/Maps/VaultMap.png?v=cb50511b7087323b10a19559bfb2be29&scaleIndex=0
vial-of-matatl:Vial of Dominance:web.poecdn.com/image/Art/2DItems/Currency/VialArchitectsHand.png?v=7511533af9f959a8bdab903c0061c75d&scaleIndex=0
vial-of-guatelitzi:Vial of Summoning:web.poecdn.com/image/Art/2DItems/Currency/VialSpiritDrinker.png?v=6f16787ae91b38c5061b26d85423aa3b&scaleIndex=0
vial-of-tacati:Vial of Awakening:web.poecdn.com/image/Art/2DItems/Currency/VialSlumber.png?v=f56a6a0af2b60ea077f28d585fef46b0&scaleIndex=0
vial-of-xopec:Vial of the Ritual:web.poecdn.com/image/Art/2DItems/Currency/VialDanceOffered.png?v=b59a893b49b6e4d383bcbe207c807ec3&scaleIndex=0
vial-of-puhuarte:Vial of Fate:web.poecdn.com/image/Art/2DItems/Currency/VialStoryVaal.png?v=06d1c6c589dfb7c574097654ecebac1d&scaleIndex=0
vial-of-citaqualotl:Vial of Consequence:web.poecdn.com/image/Art/2DItems/Currency/VialCowardsChains.png?v=d4db2a42dab6d3100560db88cc156762&scaleIndex=0
vial-of-souls:Vial of the Ghost:web.poecdn.com/image/Art/2DItems/Currency/VialSoulCatcher.png?v=dd5e81208051ef78212c78ab9d333cfe&scaleIndex=0
vial-of-trancendence:Vial of Transcendence:web.poecdn.com/image/Art/2DItems/Currency/VialTemperedFlesh.png?v=d9b4e2f84c2a562b1b3a553823c813c2&scaleIndex=0
vial-of-the-heart:Vial of Sacrifice:web.poecdn.com/image/Art/2DItems/Currency/VialSacrificialHeart.png?v=ae0d26a590b235ecbd5677349085f182&scaleIndex=0
whispering-essence-of-hatred:Whispering Essence of Hatred:web.poecdn.com/image/Art/2DItems/Currency/Essence/Hatred1.png?v=af1a86ff4108cc78b67a6b7199b8959f&scaleIndex=0
muttering-essence-of-hatred:Muttering Essence of Hatred:web.poecdn.com/image/Art/2DItems/Currency/Essence/Hatred2.png?v=c4d6f1c773d20697ab194039c6734f36&scaleIndex=0
weeping-essence-of-hatred:Weeping Essence of Hatred:web.poecdn.com/image/Art/2DItems/Currency/Essence/Hatred3.png?v=7ce462d82e43b67ce7103cb8af695057&scaleIndex=0
wailing-essence-of-hatred:Wailing Essence of Hatred:web.poecdn.com/image/Art/2DItems/Currency/Essence/Hatred4.png?v=91177a1b7ab8a636d0028718227f1f84&scaleIndex=0
screaming-essence-of-hatred:Screaming Essence of Hatred:web.poecdn.com/image/Art/2DItems/Currency/Essence/Hatred5.png?v=c3346cf8ca025bd89d8efe5604d322ab&scaleIndex=0
shrieking-essence-of-hatred:Shrieking Essence of Hatred:web.poecdn.com/image/Art/2DItems/Currency/Essence/Hatred6.png?v=7e73d6da1f6a165ebebf35dcd45638b2&scaleIndex=0
deafening-essence-of-hatred:Deafening Essence of Hatred:web.poecdn.com/image/Art/2DItems/Currency/Essence/Hatred7.png?v=484492362fc2f7e4a5fb02bd37cf621f&scaleIndex=0
whispering-essence-of-woe:Whispering Essence of Woe:web.poecdn.com/image/Art/2DItems/Currency/Essence/Woe1.png?v=c2091e476989643b8eb31d5f7ee20f71&scaleIndex=0
muttering-essence-of-woe:Muttering Essence of Woe:web.poecdn.com/image/Art/2DItems/Currency/Essence/Woe2.png?v=4214744a550dc61157b8d7c7becf9cbd&scaleIndex=0
weeping-essence-of-woe:Weeping Essence of Woe:web.poecdn.com/image/Art/2DItems/Currency/Essence/Woe3.png?v=e55316dbaaad843c3f96c0fc6b13fc72&scaleIndex=0
wailing-essence-of-woe:Wailing Essence of Woe:web.poecdn.com/image/Art/2DItems/Currency/Essence/Woe4.png?v=26d83392a96b8947d6f9741849619711&scaleIndex=0
screaming-essence-of-woe:Screaming Essence of Woe:web.poecdn.com/image/Art/2DItems/Currency/Essence/Woe5.png?v=719bf2e52c17d688e02fa3de0a7e4bf7&scaleIndex=0
shrieking-essence-of-woe:Shrieking Essence of Woe:web.poecdn.com/image/Art/2DItems/Currency/Essence/Woe6.png?v=c159ac8d2eede3c497617c262ee4e38d&scaleIndex=0
deafening-essence-of-woe:Deafening Essence of Woe:web.poecdn.com/image/Art/2DItems/Currency/Essence/Woe7.png?v=cf2014f1803f43b0cbbe21b99f03c6dd&scaleIndex=0
whispering-essence-of-greed:Whispering Essence of Greed:web.poecdn.com/image/Art/2DItems/Currency/Essence/Greed1.png?v=aa5003ecc7f4899317f84cdf7c4ee05f&scaleIndex=0
muttering-essence-of-greed:Muttering Essence of Greed:web.poecdn.com/image/Art/2DItems/Currency/Essence/Greed2.png?v=2c3f8a3a5a1454ca0883d4c17109b617&scaleIndex=0
weeping-essence-of-greed:Weeping Essence of Greed:web.poecdn.com/image/Art/2DItems/Currency/Essence/Greed3.png?v=a673402a48dd5d1a6509515f5e249dab&scaleIndex=0
wailing-essence-of-greed:Wailing Essence of Greed:web.poecdn.com/image/Art/2DItems/Currency/Essence/Greed4.png?v=15c28237a0ef6e7eb22c09bbf03a7b6a&scaleIndex=0
screaming-essence-of-greed:Screaming Essence of Greed:web.poecdn.com/image/Art/2DItems/Currency/Essence/Greed5.png?v=f2cac5eb3527123b27ef94c99edbe3a2&scaleIndex=0
shrieking-essence-of-greed:Shrieking Essence of Greed:web.poecdn.com/image/Art/2DItems/Currency/Essence/Greed6.png?v=cbd2fb41eb4648a1b47915bab39b130b&scaleIndex=0
deafening-essence-of-greed:Deafening Essence of Greed:web.poecdn.com/image/Art/2DItems/Currency/Essence/Greed7.png?v=8283aa91c31cd8759b88b7b22b8f9669&scaleIndex=0
whispering-essence-of-contempt:Whispering Essence of Contempt:web.poecdn.com/image/Art/2DItems/Currency/Essence/Contempt1.png?v=60f8830c2dadd217eb99cee02425f07d&scaleIndex=0
muttering-essence-of-contempt:Muttering Essence of Contempt:web.poecdn.com/image/Art/2DItems/Currency/Essence/Contempt2.png?v=54a248bdc119c25b458deaf1e4867821&scaleIndex=0
weeping-essence-of-contempt:Weeping Essence of Contempt:web.poecdn.com/image/Art/2DItems/Currency/Essence/Contempt3.png?v=d3673e9f9027fc270a8950d2998b1f6c&scaleIndex=0
wailing-essence-of-contempt:Wailing Essence of Contempt:web.poecdn.com/image/Art/2DItems/Currency/Essence/Contempt4.png?v=31184aca892c0697a53adc523df41a71&scaleIndex=0
screaming-essence-of-contempt:Screaming Essence of Contempt:web.poecdn.com/image/Art/2DItems/Currency/Essence/Contempt5.png?v=b0890fd15d90c31d77f88b13d488dfe2&scaleIndex=0
shrieking-essence-of-contempt:Shrieking Essence of Contempt:web.poecdn.com/image/Art/2DItems/Currency/Essence/Contempt6.png?v=60a9720e9ce66690a45e2ae48143d7df&scaleIndex=0
deafening-essence-of-contempt:Deafening Essence of Contempt:web.poecdn.com/image/Art/2DItems/Currency/Essence/Contempt7.png?v=b8ac0a6d9789465eff413f5c7a74aca8&scaleIndex=0
muttering-essence-of-sorrow:Muttering Essence of Sorrow:web.poecdn.com/image/Art/2DItems/Currency/Essence/Sorrow2.png?v=07f487552536d08952250cb242dcd3ce&scaleIndex=0
weeping-essence-of-sorrow:Weeping Essence of Sorrow:web.poecdn.com/image/Art/2DItems/Currency/Essence/Sorrow3.png?v=3bb862824c36ee034a3a727a311432f8&scaleIndex=0
wailing-essence-of-sorrow:Wailing Essence of Sorrow:web.poecdn.com/image/Art/2DItems/Currency/Essence/Sorrow4.png?v=f5512f122d9c273e57c3eb4e1aa72fec&scaleIndex=0
screaming-essence-of-sorrow:Screaming Essence of Sorrow:web.poecdn.com/image/Art/2DItems/Currency/Essence/Sorrow5.png?v=c7e6a975cf955ee33a5c25558951643d&scaleIndex=0
shrieking-essence-of-sorrow:Shrieking Essence of Sorrow:web.poecdn.com/image/Art/2DItems/Currency/Essence/Sorrow6.png?v=6b6d08a252511245324c84eec5b7485a&scaleIndex=0
deafening-essence-of-sorrow:Deafening Essence of Sorrow:web.poecdn.com/image/Art/2DItems/Currency/Essence/Sorrow7.png?v=43e1fe2b224ece09268083a2c1915c3a&scaleIndex=0
muttering-essence-of-anger:Muttering Essence of Anger:web.poecdn.com/image/Art/2DItems/Currency/Essence/Anger2.png?v=56963b86f3de434e89b427190f3c6466&scaleIndex=0
weeping-essence-of-anger:Weeping Essence of Anger:web.poecdn.com/image/Art/2DItems/Currency/Essence/Anger3.png?v=2386a07a358a9949b4f925f7b544c1d3&scaleIndex=0
wailing-essence-of-anger:Wailing Essence of Anger:web.poecdn.com/image/Art/2DItems/Currency/Essence/Anger4.png?v=35c3aaa66bb84caf183a372d38a5a07c&scaleIndex=0
screaming-essence-of-anger:Screaming Essence of Anger:web.poecdn.com/image/Art/2DItems/Currency/Essence/Anger5.png?v=6a1a5786842f93c1decfd5448ec58ea0&scaleIndex=0
shrieking-essence-of-anger:Shrieking Essence of Anger:web.poecdn.com/image/Art/2DItems/Currency/Essence/Anger6.png?v=0f4a09afd0f372cca4229e687a809558&scaleIndex=0
deafening-essence-of-anger:Deafening Essence of Anger:web.poecdn.com/image/Art/2DItems/Currency/Essence/Anger7.png?v=4ca411583ed3cd3b75a4e5ac57b0efda&scaleIndex=0
muttering-essence-of-torment:Muttering Essence of Torment:web.poecdn.com/image/Art/2DItems/Currency/Essence/Torment2.png?v=e0b2310ca8180b5887485f7a2d3558ea&scaleIndex=0
weeping-essence-of-torment:Weeping Essence of Torment:web.poecdn.com/image/Art/2DItems/Currency/Essence/Torment3.png?v=df38adcd8dfae9dc7ce43f8902aec8f3&scaleIndex=0
wailing-essence-of-torment:Wailing Essence of Torment:web.poecdn.com/image/Art/2DItems/Currency/Essence/Torment4.png?v=a9d3fedc6e36286fbfc6bf32763db2da&scaleIndex=0
screaming-essence-of-torment:Screaming Essence of Torment:web.poecdn.com/image/Art/2DItems/Currency/Essence/Torment5.png?v=e771c7292c0e1885e657b0c84f9fe7e6&scaleIndex=0
shrieking-essence-of-torment:Shrieking Essence of Torment:web.poecdn.com/image/Art/2DItems/Currency/Essence/Torment6.png?v=42d9e6702a3ad8ae56fc23fdcf3a4c15&scaleIndex=0
deafening-essence-of-torment:Deafening Essence of Torment:web.poecdn.com/image/Art/2DItems/Currency/Essence/Torment7.png?v=fea8e2d8ccb19124d227f0baaf810039&scaleIndex=0
muttering-essence-of-fear:Muttering Essence of Fear:web.poecdn.com/image/Art/2DItems/Currency/Essence/Fear2.png?v=b87e37101af11df191863915c951c2de&scaleIndex=0
weeping-essence-of-fear:Weeping Essence of Fear:web.poecdn.com/image/Art/2DItems/Currency/Essence/Fear3.png?v=89cdd73a4de987ebc1bd4136507f7576&scaleIndex=0
wailing-essence-of-fear:Wailing Essence of Fear:web.poecdn.com/image/Art/2DItems/Currency/Essence/Fear4.png?v=19d61d301f7c14a3e26e4604b3273af5&scaleIndex=0
screaming-essence-of-fear:Screaming Essence of Fear:web.poecdn.com/image/Art/2DItems/Currency/Essence/Fear5.png?v=98666642411b25e17b860dfa79499bef&scaleIndex=0
shrieking-essence-of-fear:Shrieking Essence of Fear:web.poecdn.com/image/Art/2DItems/Currency/Essence/Fear6.png?v=f3fdeff9ef90cbe29c180758e19b5fb0&scaleIndex=0
deafening-essence-of-fear:Deafening Essence of Fear:web.poecdn.com/image/Art/2DItems/Currency/Essence/Fear7.png?v=23447273b68054be5341a93e5b31253e&scaleIndex=0
weeping-essence-of-suffering:Weeping Essence of Suffering:web.poecdn.com/image/Art/2DItems/Currency/Essence/Suffering3.png?v=3198cb3a59be5c098fa3a3fcaaf0b941&scaleIndex=0
wailing-essence-of-suffering:Wailing Essence of Suffering:web.poecdn.com/image/Art/2DItems/Currency/Essence/Suffering4.png?v=e764ac660e07b09bad94914c5bcb8dc0&scaleIndex=0
screaming-essence-of-suffering:Screaming Essence of Suffering:web.poecdn.com/image/Art/2DItems/Currency/Essence/Suffering5.png?v=f308ffc5c350e8d224063040ad142f41&scaleIndex=0
shrieking-essence-of-suffering:Shrieking Essence of Suffering:web.poecdn.com/image/Art/2DItems/Currency/Essence/Suffering6.png?v=fb10322cfd6ef4deb2d3c7894cea7dba&scaleIndex=0
deafening-essence-of-suffering:Deafening Essence of Suffering:web.poecdn.com/image/Art/2DItems/Currency/Essence/Suffering7.png?v=37ee3cd0581636d2f349864b66d9823d&scaleIndex=0
weeping-essence-of-rage:Weeping Essence of Rage:web.poecdn.com/image/Art/2DItems/Currency/Essence/Rage3.png?v=4e2cc03f5494682153e0209fda0e0e9d&scaleIndex=0
wailing-essence-of-rage:Wailing Essence of Rage:web.poecdn.com/image/Art/2DItems/Currency/Essence/Rage4.png?v=35eed2dacc8916e6710bae0ecaee9367&scaleIndex=0
screaming-essence-of-rage:Screaming Essence of Rage:web.poecdn.com/image/Art/2DItems/Currency/Essence/Rage5.png?v=b75372bd4b454b6ae9f6ae841df29f89&scaleIndex=0
shrieking-essence-of-rage:Shrieking Essence of Rage:web.poecdn.com/image/Art/2DItems/Currency/Essence/Rage6.png?v=97be74aff29ea91a516964b7eed240f8&scaleIndex=0
deafening-essence-of-rage:Deafening Essence of Rage:web.poecdn.com/image/Art/2DItems/Currency/Essence/Rage7.png?v=eedc0092ba0e7d4619fd2c3d5d4447a2&scaleIndex=0
weeping-essence-of-wrath:Weeping Essence of Wrath:web.poecdn.com/image/Art/2DItems/Currency/Essence/Wrath3.png?v=3870a986ca4f5984a797427c40a4ab41&scaleIndex=0
wailing-essence-of-wrath:Wailing Essence of Wrath:web.poecdn.com/image/Art/2DItems/Currency/Essence/Wrath4.png?v=1b78b631cf91f1d6ec48173183c26257&scaleIndex=0
screaming-essence-of-wrath:Screaming Essence of Wrath:web.poecdn.com/image/Art/2DItems/Currency/Essence/Wrath5.png?v=47288253244910c2da4f0bdda069b832&scaleIndex=0
shrieking-essence-of-wrath:Shrieking Essence of Wrath:web.poecdn.com/image/Art/2DItems/Currency/Essence/Wrath6.png?v=0e3b9c9f8d8299c4a91b17b8194e1581&scaleIndex=0
deafening-essence-of-wrath:Deafening Essence of Wrath:web.poecdn.com/image/Art/2DItems/Currency/Essence/Wrath7.png?v=e9ac9c03dc5aa664db2fa29e267fc366&scaleIndex=0
weeping-essence-of-doubt:Weeping Essence of Doubt:web.poecdn.com/image/Art/2DItems/Currency/Essence/Doubt3.png?v=c9c89f93be1f54c06d99b9283a94af14&scaleIndex=0
wailing-essence-of-doubt:Wailing Essence of Doubt:web.poecdn.com/image/Art/2DItems/Currency/Essence/Doubt4.png?v=02095ead87ba8aa5bb818521d89431ba&scaleIndex=0
screaming-essence-of-doubt:Screaming Essence of Doubt:web.poecdn.com/image/Art/2DItems/Currency/Essence/Doubt5.png?v=8d3a34ab4f38108888a592ef53452eb5&scaleIndex=0
shrieking-essence-of-doubt:Shrieking Essence of Doubt:web.poecdn.com/image/Art/2DItems/Currency/Essence/Doubt6.png?v=7872ac0cd80490d0a473c825a0e7e74b&scaleIndex=0
deafening-essence-of-doubt:Deafening Essence of Doubt:web.poecdn.com/image/Art/2DItems/Currency/Essence/Doubt7.png?v=ad1c5478187b01727a9f18890dbc893b&scaleIndex=0
wailing-essence-of-anguish:Wailing Essence of Anguish:web.poecdn.com/image/Art/2DItems/Currency/Essence/Anguish4.png?v=7f159be306f742b0e4845beacdecd612&scaleIndex=0
screaming-essence-of-anguish:Screaming Essence of Anguish:web.poecdn.com/image/Art/2DItems/Currency/Essence/Anguish5.png?v=9769855172e3523de00e083ab3649ab8&scaleIndex=0
shrieking-essence-of-anguish:Shrieking Essence of Anguish:web.poecdn.com/image/Art/2DItems/Currency/Essence/Anguish6.png?v=9d967aa7f5034bd401886eb96d1f8e59&scaleIndex=0
deafening-essence-of-anguish:Deafening Essence of Anguish:web.poecdn.com/image/Art/2DItems/Currency/Essence/Anguish7.png?v=c07fad72a69d28f5e745345a01f0ce9c&scaleIndex=0
wailing-essence-of-loathing:Wailing Essence of Loathing:web.poecdn.com/image/Art/2DItems/Currency/Essence/Loathing4.png?v=d84dd2fbac9b128225b5320df6bc9c54&scaleIndex=0
screaming-essence-of-loathing:Screaming Essence of Loathing:web.poecdn.com/image/Art/2DItems/Currency/Essence/Loathing5.png?v=f081b3de4779da9cb22b6fe0c8ca018c&scaleIndex=0
shrieking-essence-of-loathing:Shrieking Essence of Loathing:web.poecdn.com/image/Art/2DItems/Currency/Essence/Loathing6.png?v=5c971036ca1ad46642dcc3c1477d2293&scaleIndex=0
deafening-essence-of-loathing:Deafening Essence of Loathing:web.poecdn.com/image/Art/2DItems/Currency/Essence/Loathing7.png?v=030d0913f9e854bd49bd444644019a10&scaleIndex=0
wailing-essence-of-spite:Wailing Essence of Spite:web.poecdn.com/image/Art/2DItems/Currency/Essence/Spite4.png?v=4e706193d7c9c0c7b390740799679a4a&scaleIndex=0
screaming-essence-of-spite:Screaming Essence of Spite:web.poecdn.com/image/Art/2DItems/Currency/Essence/Spite5.png?v=f1961cf6e92a2dfc1fcaca12c24ebf33&scaleIndex=0
shrieking-essence-of-spite:Shrieking Essence of Spite:web.poecdn.com/image/Art/2DItems/Currency/Essence/Spite6.png?v=29049cc43f21f9d13e361c796f343448&scaleIndex=0
deafening-essence-of-spite:Deafening Essence of Spite:web.poecdn.com/image/Art/2DItems/Currency/Essence/Spite7.png?v=fcba04fa7e362e287ad5eaa2fb37781c&scaleIndex=0
wailing-essence-of-zeal:Wailing Essence of Zeal:web.poecdn.com/image/Art/2DItems/Currency/Essence/Zeal4.png?v=c09f456b095f7ecf967ff10b191fba2f&scaleIndex=0
screaming-essence-of-zeal:Screaming Essence of Zeal:web.poecdn.com/image/Art/2DItems/Currency/Essence/Zeal5.png?v=9cc43fa4c69034ea6c7c13a4387c6a20&scaleIndex=0
shrieking-essence-of-zeal:Shrieking Essence of Zeal:web.poecdn.com/image/Art/2DItems/Currency/Essence/Zeal6.png?v=63609bd84db94d338d95395e4d88d946&scaleIndex=0
deafening-essence-of-zeal:Deafening Essence of Zeal:web.poecdn.com/image/Art/2DItems/Currency/Essence/Zeal7.png?v=6fd71da4ce3b602cf4a2f16229bb8674&scaleIndex=0
screaming-essence-of-misery:Screaming Essence of Misery:web.poecdn.com/image/Art/2DItems/Currency/Essence/Misery5.png?v=fe191e14d46d94e18c3650566fa1ceac&scaleIndex=0
shrieking-essence-of-misery:Shrieking Essence of Misery:web.poecdn.com/image/Art/2DItems/Currency/Essence/Misery6.png?v=de5b42946403caf0556b126f0943d586&scaleIndex=0
deafening-essence-of-misery:Deafening Essence of Misery:web.poecdn.com/image/Art/2DItems/Currency/Essence/Misery7.png?v=b36a2cc337cefd3b8300a6ef4ef46e4c&scaleIndex=0
screaming-essence-of-dread:Screaming Essence of Dread:web.poecdn.com/image/Art/2DItems/Currency/Essence/Dread5.png?v=8ef342c40fc4bef92f26c7aba4cfe496&scaleIndex=0
shrieking-essence-of-dread:Shrieking Essence of Dread:web.poecdn.com/image/Art/2DItems/Currency/Essence/Dread6.png?v=b733786165c875af4d59d33d3c4d9a50&scaleIndex=0
deafening-essence-of-dread:Deafening Essence of Dread:web.poecdn.com/image/Art/2DItems/Currency/Essence/Dread7.png?v=7c0faddc480fb35f13436d5ca0d7e0c1&scaleIndex=0
screaming-essence-of-scorn:Screaming Essence of Scorn:web.poecdn.com/image/Art/2DItems/Currency/Essence/Scorn5.png?v=21824a513b092ceb1dccb7658cbc21f8&scaleIndex=0
shrieking-essence-of-scorn:Shrieking Essence of Scorn:web.poecdn.com/image/Art/2DItems/Currency/Essence/Scorn6.png?v=7002f726840836d398521a62506ebd21&scaleIndex=0
deafening-essence-of-scorn:Deafening Essence of Scorn:web.poecdn.com/image/Art/2DItems/Currency/Essence/Scorn7.png?v=90884b626b3ad149a967110e0a7ecd2d&scaleIndex=0
screaming-essence-of-envy:Screaming Essence of Envy:web.poecdn.com/image/Art/2DItems/Currency/Essence/Envy5.png?v=bac465c20f512b02d742b69f72d84b7a&scaleIndex=0
shrieking-essence-of-envy:Shrieking Essence of Envy:web.poecdn.com/image/Art/2DItems/Currency/Essence/Envy6.png?v=bb03c6c5ecee2aecc90a1dfdfe90437d&scaleIndex=0
deafening-essence-of-envy:Deafening Essence of Envy:web.poecdn.com/image/Art/2DItems/Currency/Essence/Envy7.png?v=2da97d735b5d283ef0a52e4fe5add46b&scaleIndex=0
essence-of-hysteria:Essence of Hysteria:web.poecdn.com/image/Art/2DItems/Currency/Essence/Terror1.png?v=b6310bff75f61da7446462b7abdbb0b7&scaleIndex=0
essence-of-insanity:Essence of Insanity:web.poecdn.com/image/Art/2DItems/Currency/Essence/Insanity1.png?v=5c44e4594763c6410f49a5821b8d8c9e&scaleIndex=0
essence-of-horror:Essence of Horror:web.poecdn.com/image/Art/2DItems/Currency/Essence/Horror1.png?v=8f0acc8500da53eb36f57e9109b3fb59&scaleIndex=0
essence-of-delirium:Essence of Delirium:web.poecdn.com/image/Art/2DItems/Currency/Essence/Madness1.png?v=6f49daf1b61aa8a9dab768db23cd8f87&scaleIndex=0
remnant-of-corruption:Remnant of Corruption:web.poecdn.com/image/Art/2DItems/Currency/Essence/EssenceCorrupt.png?v=49220b99e034b4dd3c85702167958b38&scaleIndex=0