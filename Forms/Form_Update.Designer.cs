﻿namespace PoE_Trade_Overlay.Forms
{
    partial class Form_Update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_Update = new System.Windows.Forms.Button();
            this.lbl_header_Update = new System.Windows.Forms.Label();
            this.lbl_UpdateText = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.btn_close.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_close.FlatAppearance.BorderSize = 0;
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_close.Font = new System.Drawing.Font("Consolas", 8F);
            this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_close.Location = new System.Drawing.Point(0, 553);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(312, 20);
            this.btn_close.TabIndex = 6;
            this.btn_close.Text = "Close";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_Update
            // 
            this.btn_Update.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Update.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.btn_Update.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_Update.FlatAppearance.BorderSize = 0;
            this.btn_Update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Update.Font = new System.Drawing.Font("Consolas", 8F);
            this.btn_Update.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_Update.Location = new System.Drawing.Point(312, 553);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(313, 20);
            this.btn_Update.TabIndex = 7;
            this.btn_Update.Text = "Update";
            this.btn_Update.UseVisualStyleBackColor = false;
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            // 
            // lbl_header_Update
            // 
            this.lbl_header_Update.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.lbl_header_Update.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_header_Update.Font = new System.Drawing.Font("Consolas", 10F);
            this.lbl_header_Update.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_header_Update.Location = new System.Drawing.Point(0, 0);
            this.lbl_header_Update.Name = "lbl_header_Update";
            this.lbl_header_Update.Size = new System.Drawing.Size(625, 20);
            this.lbl_header_Update.TabIndex = 8;
            this.lbl_header_Update.Text = "Update";
            this.lbl_header_Update.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_UpdateText
            // 
            this.lbl_UpdateText.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_UpdateText.Font = new System.Drawing.Font("Consolas", 8F);
            this.lbl_UpdateText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_UpdateText.Location = new System.Drawing.Point(0, 20);
            this.lbl_UpdateText.Name = "lbl_UpdateText";
            this.lbl_UpdateText.Size = new System.Drawing.Size(625, 74);
            this.lbl_UpdateText.TabIndex = 9;
            this.lbl_UpdateText.Text = "label1";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Consolas", 9F);
            this.richTextBox1.Location = new System.Drawing.Point(3, 97);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(619, 452);
            this.richTextBox1.TabIndex = 10;
            this.richTextBox1.Text = "";
            // 
            // Form_Update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(625, 573);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.lbl_UpdateText);
            this.Controls.Add(this.lbl_header_Update);
            this.Controls.Add(this.btn_Update);
            this.Controls.Add(this.btn_close);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form_Update";
            this.ShowInTaskbar = false;
            this.Text = "Form_Update";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Form_Update_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.Label lbl_header_Update;
        private System.Windows.Forms.Label lbl_UpdateText;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}