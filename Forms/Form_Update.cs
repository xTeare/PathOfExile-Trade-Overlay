﻿using PoE_Trade_Overlay.Controls;
using PoE_Trade_Overlay.Queries;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace PoE_Trade_Overlay.Forms
{
    public partial class Form_Update : Form
    {
        public static Form_Update instance;
        public string newVersion;

        public Form_Update()
        {
            InitializeComponent();
            instance = this;
            lbl_header_Update.AssignMoveEvents();
            ThemeLoader.ApplyTheme(this.GetControls());
        }

        /// <summary>
        /// Use Webclient to download all Tags from repo as JSON and parse them into Collapses
        /// </summary>
        public void LoadChangelog()
        {
            string Url = "https://gitlab.com/api/v4/projects/xTeare%2FPathOfExile-Trade-Overlay/repository/tags";
            List<RepoTags> tags = new List<RepoTags>();

            using (var client = new WebClient())
            {
                client.BaseAddress = Url;
                client.Headers.Add("Content-Type:application/json");
                client.Headers.Add("Accept:application/json");
                var tagList = client.DownloadString(Url);
                Console.WriteLine(tagList);
                tags = RepoTags.FromJson(tagList);
            }
            
            int index = 0;
            foreach (RepoTags tag in tags)
            {
                

                string[] splittedLines = tag.Release.Description.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                int index2 = 0;

                richTextBox1.AppendText("v" + tag.Name.Replace("v", "") + Environment.NewLine);

                foreach (string s in splittedLines)
                {
                    string s2 = s;
                    if (s2.Contains(".zip"))
                        continue;
                   
                    if (s.Contains("**"))
                    {
                        richTextBox1.AppendText(Environment.NewLine);
                        s2 = s2.Replace("*", "");
                        richTextBox1.AppendText("**" + s2 + Environment.NewLine);

                    }
                    else
                    {
                        s2 = s2.Replace("*", "");
                        richTextBox1.AppendText("*" + s2 + Environment.NewLine);
                    }

                    
                    index2++;
                }
                richTextBox1.AppendText(Environment.NewLine);
                richTextBox1.AppendText(Environment.NewLine);
                index++;
            }
            //ThemeLoader.SetControls(flowLayoutPanel1.GetControls(), FormTheme.search);
        }

        private void Form_Update_Load(object sender, EventArgs e)
        {
            LoadChangelog();
            lbl_UpdateText.Text = string.Format("\nA new Update is aviable !\n\nCurrent Version  : {0}\nNew Version      : {1}", Config.version, newVersion);
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            Process.Start("PathOfExile Trade Overlay Updater.exe");
            Environment.Exit(Environment.ExitCode);
        }
    }
}