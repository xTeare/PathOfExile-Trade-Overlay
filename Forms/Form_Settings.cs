﻿using PoE_Trade_Overlay.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PoE_Trade_Overlay.Forms
{
    public partial class Form_Settings : Form
    {
        public static Form_Settings instance;

        private string s;

        public Form_Settings()
        {
            InitializeComponent();
            
            toggle_socketHover.On = Config.hideSocketsOnHover;
            toggle_onlyrender.On = Config.onlyRenderIfPoeIsActive;
            toggle_hideLegacy.On = Config.hideLegacyMaps;
            toggle_filterAFK.On = Config.filterAFKPlayers;
            dragBar1.Value = Config.windowOpacity;

            FillCB();
            this.Location = new Point(Config.settingsX, Config.settingsY);
            instance = this;
            ThemeLoader.ApplyTheme(this.GetControls());
            fcb_leagues.ForeColorDrop = fcb_leagues.ForeColor;
        }

        void FillCB()
        {
            fcb_themes.Items.Clear();

            foreach(DataRow dr in ThemeLoader.themeList.Rows)
            {
                fcb_themes.Items.Add(dr[0].ToString());
            }

            fcb_themes.SelectedIndex = 0;
            for (int i = 0; i < fcb_themes.Items.Count; i++)
            {
                if (Config.currentTheme == fcb_themes.Items[i].ToString())
                    fcb_themes.SelectedIndex = i;
            }

            
            fcb_leagues.Items.Clear();
            if (Form_Main.instance.leagues.Result != null)
            {
                foreach (LeagueResult lr in Form_Main.instance.leagues.Result)
                {
                    fcb_leagues.Items.Add(lr.Id);
                }
            }
            
            for (int i = 0; i < fcb_leagues.Items.Count; i++)
            {
                if (fcb_leagues.Items[i].ToString() == Config.league)
                    fcb_leagues.SelectedIndex = i;
            }


        }
        


        #region Panel Movement

        private bool clicked = false;
        private int iOldX;
        private int iOldY;
        private int iClickX;
        private int iClickY;

        private void moveMenu_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point p = ConvertFromChildToForm(e.X, e.Y, (Control)sender);
                iOldX = p.X;
                iOldY = p.Y;
                iClickX = e.X;
                iClickY = e.Y;
                clicked = true;
            }
        }

        private void moveMenu_MouseMove(object sender, MouseEventArgs e)
        {
            if (clicked)
            {
                Point p = new Point(); // New Coordinate
                p.X = e.X + this.Left;
                p.Y = e.Y + this.Top;
                this.Left = p.X - iClickX;
                this.Top = p.Y - iClickY;
            }
        }

        private void moveMenu_MouseUp(object sender, MouseEventArgs e)
        {
            clicked = false;
            Config.settingsX = this.Location.X;
            Config.settingsY = this.Location.Y;
            Config.Save();
        }

        private Point ConvertFromChildToForm(int x, int y, Control control)
        {
            Point p = new Point(x, y);
            return p;
        }

        #endregion Panel Movement
        
        private void btn_close_Click(object sender, EventArgs e)
        {
            Config.hideSocketsOnHover = toggle_socketHover.On;
            Config.onlyRenderIfPoeIsActive = toggle_onlyrender.On;
            Config.filterAFKPlayers = toggle_filterAFK.On;
            Config.hideLegacyMaps = toggle_hideLegacy.On;
            Config.league = fcb_leagues.Items[fcb_leagues.SelectedIndex].ToString();
            Config.windowOpacity = dragBar1.Value;
            Config.currentTheme = fcb_themes.Items[fcb_themes.SelectedIndex].ToString();
            Config.Save();
            this.Close();
        }

        private void cb_theme_SelectedIndexChanged(object sender, EventArgs e)
        {
            Config.currentTheme = fcb_themes.Items[fcb_themes.SelectedIndex].ToString();
            Config.Save();
        }

        private void btn_Reload_Click(object sender, EventArgs e)
        {
            Credits c = new Credits();
            c.Show();
        }

        private void fcb_leagues_SelectedIndexChanged(object sender, EventArgs e)
        {
            Config.league = fcb_leagues.Items[fcb_leagues.SelectedIndex].ToString();
            Config.Save();
            Console.WriteLine(Config.league);
            Ninja.Load(Config.league);
        }
        
        private void Form_Settings_KeyDown(object sender, KeyEventArgs e)
        {
            s += e.KeyCode.ToString();
            string s1 = "LETOUCAN";
            if (s1.Contains(s))
            {
                if(s1 == s)
                {
                    Praise p = new Praise();
                    p.Show();
                }
            }
            else
            {
                s = "";
            }
        }

        private void dragBar1_ValueChanged(object sender, EventArgs e)
        {
            this.Opacity = dragBar1.Value / 100d;
        }
    }
}
