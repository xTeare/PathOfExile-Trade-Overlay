﻿namespace PoE_Trade_Overlay.Forms
{
    partial class Credits
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_header = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl_donateWarning = new System.Windows.Forms.Label();
            this.link1 = new System.Windows.Forms.LinkLabel();
            this.link2 = new System.Windows.Forms.LinkLabel();
            this.lbl2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.donate = new System.Windows.Forms.PictureBox();
            this.btn_close = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.donate)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_header
            // 
            this.lbl_header.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.lbl_header.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_header.Font = new System.Drawing.Font("Consolas", 12F);
            this.lbl_header.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_header.Location = new System.Drawing.Point(0, 0);
            this.lbl_header.Name = "lbl_header";
            this.lbl_header.Size = new System.Drawing.Size(376, 20);
            this.lbl_header.TabIndex = 3;
            this.lbl_header.Text = "Credits";
            this.lbl_header.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbl1
            // 
            this.lbl1.Font = new System.Drawing.Font("Consolas", 10F);
            this.lbl1.Location = new System.Drawing.Point(0, 23);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(376, 17);
            this.lbl1.TabIndex = 4;
            this.lbl1.Text = "s1";
            this.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_donateWarning
            // 
            this.lbl_donateWarning.AutoSize = true;
            this.lbl_donateWarning.Font = new System.Drawing.Font("Consolas", 8F);
            this.lbl_donateWarning.Location = new System.Drawing.Point(7, 148);
            this.lbl_donateWarning.Name = "lbl_donateWarning";
            this.lbl_donateWarning.Size = new System.Drawing.Size(91, 13);
            this.lbl_donateWarning.TabIndex = 6;
            this.lbl_donateWarning.Text = "Very long text";
            this.lbl_donateWarning.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // link1
            // 
            this.link1.AutoSize = true;
            this.link1.Font = new System.Drawing.Font("Consolas", 10F);
            this.link1.LinkColor = System.Drawing.Color.Aqua;
            this.link1.Location = new System.Drawing.Point(128, 323);
            this.link1.Name = "link1";
            this.link1.Size = new System.Drawing.Size(56, 17);
            this.link1.TabIndex = 7;
            this.link1.TabStop = true;
            this.link1.Text = "Reddit";
            this.link1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link1_LinkClicked);
            // 
            // link2
            // 
            this.link2.AutoSize = true;
            this.link2.Font = new System.Drawing.Font("Consolas", 10F);
            this.link2.LinkColor = System.Drawing.Color.Aqua;
            this.link2.Location = new System.Drawing.Point(190, 323);
            this.link2.Name = "link2";
            this.link2.Size = new System.Drawing.Size(56, 17);
            this.link2.TabIndex = 9;
            this.link2.TabStop = true;
            this.link2.Text = "Forums";
            this.link2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link2_LinkClicked);
            // 
            // lbl2
            // 
            this.lbl2.Font = new System.Drawing.Font("Consolas", 10F);
            this.lbl2.Location = new System.Drawing.Point(0, 121);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(376, 17);
            this.lbl2.TabIndex = 10;
            this.lbl2.Text = "s2";
            this.lbl2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 10F);
            this.label1.Location = new System.Drawing.Point(18, 322);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Contact me :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // donate
            // 
            this.donate.Image = global::PoE_Trade_Overlay.Properties.Resources.donate;
            this.donate.Location = new System.Drawing.Point(141, 234);
            this.donate.Name = "donate";
            this.donate.Size = new System.Drawing.Size(92, 26);
            this.donate.TabIndex = 12;
            this.donate.TabStop = false;
            this.donate.Click += new System.EventHandler(this.donate_Click);
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.btn_close.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_close.FlatAppearance.BorderSize = 0;
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_close.Font = new System.Drawing.Font("Consolas", 8F);
            this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_close.Location = new System.Drawing.Point(0, 343);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(376, 23);
            this.btn_close.TabIndex = 13;
            this.btn_close.Text = "Close";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Consolas", 10F);
            this.linkLabel1.LinkColor = System.Drawing.Color.Aqua;
            this.linkLabel1.Location = new System.Drawing.Point(252, 323);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(64, 17);
            this.linkLabel1.TabIndex = 14;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Discord";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // Credits
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(376, 366);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.donate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.link2);
            this.Controls.Add(this.link1);
            this.Controls.Add(this.lbl_donateWarning);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.lbl_header);
            this.Font = new System.Drawing.Font("Consolas", 8F);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimumSize = new System.Drawing.Size(376, 100);
            this.Name = "Credits";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Credits";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.donate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_header;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl_donateWarning;
        private System.Windows.Forms.LinkLabel link1;
        private System.Windows.Forms.LinkLabel link2;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox donate;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}