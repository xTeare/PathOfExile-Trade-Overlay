﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace PoE_Trade_Overlay.Forms
{
    public partial class Credits : Form
    {

        public string s1 = "Path of Exile Trade Overlay v{0}\nCoding, Design, Idea by\nxTeare\n";
        public string s2 = "Special thanks to\n\nChnaVeeZ\nNathaniel\nThorinori\nSirKultan\n\n";

        public string u1 = "https://www.reddit.com/message/compose/?to=xTeare";
        public string u2 = "https://www.pathofexile.com/account/view-profile/xTeare";
        public string u3 = "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=F7SU93HHDWUGE&source=url";
        public string u4 = "https://discord.gg/anapEfx";

        public string waringDonation = "Please check the email you donate to.\nThe correct one is jansse.christian1995@gmail.com";
        public Credits()
        {
            InitializeComponent();
            lbl_header.AssignMoveEvents();
            lbl1.Text = string.Format(s1, Config.version);
            lbl1.Size = new Size(this.Width, TextRenderer.MeasureText(lbl1.Text, lbl1.Font).Height);
            lbl1.Location = new Point((this.Width - lbl1.Width) / 2, lbl1.Top);

            lbl2.Text = s2;
            lbl2.Size = new Size(this.Width, TextRenderer.MeasureText(lbl2.Text, lbl2.Font).Height);
            lbl2.Location = new Point((this.Width - lbl2.Width) / 2, lbl1.Bottom + 3);

            donate.Location = new Point((this.Width - donate.Width) / 2, lbl2.Bottom + 3);
            lbl_donateWarning.Text = waringDonation;
            lbl_donateWarning.Size = new Size(this.Width, TextRenderer.MeasureText(lbl_donateWarning.Text, lbl_donateWarning.Font).Height);
            lbl_donateWarning.Location = new Point( (this.Width - lbl_donateWarning.Width) /2, donate.Bottom + 3);
        }

        private void link1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(u1);
        }

        private void link2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(u2);
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void donate_Click(object sender, EventArgs e)
        {
            Process.Start(u3);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(u4);
        }
    }
}
