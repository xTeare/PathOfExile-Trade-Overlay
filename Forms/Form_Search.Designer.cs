﻿namespace PoE_Trade_Overlay
{
    partial class Form_Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_search = new System.Windows.Forms.Button();
            this.lbl_header = new System.Windows.Forms.Label();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_reset = new System.Windows.Forms.Button();
            this.lb_itemName = new System.Windows.Forms.ListBox();
            this.tp_search = new System.Windows.Forms.TabPage();
            this.cmb_accountStatus = new PoE_Trade_Overlay.Controls.FlattenCombo();
            this.lbl_online = new System.Windows.Forms.Label();
            this.lbl_itemCategory = new System.Windows.Forms.Label();
            this.cmb_itemCategory = new PoE_Trade_Overlay.Controls.FlattenCombo();
            this.lbl_itemName = new System.Windows.Forms.Label();
            this.cmb_itemRarity = new PoE_Trade_Overlay.Controls.FlattenCombo();
            this.flp_collapses = new System.Windows.Forms.FlowLayoutPanel();
            this.ec_weaponFilters = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.flp_weaponFilters = new System.Windows.Forms.FlowLayoutPanel();
            this.cf_damage = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_attacksPerSecond = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_criticalChance = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_damagePerSecond = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_physicalDPS = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_elementalDPS = new PoE_Trade_Overlay.Controls.Filter();
            this.ec_armourFilters = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.flp_armourFilters = new System.Windows.Forms.FlowLayoutPanel();
            this.cf_armour = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_block = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_energyShield = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_evasion = new PoE_Trade_Overlay.Controls.Filter();
            this.ec_socketFilters = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.flp_socketFilters = new System.Windows.Forms.FlowLayoutPanel();
            this.csf_sockets = new PoE_Trade_Overlay.Controls.Filter_Sockets();
            this.csf_links = new PoE_Trade_Overlay.Controls.Filter_Sockets();
            this.ec_Requirements = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.flp_requirements = new System.Windows.Forms.FlowLayoutPanel();
            this.cf_level = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_strength = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_dexterity = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_intelligence = new PoE_Trade_Overlay.Controls.Filter();
            this.ec_modFilters = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.flp_modFilters = new System.Windows.Forms.FlowLayoutPanel();
            this.cf_emptyPrefixes = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_emptySuffixes = new PoE_Trade_Overlay.Controls.Filter();
            this.ec_mods = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.cmb_modGroupType = new PoE_Trade_Overlay.Controls.FlattenCombo();
            this.btn_addModGroup = new System.Windows.Forms.Button();
            this.ec_mapFilters = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.flp_mapFilters = new System.Windows.Forms.FlowLayoutPanel();
            this.cf_mapTier = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_mapPacksize = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_mapIIQ = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_mapIIR = new PoE_Trade_Overlay.Controls.Filter();
            this.cfdd_shaped = new PoE_Trade_Overlay.Controls.Filter_DropDown();
            this.cfdd_elder = new PoE_Trade_Overlay.Controls.Filter_DropDown();
            this.cfdd_mapSeries = new PoE_Trade_Overlay.Controls.Filter_DropDown();
            this.ec_miscellaneous = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.flp_miscFilters = new System.Windows.Forms.FlowLayoutPanel();
            this.cf_quality = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_itemLevel = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_gemLevel = new PoE_Trade_Overlay.Controls.Filter();
            this.cf_gemExperience = new PoE_Trade_Overlay.Controls.Filter();
            this.cfdd_shaperItem = new PoE_Trade_Overlay.Controls.Filter_DropDown();
            this.cfdd_elderItem = new PoE_Trade_Overlay.Controls.Filter_DropDown();
            this.cfdd_alternateArt = new PoE_Trade_Overlay.Controls.Filter_DropDown();
            this.cfdd_identified = new PoE_Trade_Overlay.Controls.Filter_DropDown();
            this.cfdd_corrupted = new PoE_Trade_Overlay.Controls.Filter_DropDown();
            this.cfdd_mirrored = new PoE_Trade_Overlay.Controls.Filter_DropDown();
            this.cfdd_crafted = new PoE_Trade_Overlay.Controls.Filter_DropDown();
            this.cfdd_veiled = new PoE_Trade_Overlay.Controls.Filter_DropDown();
            this.cfdd_enchanted = new PoE_Trade_Overlay.Controls.Filter_DropDown();
            this.cf_talismanTier = new PoE_Trade_Overlay.Controls.Filter();
            this.ec_tradeFilters = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.flp_tradeFilters = new System.Windows.Forms.FlowLayoutPanel();
            this.cftb_trader = new PoE_Trade_Overlay.Controls.Filter_Textbox();
            this.cfdd_listed = new PoE_Trade_Overlay.Controls.Filter_DropDown();
            this.cfdd_saleType = new PoE_Trade_Overlay.Controls.Filter_DropDown();
            this.cfby_buyout = new PoE_Trade_Overlay.Controls.Filter_Buyout();
            this.lbl_itemRarity = new System.Windows.Forms.Label();
            this.tb_itemName = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tp_bulk = new System.Windows.Forms.TabPage();
            this.fc_bulkAccStatus = new PoE_Trade_Overlay.Controls.FlattenCombo();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_want = new System.Windows.Forms.Label();
            this.lbl_have = new System.Windows.Forms.Label();
            this.flp_have = new System.Windows.Forms.FlowLayoutPanel();
            this.ec_haveCurrency = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.ec_haveFrags = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.ec_haveResonators = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.ec_haveFossils = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.ec_haveEssences = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.ec_haveCards = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.ec_haveMaps = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.ec_haveShapedMap = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.ec_haveElderMap = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.flp_want = new System.Windows.Forms.FlowLayoutPanel();
            this.ec_wantCurrency = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.ec_wantFrags = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.ec_wantResonators = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.ec_wantFossils = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.ec_wantEssences = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.ec_wantCards = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.ec_wantMaps = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.ec_wantShapedMap = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.ec_wantElderMap = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.lbl_Filter = new System.Windows.Forms.Label();
            this.tb_filter = new System.Windows.Forms.TextBox();
            this.lbl_minimumStock = new System.Windows.Forms.Label();
            this.tb_BulkMinimumStock = new System.Windows.Forms.TextBox();
            this.lbl_sellerAccount = new System.Windows.Forms.Label();
            this.tb_bulkSellerAccount = new System.Windows.Forms.TextBox();
            this.btn_closeTab = new System.Windows.Forms.Button();
            this.btn_settings = new System.Windows.Forms.Button();
            this.btn_twoColumns = new System.Windows.Forms.Button();
            this.btn_defaultLayout = new System.Windows.Forms.Button();
            this.pb_vertical = new System.Windows.Forms.PictureBox();
            this.pb_horizontal = new System.Windows.Forms.PictureBox();
            this.tp_search.SuspendLayout();
            this.flp_collapses.SuspendLayout();
            this.ec_weaponFilters.DropZone.SuspendLayout();
            this.ec_weaponFilters.SuspendLayout();
            this.flp_weaponFilters.SuspendLayout();
            this.ec_armourFilters.DropZone.SuspendLayout();
            this.ec_armourFilters.SuspendLayout();
            this.flp_armourFilters.SuspendLayout();
            this.ec_socketFilters.DropZone.SuspendLayout();
            this.ec_socketFilters.SuspendLayout();
            this.flp_socketFilters.SuspendLayout();
            this.ec_Requirements.DropZone.SuspendLayout();
            this.ec_Requirements.SuspendLayout();
            this.flp_requirements.SuspendLayout();
            this.ec_modFilters.DropZone.SuspendLayout();
            this.ec_modFilters.SuspendLayout();
            this.flp_modFilters.SuspendLayout();
            this.ec_mods.DropZone.SuspendLayout();
            this.ec_mods.SuspendLayout();
            this.ec_mapFilters.DropZone.SuspendLayout();
            this.ec_mapFilters.SuspendLayout();
            this.flp_mapFilters.SuspendLayout();
            this.ec_miscellaneous.DropZone.SuspendLayout();
            this.ec_miscellaneous.SuspendLayout();
            this.flp_miscFilters.SuspendLayout();
            this.ec_tradeFilters.DropZone.SuspendLayout();
            this.ec_tradeFilters.SuspendLayout();
            this.flp_tradeFilters.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tp_bulk.SuspendLayout();
            this.flp_have.SuspendLayout();
            this.ec_haveCurrency.SuspendLayout();
            this.ec_haveFrags.SuspendLayout();
            this.ec_haveResonators.SuspendLayout();
            this.ec_haveFossils.SuspendLayout();
            this.ec_haveEssences.SuspendLayout();
            this.ec_haveCards.SuspendLayout();
            this.ec_haveMaps.SuspendLayout();
            this.ec_haveShapedMap.SuspendLayout();
            this.ec_haveElderMap.SuspendLayout();
            this.flp_want.SuspendLayout();
            this.ec_wantCurrency.SuspendLayout();
            this.ec_wantFrags.SuspendLayout();
            this.ec_wantResonators.SuspendLayout();
            this.ec_wantFossils.SuspendLayout();
            this.ec_wantEssences.SuspendLayout();
            this.ec_wantCards.SuspendLayout();
            this.ec_wantMaps.SuspendLayout();
            this.ec_wantShapedMap.SuspendLayout();
            this.ec_wantElderMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_vertical)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_horizontal)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_search
            // 
            this.btn_search.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.btn_search.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_search.FlatAppearance.BorderSize = 0;
            this.btn_search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_search.Font = new System.Drawing.Font("Consolas", 9F);
            this.btn_search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_search.Location = new System.Drawing.Point(456, 718);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(229, 20);
            this.btn_search.TabIndex = 0;
            this.btn_search.Text = "Search";
            this.btn_search.UseVisualStyleBackColor = false;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // lbl_header
            // 
            this.lbl_header.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.lbl_header.CausesValidation = false;
            this.lbl_header.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_header.Font = new System.Drawing.Font("Consolas", 12F);
            this.lbl_header.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_header.Location = new System.Drawing.Point(0, 0);
            this.lbl_header.Name = "lbl_header";
            this.lbl_header.Size = new System.Drawing.Size(690, 20);
            this.lbl_header.TabIndex = 2;
            this.lbl_header.Text = "PathOfExile Trade Overlay";
            this.lbl_header.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.btn_close.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_close.FlatAppearance.BorderSize = 0;
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_close.Font = new System.Drawing.Font("Consolas", 9F);
            this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_close.Location = new System.Drawing.Point(0, 718);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(228, 20);
            this.btn_close.TabIndex = 5;
            this.btn_close.Text = "Close";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_reset
            // 
            this.btn_reset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_reset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.btn_reset.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_reset.FlatAppearance.BorderSize = 0;
            this.btn_reset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reset.Font = new System.Drawing.Font("Consolas", 9F);
            this.btn_reset.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_reset.Location = new System.Drawing.Point(228, 718);
            this.btn_reset.Name = "btn_reset";
            this.btn_reset.Size = new System.Drawing.Size(228, 20);
            this.btn_reset.TabIndex = 6;
            this.btn_reset.Text = "Reset";
            this.btn_reset.UseVisualStyleBackColor = false;
            this.btn_reset.Click += new System.EventHandler(this.btn_reset_Click);
            // 
            // lb_itemName
            // 
            this.lb_itemName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.lb_itemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lb_itemName.Font = new System.Drawing.Font("Consolas", 10F);
            this.lb_itemName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lb_itemName.FormattingEnabled = true;
            this.lb_itemName.ItemHeight = 15;
            this.lb_itemName.Location = new System.Drawing.Point(9, -269);
            this.lb_itemName.Name = "lb_itemName";
            this.lb_itemName.Size = new System.Drawing.Size(529, 272);
            this.lb_itemName.TabIndex = 18;
            this.lb_itemName.Visible = false;
            this.lb_itemName.MouseDown += new System.Windows.Forms.MouseEventHandler(this.search_lb_itemName_MouseDown);
            // 
            // tp_search
            // 
            this.tp_search.BackColor = System.Drawing.Color.Black;
            this.tp_search.Controls.Add(this.cmb_accountStatus);
            this.tp_search.Controls.Add(this.lbl_online);
            this.tp_search.Controls.Add(this.lbl_itemCategory);
            this.tp_search.Controls.Add(this.cmb_itemCategory);
            this.tp_search.Controls.Add(this.lb_itemName);
            this.tp_search.Controls.Add(this.lbl_itemName);
            this.tp_search.Controls.Add(this.cmb_itemRarity);
            this.tp_search.Controls.Add(this.flp_collapses);
            this.tp_search.Controls.Add(this.lbl_itemRarity);
            this.tp_search.Controls.Add(this.tb_itemName);
            this.tp_search.Location = new System.Drawing.Point(4, 25);
            this.tp_search.Name = "tp_search";
            this.tp_search.Padding = new System.Windows.Forms.Padding(3);
            this.tp_search.Size = new System.Drawing.Size(670, 660);
            this.tp_search.TabIndex = 0;
            this.tp_search.Text = "Item Search";
            // 
            // cmb_accountStatus
            // 
            this.cmb_accountStatus.AutoComp = false;
            this.cmb_accountStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmb_accountStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_accountStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.cmb_accountStatus.BorderColor = System.Drawing.Color.Violet;
            this.cmb_accountStatus.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cmb_accountStatus.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.cmb_accountStatus.Data = null;
            this.cmb_accountStatus.DropDownColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cmb_accountStatus.ForeColorDrop = System.Drawing.Color.Empty;
            this.cmb_accountStatus.FormattingEnabled = true;
            this.cmb_accountStatus.Items.AddRange(new object[] {
            "Any",
            "Online only"});
            this.cmb_accountStatus.Location = new System.Drawing.Point(563, 3);
            this.cmb_accountStatus.Name = "cmb_accountStatus";
            this.cmb_accountStatus.Size = new System.Drawing.Size(97, 21);
            this.cmb_accountStatus.TabIndex = 24;
            // 
            // lbl_online
            // 
            this.lbl_online.AutoSize = true;
            this.lbl_online.BackColor = System.Drawing.Color.Transparent;
            this.lbl_online.Font = new System.Drawing.Font("Consolas", 8F);
            this.lbl_online.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_online.Location = new System.Drawing.Point(461, 8);
            this.lbl_online.Name = "lbl_online";
            this.lbl_online.Size = new System.Drawing.Size(91, 13);
            this.lbl_online.TabIndex = 23;
            this.lbl_online.Text = "Account status";
            // 
            // lbl_itemCategory
            // 
            this.lbl_itemCategory.AutoSize = true;
            this.lbl_itemCategory.BackColor = System.Drawing.Color.Transparent;
            this.lbl_itemCategory.Font = new System.Drawing.Font("Consolas", 8F);
            this.lbl_itemCategory.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_itemCategory.Location = new System.Drawing.Point(8, 8);
            this.lbl_itemCategory.Name = "lbl_itemCategory";
            this.lbl_itemCategory.Size = new System.Drawing.Size(85, 13);
            this.lbl_itemCategory.TabIndex = 8;
            this.lbl_itemCategory.Text = "Item Category";
            // 
            // cmb_itemCategory
            // 
            this.cmb_itemCategory.AutoComp = false;
            this.cmb_itemCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmb_itemCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_itemCategory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.cmb_itemCategory.BorderColor = System.Drawing.Color.Violet;
            this.cmb_itemCategory.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cmb_itemCategory.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.cmb_itemCategory.Data = null;
            this.cmb_itemCategory.DropDownColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cmb_itemCategory.ForeColorDrop = System.Drawing.Color.Empty;
            this.cmb_itemCategory.FormattingEnabled = true;
            this.cmb_itemCategory.Location = new System.Drawing.Point(99, 3);
            this.cmb_itemCategory.Name = "cmb_itemCategory";
            this.cmb_itemCategory.Size = new System.Drawing.Size(153, 21);
            this.cmb_itemCategory.TabIndex = 21;
            // 
            // lbl_itemName
            // 
            this.lbl_itemName.AutoSize = true;
            this.lbl_itemName.BackColor = System.Drawing.Color.Transparent;
            this.lbl_itemName.Font = new System.Drawing.Font("Consolas", 8F);
            this.lbl_itemName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_itemName.Location = new System.Drawing.Point(8, 35);
            this.lbl_itemName.Name = "lbl_itemName";
            this.lbl_itemName.Size = new System.Drawing.Size(61, 13);
            this.lbl_itemName.TabIndex = 17;
            this.lbl_itemName.Text = "Item Name";
            // 
            // cmb_itemRarity
            // 
            this.cmb_itemRarity.AutoComp = false;
            this.cmb_itemRarity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmb_itemRarity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_itemRarity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.cmb_itemRarity.BorderColor = System.Drawing.Color.Violet;
            this.cmb_itemRarity.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cmb_itemRarity.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.cmb_itemRarity.Data = null;
            this.cmb_itemRarity.DropDownColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cmb_itemRarity.ForeColorDrop = System.Drawing.Color.Empty;
            this.cmb_itemRarity.FormattingEnabled = true;
            this.cmb_itemRarity.Location = new System.Drawing.Point(337, 3);
            this.cmb_itemRarity.Name = "cmb_itemRarity";
            this.cmb_itemRarity.Size = new System.Drawing.Size(118, 21);
            this.cmb_itemRarity.TabIndex = 22;
            // 
            // flp_collapses
            // 
            this.flp_collapses.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flp_collapses.AutoScroll = true;
            this.flp_collapses.BackColor = System.Drawing.Color.Transparent;
            this.flp_collapses.Controls.Add(this.ec_weaponFilters);
            this.flp_collapses.Controls.Add(this.ec_armourFilters);
            this.flp_collapses.Controls.Add(this.ec_socketFilters);
            this.flp_collapses.Controls.Add(this.ec_Requirements);
            this.flp_collapses.Controls.Add(this.ec_modFilters);
            this.flp_collapses.Controls.Add(this.ec_mods);
            this.flp_collapses.Controls.Add(this.ec_mapFilters);
            this.flp_collapses.Controls.Add(this.ec_miscellaneous);
            this.flp_collapses.Controls.Add(this.ec_tradeFilters);
            this.flp_collapses.Location = new System.Drawing.Point(9, 54);
            this.flp_collapses.Name = "flp_collapses";
            this.flp_collapses.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.flp_collapses.Size = new System.Drawing.Size(655, 600);
            this.flp_collapses.TabIndex = 4;
            // 
            // ec_weaponFilters
            // 
            this.ec_weaponFilters.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            // 
            // ec_weaponFilters.DropZone
            // 
            this.ec_weaponFilters.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_weaponFilters.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_weaponFilters.DropZone.Controls.Add(this.flp_weaponFilters);
            this.ec_weaponFilters.DropZone.Location = new System.Drawing.Point(3, 19);
            this.ec_weaponFilters.DropZone.Name = "DropZone";
            this.ec_weaponFilters.DropZone.Size = new System.Drawing.Size(601, 77);
            this.ec_weaponFilters.DropZone.TabIndex = 0;
            this.ec_weaponFilters.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ec_weaponFilters.Header = "Weapon Filters";
            this.ec_weaponFilters.HeaderHeight = 20;
            this.ec_weaponFilters.Location = new System.Drawing.Point(18, 3);
            this.ec_weaponFilters.Name = "ec_weaponFilters";
            this.ec_weaponFilters.Size = new System.Drawing.Size(607, 100);
            this.ec_weaponFilters.SizeExpanded = new System.Drawing.Size(607, 100);
            this.ec_weaponFilters.TabIndex = 7;
            // 
            // flp_weaponFilters
            // 
            this.flp_weaponFilters.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flp_weaponFilters.Controls.Add(this.cf_damage);
            this.flp_weaponFilters.Controls.Add(this.cf_attacksPerSecond);
            this.flp_weaponFilters.Controls.Add(this.cf_criticalChance);
            this.flp_weaponFilters.Controls.Add(this.cf_damagePerSecond);
            this.flp_weaponFilters.Controls.Add(this.cf_physicalDPS);
            this.flp_weaponFilters.Controls.Add(this.cf_elementalDPS);
            this.flp_weaponFilters.Location = new System.Drawing.Point(0, 2);
            this.flp_weaponFilters.Name = "flp_weaponFilters";
            this.flp_weaponFilters.Size = new System.Drawing.Size(596, 79);
            this.flp_weaponFilters.TabIndex = 1;
            // 
            // cf_damage
            // 
            this.cf_damage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_damage.Header = "Damage";
            this.cf_damage.Location = new System.Drawing.Point(3, 3);
            this.cf_damage.Name = "cf_damage";
            this.cf_damage.QueryName = null;
            this.cf_damage.Size = new System.Drawing.Size(292, 20);
            this.cf_damage.TabIndex = 0;
            // 
            // cf_attacksPerSecond
            // 
            this.cf_attacksPerSecond.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_attacksPerSecond.Header = "Attacks per Second";
            this.cf_attacksPerSecond.Location = new System.Drawing.Point(301, 3);
            this.cf_attacksPerSecond.Name = "cf_attacksPerSecond";
            this.cf_attacksPerSecond.QueryName = null;
            this.cf_attacksPerSecond.Size = new System.Drawing.Size(292, 20);
            this.cf_attacksPerSecond.TabIndex = 1;
            // 
            // cf_criticalChance
            // 
            this.cf_criticalChance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_criticalChance.Header = "Critical Chance";
            this.cf_criticalChance.Location = new System.Drawing.Point(3, 29);
            this.cf_criticalChance.Name = "cf_criticalChance";
            this.cf_criticalChance.QueryName = null;
            this.cf_criticalChance.Size = new System.Drawing.Size(292, 20);
            this.cf_criticalChance.TabIndex = 2;
            // 
            // cf_damagePerSecond
            // 
            this.cf_damagePerSecond.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_damagePerSecond.Header = "Damage per Second";
            this.cf_damagePerSecond.Location = new System.Drawing.Point(301, 29);
            this.cf_damagePerSecond.Name = "cf_damagePerSecond";
            this.cf_damagePerSecond.QueryName = null;
            this.cf_damagePerSecond.Size = new System.Drawing.Size(292, 20);
            this.cf_damagePerSecond.TabIndex = 3;
            // 
            // cf_physicalDPS
            // 
            this.cf_physicalDPS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_physicalDPS.Header = "Physical DPS";
            this.cf_physicalDPS.Location = new System.Drawing.Point(3, 55);
            this.cf_physicalDPS.Name = "cf_physicalDPS";
            this.cf_physicalDPS.QueryName = null;
            this.cf_physicalDPS.Size = new System.Drawing.Size(292, 20);
            this.cf_physicalDPS.TabIndex = 4;
            // 
            // cf_elementalDPS
            // 
            this.cf_elementalDPS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_elementalDPS.Header = "Elemental DPS";
            this.cf_elementalDPS.Location = new System.Drawing.Point(301, 55);
            this.cf_elementalDPS.Name = "cf_elementalDPS";
            this.cf_elementalDPS.QueryName = null;
            this.cf_elementalDPS.Size = new System.Drawing.Size(292, 20);
            this.cf_elementalDPS.TabIndex = 5;
            // 
            // ec_armourFilters
            // 
            this.ec_armourFilters.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            // 
            // ec_armourFilters.DropZone
            // 
            this.ec_armourFilters.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_armourFilters.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_armourFilters.DropZone.Controls.Add(this.flp_armourFilters);
            this.ec_armourFilters.DropZone.Location = new System.Drawing.Point(3, 19);
            this.ec_armourFilters.DropZone.Name = "DropZone";
            this.ec_armourFilters.DropZone.Size = new System.Drawing.Size(601, 57);
            this.ec_armourFilters.DropZone.TabIndex = 0;
            this.ec_armourFilters.Header = "Armour Filters";
            this.ec_armourFilters.HeaderHeight = 20;
            this.ec_armourFilters.Location = new System.Drawing.Point(18, 109);
            this.ec_armourFilters.Name = "ec_armourFilters";
            this.ec_armourFilters.Size = new System.Drawing.Size(607, 80);
            this.ec_armourFilters.SizeExpanded = new System.Drawing.Size(607, 80);
            this.ec_armourFilters.TabIndex = 6;
            // 
            // flp_armourFilters
            // 
            this.flp_armourFilters.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flp_armourFilters.Controls.Add(this.cf_armour);
            this.flp_armourFilters.Controls.Add(this.cf_block);
            this.flp_armourFilters.Controls.Add(this.cf_energyShield);
            this.flp_armourFilters.Controls.Add(this.cf_evasion);
            this.flp_armourFilters.Location = new System.Drawing.Point(0, 2);
            this.flp_armourFilters.Name = "flp_armourFilters";
            this.flp_armourFilters.Size = new System.Drawing.Size(601, 56);
            this.flp_armourFilters.TabIndex = 1;
            // 
            // cf_armour
            // 
            this.cf_armour.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_armour.Header = "Armour";
            this.cf_armour.Location = new System.Drawing.Point(3, 3);
            this.cf_armour.Name = "cf_armour";
            this.cf_armour.QueryName = null;
            this.cf_armour.Size = new System.Drawing.Size(292, 20);
            this.cf_armour.TabIndex = 0;
            // 
            // cf_block
            // 
            this.cf_block.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_block.Header = "Block";
            this.cf_block.Location = new System.Drawing.Point(301, 3);
            this.cf_block.Name = "cf_block";
            this.cf_block.QueryName = null;
            this.cf_block.Size = new System.Drawing.Size(292, 20);
            this.cf_block.TabIndex = 1;
            // 
            // cf_energyShield
            // 
            this.cf_energyShield.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_energyShield.Header = "Energy Shield";
            this.cf_energyShield.Location = new System.Drawing.Point(3, 29);
            this.cf_energyShield.Name = "cf_energyShield";
            this.cf_energyShield.QueryName = null;
            this.cf_energyShield.Size = new System.Drawing.Size(292, 20);
            this.cf_energyShield.TabIndex = 2;
            // 
            // cf_evasion
            // 
            this.cf_evasion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_evasion.Header = "Evasion";
            this.cf_evasion.Location = new System.Drawing.Point(301, 29);
            this.cf_evasion.Name = "cf_evasion";
            this.cf_evasion.QueryName = null;
            this.cf_evasion.Size = new System.Drawing.Size(292, 20);
            this.cf_evasion.TabIndex = 3;
            // 
            // ec_socketFilters
            // 
            this.ec_socketFilters.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            // 
            // ec_socketFilters.DropZone
            // 
            this.ec_socketFilters.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_socketFilters.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_socketFilters.DropZone.Controls.Add(this.flp_socketFilters);
            this.ec_socketFilters.DropZone.Location = new System.Drawing.Point(3, 21);
            this.ec_socketFilters.DropZone.Name = "DropZone";
            this.ec_socketFilters.DropZone.Size = new System.Drawing.Size(601, 56);
            this.ec_socketFilters.DropZone.TabIndex = 0;
            this.ec_socketFilters.Header = "Socket Filters";
            this.ec_socketFilters.HeaderHeight = 20;
            this.ec_socketFilters.Location = new System.Drawing.Point(18, 195);
            this.ec_socketFilters.Name = "ec_socketFilters";
            this.ec_socketFilters.Size = new System.Drawing.Size(607, 80);
            this.ec_socketFilters.SizeExpanded = new System.Drawing.Size(607, 80);
            this.ec_socketFilters.TabIndex = 6;
            // 
            // flp_socketFilters
            // 
            this.flp_socketFilters.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flp_socketFilters.Controls.Add(this.csf_sockets);
            this.flp_socketFilters.Controls.Add(this.csf_links);
            this.flp_socketFilters.Location = new System.Drawing.Point(0, 0);
            this.flp_socketFilters.Name = "flp_socketFilters";
            this.flp_socketFilters.Size = new System.Drawing.Size(598, 59);
            this.flp_socketFilters.TabIndex = 0;
            // 
            // csf_sockets
            // 
            this.csf_sockets.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.csf_sockets.Font = new System.Drawing.Font("Consolas", 8.25F);
            this.csf_sockets.Header = "Sockets";
            this.csf_sockets.Location = new System.Drawing.Point(3, 3);
            this.csf_sockets.Name = "csf_sockets";
            this.csf_sockets.Size = new System.Drawing.Size(590, 20);
            this.csf_sockets.TabIndex = 0;
            // 
            // csf_links
            // 
            this.csf_links.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.csf_links.Header = "Links";
            this.csf_links.Location = new System.Drawing.Point(3, 29);
            this.csf_links.Name = "csf_links";
            this.csf_links.Size = new System.Drawing.Size(590, 20);
            this.csf_links.TabIndex = 1;
            // 
            // ec_Requirements
            // 
            this.ec_Requirements.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            // 
            // ec_Requirements.DropZone
            // 
            this.ec_Requirements.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_Requirements.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_Requirements.DropZone.Controls.Add(this.flp_requirements);
            this.ec_Requirements.DropZone.Location = new System.Drawing.Point(3, 19);
            this.ec_Requirements.DropZone.Name = "DropZone";
            this.ec_Requirements.DropZone.Size = new System.Drawing.Size(601, 57);
            this.ec_Requirements.DropZone.TabIndex = 0;
            this.ec_Requirements.Header = "Requirements";
            this.ec_Requirements.HeaderHeight = 20;
            this.ec_Requirements.Location = new System.Drawing.Point(18, 281);
            this.ec_Requirements.Name = "ec_Requirements";
            this.ec_Requirements.Size = new System.Drawing.Size(607, 80);
            this.ec_Requirements.SizeExpanded = new System.Drawing.Size(607, 80);
            this.ec_Requirements.TabIndex = 7;
            // 
            // flp_requirements
            // 
            this.flp_requirements.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flp_requirements.Controls.Add(this.cf_level);
            this.flp_requirements.Controls.Add(this.cf_strength);
            this.flp_requirements.Controls.Add(this.cf_dexterity);
            this.flp_requirements.Controls.Add(this.cf_intelligence);
            this.flp_requirements.Location = new System.Drawing.Point(0, 3);
            this.flp_requirements.Name = "flp_requirements";
            this.flp_requirements.Size = new System.Drawing.Size(598, 58);
            this.flp_requirements.TabIndex = 2;
            // 
            // cf_level
            // 
            this.cf_level.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_level.Header = "Level";
            this.cf_level.Location = new System.Drawing.Point(3, 3);
            this.cf_level.Name = "cf_level";
            this.cf_level.QueryName = null;
            this.cf_level.Size = new System.Drawing.Size(292, 20);
            this.cf_level.TabIndex = 0;
            // 
            // cf_strength
            // 
            this.cf_strength.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_strength.Header = "Strength";
            this.cf_strength.Location = new System.Drawing.Point(301, 3);
            this.cf_strength.Name = "cf_strength";
            this.cf_strength.QueryName = null;
            this.cf_strength.Size = new System.Drawing.Size(292, 20);
            this.cf_strength.TabIndex = 1;
            // 
            // cf_dexterity
            // 
            this.cf_dexterity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_dexterity.Header = "Dexterity";
            this.cf_dexterity.Location = new System.Drawing.Point(3, 29);
            this.cf_dexterity.Name = "cf_dexterity";
            this.cf_dexterity.QueryName = null;
            this.cf_dexterity.Size = new System.Drawing.Size(292, 20);
            this.cf_dexterity.TabIndex = 2;
            // 
            // cf_intelligence
            // 
            this.cf_intelligence.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_intelligence.Header = "Intelligence";
            this.cf_intelligence.Location = new System.Drawing.Point(301, 29);
            this.cf_intelligence.Name = "cf_intelligence";
            this.cf_intelligence.QueryName = null;
            this.cf_intelligence.Size = new System.Drawing.Size(292, 20);
            this.cf_intelligence.TabIndex = 3;
            // 
            // ec_modFilters
            // 
            this.ec_modFilters.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            // 
            // ec_modFilters.DropZone
            // 
            this.ec_modFilters.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_modFilters.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_modFilters.DropZone.Controls.Add(this.flp_modFilters);
            this.ec_modFilters.DropZone.Location = new System.Drawing.Point(3, 21);
            this.ec_modFilters.DropZone.Name = "DropZone";
            this.ec_modFilters.DropZone.Size = new System.Drawing.Size(601, 26);
            this.ec_modFilters.DropZone.TabIndex = 0;
            this.ec_modFilters.Header = "Mod Filters";
            this.ec_modFilters.HeaderHeight = 20;
            this.ec_modFilters.Location = new System.Drawing.Point(18, 367);
            this.ec_modFilters.Name = "ec_modFilters";
            this.ec_modFilters.Size = new System.Drawing.Size(607, 50);
            this.ec_modFilters.SizeExpanded = new System.Drawing.Size(607, 50);
            this.ec_modFilters.TabIndex = 5;
            // 
            // flp_modFilters
            // 
            this.flp_modFilters.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flp_modFilters.Controls.Add(this.cf_emptyPrefixes);
            this.flp_modFilters.Controls.Add(this.cf_emptySuffixes);
            this.flp_modFilters.Location = new System.Drawing.Point(0, 0);
            this.flp_modFilters.Name = "flp_modFilters";
            this.flp_modFilters.Size = new System.Drawing.Size(601, 29);
            this.flp_modFilters.TabIndex = 0;
            // 
            // cf_emptyPrefixes
            // 
            this.cf_emptyPrefixes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_emptyPrefixes.Header = "Empty Prefixes";
            this.cf_emptyPrefixes.Location = new System.Drawing.Point(3, 3);
            this.cf_emptyPrefixes.Name = "cf_emptyPrefixes";
            this.cf_emptyPrefixes.QueryName = null;
            this.cf_emptyPrefixes.Size = new System.Drawing.Size(292, 20);
            this.cf_emptyPrefixes.TabIndex = 0;
            // 
            // cf_emptySuffixes
            // 
            this.cf_emptySuffixes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_emptySuffixes.Header = "Empty Suffixes";
            this.cf_emptySuffixes.Location = new System.Drawing.Point(301, 3);
            this.cf_emptySuffixes.Name = "cf_emptySuffixes";
            this.cf_emptySuffixes.QueryName = null;
            this.cf_emptySuffixes.Size = new System.Drawing.Size(292, 20);
            this.cf_emptySuffixes.TabIndex = 1;
            // 
            // ec_mods
            // 
            this.ec_mods.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            // 
            // ec_mods.DropZone
            // 
            this.ec_mods.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_mods.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_mods.DropZone.Controls.Add(this.cmb_modGroupType);
            this.ec_mods.DropZone.Controls.Add(this.btn_addModGroup);
            this.ec_mods.DropZone.Location = new System.Drawing.Point(3, 23);
            this.ec_mods.DropZone.Name = "DropZone";
            this.ec_mods.DropZone.Size = new System.Drawing.Size(601, 37);
            this.ec_mods.DropZone.TabIndex = 0;
            this.ec_mods.Header = "Mods";
            this.ec_mods.HeaderHeight = 20;
            this.ec_mods.Location = new System.Drawing.Point(18, 423);
            this.ec_mods.MinimumSize = new System.Drawing.Size(607, 0);
            this.ec_mods.Name = "ec_mods";
            this.ec_mods.Size = new System.Drawing.Size(607, 63);
            this.ec_mods.SizeExpanded = new System.Drawing.Size(607, 57);
            this.ec_mods.TabIndex = 0;
            // 
            // cmb_modGroupType
            // 
            this.cmb_modGroupType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmb_modGroupType.AutoComp = false;
            this.cmb_modGroupType.BorderColor = System.Drawing.Color.Black;
            this.cmb_modGroupType.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.cmb_modGroupType.ButtonColor = System.Drawing.SystemColors.Control;
            this.cmb_modGroupType.Data = null;
            this.cmb_modGroupType.DropDownColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cmb_modGroupType.ForeColorDrop = System.Drawing.Color.Empty;
            this.cmb_modGroupType.FormattingEnabled = true;
            this.cmb_modGroupType.Items.AddRange(new object[] {
            "And",
            "Not ",
            "If",
            "Count",
            "Weighted Sum"});
            this.cmb_modGroupType.Location = new System.Drawing.Point(0, 14);
            this.cmb_modGroupType.Name = "cmb_modGroupType";
            this.cmb_modGroupType.Size = new System.Drawing.Size(128, 21);
            this.cmb_modGroupType.TabIndex = 3;
            // 
            // btn_addModGroup
            // 
            this.btn_addModGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_addModGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.btn_addModGroup.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_addModGroup.FlatAppearance.BorderSize = 0;
            this.btn_addModGroup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_addModGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.btn_addModGroup.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_addModGroup.Location = new System.Drawing.Point(134, 14);
            this.btn_addModGroup.Name = "btn_addModGroup";
            this.btn_addModGroup.Size = new System.Drawing.Size(90, 21);
            this.btn_addModGroup.TabIndex = 2;
            this.btn_addModGroup.Text = "Add Mod Group";
            this.btn_addModGroup.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_addModGroup.UseVisualStyleBackColor = false;
            this.btn_addModGroup.Click += new System.EventHandler(this.btn_addModGroup_Click);
            // 
            // ec_mapFilters
            // 
            this.ec_mapFilters.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            // 
            // ec_mapFilters.DropZone
            // 
            this.ec_mapFilters.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_mapFilters.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_mapFilters.DropZone.Controls.Add(this.flp_mapFilters);
            this.ec_mapFilters.DropZone.Location = new System.Drawing.Point(3, 21);
            this.ec_mapFilters.DropZone.Name = "DropZone";
            this.ec_mapFilters.DropZone.Size = new System.Drawing.Size(601, 120);
            this.ec_mapFilters.DropZone.TabIndex = 0;
            this.ec_mapFilters.Header = "Map Filters";
            this.ec_mapFilters.HeaderHeight = 20;
            this.ec_mapFilters.Location = new System.Drawing.Point(18, 492);
            this.ec_mapFilters.Name = "ec_mapFilters";
            this.ec_mapFilters.Size = new System.Drawing.Size(607, 144);
            this.ec_mapFilters.SizeExpanded = new System.Drawing.Size(607, 144);
            this.ec_mapFilters.TabIndex = 6;
            // 
            // flp_mapFilters
            // 
            this.flp_mapFilters.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flp_mapFilters.Controls.Add(this.cf_mapTier);
            this.flp_mapFilters.Controls.Add(this.cf_mapPacksize);
            this.flp_mapFilters.Controls.Add(this.cf_mapIIQ);
            this.flp_mapFilters.Controls.Add(this.cf_mapIIR);
            this.flp_mapFilters.Controls.Add(this.cfdd_shaped);
            this.flp_mapFilters.Controls.Add(this.cfdd_elder);
            this.flp_mapFilters.Controls.Add(this.cfdd_mapSeries);
            this.flp_mapFilters.Location = new System.Drawing.Point(0, 0);
            this.flp_mapFilters.Name = "flp_mapFilters";
            this.flp_mapFilters.Size = new System.Drawing.Size(604, 120);
            this.flp_mapFilters.TabIndex = 0;
            // 
            // cf_mapTier
            // 
            this.cf_mapTier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_mapTier.Header = "Map Tier";
            this.cf_mapTier.Location = new System.Drawing.Point(3, 3);
            this.cf_mapTier.Name = "cf_mapTier";
            this.cf_mapTier.QueryName = null;
            this.cf_mapTier.Size = new System.Drawing.Size(292, 20);
            this.cf_mapTier.TabIndex = 0;
            // 
            // cf_mapPacksize
            // 
            this.cf_mapPacksize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_mapPacksize.Header = "Map Packsize";
            this.cf_mapPacksize.Location = new System.Drawing.Point(301, 3);
            this.cf_mapPacksize.Name = "cf_mapPacksize";
            this.cf_mapPacksize.QueryName = null;
            this.cf_mapPacksize.Size = new System.Drawing.Size(292, 20);
            this.cf_mapPacksize.TabIndex = 1;
            // 
            // cf_mapIIQ
            // 
            this.cf_mapIIQ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_mapIIQ.Header = "Map IIQ";
            this.cf_mapIIQ.Location = new System.Drawing.Point(3, 29);
            this.cf_mapIIQ.Name = "cf_mapIIQ";
            this.cf_mapIIQ.QueryName = null;
            this.cf_mapIIQ.Size = new System.Drawing.Size(292, 20);
            this.cf_mapIIQ.TabIndex = 2;
            // 
            // cf_mapIIR
            // 
            this.cf_mapIIR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_mapIIR.Header = "Map IIR";
            this.cf_mapIIR.Location = new System.Drawing.Point(301, 29);
            this.cf_mapIIR.Name = "cf_mapIIR";
            this.cf_mapIIR.QueryName = null;
            this.cf_mapIIR.Size = new System.Drawing.Size(292, 20);
            this.cf_mapIIR.TabIndex = 3;
            // 
            // cfdd_shaped
            // 
            this.cfdd_shaped.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.cfdd_shaped.Header = "Shaped Map";
            this.cfdd_shaped.Items = new string[] {
        "Any",
        "Yes",
        "No"};
            this.cfdd_shaped.Location = new System.Drawing.Point(3, 55);
            this.cfdd_shaped.Name = "cfdd_shaped";
            this.cfdd_shaped.Size = new System.Drawing.Size(292, 26);
            this.cfdd_shaped.TabIndex = 4;
            // 
            // cfdd_elder
            // 
            this.cfdd_elder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.cfdd_elder.Header = "Elder Map";
            this.cfdd_elder.Items = new string[] {
        "Any",
        "Yes",
        "No"};
            this.cfdd_elder.Location = new System.Drawing.Point(301, 55);
            this.cfdd_elder.Name = "cfdd_elder";
            this.cfdd_elder.Size = new System.Drawing.Size(292, 26);
            this.cfdd_elder.TabIndex = 5;
            // 
            // cfdd_mapSeries
            // 
            this.cfdd_mapSeries.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.cfdd_mapSeries.Header = "Map Series";
            this.cfdd_mapSeries.Items = new string[] {
        "Any",
        "War for the Atlas",
        "Atlas of Worls",
        "The Awakening",
        "Original"};
            this.cfdd_mapSeries.Location = new System.Drawing.Point(3, 87);
            this.cfdd_mapSeries.Name = "cfdd_mapSeries";
            this.cfdd_mapSeries.Size = new System.Drawing.Size(590, 26);
            this.cfdd_mapSeries.TabIndex = 6;
            // 
            // ec_miscellaneous
            // 
            this.ec_miscellaneous.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            // 
            // ec_miscellaneous.DropZone
            // 
            this.ec_miscellaneous.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_miscellaneous.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_miscellaneous.DropZone.Controls.Add(this.flp_miscFilters);
            this.ec_miscellaneous.DropZone.Location = new System.Drawing.Point(3, 21);
            this.ec_miscellaneous.DropZone.Name = "DropZone";
            this.ec_miscellaneous.DropZone.Size = new System.Drawing.Size(601, 210);
            this.ec_miscellaneous.DropZone.TabIndex = 0;
            this.ec_miscellaneous.Header = "Miscellaneous";
            this.ec_miscellaneous.HeaderHeight = 20;
            this.ec_miscellaneous.Location = new System.Drawing.Point(18, 642);
            this.ec_miscellaneous.Name = "ec_miscellaneous";
            this.ec_miscellaneous.Size = new System.Drawing.Size(607, 234);
            this.ec_miscellaneous.SizeExpanded = new System.Drawing.Size(607, 234);
            this.ec_miscellaneous.TabIndex = 7;
            // 
            // flp_miscFilters
            // 
            this.flp_miscFilters.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flp_miscFilters.Controls.Add(this.cf_quality);
            this.flp_miscFilters.Controls.Add(this.cf_itemLevel);
            this.flp_miscFilters.Controls.Add(this.cf_gemLevel);
            this.flp_miscFilters.Controls.Add(this.cf_gemExperience);
            this.flp_miscFilters.Controls.Add(this.cfdd_shaperItem);
            this.flp_miscFilters.Controls.Add(this.cfdd_elderItem);
            this.flp_miscFilters.Controls.Add(this.cfdd_alternateArt);
            this.flp_miscFilters.Controls.Add(this.cfdd_identified);
            this.flp_miscFilters.Controls.Add(this.cfdd_corrupted);
            this.flp_miscFilters.Controls.Add(this.cfdd_mirrored);
            this.flp_miscFilters.Controls.Add(this.cfdd_crafted);
            this.flp_miscFilters.Controls.Add(this.cfdd_veiled);
            this.flp_miscFilters.Controls.Add(this.cfdd_enchanted);
            this.flp_miscFilters.Controls.Add(this.cf_talismanTier);
            this.flp_miscFilters.Location = new System.Drawing.Point(0, 0);
            this.flp_miscFilters.Name = "flp_miscFilters";
            this.flp_miscFilters.Size = new System.Drawing.Size(604, 210);
            this.flp_miscFilters.TabIndex = 0;
            // 
            // cf_quality
            // 
            this.cf_quality.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_quality.Header = "Quality";
            this.cf_quality.Location = new System.Drawing.Point(3, 3);
            this.cf_quality.Name = "cf_quality";
            this.cf_quality.QueryName = "";
            this.cf_quality.Size = new System.Drawing.Size(292, 20);
            this.cf_quality.TabIndex = 0;
            // 
            // cf_itemLevel
            // 
            this.cf_itemLevel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_itemLevel.Header = "Item Level";
            this.cf_itemLevel.Location = new System.Drawing.Point(301, 3);
            this.cf_itemLevel.Name = "cf_itemLevel";
            this.cf_itemLevel.QueryName = null;
            this.cf_itemLevel.Size = new System.Drawing.Size(292, 20);
            this.cf_itemLevel.TabIndex = 1;
            // 
            // cf_gemLevel
            // 
            this.cf_gemLevel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_gemLevel.Header = "Gem Level";
            this.cf_gemLevel.Location = new System.Drawing.Point(3, 29);
            this.cf_gemLevel.Name = "cf_gemLevel";
            this.cf_gemLevel.QueryName = null;
            this.cf_gemLevel.Size = new System.Drawing.Size(292, 20);
            this.cf_gemLevel.TabIndex = 2;
            // 
            // cf_gemExperience
            // 
            this.cf_gemExperience.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_gemExperience.Header = "Gem Experience %";
            this.cf_gemExperience.Location = new System.Drawing.Point(301, 29);
            this.cf_gemExperience.Name = "cf_gemExperience";
            this.cf_gemExperience.QueryName = null;
            this.cf_gemExperience.Size = new System.Drawing.Size(292, 20);
            this.cf_gemExperience.TabIndex = 3;
            // 
            // cfdd_shaperItem
            // 
            this.cfdd_shaperItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.cfdd_shaperItem.Header = "Shaper Item";
            this.cfdd_shaperItem.Items = new string[] {
        "Any",
        "Yes",
        "No"};
            this.cfdd_shaperItem.Location = new System.Drawing.Point(3, 55);
            this.cfdd_shaperItem.Name = "cfdd_shaperItem";
            this.cfdd_shaperItem.Size = new System.Drawing.Size(292, 26);
            this.cfdd_shaperItem.TabIndex = 4;
            // 
            // cfdd_elderItem
            // 
            this.cfdd_elderItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.cfdd_elderItem.Header = "Elder Item";
            this.cfdd_elderItem.Items = new string[] {
        "Any",
        "Yes",
        "No"};
            this.cfdd_elderItem.Location = new System.Drawing.Point(301, 55);
            this.cfdd_elderItem.Name = "cfdd_elderItem";
            this.cfdd_elderItem.Size = new System.Drawing.Size(292, 26);
            this.cfdd_elderItem.TabIndex = 5;
            // 
            // cfdd_alternateArt
            // 
            this.cfdd_alternateArt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.cfdd_alternateArt.Header = "Alternate Art";
            this.cfdd_alternateArt.Items = new string[] {
        "Any",
        "Yes",
        "No"};
            this.cfdd_alternateArt.Location = new System.Drawing.Point(3, 87);
            this.cfdd_alternateArt.Name = "cfdd_alternateArt";
            this.cfdd_alternateArt.Size = new System.Drawing.Size(292, 26);
            this.cfdd_alternateArt.TabIndex = 6;
            // 
            // cfdd_identified
            // 
            this.cfdd_identified.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.cfdd_identified.Header = "Identified";
            this.cfdd_identified.Items = new string[] {
        "Any",
        "Yes",
        "No"};
            this.cfdd_identified.Location = new System.Drawing.Point(301, 87);
            this.cfdd_identified.Name = "cfdd_identified";
            this.cfdd_identified.Size = new System.Drawing.Size(292, 26);
            this.cfdd_identified.TabIndex = 7;
            // 
            // cfdd_corrupted
            // 
            this.cfdd_corrupted.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.cfdd_corrupted.Header = "Corrupted";
            this.cfdd_corrupted.Items = new string[] {
        "Any",
        "Yes",
        "No"};
            this.cfdd_corrupted.Location = new System.Drawing.Point(3, 119);
            this.cfdd_corrupted.Name = "cfdd_corrupted";
            this.cfdd_corrupted.Size = new System.Drawing.Size(292, 26);
            this.cfdd_corrupted.TabIndex = 8;
            // 
            // cfdd_mirrored
            // 
            this.cfdd_mirrored.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.cfdd_mirrored.Header = "Mirrored";
            this.cfdd_mirrored.Items = new string[] {
        "Any",
        "Yes",
        "No"};
            this.cfdd_mirrored.Location = new System.Drawing.Point(301, 119);
            this.cfdd_mirrored.Name = "cfdd_mirrored";
            this.cfdd_mirrored.Size = new System.Drawing.Size(292, 26);
            this.cfdd_mirrored.TabIndex = 11;
            // 
            // cfdd_crafted
            // 
            this.cfdd_crafted.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.cfdd_crafted.Header = "Crafted";
            this.cfdd_crafted.Items = new string[] {
        "Any",
        "Yes",
        "No"};
            this.cfdd_crafted.Location = new System.Drawing.Point(3, 151);
            this.cfdd_crafted.Name = "cfdd_crafted";
            this.cfdd_crafted.Size = new System.Drawing.Size(292, 26);
            this.cfdd_crafted.TabIndex = 9;
            // 
            // cfdd_veiled
            // 
            this.cfdd_veiled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.cfdd_veiled.Header = "Veiled";
            this.cfdd_veiled.Items = new string[] {
        "Any",
        "Yes",
        "No"};
            this.cfdd_veiled.Location = new System.Drawing.Point(301, 151);
            this.cfdd_veiled.Name = "cfdd_veiled";
            this.cfdd_veiled.Size = new System.Drawing.Size(292, 26);
            this.cfdd_veiled.TabIndex = 13;
            // 
            // cfdd_enchanted
            // 
            this.cfdd_enchanted.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.cfdd_enchanted.Header = "Enchanted";
            this.cfdd_enchanted.Items = new string[] {
        "Any",
        "Yes",
        "No"};
            this.cfdd_enchanted.Location = new System.Drawing.Point(3, 183);
            this.cfdd_enchanted.Name = "cfdd_enchanted";
            this.cfdd_enchanted.Size = new System.Drawing.Size(292, 26);
            this.cfdd_enchanted.TabIndex = 10;
            // 
            // cf_talismanTier
            // 
            this.cf_talismanTier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.cf_talismanTier.Header = "Talisman Tier";
            this.cf_talismanTier.Location = new System.Drawing.Point(301, 183);
            this.cf_talismanTier.Name = "cf_talismanTier";
            this.cf_talismanTier.QueryName = null;
            this.cf_talismanTier.Size = new System.Drawing.Size(292, 20);
            this.cf_talismanTier.TabIndex = 12;
            // 
            // ec_tradeFilters
            // 
            this.ec_tradeFilters.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            // 
            // ec_tradeFilters.DropZone
            // 
            this.ec_tradeFilters.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_tradeFilters.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_tradeFilters.DropZone.Controls.Add(this.flp_tradeFilters);
            this.ec_tradeFilters.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_tradeFilters.DropZone.Name = "DropZone";
            this.ec_tradeFilters.DropZone.Size = new System.Drawing.Size(601, 142);
            this.ec_tradeFilters.DropZone.TabIndex = 0;
            this.ec_tradeFilters.Header = "Trade Filters";
            this.ec_tradeFilters.HeaderHeight = 20;
            this.ec_tradeFilters.Location = new System.Drawing.Point(18, 882);
            this.ec_tradeFilters.Name = "ec_tradeFilters";
            this.ec_tradeFilters.Size = new System.Drawing.Size(607, 171);
            this.ec_tradeFilters.SizeExpanded = new System.Drawing.Size(607, 171);
            this.ec_tradeFilters.TabIndex = 9;
            // 
            // flp_tradeFilters
            // 
            this.flp_tradeFilters.Controls.Add(this.cftb_trader);
            this.flp_tradeFilters.Controls.Add(this.cfdd_listed);
            this.flp_tradeFilters.Controls.Add(this.cfdd_saleType);
            this.flp_tradeFilters.Controls.Add(this.cfby_buyout);
            this.flp_tradeFilters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flp_tradeFilters.Location = new System.Drawing.Point(0, 0);
            this.flp_tradeFilters.Name = "flp_tradeFilters";
            this.flp_tradeFilters.Size = new System.Drawing.Size(601, 142);
            this.flp_tradeFilters.TabIndex = 0;
            // 
            // cftb_trader
            // 
            this.cftb_trader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.cftb_trader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.cftb_trader.Header = "Seller Account";
            this.cftb_trader.Location = new System.Drawing.Point(3, 3);
            this.cftb_trader.Name = "cftb_trader";
            this.cftb_trader.Size = new System.Drawing.Size(590, 26);
            this.cftb_trader.TabIndex = 2;
            // 
            // cfdd_listed
            // 
            this.cfdd_listed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.cfdd_listed.Header = "Listed";
            this.cfdd_listed.Items = new string[] {
        "Any Time",
        "Up to a Day ago",
        "Up to 3 Days ago",
        "Up to a Week ago",
        "Up to 2 Weeks ago",
        "Up to 1 Month ago",
        "Up to 2 Monts ago"};
            this.cfdd_listed.Location = new System.Drawing.Point(3, 35);
            this.cfdd_listed.Name = "cfdd_listed";
            this.cfdd_listed.Size = new System.Drawing.Size(590, 26);
            this.cfdd_listed.TabIndex = 0;
            // 
            // cfdd_saleType
            // 
            this.cfdd_saleType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.cfdd_saleType.Header = "Sale Type";
            this.cfdd_saleType.Items = new string[] {
        "Any",
        "Buyout or Fixed Price",
        "No listed Price"};
            this.cfdd_saleType.Location = new System.Drawing.Point(3, 67);
            this.cfdd_saleType.Name = "cfdd_saleType";
            this.cfdd_saleType.Size = new System.Drawing.Size(590, 26);
            this.cfdd_saleType.TabIndex = 1;
            // 
            // cfby_buyout
            // 
            this.cfby_buyout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.cfby_buyout.Header = "Buyout Price";
            this.cfby_buyout.Location = new System.Drawing.Point(3, 99);
            this.cfby_buyout.Name = "cfby_buyout";
            this.cfby_buyout.Size = new System.Drawing.Size(590, 26);
            this.cfby_buyout.TabIndex = 3;
            // 
            // lbl_itemRarity
            // 
            this.lbl_itemRarity.AutoSize = true;
            this.lbl_itemRarity.BackColor = System.Drawing.Color.Transparent;
            this.lbl_itemRarity.Font = new System.Drawing.Font("Consolas", 8F);
            this.lbl_itemRarity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_itemRarity.Location = new System.Drawing.Point(258, 8);
            this.lbl_itemRarity.Name = "lbl_itemRarity";
            this.lbl_itemRarity.Size = new System.Drawing.Size(73, 13);
            this.lbl_itemRarity.TabIndex = 10;
            this.lbl_itemRarity.Text = "Item Rarity";
            // 
            // tb_itemName
            // 
            this.tb_itemName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.tb_itemName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_itemName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tb_itemName.ForeColor = System.Drawing.Color.White;
            this.tb_itemName.Location = new System.Drawing.Point(99, 32);
            this.tb_itemName.Name = "tb_itemName";
            this.tb_itemName.Size = new System.Drawing.Size(561, 16);
            this.tb_itemName.TabIndex = 16;
            this.tb_itemName.TextChanged += new System.EventHandler(this.search_tb_itemName_TextChanged);
            this.tb_itemName.Leave += new System.EventHandler(this.search_tb_itemName_Leave);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.tp_search);
            this.tabControl1.Controls.Add(this.tp_bulk);
            this.tabControl1.Font = new System.Drawing.Font("Consolas", 8F);
            this.tabControl1.HotTrack = true;
            this.tabControl1.Location = new System.Drawing.Point(0, 20);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(678, 689);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 23;
            // 
            // tp_bulk
            // 
            this.tp_bulk.BackColor = System.Drawing.Color.Black;
            this.tp_bulk.Controls.Add(this.fc_bulkAccStatus);
            this.tp_bulk.Controls.Add(this.label1);
            this.tp_bulk.Controls.Add(this.lbl_want);
            this.tp_bulk.Controls.Add(this.lbl_have);
            this.tp_bulk.Controls.Add(this.flp_have);
            this.tp_bulk.Controls.Add(this.flp_want);
            this.tp_bulk.Controls.Add(this.lbl_Filter);
            this.tp_bulk.Controls.Add(this.tb_filter);
            this.tp_bulk.Controls.Add(this.lbl_minimumStock);
            this.tp_bulk.Controls.Add(this.tb_BulkMinimumStock);
            this.tp_bulk.Controls.Add(this.lbl_sellerAccount);
            this.tp_bulk.Controls.Add(this.tb_bulkSellerAccount);
            this.tp_bulk.Location = new System.Drawing.Point(4, 25);
            this.tp_bulk.Name = "tp_bulk";
            this.tp_bulk.Padding = new System.Windows.Forms.Padding(3);
            this.tp_bulk.Size = new System.Drawing.Size(670, 660);
            this.tp_bulk.TabIndex = 1;
            this.tp_bulk.Text = "Bulk Search";
            // 
            // fc_bulkAccStatus
            // 
            this.fc_bulkAccStatus.AutoComp = false;
            this.fc_bulkAccStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.fc_bulkAccStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.fc_bulkAccStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.fc_bulkAccStatus.BorderColor = System.Drawing.Color.Violet;
            this.fc_bulkAccStatus.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.fc_bulkAccStatus.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.fc_bulkAccStatus.Data = null;
            this.fc_bulkAccStatus.DropDownColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.fc_bulkAccStatus.ForeColorDrop = System.Drawing.Color.Empty;
            this.fc_bulkAccStatus.FormattingEnabled = true;
            this.fc_bulkAccStatus.Items.AddRange(new object[] {
            "Any",
            "Online only"});
            this.fc_bulkAccStatus.Location = new System.Drawing.Point(565, 6);
            this.fc_bulkAccStatus.Name = "fc_bulkAccStatus";
            this.fc_bulkAccStatus.Size = new System.Drawing.Size(111, 21);
            this.fc_bulkAccStatus.TabIndex = 26;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.label1.Location = new System.Drawing.Point(468, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Account status";
            // 
            // lbl_want
            // 
            this.lbl_want.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lbl_want.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_want.Location = new System.Drawing.Point(203, 25);
            this.lbl_want.Name = "lbl_want";
            this.lbl_want.Size = new System.Drawing.Size(249, 23);
            this.lbl_want.TabIndex = 3;
            this.lbl_want.Text = " Items I Want";
            this.lbl_want.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_have
            // 
            this.lbl_have.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lbl_have.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_have.Location = new System.Drawing.Point(670, 25);
            this.lbl_have.Name = "lbl_have";
            this.lbl_have.Size = new System.Drawing.Size(673, 23);
            this.lbl_have.TabIndex = 4;
            this.lbl_have.Text = " Items I Have";
            this.lbl_have.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flp_have
            // 
            this.flp_have.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.flp_have.AutoScroll = true;
            this.flp_have.BackColor = System.Drawing.Color.Black;
            this.flp_have.Controls.Add(this.ec_haveCurrency);
            this.flp_have.Controls.Add(this.ec_haveFrags);
            this.flp_have.Controls.Add(this.ec_haveResonators);
            this.flp_have.Controls.Add(this.ec_haveFossils);
            this.flp_have.Controls.Add(this.ec_haveEssences);
            this.flp_have.Controls.Add(this.ec_haveCards);
            this.flp_have.Controls.Add(this.ec_haveMaps);
            this.flp_have.Controls.Add(this.ec_haveShapedMap);
            this.flp_have.Controls.Add(this.ec_haveElderMap);
            this.flp_have.Location = new System.Drawing.Point(670, 51);
            this.flp_have.MaximumSize = new System.Drawing.Size(670, 0);
            this.flp_have.MinimumSize = new System.Drawing.Size(660, 600);
            this.flp_have.Name = "flp_have";
            this.flp_have.Size = new System.Drawing.Size(660, 600);
            this.flp_have.TabIndex = 1;
            // 
            // ec_haveCurrency
            // 
            this.ec_haveCurrency.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_haveCurrency.DropZone
            // 
            this.ec_haveCurrency.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_haveCurrency.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_haveCurrency.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_haveCurrency.DropZone.Name = "DropZone";
            this.ec_haveCurrency.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_haveCurrency.DropZone.TabIndex = 0;
            this.ec_haveCurrency.Header = "Currency";
            this.ec_haveCurrency.HeaderHeight = 20;
            this.ec_haveCurrency.Location = new System.Drawing.Point(3, 3);
            this.ec_haveCurrency.Name = "ec_haveCurrency";
            this.ec_haveCurrency.Size = new System.Drawing.Size(635, 100);
            this.ec_haveCurrency.SizeExpanded = new System.Drawing.Size(635, 206);
            this.ec_haveCurrency.TabIndex = 3;
            // 
            // ec_haveFrags
            // 
            this.ec_haveFrags.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_haveFrags.DropZone
            // 
            this.ec_haveFrags.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_haveFrags.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_haveFrags.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_haveFrags.DropZone.Name = "DropZone";
            this.ec_haveFrags.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_haveFrags.DropZone.TabIndex = 0;
            this.ec_haveFrags.Header = "Fragments and Sets";
            this.ec_haveFrags.HeaderHeight = 20;
            this.ec_haveFrags.Location = new System.Drawing.Point(3, 109);
            this.ec_haveFrags.Name = "ec_haveFrags";
            this.ec_haveFrags.Size = new System.Drawing.Size(635, 100);
            this.ec_haveFrags.SizeExpanded = new System.Drawing.Size(635, 124);
            this.ec_haveFrags.TabIndex = 4;
            // 
            // ec_haveResonators
            // 
            this.ec_haveResonators.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_haveResonators.DropZone
            // 
            this.ec_haveResonators.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_haveResonators.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_haveResonators.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_haveResonators.DropZone.Name = "DropZone";
            this.ec_haveResonators.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_haveResonators.DropZone.TabIndex = 0;
            this.ec_haveResonators.Header = "Delve Resonators";
            this.ec_haveResonators.HeaderHeight = 20;
            this.ec_haveResonators.Location = new System.Drawing.Point(3, 215);
            this.ec_haveResonators.Name = "ec_haveResonators";
            this.ec_haveResonators.Size = new System.Drawing.Size(635, 100);
            this.ec_haveResonators.SizeExpanded = new System.Drawing.Size(635, 74);
            this.ec_haveResonators.TabIndex = 5;
            // 
            // ec_haveFossils
            // 
            this.ec_haveFossils.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_haveFossils.DropZone
            // 
            this.ec_haveFossils.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_haveFossils.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_haveFossils.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_haveFossils.DropZone.Name = "DropZone";
            this.ec_haveFossils.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_haveFossils.DropZone.TabIndex = 0;
            this.ec_haveFossils.Header = "Delve Fossils";
            this.ec_haveFossils.HeaderHeight = 20;
            this.ec_haveFossils.Location = new System.Drawing.Point(3, 321);
            this.ec_haveFossils.Name = "ec_haveFossils";
            this.ec_haveFossils.Size = new System.Drawing.Size(635, 100);
            this.ec_haveFossils.SizeExpanded = new System.Drawing.Size(635, 124);
            this.ec_haveFossils.TabIndex = 6;
            // 
            // ec_haveEssences
            // 
            this.ec_haveEssences.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_haveEssences.DropZone
            // 
            this.ec_haveEssences.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_haveEssences.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_haveEssences.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_haveEssences.DropZone.Name = "DropZone";
            this.ec_haveEssences.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_haveEssences.DropZone.TabIndex = 0;
            this.ec_haveEssences.Header = "Essences";
            this.ec_haveEssences.HeaderHeight = 20;
            this.ec_haveEssences.Location = new System.Drawing.Point(3, 427);
            this.ec_haveEssences.Name = "ec_haveEssences";
            this.ec_haveEssences.Size = new System.Drawing.Size(635, 100);
            this.ec_haveEssences.SizeExpanded = new System.Drawing.Size(635, 384);
            this.ec_haveEssences.TabIndex = 7;
            // 
            // ec_haveCards
            // 
            this.ec_haveCards.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_haveCards.DropZone
            // 
            this.ec_haveCards.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_haveCards.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_haveCards.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_haveCards.DropZone.Name = "DropZone";
            this.ec_haveCards.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_haveCards.DropZone.TabIndex = 0;
            this.ec_haveCards.Header = "Cards";
            this.ec_haveCards.HeaderHeight = 20;
            this.ec_haveCards.Location = new System.Drawing.Point(3, 533);
            this.ec_haveCards.Name = "ec_haveCards";
            this.ec_haveCards.Size = new System.Drawing.Size(635, 100);
            this.ec_haveCards.SizeExpanded = new System.Drawing.Size(635, 1104);
            this.ec_haveCards.TabIndex = 8;
            // 
            // ec_haveMaps
            // 
            this.ec_haveMaps.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_haveMaps.DropZone
            // 
            this.ec_haveMaps.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_haveMaps.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_haveMaps.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_haveMaps.DropZone.Name = "DropZone";
            this.ec_haveMaps.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_haveMaps.DropZone.TabIndex = 0;
            this.ec_haveMaps.Header = "Maps";
            this.ec_haveMaps.HeaderHeight = 20;
            this.ec_haveMaps.Location = new System.Drawing.Point(3, 639);
            this.ec_haveMaps.Name = "ec_haveMaps";
            this.ec_haveMaps.Size = new System.Drawing.Size(635, 100);
            this.ec_haveMaps.SizeExpanded = new System.Drawing.Size(635, 704);
            this.ec_haveMaps.TabIndex = 9;
            // 
            // ec_haveShapedMap
            // 
            this.ec_haveShapedMap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_haveShapedMap.DropZone
            // 
            this.ec_haveShapedMap.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_haveShapedMap.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_haveShapedMap.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_haveShapedMap.DropZone.Name = "DropZone";
            this.ec_haveShapedMap.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_haveShapedMap.DropZone.TabIndex = 0;
            this.ec_haveShapedMap.Header = "Shaped Maps";
            this.ec_haveShapedMap.HeaderHeight = 20;
            this.ec_haveShapedMap.Location = new System.Drawing.Point(3, 745);
            this.ec_haveShapedMap.Name = "ec_haveShapedMap";
            this.ec_haveShapedMap.Size = new System.Drawing.Size(635, 100);
            this.ec_haveShapedMap.SizeExpanded = new System.Drawing.Size(635, 704);
            this.ec_haveShapedMap.TabIndex = 10;
            // 
            // ec_haveElderMap
            // 
            this.ec_haveElderMap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_haveElderMap.DropZone
            // 
            this.ec_haveElderMap.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_haveElderMap.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_haveElderMap.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_haveElderMap.DropZone.Name = "DropZone";
            this.ec_haveElderMap.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_haveElderMap.DropZone.TabIndex = 0;
            this.ec_haveElderMap.Header = "Elder Maps";
            this.ec_haveElderMap.HeaderHeight = 20;
            this.ec_haveElderMap.Location = new System.Drawing.Point(3, 851);
            this.ec_haveElderMap.Name = "ec_haveElderMap";
            this.ec_haveElderMap.Size = new System.Drawing.Size(635, 100);
            this.ec_haveElderMap.SizeExpanded = new System.Drawing.Size(635, 904);
            this.ec_haveElderMap.TabIndex = 11;
            // 
            // flp_want
            // 
            this.flp_want.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.flp_want.AutoScroll = true;
            this.flp_want.BackColor = System.Drawing.Color.Black;
            this.flp_want.Controls.Add(this.ec_wantCurrency);
            this.flp_want.Controls.Add(this.ec_wantFrags);
            this.flp_want.Controls.Add(this.ec_wantResonators);
            this.flp_want.Controls.Add(this.ec_wantFossils);
            this.flp_want.Controls.Add(this.ec_wantEssences);
            this.flp_want.Controls.Add(this.ec_wantCards);
            this.flp_want.Controls.Add(this.ec_wantMaps);
            this.flp_want.Controls.Add(this.ec_wantShapedMap);
            this.flp_want.Controls.Add(this.ec_wantElderMap);
            this.flp_want.Location = new System.Drawing.Point(6, 51);
            this.flp_want.MaximumSize = new System.Drawing.Size(670, 0);
            this.flp_want.MinimumSize = new System.Drawing.Size(660, 600);
            this.flp_want.Name = "flp_want";
            this.flp_want.Size = new System.Drawing.Size(661, 600);
            this.flp_want.TabIndex = 0;
            // 
            // ec_wantCurrency
            // 
            this.ec_wantCurrency.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_wantCurrency.DropZone
            // 
            this.ec_wantCurrency.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_wantCurrency.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_wantCurrency.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_wantCurrency.DropZone.Name = "DropZone";
            this.ec_wantCurrency.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_wantCurrency.DropZone.TabIndex = 0;
            this.ec_wantCurrency.Header = "Currency";
            this.ec_wantCurrency.HeaderHeight = 20;
            this.ec_wantCurrency.Location = new System.Drawing.Point(3, 3);
            this.ec_wantCurrency.Name = "ec_wantCurrency";
            this.ec_wantCurrency.Size = new System.Drawing.Size(635, 100);
            this.ec_wantCurrency.SizeExpanded = new System.Drawing.Size(635, 206);
            this.ec_wantCurrency.TabIndex = 0;
            // 
            // ec_wantFrags
            // 
            this.ec_wantFrags.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_wantFrags.DropZone
            // 
            this.ec_wantFrags.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_wantFrags.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_wantFrags.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_wantFrags.DropZone.Name = "DropZone";
            this.ec_wantFrags.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_wantFrags.DropZone.TabIndex = 0;
            this.ec_wantFrags.Header = "Fragments and Sets";
            this.ec_wantFrags.HeaderHeight = 20;
            this.ec_wantFrags.Location = new System.Drawing.Point(3, 109);
            this.ec_wantFrags.Name = "ec_wantFrags";
            this.ec_wantFrags.Size = new System.Drawing.Size(635, 100);
            this.ec_wantFrags.SizeExpanded = new System.Drawing.Size(635, 124);
            this.ec_wantFrags.TabIndex = 1;
            // 
            // ec_wantResonators
            // 
            this.ec_wantResonators.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_wantResonators.DropZone
            // 
            this.ec_wantResonators.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_wantResonators.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_wantResonators.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_wantResonators.DropZone.Name = "DropZone";
            this.ec_wantResonators.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_wantResonators.DropZone.TabIndex = 0;
            this.ec_wantResonators.Header = "Delve Resonators";
            this.ec_wantResonators.HeaderHeight = 20;
            this.ec_wantResonators.Location = new System.Drawing.Point(3, 215);
            this.ec_wantResonators.Name = "ec_wantResonators";
            this.ec_wantResonators.Size = new System.Drawing.Size(635, 100);
            this.ec_wantResonators.SizeExpanded = new System.Drawing.Size(635, 74);
            this.ec_wantResonators.TabIndex = 2;
            // 
            // ec_wantFossils
            // 
            this.ec_wantFossils.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_wantFossils.DropZone
            // 
            this.ec_wantFossils.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_wantFossils.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_wantFossils.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_wantFossils.DropZone.Name = "DropZone";
            this.ec_wantFossils.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_wantFossils.DropZone.TabIndex = 0;
            this.ec_wantFossils.Header = "Delve Fossils";
            this.ec_wantFossils.HeaderHeight = 20;
            this.ec_wantFossils.Location = new System.Drawing.Point(3, 321);
            this.ec_wantFossils.Name = "ec_wantFossils";
            this.ec_wantFossils.Size = new System.Drawing.Size(635, 100);
            this.ec_wantFossils.SizeExpanded = new System.Drawing.Size(635, 124);
            this.ec_wantFossils.TabIndex = 3;
            // 
            // ec_wantEssences
            // 
            this.ec_wantEssences.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_wantEssences.DropZone
            // 
            this.ec_wantEssences.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_wantEssences.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_wantEssences.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_wantEssences.DropZone.Name = "DropZone";
            this.ec_wantEssences.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_wantEssences.DropZone.TabIndex = 0;
            this.ec_wantEssences.Header = "Essences";
            this.ec_wantEssences.HeaderHeight = 20;
            this.ec_wantEssences.Location = new System.Drawing.Point(3, 427);
            this.ec_wantEssences.Name = "ec_wantEssences";
            this.ec_wantEssences.Size = new System.Drawing.Size(635, 100);
            this.ec_wantEssences.SizeExpanded = new System.Drawing.Size(635, 384);
            this.ec_wantEssences.TabIndex = 4;
            // 
            // ec_wantCards
            // 
            this.ec_wantCards.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_wantCards.DropZone
            // 
            this.ec_wantCards.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_wantCards.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_wantCards.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_wantCards.DropZone.Name = "DropZone";
            this.ec_wantCards.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_wantCards.DropZone.TabIndex = 0;
            this.ec_wantCards.Header = "Cards";
            this.ec_wantCards.HeaderHeight = 20;
            this.ec_wantCards.Location = new System.Drawing.Point(3, 533);
            this.ec_wantCards.Name = "ec_wantCards";
            this.ec_wantCards.Size = new System.Drawing.Size(635, 100);
            this.ec_wantCards.SizeExpanded = new System.Drawing.Size(635, 1104);
            this.ec_wantCards.TabIndex = 5;
            // 
            // ec_wantMaps
            // 
            this.ec_wantMaps.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_wantMaps.DropZone
            // 
            this.ec_wantMaps.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_wantMaps.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_wantMaps.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_wantMaps.DropZone.Name = "DropZone";
            this.ec_wantMaps.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_wantMaps.DropZone.TabIndex = 0;
            this.ec_wantMaps.Header = "Maps";
            this.ec_wantMaps.HeaderHeight = 20;
            this.ec_wantMaps.Location = new System.Drawing.Point(3, 639);
            this.ec_wantMaps.Name = "ec_wantMaps";
            this.ec_wantMaps.Size = new System.Drawing.Size(635, 100);
            this.ec_wantMaps.SizeExpanded = new System.Drawing.Size(635, 704);
            this.ec_wantMaps.TabIndex = 6;
            // 
            // ec_wantShapedMap
            // 
            this.ec_wantShapedMap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_wantShapedMap.DropZone
            // 
            this.ec_wantShapedMap.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_wantShapedMap.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_wantShapedMap.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_wantShapedMap.DropZone.Name = "DropZone";
            this.ec_wantShapedMap.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_wantShapedMap.DropZone.TabIndex = 0;
            this.ec_wantShapedMap.Header = "Shaped Maps";
            this.ec_wantShapedMap.HeaderHeight = 20;
            this.ec_wantShapedMap.Location = new System.Drawing.Point(3, 745);
            this.ec_wantShapedMap.Name = "ec_wantShapedMap";
            this.ec_wantShapedMap.Size = new System.Drawing.Size(635, 100);
            this.ec_wantShapedMap.SizeExpanded = new System.Drawing.Size(635, 704);
            this.ec_wantShapedMap.TabIndex = 7;
            // 
            // ec_wantElderMap
            // 
            this.ec_wantElderMap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // ec_wantElderMap.DropZone
            // 
            this.ec_wantElderMap.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ec_wantElderMap.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.ec_wantElderMap.DropZone.Location = new System.Drawing.Point(3, 25);
            this.ec_wantElderMap.DropZone.Name = "DropZone";
            this.ec_wantElderMap.DropZone.Size = new System.Drawing.Size(629, 71);
            this.ec_wantElderMap.DropZone.TabIndex = 0;
            this.ec_wantElderMap.Header = "Elder Maps";
            this.ec_wantElderMap.HeaderHeight = 20;
            this.ec_wantElderMap.Location = new System.Drawing.Point(3, 851);
            this.ec_wantElderMap.Name = "ec_wantElderMap";
            this.ec_wantElderMap.Size = new System.Drawing.Size(635, 100);
            this.ec_wantElderMap.SizeExpanded = new System.Drawing.Size(635, 904);
            this.ec_wantElderMap.TabIndex = 8;
            // 
            // lbl_Filter
            // 
            this.lbl_Filter.AutoSize = true;
            this.lbl_Filter.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Filter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_Filter.Location = new System.Drawing.Point(250, 8);
            this.lbl_Filter.Name = "lbl_Filter";
            this.lbl_Filter.Size = new System.Drawing.Size(43, 13);
            this.lbl_Filter.TabIndex = 24;
            this.lbl_Filter.Text = "Filter";
            // 
            // tb_filter
            // 
            this.tb_filter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.tb_filter.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_filter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tb_filter.ForeColor = System.Drawing.Color.White;
            this.tb_filter.Location = new System.Drawing.Point(299, 6);
            this.tb_filter.Name = "tb_filter";
            this.tb_filter.Size = new System.Drawing.Size(163, 16);
            this.tb_filter.TabIndex = 23;
            this.tb_filter.TextChanged += new System.EventHandler(this.tb_filter_TextChanged);
            // 
            // lbl_minimumStock
            // 
            this.lbl_minimumStock.AutoSize = true;
            this.lbl_minimumStock.BackColor = System.Drawing.Color.Transparent;
            this.lbl_minimumStock.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_minimumStock.Location = new System.Drawing.Point(9, 31);
            this.lbl_minimumStock.Name = "lbl_minimumStock";
            this.lbl_minimumStock.Size = new System.Drawing.Size(85, 13);
            this.lbl_minimumStock.TabIndex = 21;
            this.lbl_minimumStock.Text = "Minimum Stock";
            // 
            // tb_BulkMinimumStock
            // 
            this.tb_BulkMinimumStock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.tb_BulkMinimumStock.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_BulkMinimumStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tb_BulkMinimumStock.ForeColor = System.Drawing.Color.White;
            this.tb_BulkMinimumStock.Location = new System.Drawing.Point(106, 28);
            this.tb_BulkMinimumStock.Name = "tb_BulkMinimumStock";
            this.tb_BulkMinimumStock.Size = new System.Drawing.Size(47, 16);
            this.tb_BulkMinimumStock.TabIndex = 20;
            // 
            // lbl_sellerAccount
            // 
            this.lbl_sellerAccount.AutoSize = true;
            this.lbl_sellerAccount.BackColor = System.Drawing.Color.Transparent;
            this.lbl_sellerAccount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_sellerAccount.Location = new System.Drawing.Point(9, 8);
            this.lbl_sellerAccount.Name = "lbl_sellerAccount";
            this.lbl_sellerAccount.Size = new System.Drawing.Size(91, 13);
            this.lbl_sellerAccount.TabIndex = 19;
            this.lbl_sellerAccount.Text = "Seller Account";
            // 
            // tb_bulkSellerAccount
            // 
            this.tb_bulkSellerAccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.tb_bulkSellerAccount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_bulkSellerAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tb_bulkSellerAccount.ForeColor = System.Drawing.Color.White;
            this.tb_bulkSellerAccount.Location = new System.Drawing.Point(106, 6);
            this.tb_bulkSellerAccount.Name = "tb_bulkSellerAccount";
            this.tb_bulkSellerAccount.Size = new System.Drawing.Size(138, 16);
            this.tb_bulkSellerAccount.TabIndex = 18;
            // 
            // btn_closeTab
            // 
            this.btn_closeTab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_closeTab.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.btn_closeTab.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_closeTab.FlatAppearance.BorderSize = 0;
            this.btn_closeTab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_closeTab.Font = new System.Drawing.Font("Consolas", 9F);
            this.btn_closeTab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_closeTab.Location = new System.Drawing.Point(228, 718);
            this.btn_closeTab.Name = "btn_closeTab";
            this.btn_closeTab.Size = new System.Drawing.Size(457, 20);
            this.btn_closeTab.TabIndex = 24;
            this.btn_closeTab.Text = "Close Page";
            this.btn_closeTab.UseVisualStyleBackColor = false;
            this.btn_closeTab.Click += new System.EventHandler(this.btn_closeTab_Click);
            // 
            // btn_settings
            // 
            this.btn_settings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_settings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.btn_settings.BackgroundImage = global::PoE_Trade_Overlay.Properties.Resources.cog;
            this.btn_settings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_settings.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_settings.FlatAppearance.BorderSize = 0;
            this.btn_settings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_settings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_settings.Location = new System.Drawing.Point(671, 2);
            this.btn_settings.Name = "btn_settings";
            this.btn_settings.Size = new System.Drawing.Size(16, 16);
            this.btn_settings.TabIndex = 29;
            this.btn_settings.UseVisualStyleBackColor = false;
            this.btn_settings.Click += new System.EventHandler(this.btn_settings_Click);
            // 
            // btn_twoColumns
            // 
            this.btn_twoColumns.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_twoColumns.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.btn_twoColumns.BackgroundImage = global::PoE_Trade_Overlay.Properties.Resources.format_twoColums_small;
            this.btn_twoColumns.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_twoColumns.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_twoColumns.FlatAppearance.BorderSize = 0;
            this.btn_twoColumns.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_twoColumns.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_twoColumns.Location = new System.Drawing.Point(649, 2);
            this.btn_twoColumns.Name = "btn_twoColumns";
            this.btn_twoColumns.Size = new System.Drawing.Size(16, 16);
            this.btn_twoColumns.TabIndex = 28;
            this.btn_twoColumns.UseVisualStyleBackColor = false;
            this.btn_twoColumns.Click += new System.EventHandler(this.btn_twoColumns_Click);
            // 
            // btn_defaultLayout
            // 
            this.btn_defaultLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_defaultLayout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.btn_defaultLayout.BackgroundImage = global::PoE_Trade_Overlay.Properties.Resources.format_default_small;
            this.btn_defaultLayout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_defaultLayout.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_defaultLayout.FlatAppearance.BorderSize = 0;
            this.btn_defaultLayout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_defaultLayout.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_defaultLayout.Location = new System.Drawing.Point(627, 2);
            this.btn_defaultLayout.Name = "btn_defaultLayout";
            this.btn_defaultLayout.Size = new System.Drawing.Size(16, 16);
            this.btn_defaultLayout.TabIndex = 27;
            this.btn_defaultLayout.UseVisualStyleBackColor = false;
            this.btn_defaultLayout.Click += new System.EventHandler(this.btn_defaultLayout_Click);
            // 
            // pb_vertical
            // 
            this.pb_vertical.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pb_vertical.BackColor = System.Drawing.Color.Black;
            this.pb_vertical.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.pb_vertical.Location = new System.Drawing.Point(685, 0);
            this.pb_vertical.Name = "pb_vertical";
            this.pb_vertical.Size = new System.Drawing.Size(5, 751);
            this.pb_vertical.TabIndex = 26;
            this.pb_vertical.TabStop = false;
            this.pb_vertical.MouseDown += new System.Windows.Forms.MouseEventHandler(this.resizeBar_MouseDown);
            this.pb_vertical.MouseMove += new System.Windows.Forms.MouseEventHandler(this.resizeBar_MouseMove);
            this.pb_vertical.MouseUp += new System.Windows.Forms.MouseEventHandler(this.resizeBar_MouseUp);
            // 
            // pb_horizontal
            // 
            this.pb_horizontal.Cursor = System.Windows.Forms.Cursors.SizeNS;
            this.pb_horizontal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pb_horizontal.Location = new System.Drawing.Point(0, 738);
            this.pb_horizontal.Name = "pb_horizontal";
            this.pb_horizontal.Size = new System.Drawing.Size(690, 5);
            this.pb_horizontal.TabIndex = 25;
            this.pb_horizontal.TabStop = false;
            this.pb_horizontal.MouseDown += new System.Windows.Forms.MouseEventHandler(this.resizeBar_MouseDown);
            this.pb_horizontal.MouseMove += new System.Windows.Forms.MouseEventHandler(this.resizeBar_MouseMove);
            this.pb_horizontal.MouseUp += new System.Windows.Forms.MouseEventHandler(this.resizeBar_MouseUp);
            // 
            // Form_Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(690, 743);
            this.Controls.Add(this.btn_settings);
            this.Controls.Add(this.btn_twoColumns);
            this.Controls.Add(this.btn_defaultLayout);
            this.Controls.Add(this.pb_horizontal);
            this.Controls.Add(this.btn_closeTab);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btn_reset);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.lbl_header);
            this.Controls.Add(this.btn_search);
            this.Controls.Add(this.pb_vertical);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimumSize = new System.Drawing.Size(690, 360);
            this.Name = "Form_Search";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Form_Search_Load);
            this.tp_search.ResumeLayout(false);
            this.tp_search.PerformLayout();
            this.flp_collapses.ResumeLayout(false);
            this.ec_weaponFilters.DropZone.ResumeLayout(false);
            this.ec_weaponFilters.ResumeLayout(false);
            this.flp_weaponFilters.ResumeLayout(false);
            this.ec_armourFilters.DropZone.ResumeLayout(false);
            this.ec_armourFilters.ResumeLayout(false);
            this.flp_armourFilters.ResumeLayout(false);
            this.ec_socketFilters.DropZone.ResumeLayout(false);
            this.ec_socketFilters.ResumeLayout(false);
            this.flp_socketFilters.ResumeLayout(false);
            this.ec_Requirements.DropZone.ResumeLayout(false);
            this.ec_Requirements.ResumeLayout(false);
            this.flp_requirements.ResumeLayout(false);
            this.ec_modFilters.DropZone.ResumeLayout(false);
            this.ec_modFilters.ResumeLayout(false);
            this.flp_modFilters.ResumeLayout(false);
            this.ec_mods.DropZone.ResumeLayout(false);
            this.ec_mods.ResumeLayout(false);
            this.ec_mapFilters.DropZone.ResumeLayout(false);
            this.ec_mapFilters.ResumeLayout(false);
            this.flp_mapFilters.ResumeLayout(false);
            this.ec_miscellaneous.DropZone.ResumeLayout(false);
            this.ec_miscellaneous.ResumeLayout(false);
            this.flp_miscFilters.ResumeLayout(false);
            this.ec_tradeFilters.DropZone.ResumeLayout(false);
            this.ec_tradeFilters.ResumeLayout(false);
            this.flp_tradeFilters.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tp_bulk.ResumeLayout(false);
            this.tp_bulk.PerformLayout();
            this.flp_have.ResumeLayout(false);
            this.ec_haveCurrency.ResumeLayout(false);
            this.ec_haveFrags.ResumeLayout(false);
            this.ec_haveResonators.ResumeLayout(false);
            this.ec_haveFossils.ResumeLayout(false);
            this.ec_haveEssences.ResumeLayout(false);
            this.ec_haveCards.ResumeLayout(false);
            this.ec_haveMaps.ResumeLayout(false);
            this.ec_haveShapedMap.ResumeLayout(false);
            this.ec_haveElderMap.ResumeLayout(false);
            this.flp_want.ResumeLayout(false);
            this.ec_wantCurrency.ResumeLayout(false);
            this.ec_wantFrags.ResumeLayout(false);
            this.ec_wantResonators.ResumeLayout(false);
            this.ec_wantFossils.ResumeLayout(false);
            this.ec_wantEssences.ResumeLayout(false);
            this.ec_wantCards.ResumeLayout(false);
            this.ec_wantMaps.ResumeLayout(false);
            this.ec_wantShapedMap.ResumeLayout(false);
            this.ec_wantElderMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb_vertical)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_horizontal)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.Label lbl_header;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_reset;
        private System.Windows.Forms.ListBox lb_itemName;
        private System.Windows.Forms.TabPage tp_search;
        private System.Windows.Forms.Label lbl_itemCategory;
        private Controls.FlattenCombo cmb_itemCategory;
        private System.Windows.Forms.Label lbl_itemName;
        private Controls.FlattenCombo cmb_itemRarity;
        private System.Windows.Forms.FlowLayoutPanel flp_collapses;
        private Controls.ExpandCollapse ec_weaponFilters;
        private System.Windows.Forms.FlowLayoutPanel flp_weaponFilters;
        private Controls.Filter cf_damage;
        private Controls.Filter cf_attacksPerSecond;
        private Controls.Filter cf_criticalChance;
        private Controls.Filter cf_damagePerSecond;
        private Controls.Filter cf_physicalDPS;
        private Controls.Filter cf_elementalDPS;
        private Controls.ExpandCollapse ec_armourFilters;
        private System.Windows.Forms.FlowLayoutPanel flp_armourFilters;
        private Controls.Filter cf_armour;
        private Controls.Filter cf_block;
        private Controls.Filter cf_energyShield;
        private Controls.Filter cf_evasion;
        private Controls.ExpandCollapse ec_socketFilters;
        private System.Windows.Forms.FlowLayoutPanel flp_socketFilters;
        private Controls.Filter_Sockets csf_sockets;
        private Controls.Filter_Sockets csf_links;
        private Controls.ExpandCollapse ec_Requirements;
        private System.Windows.Forms.FlowLayoutPanel flp_requirements;
        private Controls.Filter cf_level;
        private Controls.Filter cf_strength;
        private Controls.Filter cf_dexterity;
        private Controls.Filter cf_intelligence;
        private Controls.ExpandCollapse ec_modFilters;
        private System.Windows.Forms.FlowLayoutPanel flp_modFilters;
        private Controls.Filter cf_emptyPrefixes;
        private Controls.Filter cf_emptySuffixes;
        private Controls.ExpandCollapse ec_mods;
        private Controls.ExpandCollapse ec_mapFilters;
        private System.Windows.Forms.FlowLayoutPanel flp_mapFilters;
        private Controls.Filter cf_mapTier;
        private Controls.Filter cf_mapPacksize;
        private Controls.Filter cf_mapIIQ;
        private Controls.Filter cf_mapIIR;
        private Controls.Filter_DropDown cfdd_shaped;
        private Controls.Filter_DropDown cfdd_elder;
        private Controls.Filter_DropDown cfdd_mapSeries;
        private Controls.ExpandCollapse ec_miscellaneous;
        private System.Windows.Forms.FlowLayoutPanel flp_miscFilters;
        private Controls.Filter cf_quality;
        private Controls.Filter cf_itemLevel;
        private Controls.Filter cf_gemLevel;
        private Controls.Filter cf_gemExperience;
        private Controls.Filter_DropDown cfdd_shaperItem;
        private Controls.Filter_DropDown cfdd_elderItem;
        private Controls.Filter_DropDown cfdd_alternateArt;
        private Controls.Filter_DropDown cfdd_identified;
        private Controls.Filter_DropDown cfdd_corrupted;
        private Controls.Filter_DropDown cfdd_crafted;
        private Controls.Filter_DropDown cfdd_enchanted;
        private Controls.Filter_DropDown cfdd_mirrored;
        private Controls.Filter cf_talismanTier;
        private Controls.ExpandCollapse ec_tradeFilters;
        private System.Windows.Forms.FlowLayoutPanel flp_tradeFilters;
        private Controls.Filter_Textbox cftb_trader;
        private Controls.Filter_DropDown cfdd_listed;
        private Controls.Filter_DropDown cfdd_saleType;
        private Controls.Filter_Buyout cfby_buyout;
        private System.Windows.Forms.Label lbl_itemRarity;
        private System.Windows.Forms.TextBox tb_itemName;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button btn_closeTab;
        private System.Windows.Forms.PictureBox pb_horizontal;
        private Controls.FlattenCombo cmb_accountStatus;
        private System.Windows.Forms.Label lbl_online;
        private System.Windows.Forms.TabPage tp_bulk;
        private System.Windows.Forms.PictureBox pb_vertical;
        private System.Windows.Forms.Button btn_defaultLayout;
        private System.Windows.Forms.Button btn_twoColumns;
        private System.Windows.Forms.Button btn_settings;
        private System.Windows.Forms.Label lbl_minimumStock;
        private System.Windows.Forms.TextBox tb_BulkMinimumStock;
        private System.Windows.Forms.Label lbl_sellerAccount;
        private System.Windows.Forms.TextBox tb_bulkSellerAccount;
        private System.Windows.Forms.FlowLayoutPanel flp_want;
        private System.Windows.Forms.FlowLayoutPanel flp_have;
        private System.Windows.Forms.Label lbl_want;
        private System.Windows.Forms.Label lbl_have;
        private Controls.ExpandCollapse ec_wantCurrency;
        private Controls.ExpandCollapse ec_wantFrags;
        private Controls.ExpandCollapse ec_wantResonators;
        private Controls.ExpandCollapse ec_wantFossils;
        private Controls.ExpandCollapse ec_wantEssences;
        private Controls.ExpandCollapse ec_wantCards;
        private Controls.ExpandCollapse ec_wantMaps;
        private Controls.ExpandCollapse ec_wantShapedMap;
        private Controls.ExpandCollapse ec_wantElderMap;
        private Controls.ExpandCollapse ec_haveCurrency;
        private Controls.ExpandCollapse ec_haveFrags;
        private Controls.ExpandCollapse ec_haveResonators;
        private Controls.ExpandCollapse ec_haveFossils;
        private Controls.ExpandCollapse ec_haveEssences;
        private Controls.ExpandCollapse ec_haveCards;
        private Controls.ExpandCollapse ec_haveMaps;
        private Controls.ExpandCollapse ec_haveShapedMap;
        private Controls.ExpandCollapse ec_haveElderMap;
        private System.Windows.Forms.Label lbl_Filter;
        private System.Windows.Forms.TextBox tb_filter;
        private Controls.FlattenCombo fc_bulkAccStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_addModGroup;
        private Controls.FlattenCombo cmb_modGroupType;
        private Controls.Filter_DropDown cfdd_veiled;
    }
}