﻿using PoE_Trade_Overlay.Queries;
using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.DirectWrite;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using Factory = SharpDX.Direct2D1.Factory;
using FontFactory = SharpDX.DirectWrite.Factory;
using Format = SharpDX.DXGI.Format;

namespace PoE_Trade_Overlay.Forms
{
    public partial class Form_Main : Form
    {

        #region DLLImport
        [DllImport("user32.dll")]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);
        [DllImport("user32.dll")]
        private static extern bool SetLayeredWindowAttributes(IntPtr hwnd, uint crKey, byte bAlpha, uint dwFlags);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);
        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetWindowThreadProcessId(IntPtr handle, out int processId);
        [DllImport("dwmapi.dll")]
        public static extern void DwmExtendFrameIntoClientArea(IntPtr hWnd, ref int[] pMargins);
        #endregion

        #region SDX Stuff
        private WindowRenderTarget device;
        private HwndRenderTargetProperties renderProperties;
        private SolidColorBrush solidColorBrush;
        private Factory factory;
        
        private TextFormat font, fontSmall;

        private FontFactory fontFactory;
        private const string fontFamily = "Consolas";
        private const float fontSize = 12.0f;
        private const float fontSizeSmall = 10.0f;
        private IntPtr handle;

        public const UInt32 SWP_NOSIZE = 0x0001;
        public const UInt32 SWP_NOMOVE = 0x0002;
        public const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;
        public static IntPtr HWND_TOPMOST = new IntPtr(-1);

        public Thread sDX = null;
        #endregion

        public static Form_Main instance;
        public Leagues leagues;
        public Form_Search fs;

        private bool renderOverlay;
        public NotifyIcon notify;
        public System.Timers.Timer t_poeChecker;
        public ContextMenu rightClickContext;

        KeyboardHook hook = new KeyboardHook();

        private void SetupSDX()
        {
            this.DoubleBuffered = true;
            this.Width = Screen.PrimaryScreen.WorkingArea.Width;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer |
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.DoubleBuffer |
                ControlStyles.UserPaint |
                ControlStyles.Opaque |
                ControlStyles.ResizeRedraw |
                ControlStyles.SupportsTransparentBackColor, true);
            this.TopMost = true;
            this.Visible = true;

            factory = new Factory();
            fontFactory = new FontFactory();
            renderProperties = new HwndRenderTargetProperties()
            {
                Hwnd = this.Handle,
                PixelSize = new Size2(Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height),
                PresentOptions = PresentOptions.Immediately
            };

            device = new WindowRenderTarget(factory, new RenderTargetProperties(new PixelFormat(Format.B8G8R8A8_UNorm, AlphaMode.Premultiplied)), renderProperties);
            solidColorBrush = new SolidColorBrush(device, new SharpDX.Mathematics.Interop.RawColor4(255, 255, 255, 255));
            font = new TextFormat(fontFactory, fontFamily, fontSize);
            fontSmall = new TextFormat(fontFactory, fontFamily, fontSizeSmall);
            sDX = new Thread(new ParameterizedThreadStart(sDXThread));
            sDX.Priority = ThreadPriority.Highest;
            sDX.IsBackground = true;
            sDX.Start();
        }

        private void SetupContextsAndTimer()
        {
            notifyIcon2.ContextMenuStrip = new ContextMenuStrip();
            notifyIcon2.ContextMenuStrip.Items.Add("Check for Update", null, Context_Update);
            notifyIcon2.ContextMenuStrip.Items.Add("Credits", null, Context_Credits);
            notifyIcon2.ContextMenuStrip.Items.Add("Settings", null, Context_Settings);
            notifyIcon2.ContextMenuStrip.Items.Add("Tools", null, Context_Tools);
            notifyIcon2.ContextMenuStrip.Items.Add("-");
            notifyIcon2.ContextMenuStrip.Items.Add("Exit", null, Context_Exit);
            notify = notifyIcon2;

            rightClickContext = new ContextMenu();
            rightClickContext.MenuItems.Add(new MenuItem("Return to Hideout", Context_HO));
            rightClickContext.MenuItems.Add(new MenuItem("-"));
            rightClickContext.MenuItems.Add(new MenuItem("Check for Update", Context_Update));
            rightClickContext.MenuItems.Add(new MenuItem("Credits", Context_Credits));
            rightClickContext.MenuItems.Add(new MenuItem("Tools", Context_Tools));
            rightClickContext.MenuItems.Add(new MenuItem("Exit", Context_Exit));

            // Create Timer and check in tick event if poe is active
            t_poeChecker = new System.Timers.Timer();
            t_poeChecker.Elapsed += new ElapsedEventHandler(t_poeChecker_Tick);
            t_poeChecker.Interval = 600;
            t_poeChecker.Enabled = true;
        }

        public Form_Main()
        {
            this.handle = Handle;
            int initialStyle = GetWindowLong(this.Handle, -20);
            SetWindowLong(this.Handle, -20, initialStyle | 0x80000 | 0x20);
            SetWindowPos(this.Handle, HWND_TOPMOST, 0, 0, 0, 0, 0);
            OnResize(null);

            InitializeComponent();

            instance = this;
            ThemeLoader.ApplyTheme(this.GetControls());

            leagues = QueryHelper.GetLeagueData();
            panel_main.Location = new Point(Config.mainX, Config.mainY);
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);
            
            SetupContextsAndTimer();
            SetupSDX();

            pb_drag.AssignMoveEvents();


            // register the event that is fired after the key press.
            //hook.KeyPressed += new EventHandler<KeyPressedEventArgs>(hook_KeyPressed);
            // register the control + c combination as hot key.
            //hook.RegisterHotKey(PoE_Trade_Overlay.ModifierKeys.Control , Keys.C);
        }

        #region Context Menu Events
        private void Context_HO(object sender, EventArgs e)
        {
            ChatHelper.SendChatMessage("/hideout");
        }

        private void Context_Credits(object sender, EventArgs e)
        {

            Credits credits = new Credits();
            credits.Show();

        }

        private void Context_Exit(object sender, EventArgs e)
        {
            Config.Save();
            Application.Exit();
        }

        private void Context_Update(object sender, EventArgs e)
        {
            PoE_Trade_Overlay.Update.Check();
        }

        private void Context_Settings(object sender, EventArgs e)
        {
            Form_Settings form_Settings = new Form_Settings();
            form_Settings.ShowDialog(this);
        }
        private void Context_Tools(object sender, EventArgs e)
        {
            Tools t = new Tools();
            ThemeLoader.ApplyTheme(t.GetControls());
            t.Show();
        }

        #endregion




        void hook_KeyPressed(object sender, KeyPressedEventArgs e)
        {
            // show the keys pressed in a label.
            Console.WriteLine(e.Modifier.ToString() + " + " + e.Key.ToString());
            string text = Clipboard.GetText(TextDataFormat.UnicodeText);
            Console.WriteLine(text);
        }






        public void LoadTheme()
        {
            /*
            panel_main.BackColor = ThemeLoader.LoadColor(ThemeLoader.theme_Main, "Window", "BackColor");
            pb_drag.Image = ThemeLoader.LoadImage(ThemeLoader.theme_Main, "Window", "DragImage");
            btn_settings.Image = ThemeLoader.LoadImage(ThemeLoader.theme_Main, "Window", "Settings");
            btn_settings.BackColor = ThemeLoader.LoadColor(ThemeLoader.theme_Main, "Button", "BackColor");
            btn_settings.FlatAppearance.BorderColor = ThemeLoader.LoadColor(ThemeLoader.theme_Main, "Button", "BackColor");
            btn_search.ForeColor = ThemeLoader.LoadColor(ThemeLoader.theme_Main, "Button", "ForeColor");
            btn_search.BackColor = ThemeLoader.LoadColor(ThemeLoader.theme_Main, "Button", "BackColor");
            */
            TransparencyKey = Color.Black;
        }

        protected override void OnPaint(PaintEventArgs e)// create the whole form
        {
            int[] marg = new int[] { 0, 0, Width, Height };
            DwmExtendFrameIntoClientArea(this.Handle, ref marg);
        }

        public static bool ApplicationIsActivated(string app)
        {
            var activatedHandle = GetForegroundWindow();

            if (activatedHandle == IntPtr.Zero)
                return false;

            var procId = Process.GetCurrentProcess().Id;
            int activeProcId;

            GetWindowThreadProcessId(activatedHandle, out activeProcId);
            string activeProcName = Process.GetProcessById(activeProcId).ProcessName.ToString();

            if (activeProcName.Equals(app))
                return true;
            else
                return false;
        }

        private static void OnProcessExit(object sender, EventArgs e)
        {
            Config.Save();
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            if (fs == null)
                fs = new Form_Search();

            if (!fs.Visible)
            {
                fs.hide = false;
                fs.Show(this);
            }

            panel_main.Focus();
        }

        private void btn_settings_Click(object sender, EventArgs e)
        {
            if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl))
            {
                Application.Exit();
            }
            else
            {
                Form_Settings form_Settings = new Form_Settings();
                form_Settings.ShowDialog(this);
            }
        }
        
        private void moveMenu_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                rightClickContext.Show(pb_drag, new Point(e.X, e.Y));
            }
        }
        
        public void AbortSDX()
        {
            sDX.Abort();
        }

        private void sDXThread(object sender)
        {
            while (renderOverlay)
            {
                device.BeginDraw();
                device.Clear(new SharpDX.Mathematics.Interop.RawColor4(0, 0, 0, 0));
                device.EndDraw();
                Thread.Sleep(600);
            }
        }

        private void t_poeChecker_Tick(object source, ElapsedEventArgs e)
        {
            if (Config.onlyRenderIfPoeIsActive)
            {
                if (ApplicationIsActivated("PathOfExile_x64") | ApplicationIsActivated("PathOfExile_x64Steam"))
                    renderOverlay = true;
                else if (ApplicationIsActivated("PathOfExile Trade Overlay"))
                    renderOverlay = true;
                else
                    renderOverlay = false;
            }
            else
            {
                renderOverlay = true;
            }

            if (this.InvokeRequired)
                this.Invoke(new MethodInvoker(delegate { this.Visible = renderOverlay; }));
            else
                this.Visible = renderOverlay;

            if (Form_Search.instance != null)
            {
                if (Form_Search.instance.InvokeRequired)
                    if (!Form_Search.instance.hide)
                        Form_Search.instance.Invoke(new MethodInvoker(delegate { Form_Search.instance.Visible = renderOverlay; }));
            }
        }
    }
}
 