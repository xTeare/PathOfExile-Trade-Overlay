﻿using PoE_Trade_Overlay.Controls;
using PoE_Trade_Overlay.Forms;
using PoE_Trade_Overlay.Queries;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace PoE_Trade_Overlay
{
    public partial class Form_Search : Form
    {
        public static Form_Search instance;

        // If this is false, the search Window does get rendered even it it was closed with .Hide(),IF only render if poe is enabled !
        public bool hide;
        
        public List<QueryManager> queries = new List<QueryManager>();

        private List<FastBulkButton> bulkButtons = new List<FastBulkButton>();

        public int from;
        public int to;

        public string defaultHeader;

        public List<ModGroup> modGroups = new List<ModGroup>();

        public List<string> headers = new List<string>() { "Item Search", "Bulk Trade" };

        public TabControl tc;

        public Form_Search()
        {
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.Location = new Point(Config.searchX, Config.searchY);

            InitializeComponent();

            tc = tabControl1;

            PopulateControls();
            instance = this;
            lb_itemName.Location = new Point(tb_itemName.Left, tb_itemName.Bottom);

            // Remove Borders from Tab Control
            var tab = new FlatTab(tabControl1);
            tabControl1.SelectedIndexChanged += TabControl1_SelectedIndexChanged;
            tabControl1.Selecting += TabControl1_Selecting;
            btn_closeTab.Visible = false;

            this.Size = new Size(Config.searchWidth, Config.searchHeight);

            defaultHeader = string.Format("Item Search ({0})", Config.league);

            SetHeader();

            lbl_header.AssignMoveEvents();

            AddModGroup("and");
            CalculateECMod();

            GenerateBulkForm();
            LoadTheme();

        }

        private void TabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            tabControl1.SuspendLayout();
            this.SuspendLayout();
        }

        public void SetHeader()
        {
            if (headers.Count >= tabControl1.SelectedIndex)
                lbl_header.Text = headers[tabControl1.SelectedIndex];
        }

        private void TabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0)
            {
                btn_close.Visible = true;
                btn_reset.Visible = true;
                btn_search.Visible = true;
                btn_closeTab.Visible = false;
            }
            else if (tabControl1.SelectedIndex == 1)
            {
                btn_close.Visible = true;
                btn_reset.Visible = true;
                btn_search.Visible = true;
                btn_closeTab.Visible = false;
                ResizeWindow(new Size(1360, Config.searchHeight));
            }
            else
            {
                btn_close.Visible = true;
                btn_reset.Visible = false;
                btn_search.Visible = false;
                btn_closeTab.Visible = true;
            }
            tabControl1.ResumeLayout(false);
            this.ResumeLayout(true);

            if (headers.Count >= tabControl1.SelectedIndex)
                lbl_header.Text = headers[tabControl1.SelectedIndex];
        }

        public void LoadTheme()
        {
            ThemeLoader.ApplyTheme(this.GetControls());
            this.BackColor = ThemeLoader.BackColor;
            pb_horizontal.BackColor = ThemeLoader.BackColor;
            pb_vertical.BackColor = ThemeLoader.BackColor;

            foreach (TabPage tp in tabControl1.TabPages)
                tp.BackColor = ThemeLoader.BackColor;

            this.Opacity = Config.windowOpacity / 100d;
        }

        public void PopulateControls()
        {
            cmb_itemCategory.Items.Clear();
            cmb_itemRarity.Items.Clear();
            foreach (DataRow dr in Data.itemRarity.Rows)
            {
                cmb_itemRarity.Items.Add(dr[1].ToString());
            }
            cmb_itemCategory.Items.LoadItemCategories();
            cmb_itemCategory.SelectedIndex = 0;
            cmb_itemRarity.SelectedIndex = 0;
            cmb_accountStatus.SelectedIndex = 1;
            fc_bulkAccStatus.SelectedIndex = 1;
            cmb_modGroupType.SelectedIndex = 0;
        }

        #region ModGroup

        public void AddModGroup(string groupType)
        {
            int y = 0;

            if (modGroups.Count > 0)
            {
                y = modGroups[modGroups.Count - 1].Bottom + 3;
            }

            ModGroup mg = new ModGroup();
            mg.type = groupType;
            mg.Location = new Point(0, y);
            mg.AddSearch();
            ec_mods.DropZone.Controls.Add(mg);
            modGroups.Add(mg);

            ThemeLoader.ApplyTheme(new List<Control>() { mg });
            RepositionModGroups();
            CalculateECMod();
        }

        public void RepositionModGroups()
        {
            for (int i = 0; i < modGroups.Count; i++)
            {
                Point loc = new Point(0, 0);
                if (i == 0)
                {
                    modGroups[i].Location = loc;
                }
                else
                {
                    loc = new Point(0, modGroups[i - 1].Bottom + 3);
                    modGroups[i].Location = loc;
                }
            }
        }

        public void RemoveModGroup(ModGroup mg)
        {
            ec_mods.DropZone.Controls.Remove(mg);
            modGroups.Remove(mg);
            RepositionModGroups();
            CalculateECMod();
        }

        public void CalculateECMod()
        {
            int height = 21; // base height of the header

            foreach (ModGroup mg in modGroups)
            {
                height += mg.Height;
                height += 6;
            }

            height += 28; // Add space for cmb and btn

            Size newSize = new Size(607, height);
            ec_mods.Size = newSize;
            ec_mods.SizeExpanded = newSize;
        }

        #endregion ModGroup

        public void GetTradeData(string query, SearchQuery sq, TabPage page = null, string pseudos = "", string modToSort = "", bool sortDesc = false, string header = "")
        {
            QueryManager qm = new QueryManager();
            qm.sq = sq;
            qm.results = qm.GetSearchResults(query);

            if (qm.results != null && qm.results.Count > 1)
            {
                // Generate fetch url from querymanager
                string fetchURL = qm.GetFetchURLNew(pseudos);
                Console.WriteLine(fetchURL);
                // recieve actual trade data
                TradeData tradeData = QueryHelper.GetTradeData(fetchURL);

                if (tradeData != null)
                {
                    if (tradeData.Result == null || tradeData.Result.Count == 0)
                        return;

                    FlowLayoutPanel flp = new FlowLayoutPanel();
                    if (page == null)
                    {
                        // Create a new Tab and FlowLayoutPanel
                        TabPage tp = new TabPage(string.Format("Results ({0})", qm.results.Count));
                        tp.Name = "tp_results";
                        tp.BackColor = ThemeLoader.BackColor;
                        tp.Controls.Add(flp);
                        flp.Location = new Point(0, 0);
                        flp.BackColor = Color.Transparent;
                        flp.Dock = DockStyle.Fill;
                        flp.AutoScroll = true;
                        flp.MouseWheel += FLP_MouseWheel;
                        qm.page = tp;
                        tabControl1.TabPages.Add(tp);
                        page = tp;
                        headers.Add(string.Format("Results ({0})", qm.results.Count));
                    }
                    else
                    {
                        // We get here when we Click on a sortable mod
                        foreach (Control c in page.Controls) // Find FlowLayoutPanel
                        {
                            if (c.GetType() == typeof(FlowLayoutPanel))
                            {
                                c.Controls.Clear(); // Remove all results
                                flp = (FlowLayoutPanel)c;
                            }
                        }
                    }

                    // Filter Results and add them to flp
                    foreach (Result result in tradeData.Result)
                    {
                        if (result == null)
                            continue;

                        if (result.IsAfk() && Config.filterAFKPlayers)
                            continue;

                        AddResultToFlp(flp, result, sq, page, pseudos, modToSort, sortDesc);
                    }
                    //ResizeSearchResults(flp);
                    queries.Add(qm);
                    tabControl1.SelectedIndex = tabControl1.TabPages.IndexOf(page);
                }
            }
            btn_search.Enabled = true;
            btn_reset.Enabled = true;
            btn_close.Enabled = true;
        }

        public void AddResultToFlp(FlowLayoutPanel panel, Result result, SearchQuery sq, TabPage page, string pseudos = "", string modToSort = "", bool sortDesc = false)
        {
            SearchResult_Minimalistic srm = new SearchResult_Minimalistic();
            srm.CopyColors();
            srm.sq = sq;
            srm.pseudo = pseudos;
            srm.parent = page;
            srm.modToSort = modToSort;
            srm.sortModDesc = sortDesc;
            srm.sortPriceDesc = sortDesc;
            srm.sortGemProgressDesc = sortDesc;
            srm.result = result;
            panel.Controls.Add(srm);
        }

        public void ResizeSearchResults(FlowLayoutPanel parent)
        {
            if (this.Width < 1340)
                return;

            for (int i = 0; i < parent.Controls.Count; i++)
            {
                if (parent.Controls[i].GetType().Name != "SearchResult_Minimalistic")
                    continue;

                Size left = parent.Controls[i].Size;
                Size right = Size.Empty;

                SearchResult_Minimalistic leftResult = (SearchResult_Minimalistic)parent.Controls[i];
                SearchResult_Minimalistic rightResult = null;

                if (i + 1 <= parent.Controls.Count - 1)
                {
                    right = parent.Controls[i + 1].Size;
                    rightResult = (SearchResult_Minimalistic)parent.Controls[i + 1];
                }
                if (right.Height != 0)
                {
                    if (left.Height < right.Height)
                    {
                        leftResult.AutoSize = false;
                        leftResult.Size = right;
                    }
                    else
                    {
                        rightResult.AutoSize = false;
                        rightResult.Size = left;
                    }
                }
            }
        }

        public void GenerateSearchQuery()
        {
            string header = "";
            SearchQuery searchQuery = new SearchQuery();
            Query query = new Query();
            Sort sort = new Sort();
            QueryFilters queryFilters = new QueryFilters();

            if (tb_itemName.Text != string.Empty)
            {
                if (tb_itemName.Text.IsUniqueMap())
                {
                    TypeClass name = new TypeClass();
                    TypeClass type = new TypeClass();
                    name.Discriminator = Data.GetEntryFromRow(Data.items, 1, "combined", tb_itemName.Text.Replace("'", "%27")).ToDiscriminator();
                    type.Discriminator = Data.GetEntryFromRow(Data.items, 1, "combined", tb_itemName.Text.Replace("'", "%27")).ToDiscriminator();
                    name.Option = Data.GetEntryFromRow(Data.items, 0, "combined", tb_itemName.Text.Replace("'", "%27")).Replace("%27", "'");
                    type.Option = Data.GetEntryFromRow(Data.items, 1, "combined", tb_itemName.Text.Replace("'", "%27")).RemoveFromTo("(", ")", -1);
                    query.Name = name;
                    query.Type = type;
                    header += string.Format("{0} {1}", name.Option, type.Option);
                }
                else if (tb_itemName.Text.Contains("Map"))
                {
                    TypeClass tc = new TypeClass();
                    tc.Discriminator = Data.GetEntryFromRow(Data.items, 1, "combined", tb_itemName.Text).ToDiscriminator();
                    tc.Option = Data.GetEntryFromRow(Data.items, 0, "combined", tb_itemName.Text);
                    query.Type = tc;
                }
                else
                {
                    string name = Data.GetEntryFromRow(Data.items, 0, "combined", tb_itemName.Text.RemoveFromTo("(", ")").Replace("'", "%27"));
                    string type = Data.GetEntryFromRow(Data.items, 1, "combined", tb_itemName.Text.RemoveFromTo("(", ")").Replace("'", "%27"));

                    if (name == type)
                    {
                        query.Type = type;
                    }
                    else
                    {
                        query.Name = name.Replace("%27", "'");
                        query.Type = type.Replace("%27", "'");
                    }
                }
            }

            if (cmb_itemCategory.SelectedIndex != 0)
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.TypeFilters == null)
                    queryFilters.TypeFilters = new TypeFilters();

                if (queryFilters.TypeFilters.Filters == null)
                    queryFilters.TypeFilters.Filters = new TypeFiltersFilters();

                queryFilters.TypeFilters.Filters.Category = new Status();
                queryFilters.TypeFilters.Filters.Category.Option = Data.GetEntryFromRow(Data.itemTypes, 0, "text", cmb_itemCategory.Items[cmb_itemCategory.SelectedIndex].ToString());
            }

            if (cmb_itemRarity.SelectedIndex != 0)
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.TypeFilters == null)
                    queryFilters.TypeFilters = new TypeFilters();

                if (queryFilters.TypeFilters.Filters == null)
                    queryFilters.TypeFilters.Filters = new TypeFiltersFilters();

                queryFilters.TypeFilters.Filters.Rarity = new Status();
                queryFilters.TypeFilters.Filters.Rarity.Option = Data.GetEntryFromRow(Data.itemRarity, 0, "text", cmb_itemRarity.Items[cmb_itemRarity.SelectedIndex].ToString());
            }

            #region WEAPON FILTERS

            if (!cf_damage.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.WeaponFilters == null)
                {
                    queryFilters.WeaponFilters = new WeaponFilters();
                    queryFilters.WeaponFilters.Disabled = false;
                }

                if (queryFilters.WeaponFilters.Filters == null)
                    queryFilters.WeaponFilters.Filters = new WeaponFiltersFilters();

                queryFilters.WeaponFilters.Filters.Damage = Extensions.SetDataFromFilterControl(cf_damage);
            }

            if (!cf_criticalChance.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.WeaponFilters == null)
                {
                    queryFilters.WeaponFilters = new WeaponFilters();
                    queryFilters.WeaponFilters.Disabled = false;
                }

                if (queryFilters.WeaponFilters.Filters == null)
                    queryFilters.WeaponFilters.Filters = new WeaponFiltersFilters();

                queryFilters.WeaponFilters.Filters.Crit = Extensions.SetDataFromFilterControl(cf_criticalChance);
            }

            if (!cf_physicalDPS.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.WeaponFilters == null)
                {
                    queryFilters.WeaponFilters = new WeaponFilters();
                    queryFilters.WeaponFilters.Disabled = false;
                }

                if (queryFilters.WeaponFilters.Filters == null)
                    queryFilters.WeaponFilters.Filters = new WeaponFiltersFilters();

                queryFilters.WeaponFilters.Filters.Pdps = Extensions.SetDataFromFilterControl(cf_physicalDPS);
            }

            if (!cf_attacksPerSecond.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.WeaponFilters == null)
                {
                    queryFilters.WeaponFilters = new WeaponFilters();
                    queryFilters.WeaponFilters.Disabled = false;
                }

                if (queryFilters.WeaponFilters.Filters == null)
                    queryFilters.WeaponFilters.Filters = new WeaponFiltersFilters();

                queryFilters.WeaponFilters.Filters.Aps = Extensions.SetDataFromFilterControl(cf_attacksPerSecond);
            }
            if (!cf_damagePerSecond.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.WeaponFilters == null)
                {
                    queryFilters.WeaponFilters = new WeaponFilters();
                    queryFilters.WeaponFilters.Disabled = false;
                }

                if (queryFilters.WeaponFilters.Filters == null)
                    queryFilters.WeaponFilters.Filters = new WeaponFiltersFilters();

                queryFilters.WeaponFilters.Filters.Dps = Extensions.SetDataFromFilterControl(cf_damagePerSecond);
            }
            if (!cf_elementalDPS.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.WeaponFilters == null)
                {
                    queryFilters.WeaponFilters = new WeaponFilters();
                    queryFilters.WeaponFilters.Disabled = false;
                }

                if (queryFilters.WeaponFilters.Filters == null)
                    queryFilters.WeaponFilters.Filters = new WeaponFiltersFilters();

                queryFilters.WeaponFilters.Filters.Edps = Extensions.SetDataFromFilterControl(cf_elementalDPS);
            }

            #endregion WEAPON FILTERS

            #region ARMOUR FILTERS

            // Armour Filters
            if (!cf_armour.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.ArmourFilters == null)
                {
                    queryFilters.ArmourFilters = new ArmourFilters();
                    queryFilters.ArmourFilters.Disabled = false;
                }

                if (queryFilters.ArmourFilters.Filters == null)
                    queryFilters.ArmourFilters.Filters = new ArmourFiltersFilters();

                queryFilters.ArmourFilters.Filters.Ar = Extensions.SetDataFromFilterControl(cf_armour);
            }

            if (!cf_block.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.ArmourFilters == null)
                {
                    queryFilters.ArmourFilters = new ArmourFilters();
                    queryFilters.ArmourFilters.Disabled = false;
                }

                if (queryFilters.ArmourFilters.Filters == null)
                    queryFilters.ArmourFilters.Filters = new ArmourFiltersFilters();

                queryFilters.ArmourFilters.Filters.Block = Extensions.SetDataFromFilterControl(cf_block);
            }

            if (!cf_evasion.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.ArmourFilters == null)
                {
                    queryFilters.ArmourFilters = new ArmourFilters();
                    queryFilters.ArmourFilters.Disabled = false;
                }

                if (queryFilters.ArmourFilters.Filters == null)
                    queryFilters.ArmourFilters.Filters = new ArmourFiltersFilters();
                queryFilters.ArmourFilters.Filters.Ev = Extensions.SetDataFromFilterControl(cf_evasion);
            }

            if (!cf_energyShield.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.ArmourFilters == null)
                {
                    queryFilters.ArmourFilters = new ArmourFilters();
                    queryFilters.ArmourFilters.Disabled = false;
                }

                if (queryFilters.ArmourFilters.Filters == null)
                    queryFilters.ArmourFilters.Filters = new ArmourFiltersFilters();
                queryFilters.ArmourFilters.Filters.Es = Extensions.SetDataFromFilterControl(cf_energyShield);
            }

            #endregion ARMOUR FILTERS

            #region SOCKET FILTERS

            if (!csf_sockets.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.SocketFilters == null)
                {
                    queryFilters.SocketFilters = new SocketFilters();
                    queryFilters.SocketFilters.Disabled = false;
                }

                if (queryFilters.SocketFilters.Filters == null)
                    queryFilters.SocketFilters.Filters = new SocketFiltersFilters();

                if (queryFilters.SocketFilters.Filters.Sockets == null)
                    queryFilters.SocketFilters.Filters.Sockets = new Links();

                queryFilters.SocketFilters.Filters.Sockets = csf_sockets.SetDataFromControlSocketFilter();
            }

            if (!csf_links.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.SocketFilters == null)
                {
                    queryFilters.SocketFilters = new SocketFilters();
                    queryFilters.SocketFilters.Disabled = false;
                }

                if (queryFilters.SocketFilters.Filters == null)
                    queryFilters.SocketFilters.Filters = new SocketFiltersFilters();

                if (queryFilters.SocketFilters.Filters.Links == null)
                    queryFilters.SocketFilters.Filters.Links = new Links();

                queryFilters.SocketFilters.Filters.Links = csf_links.SetDataFromControlSocketFilter();
            }

            #endregion SOCKET FILTERS

            #region REQUIREMENT FILTERS

            if (!cf_level.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.ReqFilters == null)
                {
                    queryFilters.ReqFilters = new ReqFilters();
                    queryFilters.ReqFilters.Disabled = false;
                }

                if (queryFilters.ReqFilters.Filters == null)
                    queryFilters.ReqFilters.Filters = new ReqFiltersFilters();

                queryFilters.ReqFilters.Filters.Lvl = Extensions.SetDataFromFilterControl(cf_level);
            }

            if (!cf_strength.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.ReqFilters == null)
                {
                    queryFilters.ReqFilters = new ReqFilters();
                    queryFilters.ReqFilters.Disabled = false;
                }

                if (queryFilters.ReqFilters.Filters == null)
                    queryFilters.ReqFilters.Filters = new ReqFiltersFilters();

                queryFilters.ReqFilters.Filters.Str = Extensions.SetDataFromFilterControl(cf_strength);
            }

            if (!cf_dexterity.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.ReqFilters == null)
                {
                    queryFilters.ReqFilters = new ReqFilters();
                    queryFilters.ReqFilters.Disabled = false;
                }

                if (queryFilters.ReqFilters.Filters == null)
                    queryFilters.ReqFilters.Filters = new ReqFiltersFilters();

                queryFilters.ReqFilters.Filters.Dex = Extensions.SetDataFromFilterControl(cf_dexterity);
            }

            if (!cf_intelligence.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.ReqFilters == null)
                {
                    queryFilters.ReqFilters = new ReqFilters();
                    queryFilters.ReqFilters.Disabled = false;
                }

                if (queryFilters.ReqFilters.Filters == null)
                    queryFilters.ReqFilters.Filters = new ReqFiltersFilters();

                queryFilters.ReqFilters.Filters.Int = Extensions.SetDataFromFilterControl(cf_intelligence);
            }

            #endregion REQUIREMENT FILTERS

            #region MOD FILTERS

            if (!cf_emptyPrefixes.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.ModFilters == null)
                {
                    queryFilters.ModFilters = new ModFilters();
                    queryFilters.ModFilters.Disabled = false;
                }

                if (queryFilters.ModFilters.Filters == null)
                    queryFilters.ModFilters.Filters = new ModFiltersFilters();

                queryFilters.ModFilters.Filters.EmptyPrefix = Extensions.SetDataFromFilterControl(cf_emptyPrefixes);
            }
            if (!cf_emptySuffixes.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.ModFilters == null)
                {
                    queryFilters.ModFilters = new ModFilters();
                    queryFilters.ModFilters.Disabled = false;
                }

                if (queryFilters.ModFilters.Filters == null)
                    queryFilters.ModFilters.Filters = new ModFiltersFilters();

                queryFilters.ModFilters.Filters.EmptySuffix = Extensions.SetDataFromFilterControl(cf_emptySuffixes);
            }

            #endregion MOD FILTERS

            #region MAP FILTERS

            if (!cf_mapTier.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MapFilters == null)
                {
                    queryFilters.MapFilters = new MapFilters();
                    queryFilters.MapFilters.Disabled = false;
                }

                if (queryFilters.MapFilters.Filters == null)
                    queryFilters.MapFilters.Filters = new MapFiltersFilters();

                queryFilters.MapFilters.Filters.MapTier = Extensions.SetDataFromFilterControl(cf_mapTier);
            }

            if (!cf_mapPacksize.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MapFilters == null)
                {
                    queryFilters.MapFilters = new MapFilters();
                    queryFilters.MapFilters.Disabled = false;
                }

                if (queryFilters.MapFilters.Filters == null)
                    queryFilters.MapFilters.Filters = new MapFiltersFilters();

                queryFilters.MapFilters.Filters.MapPacksize = Extensions.SetDataFromFilterControl(cf_mapPacksize);
            }

            if (!cf_mapIIQ.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MapFilters == null)
                {
                    queryFilters.MapFilters = new MapFilters();
                    queryFilters.MapFilters.Disabled = false;
                }

                if (queryFilters.MapFilters.Filters == null)
                    queryFilters.MapFilters.Filters = new MapFiltersFilters();

                queryFilters.MapFilters.Filters.MapIiq = Extensions.SetDataFromFilterControl(cf_mapIIQ);
            }

            if (!cf_mapIIR.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MapFilters == null)
                {
                    queryFilters.MapFilters = new MapFilters();
                    queryFilters.MapFilters.Disabled = false;
                }

                if (queryFilters.MapFilters.Filters == null)
                    queryFilters.MapFilters.Filters = new MapFiltersFilters();

                queryFilters.MapFilters.Filters.MapIir = Extensions.SetDataFromFilterControl(cf_mapIIR);
            }

            if (!cfdd_shaped.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MapFilters == null)
                {
                    queryFilters.MapFilters = new MapFilters();
                    queryFilters.MapFilters.Disabled = false;
                }

                if (queryFilters.MapFilters.Filters == null)
                    queryFilters.MapFilters.Filters = new MapFiltersFilters();

                queryFilters.MapFilters.Filters.MapShaped = new Status();
                queryFilters.MapFilters.Filters.MapShaped.Option = cfdd_shaped.GetTrueFalse();
            }
            if (!cfdd_elder.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MapFilters == null)
                {
                    queryFilters.MapFilters = new MapFilters();
                    queryFilters.MapFilters.Disabled = false;
                }

                if (queryFilters.MapFilters.Filters == null)
                    queryFilters.MapFilters.Filters = new MapFiltersFilters();

                queryFilters.MapFilters.Filters.MapElder = new Status();
                queryFilters.MapFilters.Filters.MapElder.Option = cfdd_elder.GetTrueFalse();
            }

            if (!cfdd_mapSeries.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MapFilters == null)
                {
                    queryFilters.MapFilters = new MapFilters();
                    queryFilters.MapFilters.Disabled = false;
                }

                if (queryFilters.MapFilters.Filters == null)
                    queryFilters.MapFilters.Filters = new MapFiltersFilters();

                queryFilters.MapFilters.Filters.MapSeries = new Status();
                string opt = "";
                switch (cfdd_mapSeries.GetIndex())
                {
                    case 0:
                        // This should not happen
                        break;

                    case 1:
                        opt = "warfortheatlas";
                        break;

                    case 2:
                        opt = "atlasofworlds";
                        break;

                    case 3:
                        opt = "theawakening";
                        break;

                    case 4:
                        opt = "original";
                        break;
                }

                queryFilters.MapFilters.Filters.MapSeries.Option = opt;
            }

            #endregion MAP FILTERS

            #region MISCELLANEOUS FILTERS

            if (!cf_quality.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MiscFilters == null)
                {
                    queryFilters.MiscFilters = new MiscFilters();
                    queryFilters.MiscFilters.Disabled = false;
                }
                if (queryFilters.MiscFilters.Filters == null)
                    queryFilters.MiscFilters.Filters = new MiscFiltersFilters();

                queryFilters.MiscFilters.Filters.Quality = Extensions.SetDataFromFilterControl(cf_quality);
            }

            if (!cf_itemLevel.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MiscFilters == null)
                {
                    queryFilters.MiscFilters = new MiscFilters();
                    queryFilters.MiscFilters.Disabled = false;
                }
                if (queryFilters.MiscFilters.Filters == null)
                    queryFilters.MiscFilters.Filters = new MiscFiltersFilters();

                queryFilters.MiscFilters.Filters.Ilvl = Extensions.SetDataFromFilterControl(cf_itemLevel);
            }

            if (!cf_gemLevel.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MiscFilters == null)
                {
                    queryFilters.MiscFilters = new MiscFilters();
                    queryFilters.MiscFilters.Disabled = false;
                }
                if (queryFilters.MiscFilters.Filters == null)
                    queryFilters.MiscFilters.Filters = new MiscFiltersFilters();

                queryFilters.MiscFilters.Filters.GemLevel = Extensions.SetDataFromFilterControl(cf_gemLevel);
            }

            if (!cf_gemExperience.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MiscFilters == null)
                {
                    queryFilters.MiscFilters = new MiscFilters();
                    queryFilters.MiscFilters.Disabled = false;
                }
                if (queryFilters.MiscFilters.Filters == null)
                    queryFilters.MiscFilters.Filters = new MiscFiltersFilters();

                queryFilters.MiscFilters.Filters.GemLevelProgress = Extensions.SetDataFromFilterControl(cf_gemExperience);
            }

            if (!cf_talismanTier.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MiscFilters == null)
                {
                    queryFilters.MiscFilters = new MiscFilters();
                    queryFilters.MiscFilters.Disabled = false;
                }
                if (queryFilters.MiscFilters.Filters == null)
                    queryFilters.MiscFilters.Filters = new MiscFiltersFilters();

                queryFilters.MiscFilters.Filters.TalismanTier = Extensions.SetDataFromFilterControl(cf_talismanTier);
            }

            if (!cfdd_shaperItem.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MiscFilters == null)
                {
                    queryFilters.MiscFilters = new MiscFilters();
                    queryFilters.MiscFilters.Disabled = false;
                }
                if (queryFilters.MiscFilters.Filters == null)
                    queryFilters.MiscFilters.Filters = new MiscFiltersFilters();

                queryFilters.MiscFilters.Filters.ShaperItem = new Status();
                queryFilters.MiscFilters.Filters.ShaperItem.Option = cfdd_shaperItem.GetTrueFalse();
            }

            if (!cfdd_elderItem.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MiscFilters == null)
                {
                    queryFilters.MiscFilters = new MiscFilters();
                    queryFilters.MiscFilters.Disabled = false;
                }
                if (queryFilters.MiscFilters.Filters == null)
                    queryFilters.MiscFilters.Filters = new MiscFiltersFilters();

                queryFilters.MiscFilters.Filters.ElderItem = new Status();
                queryFilters.MiscFilters.Filters.ElderItem.Option = cfdd_elderItem.GetTrueFalse();
            }

            if (!cfdd_alternateArt.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MiscFilters == null)
                {
                    queryFilters.MiscFilters = new MiscFilters();
                    queryFilters.MiscFilters.Disabled = false;
                }
                if (queryFilters.MiscFilters.Filters == null)
                    queryFilters.MiscFilters.Filters = new MiscFiltersFilters();

                queryFilters.MiscFilters.Filters.AlternateArt = new Status();
                queryFilters.MiscFilters.Filters.AlternateArt.Option = cfdd_alternateArt.GetTrueFalse();
            }

            if (!cfdd_identified.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MiscFilters == null)
                {
                    queryFilters.MiscFilters = new MiscFilters();
                    queryFilters.MiscFilters.Disabled = false;
                }
                if (queryFilters.MiscFilters.Filters == null)
                    queryFilters.MiscFilters.Filters = new MiscFiltersFilters();

                queryFilters.MiscFilters.Filters.Identified = new Status();
                queryFilters.MiscFilters.Filters.Identified.Option = cfdd_identified.GetTrueFalse();
            }

            if (!cfdd_corrupted.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MiscFilters == null)
                {
                    queryFilters.MiscFilters = new MiscFilters();
                    queryFilters.MiscFilters.Disabled = false;
                }
                if (queryFilters.MiscFilters.Filters == null)
                    queryFilters.MiscFilters.Filters = new MiscFiltersFilters();

                queryFilters.MiscFilters.Filters.Corrupted = new Status();
                queryFilters.MiscFilters.Filters.Corrupted.Option = cfdd_corrupted.GetTrueFalse();
            }

            if (!cfdd_crafted.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MiscFilters == null)
                {
                    queryFilters.MiscFilters = new MiscFilters();
                    queryFilters.MiscFilters.Disabled = false;
                }
                if (queryFilters.MiscFilters.Filters == null)
                    queryFilters.MiscFilters.Filters = new MiscFiltersFilters();

                queryFilters.MiscFilters.Filters.Crafted = new Status();
                queryFilters.MiscFilters.Filters.Crafted.Option = cfdd_crafted.GetTrueFalse();
            }

            if (!cfdd_enchanted.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MiscFilters == null)
                {
                    queryFilters.MiscFilters = new MiscFilters();
                    queryFilters.MiscFilters.Disabled = false;
                }
                if (queryFilters.MiscFilters.Filters == null)
                    queryFilters.MiscFilters.Filters = new MiscFiltersFilters();

                queryFilters.MiscFilters.Filters.Enchanted = new Status();
                queryFilters.MiscFilters.Filters.Enchanted.Option = cfdd_enchanted.GetTrueFalse();
            }

            if (!cfdd_mirrored.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MiscFilters == null)
                {
                    queryFilters.MiscFilters = new MiscFilters();
                    queryFilters.MiscFilters.Disabled = false;
                }
                if (queryFilters.MiscFilters.Filters == null)
                    queryFilters.MiscFilters.Filters = new MiscFiltersFilters();

                queryFilters.MiscFilters.Filters.Mirrored = new Status();
                queryFilters.MiscFilters.Filters.Mirrored.Option = cfdd_mirrored.GetTrueFalse();
            }

            if (!cfdd_veiled.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.MiscFilters == null)
                {
                    queryFilters.MiscFilters = new MiscFilters();
                    queryFilters.MiscFilters.Disabled = false;
                }
                if (queryFilters.MiscFilters.Filters == null)
                    queryFilters.MiscFilters.Filters = new MiscFiltersFilters();

                queryFilters.MiscFilters.Filters.Veiled = new Status();
                queryFilters.MiscFilters.Filters.Veiled.Option = cfdd_veiled.GetTrueFalse();
            }
            #endregion MISCELLANEOUS FILTERS

            #region TRADE FILTERS

            if (!cftb_trader.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.TradeFilters == null)
                {
                    queryFilters.TradeFilters = new TradeFilters();
                    //queryFilters.TradeFilters.Disabled = false;
                }
                if (queryFilters.TradeFilters.Filters == null)
                    queryFilters.TradeFilters.Filters = new TradeFiltersFilters();

                if (queryFilters.TradeFilters.Filters.Account == null)
                    queryFilters.TradeFilters.Filters.Account = new Account();

                queryFilters.TradeFilters.Filters.Account.Input = cftb_trader.GetText();
            }

            if (!cfdd_listed.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.TradeFilters == null)
                {
                    queryFilters.TradeFilters = new TradeFilters();
                    //queryFilters.TradeFilters.Disabled = false;
                }
                if (queryFilters.TradeFilters.Filters == null)
                    queryFilters.TradeFilters.Filters = new TradeFiltersFilters();

                if (queryFilters.TradeFilters.Filters.Indexed == null)
                    queryFilters.TradeFilters.Filters.Indexed = new Status();

                string opt = "";

                switch (cfdd_listed.GetIndex())
                {
                    case 1:
                        opt = "1day";
                        break;

                    case 2:
                        opt = "3day";
                        break;

                    case 3:
                        opt = "1week";
                        break;

                    case 4:
                        opt = "2week";
                        break;

                    case 5:
                        opt = "1month";
                        break;

                    case 6:
                        opt = "2month";
                        break;
                }

                queryFilters.TradeFilters.Filters.Indexed.Option = opt;
            }

            if (!cfdd_saleType.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.TradeFilters == null)
                {
                    queryFilters.TradeFilters = new TradeFilters();
                    //queryFilters.TradeFilters.Disabled = false;
                }
                if (queryFilters.TradeFilters.Filters == null)
                    queryFilters.TradeFilters.Filters = new TradeFiltersFilters();

                if (queryFilters.TradeFilters.Filters.SaleType == null)
                    queryFilters.TradeFilters.Filters.SaleType = new Status();

                string opt = "";

                switch (cfdd_saleType.GetIndex())
                {
                    case 1:
                        opt = "priced";
                        break;

                    case 2:
                        opt = "unpriced";
                        break;
                }

                queryFilters.TradeFilters.Filters.SaleType.Option = opt;
            }

            if (!cfby_buyout.IsEmpty())
            {
                if (query.Filters == null)
                    query.Filters = queryFilters;

                if (queryFilters.TradeFilters == null)
                {
                    queryFilters.TradeFilters = new TradeFilters();
                    //queryFilters.TradeFilters.Disabled = false;
                }
                if (queryFilters.TradeFilters.Filters == null)
                    queryFilters.TradeFilters.Filters = new TradeFiltersFilters();

                if (queryFilters.TradeFilters.Filters.Price == null)
                    queryFilters.TradeFilters.Filters.Price = new SPrice();

                if (cfby_buyout.GetMin() != null)
                    queryFilters.TradeFilters.Filters.Price.Min = cfby_buyout.GetMin().ToLong();
                else
                    queryFilters.TradeFilters.Filters.Price.Min = null;

                if (cfby_buyout.GetMax() != null)
                    queryFilters.TradeFilters.Filters.Price.Max = cfby_buyout.GetMax().ToLong();
                else
                    queryFilters.TradeFilters.Filters.Price.Max = null;

                if (cfby_buyout.GetIndex() != 0)
                {
                    queryFilters.TradeFilters.Filters.Price.Option = cfby_buyout.GetSelectedItem().GetCurrencyAbb();
                }
            }

            #endregion TRADE FILTERS

            List<Stat> stats = new List<Stat>();

            Status status = new Status();

            status.Option = (cmb_accountStatus.SelectedIndex == 0) ? "any" : "online"; // Any or Online (only)

            string pseudos = string.Empty;

            if (modGroups.Count == 0)
            {
                // Create new Mod Group
                Stat modGroup = new Stat();
                modGroup.Type = "and";
                modGroup.Filters = new List<Queries.Filter>();
                stats.Add(modGroup);
            }
            else
            {
                foreach (ModGroup mg in modGroups)
                {
                    string type = mg.type;
                    // Create new Mod Group
                    Stat modGroup = new Stat();
                    modGroup.Type = type;
                    modGroup.Filters = new List<Queries.Filter>();

                    if (type == "count")
                    {
                        if (!mg.isMinMaxEmpty())
                        {
                            modGroup.Value = new StatValue();

                            if (mg.min != "min")
                                if (mg.min.ToLong() != 0)
                                    modGroup.Value.Min = mg.min.ToLong();

                            if (mg.max != "max")
                                if (mg.max.ToLong() != 0)
                                    modGroup.Value.Max = mg.max.ToLong();
                        }
                    }
                    if (type == "weight")
                    {
                        if (!mg.isMinMaxEmpty())
                        {
                            modGroup.Value = new StatValue();

                            if (mg.min != "min")
                                if (mg.min.ToLong() != 0)
                                    modGroup.Value.Min = mg.min.ToLong();

                            if (mg.max != "max")
                                if (mg.max.ToLong() != 0)
                                    modGroup.Value.Max = mg.max.ToLong();
                        }
                    }

                    foreach (ModSearch cms in mg.GetMods())
                    {
                        if (cms.mod == null)
                            continue;

                        if (modGroup.Filters == null)
                            modGroup.Filters = new List<Queries.Filter>();

                        Queries.Filter f = new Queries.Filter(Data.GetModIdByText(cms.mod));

                        if (f.Id.Contains("pseudo"))
                        {
                            pseudos += "&pseudos[]=" + f.Id;
                        }

                        if (!cms.isMinMaxEmpty())
                        {
                            f.Value = new ModValue();
                            if (cms.min != "min")
                                if (cms.min.ToLong() != 0)
                                    f.Value.Min = cms.min.ToLong();

                            if (cms.max != "max")
                                if (cms.max.ToLong() != 0)
                                    f.Value.Max = cms.max.ToLong();
                        }
                        if (cms.isWeighted)
                        {
                            if (f.Value == null)
                                f.Value = new ModValue();

                            if (cms.weight != "weight")
                                if (cms.weight.ToLong() != 0)
                                    f.Value.Weight = cms.weight.ToLong();
                        }

                        f.Disabled = false;
                        modGroup.Filters.Add(f);
                    }

                    stats.Add(modGroup);
                }
            }

            TypeFilters tf = new TypeFilters();
            tf.Filters = new TypeFiltersFilters();
            tf.Filters.Rarity = new Status();
            if (cmb_itemRarity.Text != string.Empty && cmb_itemRarity.Text != "Any")
            {
                tf.Filters.Rarity.Option = Data.GetEntryFromRow(Data.itemRarity, 0, "text", cmb_itemRarity.Text);
                queryFilters.TypeFilters = tf;
            }

            searchQuery.Query = query;
            searchQuery.Query.Status = status;
            searchQuery.Query.Stats = stats;
            searchQuery.Sort = sort;

            sort.Price = "asc";
            Console.WriteLine(searchQuery.ToJson());
            GetTradeData(searchQuery.ToJson(), searchQuery, null, pseudos, "", false, header);
        }

        #region Resizing

        public void ResizeWindow(Size size)
        {
            this.Size = size;
            Config.searchHeight = size.Height;
            Config.searchWidth = size.Width;
            Config.Save();
        }

        private bool isResizing = false;

        private void resizeBar_MouseMove(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;

            if (isResizing)
            {
                if (pb.Name == "pb_horizontal")
                {
                    int newHeight = e.Y + this.Height;
                    this.Height = (newHeight < this.MinimumSize.Height) ? this.MinimumSize.Height : newHeight;
                }
                if (pb.Name == "pb_vertical")
                {
                    int newWidth = e.X + this.Width;
                    this.Width = (newWidth < this.MinimumSize.Width) ? this.MinimumSize.Width : newWidth;

                    if (tabControl1.SelectedIndex != 0 && tabControl1.SelectedIndex != 1)
                        ResizeSearchResults((FlowLayoutPanel)tabControl1.TabPages[tabControl1.SelectedIndex].Controls[0]);
                }
            }
        }

        private void resizeBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                isResizing = true;
        }

        private void resizeBar_MouseUp(object sender, MouseEventArgs e)
        {
            isResizing = false;
            Config.searchHeight = this.Height;
            Config.searchWidth = this.Width;
            Config.Save();
        }

        #endregion Resizing
        

        public void FLP_MouseWheel(object sender, MouseEventArgs e)
        {
            FlowLayoutPanel flp = (FlowLayoutPanel)sender;
            
            QueryManager qm = new QueryManager();
            TabPage parent = (TabPage)flp.Parent;

            foreach (QueryManager q in queries)
                if (q.page == parent)
                    qm = q;
            
            if(qm.sq != null) // Item Search
            {
                if (flp.VerticalScroll.Maximum - flp.VerticalScroll.Value < flp.Size.Height && !qm.isLoading)
                {
                    qm.isLoading = true;
                    string ur = qm.GetFetchURLNew();

                    if (ur != string.Empty)
                    {
                        TradeData td = QueryHelper.GetTradeData(ur);
                        if (td != null)
                        {
                            foreach (Result result in td.Result)
                            {
                                if (result == null)
                                    continue;
                                if (result.IsAfk() && Config.filterAFKPlayers)
                                    continue;

                                AddResultToFlp(flp, result, qm.sq, parent);
                                ResizeSearchResults(flp);
                            }
                        }
                    }
                    qm.isLoading = false;
                }
            }
            else
            {
                if (flp.VerticalScroll.Maximum - flp.VerticalScroll.Value < flp.Size.Height && !qm.isLoading)
                {
                    qm.isLoading = true;
                    string ur = qm.GetBulkURLNew();

                    if (ur != string.Empty)
                    {
                        TradeData td = QueryHelper.GetTradeData(ur);
                        if (td != null)
                        {
                            
                            // Filter Results and add them to flp
                            foreach (Result result in td.Result)
                            {
                                if (result == null)
                                    continue;

                                if (result.IsAfk() && Config.filterAFKPlayers)
                                    continue;

                                AddBulkResultToFlp(flp, result, qm.bq, parent);
                            }
                        }
                    }
                    qm.isLoading = false;
                }
            }

            
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            btn_search.Enabled = false;
            btn_reset.Enabled = false;
            btn_close.Enabled = false;
            if (tabControl1.SelectedIndex == 0)
                GenerateSearchQuery();
            else if (tabControl1.SelectedIndex == 1)
                GenerateBulkQuery();
        }

        #region Textbox Events

        private void search_tb_itemName_TextChanged(object sender, EventArgs e)
        {
            if (tb_itemName.Text.Length > 2)
            {
                List<string> results = new List<string>();
                List<string> resultsToRemove = new List<string>();
                results = TextboxEvents.SearchDataTableMods(Data.items, 3, tb_itemName.Text.Replace("'", "%27"));
                lb_itemName.Visible = (results.Count > 0) ? true : false;
                lb_itemName.Items.Clear();

                foreach (string s in results)
                    if (s.Contains("Map") && Config.hideLegacyMaps)
                    {
                        foreach(string s1 in Data.oldAtlasVersions)
                        {
                            if(s.Contains(s1))
                                resultsToRemove.Add(s);
                        }
                    }

                

                foreach (string s2 in resultsToRemove)
                    results.Remove(s2);

                foreach (string result in results)
                {
                    lb_itemName.Items.Add(result.Replace("%27", "'"));
                }

                if (results.Count > 14)
                    lb_itemName.Size = new Size(lb_itemName.Width, 15 * lb_itemName.ItemHeight);
                else
                    lb_itemName.Size = new Size(lb_itemName.Width, (lb_itemName.Items.Count + 1) * lb_itemName.ItemHeight);
            }
            else
            {
                lb_itemName.Visible = false;
                lb_itemName.Items.Clear();
            }
        }

        private void search_tb_itemName_Leave(object sender, EventArgs e)
        {
            if (!lb_itemName.Focused)
            {
                lb_itemName.Items.Clear();
                lb_itemName.Visible = false;
            }
        }

        private void search_lb_itemName_MouseDown(object sender, MouseEventArgs e)
        {
            if (lb_itemName.SelectedIndex != -1)
                tb_itemName.Text = lb_itemName.Items[lb_itemName.SelectedIndex].ToString();

            lb_itemName.Visible = false;
            tb_itemName.Focus();
        }

        #endregion Textbox Events

        #region Button Events

        private void btn_addModGroup_Click(object sender, EventArgs e)
        {
            string type = "and";
            switch (cmb_modGroupType.SelectedIndex)
            {
                case 0:
                    type = "and";
                    break;

                case 1:
                    type = "not";
                    break;

                case 2:
                    type = "if";
                    break;

                case 3:
                    type = "count";
                    break;

                case 4:
                    type = "weight";
                    break;
            }

            AddModGroup(type);
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            hide = true;
            this.Hide();
        }

        private void btn_reset_Click(object sender, EventArgs e)
        {
            List<object> controls = new List<object>();

            //                                     Panel      FlowLayout
            foreach (Control c in ec_weaponFilters.Controls[1].Controls[0].Controls)
                controls.Add(c);

            foreach (Control c in ec_armourFilters.Controls[1].Controls[0].Controls)
                controls.Add(c);

            foreach (Control c in ec_socketFilters.Controls[1].Controls[0].Controls)
                controls.Add(c);

            foreach (Control c in ec_Requirements.Controls[1].Controls[0].Controls)
                controls.Add(c);

            foreach (Control c in ec_modFilters.Controls[1].Controls[0].Controls)
                controls.Add(c);

            foreach (Control c in ec_mapFilters.Controls[1].Controls[0].Controls)
                controls.Add(c);

            foreach (Control c in ec_miscellaneous.Controls[1].Controls[0].Controls)
                controls.Add(c);

            foreach (Control c in ec_tradeFilters.Controls[1].Controls[0].Controls)
                controls.Add(c);

            modGroups.Clear();
            AddModGroup("and");

            foreach (object c in controls)
            {
                if (c.GetType() == typeof(Controls.Filter))
                {
                    Controls.Filter ctrl = (Controls.Filter)c;
                    ctrl.Reset();
                }
                else if (c.GetType() == typeof(Filter_Sockets))
                {
                    Filter_Sockets ctrl = (Filter_Sockets)c;
                    ctrl.Reset();
                }
                else if (c.GetType() == typeof(Filter_DropDown))
                {
                    Filter_DropDown ctrl = (Filter_DropDown)c;
                    ctrl.Reset();
                }
                else if (c.GetType() == typeof(Filter_Buyout))
                {
                    Filter_Buyout ctrl = (Filter_Buyout)c;
                    ctrl.Reset();
                }
                else if (c.GetType() == typeof(Filter_Textbox))
                {
                    Filter_Textbox ctrl = (Filter_Textbox)c;
                    ctrl.Reset();
                }
            }
            foreach(FastBulkButton fbb in bulkButtons)
            {
                tb_BulkMinimumStock.Text = "";
                tb_bulkSellerAccount.Text = "";
                tb_filter.Text = "";
                fbb.isSelected = false;
                fbb.isWanted = false;
                fbb.isHighlighted = false;
            }

            tb_itemName.Text = string.Empty;
            cmb_itemCategory.SelectedIndex = 0;
            cmb_itemRarity.SelectedIndex = 0;
        }

        private void btn_Reload_Click(object sender, EventArgs e)
        {
            ThemeLoader.GetAllThemes();
        }

        private void btn_closeTab_Click(object sender, EventArgs e)
        {
            List<Control> controls = new List<Control>();
            controls = tabControl1.TabPages[tabControl1.SelectedIndex].GetControls();

            // Dispose every Control
            foreach (Control c in controls)
                c.Dispose();

            // Let the GarbageCollector do his important work :)
            GC.Collect();
            GC.WaitForPendingFinalizers();

            tabControl1.TabPages.RemoveAt(tabControl1.SelectedIndex);
            headers.RemoveAt(tabControl1.SelectedIndex);
            queries.RemoveAt(tabControl1.SelectedIndex);
        }

        private void btn_defaultLayout_Click(object sender, EventArgs e)
        {
            ResizeWindow(new Size(690, Config.searchHeight));
        }

        private void btn_twoColumns_Click(object sender, EventArgs e)
        {
            ResizeWindow(new Size(1360, Config.searchHeight));
        }

        #endregion Button Events

        #region Bulk

        public void GenerateBulkForm()
        {
            flp_want.SuspendLayout();
            flp_have.SuspendLayout();

            GenerateButtonsNew(new ExpandCollapse[] { ec_haveCurrency, ec_wantCurrency });
            GenerateButtonsNew(new ExpandCollapse[] { ec_haveFrags, ec_wantFrags });
            GenerateButtonsNew(new ExpandCollapse[] { ec_haveResonators, ec_wantResonators });
            GenerateButtonsNew(new ExpandCollapse[] { ec_haveFossils, ec_wantFossils });
            GenerateButtonsNew(new ExpandCollapse[] { ec_haveEssences, ec_wantEssences });

            GenerateButtonsNew(new ExpandCollapse[] { ec_wantCards, ec_haveCards });
            GenerateButtonsNew(new ExpandCollapse[] { ec_wantMaps, ec_haveMaps });
            GenerateButtonsNew(new ExpandCollapse[] { ec_wantShapedMap, ec_haveShapedMap });
            GenerateButtonsNew(new ExpandCollapse[] { ec_wantElderMap, ec_haveElderMap });

            foreach (ExpandCollapse ec in new ExpandCollapse[] { ec_haveFrags, ec_wantFrags, ec_haveResonators, ec_wantResonators, ec_haveFossils, ec_wantFossils, ec_haveEssences, ec_wantEssences, ec_wantCards, ec_haveCards, ec_wantMaps, ec_haveMaps, ec_wantShapedMap, ec_haveShapedMap, ec_wantElderMap, ec_haveElderMap })
            {
                ec.TogglePanel(ec, null);
            }

            flp_want.ResumeLayout(false);
            flp_have.ResumeLayout(false);
        }

        public void GenerateBulkQuery()
        {
            BulkQuery bq = new BulkQuery();
            bq.Exchange = new Exchange();
            bq.Exchange.Status = new BulkQueryStatus();
            bq.Exchange.Status.Option = (fc_bulkAccStatus.SelectedIndex == 0) ? "any" : "online";

            if (tb_bulkSellerAccount.Text != string.Empty)
                bq.Exchange.Account = tb_bulkSellerAccount.Text;
            if (tb_BulkMinimumStock.Text != string.Empty)
                bq.Exchange.Minimum = tb_BulkMinimumStock.Text;

            List<string> want = new List<string>();
            List<string> have = new List<string>();

            foreach (FastBulkButton bb in bulkButtons)
            {
                if (bb.isSelected)
                {
                    if (bb.isWanted)
                        want.Add(bb.id);
                    else
                        have.Add(bb.id);
                }
            }

            if (want.Count > 0)
                bq.Exchange.Want = want;
            if (have.Count > 0)
                bq.Exchange.Have = have;

            string s1 = "";
            string s2 = "";

            for (int i = 0; i < have.Count; i++)
                s1 += have[i] + ((i != have.Count - 1) ? "," : "");

            for (int i = 0; i < want.Count; i++)
                s2 += want[i] + ((i != want.Count - 1) ? "," : "");

            if (s1 == string.Empty)
                s1 = "any";

            if (s2 == string.Empty)
                s2 = "any";

            string header = string.Format("Trade {0} for {1}", s1, s2);

            headers.Add(header);
            GenerateControlsFromQueryResults(bq.ToJson(), bq, null, string.Format("{0} - {1}", s1, s2));
        }

        public void AddBulkResultToFlp(FlowLayoutPanel panel, Result result, BulkQuery bq, TabPage page)
        {
            ExchangeResult er = new ExchangeResult();
            er.CopyColors();
            er.result = result;
            er.bq = bq;
            er.page = page;

            er.LoadResult();
            panel.Controls.Add(er);
        }

        public void GenerateControlsFromQueryResults(string query, BulkQuery bq, TabPage page = null, string text = "")
        {
            QueryManager qm = new QueryManager();
            qm.bq = bq;
            qm.bulkResults = qm.GetBulkResults(query);

            if (qm.bulkResults != null && qm.bulkResults.Count > 1)
            {
                // Generate fetch url from querymanager
                string fetchURL = qm.GetBulkURLNew();

                // recieve actual trade data
                TradeData tradeData = QueryHelper.GetTradeData(fetchURL);

                if (tradeData != null)
                {
                    if (tradeData.Result == null || tradeData.Result.Count == 0)
                        return;

                    FlowLayoutPanel flp = new FlowLayoutPanel();
                    if (page == null)
                    {
                        // Create a new Tab and FlowLayoutPanel

                        TabPage tp = new TabPage((text != string.Empty) ? text : "Bulk Results");
                        tp.Name = "tp_results";
                        tp.BackColor = ThemeLoader.BackColor;
                        tp.Controls.Add(flp);
                        flp.Location = new Point(0, 0);
                        flp.BackColor = Color.Transparent;
                        flp.Dock = DockStyle.Fill;
                        flp.AutoScroll = true;
                        flp.MouseWheel += FLP_MouseWheel;
                        qm.page = tp;
                        tabControl1.TabPages.Add(tp);
                        page = tp;
                    }
                    else
                    {
                        // We get here when we Click on a sortable mod
                        foreach (Control c in page.Controls) // Find FlowLayoutPanel
                        {
                            if (c.GetType() == typeof(FlowLayoutPanel))
                            {
                                c.Controls.Clear(); // Remove all results
                                flp = (FlowLayoutPanel)c;
                            }
                        }
                    }

                    // Filter Results and add them to flp
                    foreach (Result result in tradeData.Result)
                    {
                        if (result == null)
                            continue;

                        if (result.IsAfk() && Config.filterAFKPlayers)
                            continue;

                        AddBulkResultToFlp(flp, result, bq, page);
                    }

                    queries.Add(qm);
                    tabControl1.SelectedIndex = tabControl1.TabPages.IndexOf(page);
                }
            }
            btn_search.Enabled = true;
            btn_reset.Enabled = true;
            btn_close.Enabled = true;
        }

        public void GenerateButtonsNew(ExpandCollapse[] panels)
        {
            Currency c = Data.loadedCurrency;
            List<CurrencyElement> currencyElements = new List<CurrencyElement>();
            List<CurrencyCard> currencyCards = new List<CurrencyCard>();
            string dir = "./images/";

            /// Determine which type of item
            foreach (ExpandCollapse ec in panels)
            {
                if (!ec.Name.ContainsAnyElement(new string[] { "Card", "Maps", "ShapedMap", "ElderMap" }))
                {
                    if (ec.Name.Contains("Currency"))
                        currencyElements = c.Result.Currency;
                    if (ec.Name.Contains("Frags"))
                        currencyElements = c.Result.Fragments;
                    if (ec.Name.Contains("Resonators"))
                        currencyElements = c.Result.Resonators;
                    if (ec.Name.Contains("Fossils"))
                        currencyElements = c.Result.Fossils;
                    if (ec.Name.Contains("Essences"))
                        currencyElements = c.Result.Essences;
                }
                else
                {
                    if (ec.Name.Contains("Card"))
                        currencyCards = c.Result.Cards;
                    if (ec.Name.Contains("Maps"))
                        currencyCards = c.Result.Maps;
                    if (ec.Name.Contains("ShapedMap"))
                        currencyCards = c.Result.ShapedMaps;
                    if (ec.Name.Contains("ElderMap"))
                        currencyCards = c.Result.ElderMaps;
                }
            }

            if (currencyCards.Count > 0)
            {
                foreach (ExpandCollapse expand in panels)
                {
                    int row = 1;
                    int bbWidth = 150;
                    int bbHeight = 22;
                    int x = 0;
                    int totalRows = (int)Math.Ceiling(currencyCards.Count / 4d);
                    int height = (bbHeight + 3) * totalRows + (3 * totalRows);
                    Size ecSize = new Size(635, height + 40);

                    expand.Size = ecSize;
                    expand.SizeExpanded = ecSize;

                    for (int i = 0; i < currencyCards.Count; i++)
                    {
                        row = (int)Math.Ceiling((i + 1) / 4d);

                        FastBulkButton fbb = new FastBulkButton
                        {
                            itemName = currencyCards[i].Text,
                            text = currencyCards[i].Text,
                            id = currencyCards[i].Id,
                            tip = currencyCards[i].Text,
                            isWanted = expand.Name.Contains("want"),
                            Location = new Point(((bbWidth + 3) * x) + (3 * x), (bbHeight + 3) * (row - 1) + (3 * row))
                        };
                        fbb.CopyColors();
                        bulkButtons.Add(fbb);
                        expand.DropZone.Controls.Add(fbb);

                        x++;
                        if (x > 3)
                            x = 0;
                    }
                }
            }

            if (currencyElements.Count > 0)
            {
                foreach (ExpandCollapse expand in panels)
                {
                    int row = 1;
                    int size = 32;
                    int x = 0;
                    int totalRows = (int)Math.Ceiling(currencyElements.Count / 14.0d);
                    int height = (size + 6) * totalRows + (6 * totalRows);
                    Size ecSize = new Size(635, height + 40);

                    expand.Size = ecSize;
                    expand.SizeExpanded = ecSize;

                    for (int i = 0; i < currencyElements.Count; i++)
                    {
                        row = (int)Math.Ceiling((i + 1) / 14.0d);

                        FastBulkButton fbb = new FastBulkButton
                        {
                            itemName = currencyElements[i].Text,
                            image = Image.FromFile(dir + currencyElements[i].Id + ".png"),
                            id = currencyElements[i].Id,
                            tip = currencyElements[i].Text,
                            isWanted = expand.Name.Contains("want"),
                            Location = new Point(((size + 6) * x) + (6 * x), (size + 6) * (row - 1) + (6 * row))
                        };
                        fbb.CopyColors();
                        fbb.Refresh();
                        bulkButtons.Add(fbb);
                        expand.DropZone.Controls.Add(fbb);

                        x++;
                        if (x > 13)
                            x = 0;
                    }
                }
            }
        }

        private void tb_filter_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;

            string s = tb.Text;

            if (s.Contains(";"))
            {
                string[] splitted = s.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (FastBulkButton bb in bulkButtons)
                {
                    bool contains = false;
                    foreach (string s2 in splitted)
                    {
                        if (s2 == string.Empty)
                            bb.isHighlighted = false;
                        else
                        {
                            contains = bb.itemName.Contains(s2);
                        }
                    }
                    bb.isHighlighted = contains;
                }
            }
            else
            {
                foreach (FastBulkButton bb in bulkButtons)
                {
                    if (s == string.Empty)
                        bb.isHighlighted = false;
                    else
                        bb.isHighlighted = bb.itemName.Contains(tb.Text);
                }
            }
        }

        #endregion Bulk

        private void btn_settings_Click(object sender, EventArgs e)
        {
            Form_Settings form_Settings = new Form_Settings();
            form_Settings.ShowDialog(this);
        }

        private void Form_Search_Load(object sender, EventArgs e)
        {

        }
    }
}