﻿namespace PoE_Trade_Overlay.Forms
{
    partial class ThemeEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb = new System.Windows.Forms.ListBox();
            this.tab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.B = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.G = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.R = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.A = new System.Windows.Forms.NumericUpDown();
            this.lblA = new System.Windows.Forms.Label();
            this.select = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tb_themeName = new System.Windows.Forms.TextBox();
            this.cmb = new System.Windows.Forms.ComboBox();
            this.lv = new System.Windows.Forms.ListView();
            this.col_type = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_val = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button2 = new System.Windows.Forms.Button();
            this.load = new System.Windows.Forms.Button();
            this.pb = new System.Windows.Forms.PictureBox();
            this.modLabel1 = new PoE_Trade_Overlay.Controls.ModLabel();
            this.fc = new PoE_Trade_Overlay.Controls.FlattenCombo();
            this.expandCollapse1 = new PoE_Trade_Overlay.Controls.ExpandCollapse();
            this.exchangeResult1 = new PoE_Trade_Overlay.Controls.ExchangeResult();
            this.filter1 = new PoE_Trade_Overlay.Controls.Filter();
            this.filter_Buyout1 = new PoE_Trade_Overlay.Controls.Filter_Buyout();
            this.filter_DropDown1 = new PoE_Trade_Overlay.Controls.Filter_DropDown();
            this.filter_Sockets1 = new PoE_Trade_Overlay.Controls.Filter_Sockets();
            this.modGroup1 = new PoE_Trade_Overlay.Controls.ModGroup();
            this.modSearch1 = new PoE_Trade_Overlay.Controls.ModSearch();
            this.searchResult_Minimalistic1 = new PoE_Trade_Overlay.Controls.SearchResult_Minimalistic();
            this.toggle1 = new PoE_Trade_Overlay.Controls.Toggle();
            this.dragBar1 = new PoE_Trade_Overlay.Controls.DragBar();
            this.tab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.tabPage12.SuspendLayout();
            this.tabPage13.SuspendLayout();
            this.tabPage14.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).BeginInit();
            this.expandCollapse1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lb
            // 
            this.lb.FormattingEnabled = true;
            this.lb.Items.AddRange(new object[] {
            "BulkButton",
            "DropDown",
            "ExpandCollapse",
            "ExchangeResult",
            "Filter",
            "Filter_Buyout",
            "Filter_DropDown",
            "Filter_Sockets",
            "Filter_Textbox",
            "ModGroup",
            "ModSearch",
            "RichTextBox",
            "SearchResult",
            "Toggle",
            "DragBar"});
            this.lb.Location = new System.Drawing.Point(12, 25);
            this.lb.Name = "lb";
            this.lb.Size = new System.Drawing.Size(120, 485);
            this.lb.TabIndex = 0;
            this.lb.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // tab
            // 
            this.tab.Controls.Add(this.tabPage1);
            this.tab.Controls.Add(this.tabPage2);
            this.tab.Controls.Add(this.tabPage3);
            this.tab.Controls.Add(this.tabPage4);
            this.tab.Controls.Add(this.tabPage5);
            this.tab.Controls.Add(this.tabPage6);
            this.tab.Controls.Add(this.tabPage7);
            this.tab.Controls.Add(this.tabPage8);
            this.tab.Controls.Add(this.tabPage15);
            this.tab.Controls.Add(this.tabPage9);
            this.tab.Controls.Add(this.tabPage10);
            this.tab.Controls.Add(this.tabPage11);
            this.tab.Controls.Add(this.tabPage12);
            this.tab.Controls.Add(this.tabPage13);
            this.tab.Controls.Add(this.tabPage14);
            this.tab.Location = new System.Drawing.Point(138, 51);
            this.tab.Name = "tab";
            this.tab.SelectedIndex = 0;
            this.tab.Size = new System.Drawing.Size(678, 267);
            this.tab.TabIndex = 1;
            this.tab.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.modLabel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(670, 241);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "BulkButton";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.fc);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(670, 241);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "DropDown";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.expandCollapse1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(670, 241);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "ExpandCollapse";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.exchangeResult1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(670, 241);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "ExchangeResult";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.filter1);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(670, 241);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Filter";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.filter_Buyout1);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(670, 241);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Filter_Buyout";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.filter_DropDown1);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(670, 241);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Filter_DropDown";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.filter_Sockets1);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(670, 241);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Filter_Sockets";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // tabPage15
            // 
            this.tabPage15.Location = new System.Drawing.Point(4, 22);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage15.Size = new System.Drawing.Size(670, 241);
            this.tabPage15.TabIndex = 14;
            this.tabPage15.Text = "Filter_Textbox";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.modGroup1);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(670, 241);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "ModGroup";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.modSearch1);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(670, 241);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "ModSearch";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.richTextBox1);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(670, 241);
            this.tabPage11.TabIndex = 10;
            this.tabPage11.Text = "RichTextBox";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(6, 6);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(306, 129);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.searchResult_Minimalistic1);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(670, 241);
            this.tabPage12.TabIndex = 11;
            this.tabPage12.Text = "SearchResult";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.toggle1);
            this.tabPage13.Location = new System.Drawing.Point(4, 22);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage13.Size = new System.Drawing.Size(670, 241);
            this.tabPage13.TabIndex = 12;
            this.tabPage13.Text = "Toggle";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.dragBar1);
            this.tabPage14.Location = new System.Drawing.Point(4, 22);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage14.Size = new System.Drawing.Size(670, 241);
            this.tabPage14.TabIndex = 13;
            this.tabPage14.Text = "DragBar";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(135, 321);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Properties";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Properties";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.B);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.G);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.R);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.A);
            this.panel1.Controls.Add(this.lblA);
            this.panel1.Controls.Add(this.pb);
            this.panel1.Controls.Add(this.select);
            this.panel1.Location = new System.Drawing.Point(555, 337);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(253, 173);
            this.panel1.TabIndex = 5;
            // 
            // B
            // 
            this.B.Location = new System.Drawing.Point(206, 35);
            this.B.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(39, 20);
            this.B.TabIndex = 7;
            this.B.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.B.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(189, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "B";
            // 
            // G
            // 
            this.G.Location = new System.Drawing.Point(144, 35);
            this.G.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.G.Name = "G";
            this.G.Size = new System.Drawing.Size(39, 20);
            this.G.TabIndex = 6;
            this.G.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.G.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(127, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "G";
            // 
            // R
            // 
            this.R.Location = new System.Drawing.Point(82, 35);
            this.R.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.R.Name = "R";
            this.R.Size = new System.Drawing.Size(39, 20);
            this.R.TabIndex = 5;
            this.R.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.R.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(65, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(15, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "R";
            // 
            // A
            // 
            this.A.Location = new System.Drawing.Point(20, 35);
            this.A.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.A.Name = "A";
            this.A.Size = new System.Drawing.Size(39, 20);
            this.A.TabIndex = 4;
            this.A.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.A.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // lblA
            // 
            this.lblA.AutoSize = true;
            this.lblA.Location = new System.Drawing.Point(3, 37);
            this.lblA.Name = "lblA";
            this.lblA.Size = new System.Drawing.Size(14, 13);
            this.lblA.TabIndex = 3;
            this.lblA.Text = "A";
            // 
            // select
            // 
            this.select.Location = new System.Drawing.Point(6, 6);
            this.select.Name = "select";
            this.select.Size = new System.Drawing.Size(239, 23);
            this.select.TabIndex = 0;
            this.select.Text = "btn";
            this.select.UseVisualStyleBackColor = true;
            this.select.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(244, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 21);
            this.button1.TabIndex = 6;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tb_themeName
            // 
            this.tb_themeName.Location = new System.Drawing.Point(138, 25);
            this.tb_themeName.Name = "tb_themeName";
            this.tb_themeName.Size = new System.Drawing.Size(100, 20);
            this.tb_themeName.TabIndex = 7;
            // 
            // cmb
            // 
            this.cmb.FormattingEnabled = true;
            this.cmb.Location = new System.Drawing.Point(673, 24);
            this.cmb.Name = "cmb";
            this.cmb.Size = new System.Drawing.Size(143, 21);
            this.cmb.TabIndex = 8;
            // 
            // lv
            // 
            this.lv.AutoArrange = false;
            this.lv.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_type,
            this.col_name,
            this.col_val});
            this.lv.FullRowSelect = true;
            this.lv.GridLines = true;
            this.lv.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lv.Location = new System.Drawing.Point(138, 337);
            this.lv.MultiSelect = false;
            this.lv.Name = "lv";
            this.lv.Size = new System.Drawing.Size(411, 173);
            this.lv.TabIndex = 9;
            this.lv.UseCompatibleStateImageBehavior = false;
            this.lv.View = System.Windows.Forms.View.Details;
            this.lv.SelectedIndexChanged += new System.EventHandler(this.lv_SelectedIndexChanged);
            // 
            // col_type
            // 
            this.col_type.Text = "Type";
            this.col_type.Width = 100;
            // 
            // col_name
            // 
            this.col_name.Text = "Name";
            this.col_name.Width = 200;
            // 
            // col_val
            // 
            this.col_val.Text = "Value";
            this.col_val.Width = 100;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(325, 24);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(136, 21);
            this.button2.TabIndex = 10;
            this.button2.Text = "Set TabPage BackColor";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // load
            // 
            this.load.Location = new System.Drawing.Point(592, 24);
            this.load.Name = "load";
            this.load.Size = new System.Drawing.Size(75, 21);
            this.load.TabIndex = 11;
            this.load.Text = "Load";
            this.load.UseVisualStyleBackColor = true;
            this.load.Click += new System.EventHandler(this.load_Click);
            // 
            // pb
            // 
            this.pb.Location = new System.Drawing.Point(6, 61);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(239, 109);
            this.pb.TabIndex = 1;
            this.pb.TabStop = false;
            // 
            // modLabel1
            // 
            this.modLabel1.Location = new System.Drawing.Point(286, 106);
            this.modLabel1.modInfo = null;
            this.modLabel1.Name = "modLabel1";
            this.modLabel1.Size = new System.Drawing.Size(310, 13);
            this.modLabel1.TabIndex = 0;
            this.modLabel1.Text = "modLabel1";
            // 
            // fc
            // 
            this.fc.AutoComp = false;
            this.fc.BorderColor = System.Drawing.Color.Black;
            this.fc.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.fc.ButtonColor = System.Drawing.SystemColors.Control;
            this.fc.Data = null;
            this.fc.DropDownColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.fc.ForeColorDrop = System.Drawing.Color.Empty;
            this.fc.FormattingEnabled = true;
            this.fc.Items.AddRange(new object[] {
            "One ",
            "Two",
            "Three"});
            this.fc.Location = new System.Drawing.Point(6, 6);
            this.fc.Name = "fc";
            this.fc.Size = new System.Drawing.Size(121, 21);
            this.fc.TabIndex = 0;
            // 
            // expandCollapse1
            // 
            this.expandCollapse1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            // 
            // expandCollapse1.DropZone
            // 
            this.expandCollapse1.DropZone.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.expandCollapse1.DropZone.BackColor = System.Drawing.Color.Transparent;
            this.expandCollapse1.DropZone.Location = new System.Drawing.Point(3, 25);
            this.expandCollapse1.DropZone.MinimumSize = new System.Drawing.Size(522, 20);
            this.expandCollapse1.DropZone.Name = "DropZone";
            this.expandCollapse1.DropZone.Size = new System.Drawing.Size(522, 122);
            this.expandCollapse1.DropZone.TabIndex = 0;
            this.expandCollapse1.Header = "Header";
            this.expandCollapse1.HeaderHeight = 20;
            this.expandCollapse1.Location = new System.Drawing.Point(6, 6);
            this.expandCollapse1.Name = "expandCollapse1";
            this.expandCollapse1.Size = new System.Drawing.Size(390, 151);
            this.expandCollapse1.SizeExpanded = new System.Drawing.Size(390, 151);
            this.expandCollapse1.TabIndex = 0;
            // 
            // exchangeResult1
            // 
            this.exchangeResult1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.exchangeResult1.Font = new System.Drawing.Font("Consolas", 8.25F);
            this.exchangeResult1.Location = new System.Drawing.Point(6, 6);
            this.exchangeResult1.Name = "exchangeResult1";
            this.exchangeResult1.Size = new System.Drawing.Size(590, 172);
            this.exchangeResult1.TabIndex = 0;
            // 
            // filter1
            // 
            this.filter1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.filter1.Header = "label1";
            this.filter1.Location = new System.Drawing.Point(6, 6);
            this.filter1.Name = "filter1";
            this.filter1.QueryName = null;
            this.filter1.Size = new System.Drawing.Size(292, 20);
            this.filter1.TabIndex = 0;
            // 
            // filter_Buyout1
            // 
            this.filter_Buyout1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.filter_Buyout1.Header = "Label";
            this.filter_Buyout1.Location = new System.Drawing.Point(6, 6);
            this.filter_Buyout1.Name = "filter_Buyout1";
            this.filter_Buyout1.Size = new System.Drawing.Size(590, 26);
            this.filter_Buyout1.TabIndex = 0;
            // 
            // filter_DropDown1
            // 
            this.filter_DropDown1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.filter_DropDown1.Header = "label1";
            this.filter_DropDown1.Items = new string[0];
            this.filter_DropDown1.Location = new System.Drawing.Point(6, 6);
            this.filter_DropDown1.Name = "filter_DropDown1";
            this.filter_DropDown1.Size = new System.Drawing.Size(292, 26);
            this.filter_DropDown1.TabIndex = 0;
            // 
            // filter_Sockets1
            // 
            this.filter_Sockets1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.filter_Sockets1.Header = "label1";
            this.filter_Sockets1.Location = new System.Drawing.Point(6, 6);
            this.filter_Sockets1.Name = "filter_Sockets1";
            this.filter_Sockets1.Size = new System.Drawing.Size(584, 20);
            this.filter_Sockets1.TabIndex = 0;
            // 
            // modGroup1
            // 
            this.modGroup1.AutoSize = true;
            this.modGroup1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.modGroup1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.modGroup1.Font = new System.Drawing.Font("Consolas", 8.25F);
            this.modGroup1.Location = new System.Drawing.Point(6, 6);
            this.modGroup1.MaximumSize = new System.Drawing.Size(514, 0);
            this.modGroup1.MinimumSize = new System.Drawing.Size(514, 65);
            this.modGroup1.Name = "modGroup1";
            this.modGroup1.Size = new System.Drawing.Size(514, 65);
            this.modGroup1.TabIndex = 0;
            this.modGroup1.type = null;
            // 
            // modSearch1
            // 
            this.modSearch1.BackColor = System.Drawing.Color.Transparent;
            this.modSearch1.Location = new System.Drawing.Point(6, 6);
            this.modSearch1.Name = "modSearch1";
            this.modSearch1.Size = new System.Drawing.Size(505, 20);
            this.modSearch1.TabIndex = 0;
            // 
            // searchResult_Minimalistic1
            // 
            this.searchResult_Minimalistic1.AutoSize = true;
            this.searchResult_Minimalistic1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.searchResult_Minimalistic1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(8)))), ((int)(((byte)(8)))));
            this.searchResult_Minimalistic1.Location = new System.Drawing.Point(6, 6);
            this.searchResult_Minimalistic1.MinimumSize = new System.Drawing.Size(590, 0);
            this.searchResult_Minimalistic1.Name = "searchResult_Minimalistic1";
            this.searchResult_Minimalistic1.result = null;
            this.searchResult_Minimalistic1.Size = new System.Drawing.Size(593, 191);
            this.searchResult_Minimalistic1.TabIndex = 0;
            // 
            // toggle1
            // 
            this.toggle1.Bar_Off = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.toggle1.Bar_On = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.toggle1.BarSize = new System.Drawing.Size(40, 10);
            this.toggle1.Border_Off = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.toggle1.Border_On = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.toggle1.BorderRadius = 5;
            this.toggle1.Caption = null;
            this.toggle1.ColorMode = PoE_Trade_Overlay.Controls.ColorMode.Follow;
            this.toggle1.KnobColor = System.Drawing.Color.White;
            this.toggle1.KnobImage = global::PoE_Trade_Overlay.Properties.Resources.knob;
            this.toggle1.KnobImageOn = null;
            this.toggle1.KnobSize = new System.Drawing.Size(20, 20);
            this.toggle1.KnobStyle = PoE_Trade_Overlay.Controls.KnobStyle.Image;
            this.toggle1.Location = new System.Drawing.Point(7, 6);
            this.toggle1.Name = "toggle1";
            this.toggle1.On = false;
            this.toggle1.Size = new System.Drawing.Size(40, 20);
            this.toggle1.TabIndex = 0;
            this.toggle1.ToggleSpeed = 5;
            // 
            // dragBar1
            // 
            this.dragBar1.BarShape = PoE_Trade_Overlay.Controls.DragBarShape.Rounded;
            this.dragBar1.BarSize = new System.Drawing.Size(150, 8);
            this.dragBar1.BorderRadius = 2;
            this.dragBar1.BorderWidth = 1F;
            this.dragBar1.Caption = "Value : ";
            this.dragBar1.ColorBar = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.dragBar1.ColorBarFill = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.dragBar1.ColorBorder = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.dragBar1.ColorKnob = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.dragBar1.drawValue = true;
            this.dragBar1.KnobShape = PoE_Trade_Overlay.Controls.DragKnobShape.Box;
            this.dragBar1.KnobSize = new System.Drawing.Size(8, 16);
            this.dragBar1.Location = new System.Drawing.Point(6, 6);
            this.dragBar1.Maximum = 100;
            this.dragBar1.Minimum = 75;
            this.dragBar1.Name = "dragBar1";
            this.dragBar1.Size = new System.Drawing.Size(192, 40);
            this.dragBar1.TabIndex = 0;
            this.dragBar1.Value = 75;
            // 
            // ThemeEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 515);
            this.Controls.Add(this.load);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.lv);
            this.Controls.Add(this.cmb);
            this.Controls.Add(this.tb_themeName);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tab);
            this.Controls.Add(this.lb);
            this.Name = "ThemeEditor";
            this.Text = "ThemeEditor";
            this.tab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            this.tabPage10.ResumeLayout(false);
            this.tabPage11.ResumeLayout(false);
            this.tabPage12.ResumeLayout(false);
            this.tabPage12.PerformLayout();
            this.tabPage13.ResumeLayout(false);
            this.tabPage14.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).EndInit();
            this.expandCollapse1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lb;
        private System.Windows.Forms.TabControl tab;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tb_themeName;
        private System.Windows.Forms.ComboBox cmb;
        private Controls.FlattenCombo fc;
        private System.Windows.Forms.ListView lv;
        private System.Windows.Forms.ColumnHeader col_type;
        private System.Windows.Forms.ColumnHeader col_name;
        private System.Windows.Forms.PictureBox pb;
        private System.Windows.Forms.Button select;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label lblA;
        private System.Windows.Forms.ColumnHeader col_val;
        private System.Windows.Forms.NumericUpDown B;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown G;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown R;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown A;
        private Controls.ExpandCollapse expandCollapse1;
        private Controls.ExchangeResult exchangeResult1;
        private Controls.Filter filter1;
        private Controls.Filter_Buyout filter_Buyout1;
        private Controls.Filter_DropDown filter_DropDown1;
        private Controls.Filter_Sockets filter_Sockets1;
        private Controls.ModGroup modGroup1;
        private Controls.ModSearch modSearch1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private Controls.SearchResult_Minimalistic searchResult_Minimalistic1;
        private Controls.Toggle toggle1;
        private System.Windows.Forms.TabPage tabPage14;
        private Controls.DragBar dragBar1;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button load;
        private Controls.ModLabel modLabel1;
    }
}