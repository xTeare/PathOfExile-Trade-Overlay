﻿namespace PoE_Trade_Overlay.Forms
{
    partial class Form_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.panel_main = new System.Windows.Forms.Panel();
            this.btn_search = new NoFocusButton();
            this.btn_settings = new NoFocusButton();
            this.pb_drag = new System.Windows.Forms.PictureBox();
            this.notifyIcon2 = new System.Windows.Forms.NotifyIcon(this.components);
            this.panel_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_drag)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_main
            // 
            this.panel_main.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.panel_main.Controls.Add(this.btn_search);
            this.panel_main.Controls.Add(this.btn_settings);
            this.panel_main.Controls.Add(this.pb_drag);
            this.panel_main.Location = new System.Drawing.Point(81, 519);
            this.panel_main.Name = "panel_main";
            this.panel_main.Size = new System.Drawing.Size(119, 25);
            this.panel_main.TabIndex = 0;
            // 
            // btn_search
            // 
            this.btn_search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.btn_search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_search.FlatAppearance.BorderSize = 0;
            this.btn_search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_search.Font = new System.Drawing.Font("Consolas", 9F);
            this.btn_search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_search.Location = new System.Drawing.Point(28, 3);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(61, 19);
            this.btn_search.TabIndex = 4;
            this.btn_search.Text = "Search";
            this.btn_search.UseVisualStyleBackColor = false;
            this.btn_search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // btn_settings
            // 
            this.btn_settings.BackgroundImage = global::PoE_Trade_Overlay.Properties.Resources.cog;
            this.btn_settings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_settings.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.btn_settings.FlatAppearance.BorderSize = 0;
            this.btn_settings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_settings.Location = new System.Drawing.Point(95, 3);
            this.btn_settings.Name = "btn_settings";
            this.btn_settings.Size = new System.Drawing.Size(19, 19);
            this.btn_settings.TabIndex = 3;
            this.btn_settings.UseVisualStyleBackColor = true;
            this.btn_settings.Click += new System.EventHandler(this.btn_settings_Click);
            // 
            // pb_drag
            // 
            this.pb_drag.Image = global::PoE_Trade_Overlay.Properties.Resources.hamburger_menu;
            this.pb_drag.Location = new System.Drawing.Point(3, 3);
            this.pb_drag.Name = "pb_drag";
            this.pb_drag.Size = new System.Drawing.Size(19, 19);
            this.pb_drag.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb_drag.TabIndex = 1;
            this.pb_drag.TabStop = false;
            this.pb_drag.MouseDown += new System.Windows.Forms.MouseEventHandler(this.moveMenu_MouseDown);
            // 
            // notifyIcon2
            // 
            this.notifyIcon2.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon2.Icon")));
            this.notifyIcon2.Text = "PathOfExile-Trade-Overlay";
            this.notifyIcon2.Visible = true;
            // 
            // Form_Main
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Black;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(1920, 566);
            this.Controls.Add(this.panel_main);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Main";
            this.Text = "PathOfExile Trade Overlay";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.Black;
            this.panel_main.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb_drag)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_main;
        private System.Windows.Forms.PictureBox pb_drag;
        private NoFocusButton btn_settings;
        private NoFocusButton btn_search;
        private System.Windows.Forms.NotifyIcon notifyIcon2;
    }
}
 