﻿namespace PoE_Trade_Overlay.Forms
{
    partial class Form_Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_header = new System.Windows.Forms.Label();
            this.btn_close = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.toggle_hideLegacy = new PoE_Trade_Overlay.Controls.Toggle();
            this.toggle_filterAFK = new PoE_Trade_Overlay.Controls.Toggle();
            this.toggle_onlyrender = new PoE_Trade_Overlay.Controls.Toggle();
            this.toggle_socketHover = new PoE_Trade_Overlay.Controls.Toggle();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.fcb_leagues = new PoE_Trade_Overlay.Controls.FlattenCombo();
            this.lbl_league = new System.Windows.Forms.Label();
            this.fcb_themes = new PoE_Trade_Overlay.Controls.FlattenCombo();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_Credits = new System.Windows.Forms.Button();
            this.pb_title = new System.Windows.Forms.PictureBox();
            this.dragBar1 = new PoE_Trade_Overlay.Controls.DragBar();
            this.button1 = new System.Windows.Forms.Button();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_title)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_header
            // 
            this.lbl_header.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_header.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.lbl_header.CausesValidation = false;
            this.lbl_header.Font = new System.Drawing.Font("Consolas", 10F);
            this.lbl_header.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_header.Location = new System.Drawing.Point(0, 0);
            this.lbl_header.Name = "lbl_header";
            this.lbl_header.Size = new System.Drawing.Size(263, 20);
            this.lbl_header.TabIndex = 4;
            this.lbl_header.Text = "Settings";
            this.lbl_header.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.lbl_header.MouseDown += new System.Windows.Forms.MouseEventHandler(this.moveMenu_MouseDown);
            this.lbl_header.MouseMove += new System.Windows.Forms.MouseEventHandler(this.moveMenu_MouseMove);
            this.lbl_header.MouseUp += new System.Windows.Forms.MouseEventHandler(this.moveMenu_MouseUp);
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.btn_close.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_close.FlatAppearance.BorderSize = 0;
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_close.Font = new System.Drawing.Font("Consolas", 8F);
            this.btn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_close.Location = new System.Drawing.Point(0, 394);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(263, 23);
            this.btn_close.TabIndex = 7;
            this.btn_close.Text = "Close";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.panel2);
            this.flowLayoutPanel1.Controls.Add(this.panel3);
            this.flowLayoutPanel1.Controls.Add(this.panel4);
            this.flowLayoutPanel1.Controls.Add(this.panel1);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 22);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(263, 366);
            this.flowLayoutPanel1.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.panel2.Controls.Add(this.toggle_hideLegacy);
            this.panel2.Controls.Add(this.toggle_filterAFK);
            this.panel2.Controls.Add(this.toggle_onlyrender);
            this.panel2.Controls.Add(this.toggle_socketHover);
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.MinimumSize = new System.Drawing.Size(254, 0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(10);
            this.panel2.Size = new System.Drawing.Size(254, 108);
            this.panel2.TabIndex = 7;
            // 
            // toggle_hideLegacy
            // 
            this.toggle_hideLegacy.Bar_Off = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.toggle_hideLegacy.Bar_On = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.toggle_hideLegacy.BarSize = new System.Drawing.Size(30, 10);
            this.toggle_hideLegacy.Border_Off = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.toggle_hideLegacy.Border_On = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.toggle_hideLegacy.BorderRadius = 5;
            this.toggle_hideLegacy.Caption = "Hide legacy maps in search";
            this.toggle_hideLegacy.ColorMode = PoE_Trade_Overlay.Controls.ColorMode.Follow;
            this.toggle_hideLegacy.Dock = System.Windows.Forms.DockStyle.Top;
            this.toggle_hideLegacy.Font = new System.Drawing.Font("Consolas", 9F);
            this.toggle_hideLegacy.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.toggle_hideLegacy.KnobColor = System.Drawing.Color.White;
            this.toggle_hideLegacy.KnobImage = global::PoE_Trade_Overlay.Properties.Resources.knob;
            this.toggle_hideLegacy.KnobSize = new System.Drawing.Size(18, 18);
            this.toggle_hideLegacy.KnobStyle = PoE_Trade_Overlay.Controls.KnobStyle.Image;
            this.toggle_hideLegacy.Location = new System.Drawing.Point(10, 76);
            this.toggle_hideLegacy.Margin = new System.Windows.Forms.Padding(35, 34, 35, 34);
            this.toggle_hideLegacy.Name = "toggle_hideLegacy";
            this.toggle_hideLegacy.On = false;
            this.toggle_hideLegacy.Size = new System.Drawing.Size(234, 22);
            this.toggle_hideLegacy.TabIndex = 24;
            this.toggle_hideLegacy.ToggleSpeed = 5;
            // 
            // toggle_filterAFK
            // 
            this.toggle_filterAFK.Bar_Off = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.toggle_filterAFK.Bar_On = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.toggle_filterAFK.BarSize = new System.Drawing.Size(30, 10);
            this.toggle_filterAFK.Border_Off = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.toggle_filterAFK.Border_On = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.toggle_filterAFK.BorderRadius = 5;
            this.toggle_filterAFK.Caption = "Filter AFK players";
            this.toggle_filterAFK.ColorMode = PoE_Trade_Overlay.Controls.ColorMode.Follow;
            this.toggle_filterAFK.Dock = System.Windows.Forms.DockStyle.Top;
            this.toggle_filterAFK.Font = new System.Drawing.Font("Consolas", 9F);
            this.toggle_filterAFK.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.toggle_filterAFK.KnobColor = System.Drawing.Color.White;
            this.toggle_filterAFK.KnobImage = global::PoE_Trade_Overlay.Properties.Resources.knob;
            this.toggle_filterAFK.KnobSize = new System.Drawing.Size(18, 18);
            this.toggle_filterAFK.KnobStyle = PoE_Trade_Overlay.Controls.KnobStyle.Image;
            this.toggle_filterAFK.Location = new System.Drawing.Point(10, 54);
            this.toggle_filterAFK.Margin = new System.Windows.Forms.Padding(35, 34, 35, 34);
            this.toggle_filterAFK.Name = "toggle_filterAFK";
            this.toggle_filterAFK.On = false;
            this.toggle_filterAFK.Size = new System.Drawing.Size(234, 22);
            this.toggle_filterAFK.TabIndex = 23;
            this.toggle_filterAFK.ToggleSpeed = 5;
            // 
            // toggle_onlyrender
            // 
            this.toggle_onlyrender.Bar_Off = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.toggle_onlyrender.Bar_On = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.toggle_onlyrender.BarSize = new System.Drawing.Size(30, 10);
            this.toggle_onlyrender.Border_Off = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.toggle_onlyrender.Border_On = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.toggle_onlyrender.BorderRadius = 5;
            this.toggle_onlyrender.Caption = "Only render if PoE is active";
            this.toggle_onlyrender.ColorMode = PoE_Trade_Overlay.Controls.ColorMode.Follow;
            this.toggle_onlyrender.Dock = System.Windows.Forms.DockStyle.Top;
            this.toggle_onlyrender.Font = new System.Drawing.Font("Consolas", 9F);
            this.toggle_onlyrender.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.toggle_onlyrender.KnobColor = System.Drawing.Color.White;
            this.toggle_onlyrender.KnobImage = global::PoE_Trade_Overlay.Properties.Resources.knob;
            this.toggle_onlyrender.KnobSize = new System.Drawing.Size(18, 18);
            this.toggle_onlyrender.KnobStyle = PoE_Trade_Overlay.Controls.KnobStyle.Image;
            this.toggle_onlyrender.Location = new System.Drawing.Point(10, 32);
            this.toggle_onlyrender.Margin = new System.Windows.Forms.Padding(35, 34, 35, 34);
            this.toggle_onlyrender.Name = "toggle_onlyrender";
            this.toggle_onlyrender.On = false;
            this.toggle_onlyrender.Size = new System.Drawing.Size(234, 22);
            this.toggle_onlyrender.TabIndex = 22;
            this.toggle_onlyrender.ToggleSpeed = 5;
            // 
            // toggle_socketHover
            // 
            this.toggle_socketHover.BackColor = System.Drawing.Color.Transparent;
            this.toggle_socketHover.Bar_Off = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.toggle_socketHover.Bar_On = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.toggle_socketHover.BarSize = new System.Drawing.Size(30, 10);
            this.toggle_socketHover.Border_Off = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.toggle_socketHover.Border_On = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.toggle_socketHover.BorderRadius = 5;
            this.toggle_socketHover.Caption = "Hide Sockets on Hover";
            this.toggle_socketHover.ColorMode = PoE_Trade_Overlay.Controls.ColorMode.Follow;
            this.toggle_socketHover.Dock = System.Windows.Forms.DockStyle.Top;
            this.toggle_socketHover.Font = new System.Drawing.Font("Consolas", 9F);
            this.toggle_socketHover.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.toggle_socketHover.KnobColor = System.Drawing.Color.White;
            this.toggle_socketHover.KnobImage = global::PoE_Trade_Overlay.Properties.Resources.knob;
            this.toggle_socketHover.KnobSize = new System.Drawing.Size(18, 18);
            this.toggle_socketHover.KnobStyle = PoE_Trade_Overlay.Controls.KnobStyle.Image;
            this.toggle_socketHover.Location = new System.Drawing.Point(10, 10);
            this.toggle_socketHover.Margin = new System.Windows.Forms.Padding(35, 34, 35, 34);
            this.toggle_socketHover.Name = "toggle_socketHover";
            this.toggle_socketHover.On = false;
            this.toggle_socketHover.Size = new System.Drawing.Size(234, 22);
            this.toggle_socketHover.TabIndex = 21;
            this.toggle_socketHover.ToggleSpeed = 5;
            // 
            // panel3
            // 
            this.panel3.AutoSize = true;
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.panel3.Controls.Add(this.dragBar1);
            this.panel3.Location = new System.Drawing.Point(3, 117);
            this.panel3.MinimumSize = new System.Drawing.Size(254, 0);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(10);
            this.panel3.Size = new System.Drawing.Size(254, 60);
            this.panel3.TabIndex = 8;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.panel4.Controls.Add(this.fcb_leagues);
            this.panel4.Controls.Add(this.lbl_league);
            this.panel4.Controls.Add(this.fcb_themes);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Location = new System.Drawing.Point(3, 183);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(10, 10, 10, 0);
            this.panel4.Size = new System.Drawing.Size(254, 122);
            this.panel4.TabIndex = 11;
            // 
            // fcb_leagues
            // 
            this.fcb_leagues.AutoComp = false;
            this.fcb_leagues.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.fcb_leagues.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.fcb_leagues.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.fcb_leagues.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.fcb_leagues.Data = null;
            this.fcb_leagues.Dock = System.Windows.Forms.DockStyle.Top;
            this.fcb_leagues.DropDownColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.fcb_leagues.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fcb_leagues.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.fcb_leagues.ForeColorDrop = System.Drawing.Color.Empty;
            this.fcb_leagues.FormattingEnabled = true;
            this.fcb_leagues.Location = new System.Drawing.Point(10, 79);
            this.fcb_leagues.Name = "fcb_leagues";
            this.fcb_leagues.Size = new System.Drawing.Size(234, 21);
            this.fcb_leagues.TabIndex = 23;
            this.fcb_leagues.SelectedIndexChanged += new System.EventHandler(this.fcb_leagues_SelectedIndexChanged);
            // 
            // lbl_league
            // 
            this.lbl_league.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_league.Font = new System.Drawing.Font("Consolas", 8F);
            this.lbl_league.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.lbl_league.Location = new System.Drawing.Point(10, 55);
            this.lbl_league.Name = "lbl_league";
            this.lbl_league.Size = new System.Drawing.Size(234, 24);
            this.lbl_league.TabIndex = 22;
            this.lbl_league.Text = "League";
            this.lbl_league.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // fcb_themes
            // 
            this.fcb_themes.AutoComp = false;
            this.fcb_themes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.fcb_themes.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.fcb_themes.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid;
            this.fcb_themes.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.fcb_themes.Data = null;
            this.fcb_themes.Dock = System.Windows.Forms.DockStyle.Top;
            this.fcb_themes.DropDownColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.fcb_themes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fcb_themes.ForeColor = System.Drawing.Color.White;
            this.fcb_themes.ForeColorDrop = System.Drawing.Color.Empty;
            this.fcb_themes.FormattingEnabled = true;
            this.fcb_themes.Location = new System.Drawing.Point(10, 34);
            this.fcb_themes.Name = "fcb_themes";
            this.fcb_themes.Size = new System.Drawing.Size(234, 21);
            this.fcb_themes.TabIndex = 21;
            this.fcb_themes.SelectedIndexChanged += new System.EventHandler(this.cb_theme_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Consolas", 8F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.label4.Location = new System.Drawing.Point(10, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(234, 24);
            this.label4.TabIndex = 0;
            this.label4.Text = "Theme";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btn_Credits);
            this.panel1.Location = new System.Drawing.Point(3, 311);
            this.panel1.MinimumSize = new System.Drawing.Size(254, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(254, 44);
            this.panel1.TabIndex = 21;
            // 
            // btn_Credits
            // 
            this.btn_Credits.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.btn_Credits.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btn_Credits.FlatAppearance.BorderSize = 0;
            this.btn_Credits.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Credits.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.btn_Credits.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.btn_Credits.Location = new System.Drawing.Point(13, 10);
            this.btn_Credits.Name = "btn_Credits";
            this.btn_Credits.Size = new System.Drawing.Size(104, 20);
            this.btn_Credits.TabIndex = 20;
            this.btn_Credits.Text = "Credits";
            this.btn_Credits.UseVisualStyleBackColor = false;
            this.btn_Credits.Click += new System.EventHandler(this.btn_Reload_Click);
            // 
            // pb_title
            // 
            this.pb_title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.pb_title.Dock = System.Windows.Forms.DockStyle.Top;
            this.pb_title.Location = new System.Drawing.Point(0, 0);
            this.pb_title.Name = "pb_title";
            this.pb_title.Size = new System.Drawing.Size(263, 20);
            this.pb_title.TabIndex = 3;
            this.pb_title.TabStop = false;
            this.pb_title.MouseDown += new System.Windows.Forms.MouseEventHandler(this.moveMenu_MouseDown);
            this.pb_title.MouseMove += new System.Windows.Forms.MouseEventHandler(this.moveMenu_MouseMove);
            this.pb_title.MouseUp += new System.Windows.Forms.MouseEventHandler(this.moveMenu_MouseUp);
            // 
            // dragBar1
            // 
            this.dragBar1.BarShape = PoE_Trade_Overlay.Controls.DragBarShape.Rounded;
            this.dragBar1.BarSize = new System.Drawing.Size(200, 8);
            this.dragBar1.BorderRadius = 2;
            this.dragBar1.BorderWidth = 1F;
            this.dragBar1.Caption = "Window Opacity : ";
            this.dragBar1.ColorBar = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.dragBar1.ColorBarFill = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.dragBar1.ColorBorder = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.dragBar1.ColorKnob = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.dragBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.dragBar1.Font = new System.Drawing.Font("Consolas", 8F);
            this.dragBar1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.dragBar1.KnobShape = PoE_Trade_Overlay.Controls.DragKnobShape.Box;
            this.dragBar1.KnobSize = new System.Drawing.Size(10, 16);
            this.dragBar1.Location = new System.Drawing.Point(10, 10);
            this.dragBar1.Maximum = 100;
            this.dragBar1.Minimum = 75;
            this.dragBar1.Name = "dragBar1";
            this.dragBar1.Size = new System.Drawing.Size(234, 40);
            this.dragBar1.TabIndex = 11;
            this.dragBar1.Value = 75;
            this.dragBar1.ValueChanged += new System.EventHandler(this.dragBar1_ValueChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.button1.Location = new System.Drawing.Point(137, 11);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 20);
            this.button1.TabIndex = 21;
            this.button1.Text = "Theme Editor (WIP)";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // Form_Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(263, 417);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.lbl_header);
            this.Controls.Add(this.pb_title);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_Settings";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Form_Settings";
            this.TopMost = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form_Settings_KeyDown);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pb_title)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_header;
        private System.Windows.Forms.PictureBox pb_title;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_Credits;
        private Controls.FlattenCombo fcb_themes;
        private Controls.FlattenCombo fcb_leagues;
        private System.Windows.Forms.Label lbl_league;
        private Controls.Toggle toggle_socketHover;
        private Controls.Toggle toggle_hideLegacy;
        private Controls.Toggle toggle_filterAFK;
        private Controls.Toggle toggle_onlyrender;
        private System.Windows.Forms.Panel panel1;
        private Controls.DragBar dragBar1;
        private System.Windows.Forms.Button button1;
    }
}