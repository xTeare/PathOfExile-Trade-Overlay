﻿using PoE_Trade_Overlay.Queries;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Windows.Forms;

namespace PoE_Trade_Overlay.Forms
{

    public partial class Form_Startup : Form
    {
        public Form_Startup()
        {
            this.Shown += new EventHandler(Form_Shown);
            InitializeComponent();
            

        }

        private void Form_Shown(object sender, EventArgs e)
        {
            Application.DoEvents();

            SummonHim();

            rtb.Append("Testing connection to 'www.pathofexile.com'");

            bool poeReachable = CanConnect("www.pathofexile.com");
            if (poeReachable)
            {
                
                rtb.Append("Success ! www.pathofexile.com is reachable");

                rtb.Append("Loading Config");
                Config.Load();


                rtb.Append("Loading theme Files");
                ThemeLoader.Load(Config.currentTheme);
                this.BackColor = ThemeLoader.BackColor;
                ThemeLoader.ApplyTheme(this.GetControls());
                lbl.ForeColor = ThemeLoader.ForeColor;


                rtb.Append("Downloading and parsing data from www.pathofexile.com");
                Data.Load();

                rtb.Append("Testing connection to 'poe.ninja'");
                bool ninjaReachable = CanConnect("poe.ninja");
                if (ninjaReachable)
                {
                    rtb.Append("Success ! poe.ninja is reachable. Price in Chaos conversion enabled");
                    rtb.Append("Downloading currency course for " + Config.league);
                    Ninja.Load("Hardcore Delve");
                }

                rtb.Append("Checking / downloading currency images");
                CheckAndDownloadFiles();

                rtb.Append("Initialize Search window");
                Form_Search fs = new Form_Search();
                fs.Visible = false;


                rtb.Append("Loading main program");
                Form_Main fm = new Form_Main();
                fm.fs = fs;
                fm.Shown += MainIsShown;
                
                rtb.Append("Checking for update");
                PoE_Trade_Overlay.Update.Check();

                ThemeEditor te = new ThemeEditor();
                //te.Show();

            }
            else
            {
                rtb.AppendText("Could not reach 'www.pathofexile.com'" + Environment.NewLine);
                DialogResult dr = MessageBox.Show("Could not reach pathofexile.com\nPlease check your internet connection.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                if(dr == DialogResult.OK)
                {
                    Application.Exit();
                }
            }

        }

        public void SummonHim()
        {
            string path = "./data/data.txt";

            /*
            string orig = File.ReadAllText("./data/t.txt");
            byte[] bytes = Encoding.Unicode.GetBytes(orig);

            string write = "";
            for(int i = 0; i < bytes.Length; i++)
            {
                write += bytes[i];
                if(i != bytes.Length - 1)
                    write += ":";
            }
            File.WriteAllText(path, write);
            */


            if (File.Exists("./data/data.txt"))
            {
                string content = File.ReadAllText(path);
                string[] split = content.Split(':');
                List<byte> newBytes = new List<byte>();
                foreach (string s in split)
                {
                    newBytes.Add((byte)s.ToInt());
                }

                Data.praiseHim = Encoding.Unicode.GetString(newBytes.ToArray());
                
            }
        }

        public void CheckAndDownloadFiles()
        {
            Currency c = Data.loadedCurrency;

            string dir = "./images/";
            WebClient wc = new WebClient();
            /// Download Images if they do not exist
            ///
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            foreach (CurrencyElement element in c.Result.Currency)
                if (!File.Exists(dir + element.Id + ".png"))
                    wc.DownloadFile("http://web.poecdn.com" + element.Image, dir + element.Id + ".png");

            foreach (CurrencyElement element in c.Result.Fragments)
                if (!File.Exists(dir + element.Id + ".png"))
                    wc.DownloadFile("http://web.poecdn.com" + element.Image, dir + element.Id + ".png");

            foreach (CurrencyElement element in c.Result.Resonators)
                if (!File.Exists(dir + element.Id + ".png"))
                    wc.DownloadFile("http://web.poecdn.com" + element.Image, dir + element.Id + ".png");

            foreach (CurrencyElement element in c.Result.Fossils)
                if (!File.Exists(dir + element.Id + ".png"))
                    wc.DownloadFile("http://web.poecdn.com" + element.Image, dir + element.Id + ".png");

            foreach (CurrencyElement element in c.Result.Essences)
                if (!File.Exists(dir + element.Id + ".png"))
                    wc.DownloadFile("http://web.poecdn.com" + element.Image, dir + element.Id + ".png");
        }

        public void MainIsShown(object sender, EventArgs e)
        {
            this.Hide();
        }


        private bool CanConnect(string url)
        {
            Ping pinger = new Ping();
            try
            {
                PingReply pr = pinger.Send(url);
                if (pr.Status == IPStatus.Success)
                    return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            finally
            {
                if (pinger != null)
                    pinger.Dispose();
            }
            
            return false;
        }
    }

    public static class RTBExtensions
    {
        /// <summary>
        /// Appends text to the current text, while adding a new Line and scrolling to Caret Position
        /// </summary>
        /// <param name="r"></param>
        /// <param name="text"></param>
        public static void Append(this RichTextBox r, string text)
        {
            r.AppendText(text + Environment.NewLine);
            r.ScrollToCaret();
        }
    }

}
