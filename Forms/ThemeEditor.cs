﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace PoE_Trade_Overlay.Forms
{
    public partial class ThemeEditor : Form
    {
        private DataTable props = new DataTable();
        private bool openColorDialog;
        private string currentControl = "";
        private string currentProperty = "";
        private bool supportsAlpha = false;

        public ThemeEditor()
        {
            InitializeComponent();

            fc.SelectedIndex = 0;
            GenerateProps();
            LoadTheme();

            modLabel1.AddText("Test", "255,0,0");
            modLabel1.AddText("Test 2 ", "0,20,250");
            modLabel1.modInfo = "TEST INFO";

            foreach(DataRow dr in ThemeLoader.themeList.Rows)
            {
                cmb.Items.Add(dr[0].ToString());
            }
            if (cmb.Items.Count > 0)
                cmb.SelectedIndex = 0;
        }

        private void LoadTheme()
        {
            ThemeLoader.ApplyTheme(new List<Control>() { fc, filter1, filter_Buyout1, filter_DropDown1, filter_Sockets1, exchangeResult1, expandCollapse1 });
        }

        private void GenerateProps()
        {
            props = new DataTable();
            props.Columns.Add("Control");
            props.Columns.Add("Type");
            props.Columns.Add("Name");
            props.Columns.Add("Default");
            props.Columns.Add("AphaSupport");


            props.Rows.Add("DropDown", "Color", "ForeColor",            ThemeLoader.fc.ForeColor.ToShortColor(), "True");
            props.Rows.Add("DropDown", "Color", "BackColor",            ThemeLoader.fc.BackColor.ToShortColor(), "False");
            props.Rows.Add("DropDown", "Color", "BackColor_DropDown",   ThemeLoader.fc.DropDownColor.ToShortColor(), "False");
            props.Rows.Add("DropDown", "Color", "BackColor_Button",     ThemeLoader.fc.ButtonColor.ToShortColor(), "False");
            props.Rows.Add("DropDown", "Color", "BackColor_Border",     ThemeLoader.fc.Border.ToShortColor(), "False");
            props.Rows.Add("DropDown", "Color", "Arrow_Active",         ThemeLoader.fc.Arrow_Active.ToShortColor(), "False");
            props.Rows.Add("DropDown", "Color", "Arrow_Inactive",       ThemeLoader.fc.Arrow_Inactive.ToShortColor(), "False");

             
            props.Rows.Add("ExpandCollapse", "Color", "ForeColor_Active",           ThemeLoader.ec.ForeColor_Active.ToShortColor(), "True");
            props.Rows.Add("ExpandCollapse", "Color", "ForeColor_Inactive",         ThemeLoader.ec.ForeColor_Inactive.ToShortColor(), "False");
            props.Rows.Add("ExpandCollapse", "Color", "BackColor",                  ThemeLoader.ec.BackColor.ToShortColor(), "False");
            props.Rows.Add("ExpandCollapse", "Color", "BackColor_HeaderActive",     ThemeLoader.ec.BackColor_HeaderActive.ToShortColor(), "False");
            props.Rows.Add("ExpandCollapse", "Color", "BackColor_HeaderInactive",   ThemeLoader.ec.BackColor_HeaderInactive.ToShortColor(), "False");
            props.Rows.Add("ExpandCollapse", "Image", "Active",                     "", "False");
            props.Rows.Add("ExpandCollapse", "Image", "Inactive",                   "", "False");


            props.Rows.Add("Filter", "Color", "ForeColor", "", "True");
            props.Rows.Add("Filter", "Color", "ForeColor_Textbox", "", "True");
            props.Rows.Add("Filter", "Color", "ForeColor_Placeholder", "", "False");
            props.Rows.Add("Filter", "Color", "BackColor", "", "False");
            props.Rows.Add("Filter", "Color", "BackColor_Textbox", "", "False");
            props.Rows.Add("Filter", "Color", "BackColor_Label", "", "False");

            props.Rows.Add("Filter_Textbox", "Color", "ForeColor", "", "True");
            props.Rows.Add("Filter_Textbox", "Color", "ForeColor_Textbox", "", "True");
            props.Rows.Add("Filter_Textbox", "Color", "BackColor", "", "False");
            props.Rows.Add("Filter_Textbox", "Color", "BackColor_Textbox", "", "False");
            props.Rows.Add("Filter_Textbox", "Color", "BackColor_Label", "", "False");
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            tab.SelectedIndex = lb.SelectedIndex;
            SelectProps(lb.SelectedIndex);
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {
        }

        private void SelectProps(int index)
        {
            string propName = lb.Items[index].ToString();
            lv.Items.Clear();
            foreach (DataRow dr in props.Rows)
            {
                if (dr[0].ToString() == propName)
                {
                    lv.Items.Add(dr[1].ToString()).SubItems.AddRange(new string[] { dr[2].ToString(), dr[3].ToString() });
                }
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            lb.SelectedIndex = tab.SelectedIndex;
            SelectProps(tab.SelectedIndex);
            if (lv.Items.Count > 0)
            {
                lv.Items[0].Selected = true;
                lv.Select();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openColorDialog)
            {
                colorDialog1.ShowDialog();
                Color c = colorDialog1.Color;
                string colorCode = string.Format("{0},{1},{2},{3}", c.A, c.R, c.G, c.B);
                A.Value = c.A;
                R.Value = c.R;
                G.Value = c.G;
                B.Value = c.B;
                pb.BackColor = c;
                SetControl(currentControl, currentProperty, colorCode);
            }
        }

        private void SetControl(string control, string property, string value)
        {
            foreach(DataRow dr in props.Rows)
            {
                if (dr[0].ToString() == control && dr[2].ToString() == property)
                    dr[3] = value;
            }

            foreach (ListViewItem item in lv.Items)
            {
                if (item.SubItems[1].Text == property)
                {
                    if (item.SubItems.Count < 3)
                        item.SubItems.Add(value);
                    else
                        item.SubItems[2].Text = value;
                }
            }

            switch (control)
            {
                case "DropDown":

                    switch (property)
                    {
                        case "ForeColor":
                            fc.ForeColor = value.ToColor();
                            fc.DropDownColor = value.ToColor();
                            break;

                        case "BackColor":
                            fc.BackColor = value.ToColor();
                            break;

                        case "BackColor_DropDown":
                            fc.DropDownColor = value.ToColor();
                            break;

                        case "BackColor_Button":
                            fc.ButtonColor = value.ToColor();
                            break;

                        case "BackColor_Border":
                            fc.Border = value.ToColor();
                            break;

                        case "Arrow_Active":
                            fc.Arrow_Active = value.ToColor();
                            break;

                        case "Arrow_Inactive":
                            fc.Arrow_Inactive = value.ToColor();
                            break;
                    }
                    fc.Invalidate();
                    break;
            }
        }

        private void lv_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lv.SelectedItems.Count != 0)
            {
                if (lv.SelectedItems[0].Text == "Color")
                {
                    currentControl = lb.Items[lb.SelectedIndex].ToString();
                    currentProperty = lv.SelectedItems[0].SubItems[1].Text;
                    select.Text = "Select Color";
                    //Console.WriteLine("Current Control {0}, Property {1}", currentControl, currentProperty);
                    openColorDialog = true;

                    foreach(DataRow dr in props.Rows)
                    {
                        if (dr[0].ToString() == currentControl && dr[2].ToString() == currentProperty)
                            supportsAlpha = (dr[4].ToString() == "False") ? false : true;
                    }
                    lblA.Visible = supportsAlpha;
                    A.Visible = supportsAlpha;
                    A.Value = 255;
                    
                    string[] colors = lv.SelectedItems[0].SubItems[2].Text.Split(',');
                    if(colors.Length >= 4)
                    {
                        A.Value = colors[0].ToInt();
                        R.Value = colors[1].ToInt();
                        G.Value = colors[2].ToInt();
                        B.Value = colors[3].ToInt();
                    }

                }
                /*
                if (lv.SelectedItems[0].SubItems.Count == 3)
                    tb_selected.Text = lv.SelectedItems[0].SubItems[2].Text;
                else
                    tb_selected.Text = string.Empty;*/
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            Color c = Color.FromArgb((int)A.Value, (int)R.Value, (int)G.Value, (int)B.Value);
            string colorCode = string.Format("{0},{1},{2},{3}", c.A, c.R, c.G, c.B);
            
            pb.BackColor = c;
            SetControl(currentControl, currentProperty, colorCode);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Theme theme = new Theme();
            theme.Name = tb_themeName.Text;
            theme.Controls = new List<Ctrl>();
            for(int i = 0; i < lb.Items.Count; i++)
            {
                lb.SelectedIndex = i;
                Ctrl c = new Ctrl();
                c.Type = lb.Items[i].ToString();

                if (c.Colors == null)
                    c.Colors = new Colors();

                foreach (ListViewItem item in lv.Items)
                {
                    if(item.Text == "Color")
                    {
                        string val = "255";
                        if (item.SubItems.Count > 2)
                            val = item.SubItems[2].Text;

                        c.Colors = SetColors(c.Colors, item.SubItems[1].Text, val);
                        

                    }
                }
                theme.Controls.Add(c);
            }

            

            Console.WriteLine(theme.ToJson());
        }

        private Colors SetColors(Colors colors, string prop, string val)
        {
            switch (prop)
            {
                case "ForeColor":
                    colors.Forecolor = val;
                    break;
                case "BackColor_DropDown":
                    colors.Backcolor_DropDown = val;
                    break;
                case "BackColor_Button":
                    colors.Backcolor_Bottom = val;
                    break;
                case "BackColor_Border":
                    colors.Backcolor_Border = val;
                    break;
            }

            return colors;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            foreach(TabPage tp in tab.TabPages)
            {
                tp.BackColor = colorDialog1.Color;
            }
        }

        private void load_Click(object sender, EventArgs e)
        {
            ThemeLoader.Load(cmb.Items[cmb.SelectedIndex].ToString());
            LoadTheme();
            GenerateProps();
        }
    }
}