﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PoE_Trade_Overlay.Forms
{
    public partial class Tools : Form
    {
        public Tools()
        {
            InitializeComponent();
            Setup();
        }

        private void Setup()
        {
            lbl_header.AssignMoveEvents();
            foreach (DataRow dr in Data.ninja.Rows)
            {
                fc.Items.Add(dr[0].ToString());
            }
            fc.SelectedIndex = 0;
        }

        private void fc_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalcCurrencyToChaos();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            CalcCurrencyToChaos();
        }

        private void CalcCurrencyToChaos()
        {
            string currency = fc.Items[fc.SelectedIndex].ToString();
            float amount = (float)numericUpDown1.Value;
            float result = Extensions.LoadPriceInChaos(currency, amount);

            lbl_isWorth.Text = string.Format("{0} x {1} " + ((amount == 1) ? "is" : "are") +  " worth {2} Chaos", amount, currency, string.Format("{0:n}", result));
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
