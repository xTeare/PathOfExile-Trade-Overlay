﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PoE_Trade_Overlay
{


    public partial class Theme
    {
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("Controls", NullValueHandling = NullValueHandling.Ignore)]
        public List<Ctrl> Controls { get; set; }

        [JsonProperty("ForeColor", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor { get; set; }

        [JsonProperty("ForeColor_Button", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Button { get; set; }

        [JsonProperty("ForeColor_Header", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Header { get; set; }

        [JsonProperty("ForeColor_Textbox", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Textbox { get; set; }

        [JsonProperty("BackColor", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor { get; set; }

        [JsonProperty("BackColor_Button", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_Button { get; set; }

        [JsonProperty("BackColor_Header", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_Header { get; set; }

        [JsonProperty("BackColor_Panel", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_Panel { get; set; }

        [JsonProperty("BackColor_Textbox", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_Textbox { get; set; }

        [JsonProperty("Image_Drag", NullValueHandling = NullValueHandling.Ignore)]
        public string Drag_Image { get; set; }

        [JsonProperty("Image_LayoutSmall", NullValueHandling = NullValueHandling.Ignore)]
        public string Layout_Small { get; set; }

        [JsonProperty("Image_LayoutBig", NullValueHandling = NullValueHandling.Ignore)]
        public string Layout_Big { get; set; }

        [JsonProperty("Image_Settings", NullValueHandling = NullValueHandling.Ignore)]
        public string Settings { get; set; }

    }

    public partial class Ctrl
    {
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("colors", NullValueHandling = NullValueHandling.Ignore)]
        public Colors Colors { get; set; }

        [JsonProperty("images", NullValueHandling = NullValueHandling.Ignore)]
        public Images Images { get; set; }
    }

    public partial class Colors
    {
        [JsonProperty("ForeColor", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor { get; set; }

        [JsonProperty("ForeColor_Active", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Active { get; set; }

        [JsonProperty("ForeColor_Button", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Button { get; set; }

        [JsonProperty("ForeColor_Placeholder", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Placeholder { get; set; }
        
        [JsonProperty("ForeColor_Textbox", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Textbox { get; set; }

        [JsonProperty("ForeColor_Listbox", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Listbox { get; set; }

        [JsonProperty("ForeColor_Dropdown", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Dropdown { get; set; }

        [JsonProperty("ForeColor_Inactive", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Inactive { get; set; }

        [JsonProperty("ForeColor_Header", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Header { get; set; }

        [JsonProperty("ForeColor_Online", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Online { get; set; }

        [JsonProperty("ForeColor_Offline", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Offline { get; set; }

        [JsonProperty("ForeColor_Afk", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Afk { get; set; }

        [JsonProperty("ForeColor_Normal", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Normal { get; set; }

        [JsonProperty("ForeColor_Magic", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Magic { get; set; }

        [JsonProperty("ForeColor_Rare", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Rare { get; set; }

        [JsonProperty("ForeColor_Unique", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Unique { get; set; }

        [JsonProperty("ForeColor_Relic", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Relic { get; set; }

        [JsonProperty("ForeColor_Divination", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Divination { get; set; }

        [JsonProperty("ForeColor_Gem", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Gem { get; set; }

        [JsonProperty("ForeColor_Curreny", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Curreny { get; set; }

        [JsonProperty("ForeColor_Crafted", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Crafted { get; set; }
        
        [JsonProperty("ForeColor_Veiled", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Veiled { get; set; }
        
        [JsonProperty("ForeColor_Prophecy", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Prophecy { get; set; }

        [JsonProperty("ForeColor_Pseudo", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Pseudo { get; set; }

        [JsonProperty("ForeColor_Corrupted", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Corrupted { get; set; }

        [JsonProperty("ForeColor_Unidentified", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Unidentified { get; set; }
        
        [JsonProperty("ForeColor_Exchange", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Exchange { get; set; }

        [JsonProperty("ForeColor_ExchangeHover", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_ExchangeHover { get; set; }

        [JsonProperty("ForeColor_Warning", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Warning { get; set; }


        [JsonProperty("ForeColor_Selected", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Selected { get; set; }

        [JsonProperty("ForeColor_Hover", NullValueHandling = NullValueHandling.Ignore)]
        public string Forecolor_Hover { get; set; }

        [JsonProperty("BackColor", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor { get; set; }

        [JsonProperty("BackColor_Listbox", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_Listbox { get; set; }

        [JsonProperty("BackColor_DropDown", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_DropDown { get; set; }

        [JsonProperty("BackColor_Label", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_Label { get; set; }

        [JsonProperty("BackColor_Textbox", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_Textbox { get; set; }

        [JsonProperty("BackColor_Header", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_Header { get; set; }

        [JsonProperty("BackColor_Button", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_Button { get; set; }

        [JsonProperty("BackColor_HeaderActive", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_HeaderActive { get; set; }

        [JsonProperty("BackColor_HeaderInactive", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_HeaderInactive { get; set; }

        [JsonProperty("BackColor_Online", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_Online { get; set; }

        [JsonProperty("BackColor_Offline", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_Offline { get; set; }

        [JsonProperty("BackColor_Afk", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_Afk { get; set; }

        [JsonProperty("BackColor_ModHover", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_ModHover { get; set; }

        [JsonProperty("BackColor_Top", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_Top { get; set; }

        [JsonProperty("BackColor_Bottom", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_Bottom { get; set; }

        [JsonProperty("BackColor_Exchange", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_Exchange { get; set; }

        [JsonProperty("BackColor_ExchangeHover", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_ExchangeHover { get; set; }

        [JsonProperty("BackColor_Border", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_Border { get; set; }


        [JsonProperty("BackColor_Selected", NullValueHandling = NullValueHandling.Ignore)]
        public string Backcolor_Selected { get; set; }


        [JsonProperty("Border", NullValueHandling = NullValueHandling.Ignore)]
        public string Border { get; set; }

        [JsonProperty("Border_Off", NullValueHandling = NullValueHandling.Ignore)]
        public string Border_Off { get; set; }
        [JsonProperty("Border_On", NullValueHandling = NullValueHandling.Ignore)]
        public string Border_On { get; set; }
        [JsonProperty("Bar_On", NullValueHandling = NullValueHandling.Ignore)]
        public string Bar_On { get; set; }
        [JsonProperty("Bar_Off", NullValueHandling = NullValueHandling.Ignore)]
        public string Bar_Off { get; set; }

        [JsonProperty("Border_Hightlighted", NullValueHandling = NullValueHandling.Ignore)]
        public string Border_Hightlighted { get; set; }

        [JsonProperty("Color_Separator", NullValueHandling = NullValueHandling.Ignore)]
        public string Separator { get; set; }
        [JsonProperty("Color_ExpbarFill", NullValueHandling = NullValueHandling.Ignore)]
        public string Bar { get; set; }
        
        [JsonProperty("Arrow_Active", NullValueHandling = NullValueHandling.Ignore)]
        public string Arrow_Active { get; set; }

        [JsonProperty("Arrow_Inactive", NullValueHandling = NullValueHandling.Ignore)]
        public string Arrow_Inactive { get; set; }

    }

    public partial class Images
    {
        [JsonProperty("Active", NullValueHandling = NullValueHandling.Ignore)]
        public string Active { get; set; }

        [JsonProperty("Inactive", NullValueHandling = NullValueHandling.Ignore)]
        public string Inactive { get; set; }

        [JsonProperty("Knob", NullValueHandling = NullValueHandling.Ignore)]
        public string Knob { get; set; }
    }

    public partial class Theme
    {
        public static Theme FromJson(string json) => JsonConvert.DeserializeObject<Theme>(json, ThemeConverter.Settings);
    }

    public static class ThemeSerialize
    {
        public static string ToJson(this Theme self) => JsonConvert.SerializeObject(self, ThemeConverter.Settings);
    }

    internal static class ThemeConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
