﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PoE_Trade_Overlay.Queries
{
    public partial class Stats
    {
        [JsonProperty("result", NullValueHandling = NullValueHandling.Ignore)]
        public List<StatResult> Result { get; set; }
    }

    public partial class StatResult
    {
        [JsonProperty("label", NullValueHandling = NullValueHandling.Ignore)]
        public string Label { get; set; }

        [JsonProperty("entries", NullValueHandling = NullValueHandling.Ignore)]
        public List<StatEntry> Entries { get; set; }
    }

    public partial class StatEntry
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("text", NullValueHandling = NullValueHandling.Ignore)]
        public string Text { get; set; }

        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public TypeEnum? Type { get; set; }
    }

    public enum TypeEnum { Crafted, Delve, Enchant, Explicit, Implicit, Monster, Pseudo, Veiled };

    public partial class Stats
    {
        public static Stats FromJson(string json) => JsonConvert.DeserializeObject<Stats>(json, PoE_Trade_Overlay.Queries.StatsConverter.Settings);
    }

    public static class StatsSerialize
    {
        public static string ToJson(this Stats self) => JsonConvert.SerializeObject(self, PoE_Trade_Overlay.Queries.StatsConverter.Settings);
    }

    internal static class StatsConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                TypeEnumConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class TypeEnumConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(TypeEnum) || t == typeof(TypeEnum?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "crafted":
                    return TypeEnum.Crafted;
                case "delve":
                    return TypeEnum.Delve;
                case "enchant":
                    return TypeEnum.Enchant;
                case "explicit":
                    return TypeEnum.Explicit;
                case "implicit":
                    return TypeEnum.Implicit;
                case "monster":
                    return TypeEnum.Monster;
                case "pseudo":
                    return TypeEnum.Pseudo;
                case "veiled":
                    return TypeEnum.Veiled;
            }
            throw new Exception("Cannot unmarshal type TypeEnum");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (TypeEnum)untypedValue;
            switch (value)
            {
                case TypeEnum.Crafted:
                    serializer.Serialize(writer, "crafted");
                    return;
                case TypeEnum.Delve:
                    serializer.Serialize(writer, "delve");
                    return;
                case TypeEnum.Enchant:
                    serializer.Serialize(writer, "enchant");
                    return;
                case TypeEnum.Explicit:
                    serializer.Serialize(writer, "explicit");
                    return;
                case TypeEnum.Implicit:
                    serializer.Serialize(writer, "implicit");
                    return;
                case TypeEnum.Monster:
                    serializer.Serialize(writer, "monster");
                    return;
                case TypeEnum.Pseudo:
                    serializer.Serialize(writer, "pseudo");
                    return;
            }
            throw new Exception("Cannot marshal type TypeEnum");
        }

        public static readonly TypeEnumConverter Singleton = new TypeEnumConverter();
    }
}