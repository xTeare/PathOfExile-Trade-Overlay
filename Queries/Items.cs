﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PoE_Trade_Overlay.Queries
{


    public partial class Items
    {
        [JsonProperty("result", NullValueHandling = NullValueHandling.Ignore)]
        public List<ItemResult> Result { get; set; }
    }

    public partial class ItemResult
    {
        [JsonProperty("label", NullValueHandling = NullValueHandling.Ignore)]
        public string Label { get; set; }

        [JsonProperty("entries", NullValueHandling = NullValueHandling.Ignore)]
        public List<ItemEntry> Entries { get; set; }
    }

    public partial class ItemEntry
    {
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("text", NullValueHandling = NullValueHandling.Ignore)]
        public string Text { get; set; }

        [JsonProperty("flags", NullValueHandling = NullValueHandling.Ignore)]
        public ItemFlags Flags { get; set; }

        [JsonProperty("disc", NullValueHandling = NullValueHandling.Ignore)]
        public Disc? Disc { get; set; }
    }

    public partial class ItemFlags
    {
        [JsonProperty("unique", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Unique { get; set; }

        [JsonProperty("prophecy", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Prophecy { get; set; }
    }

    public enum Disc { Atlasofworlds, Catarina, Elreon, Haku, Original, Theawakening, Tora, Vagan, Vorici, Warfortheatlas, Zana, Alva, Einhar, Niko };

    public partial class Items
    {
        public static Items FromJson(string json) => JsonConvert.DeserializeObject<Items>(json, PoE_Trade_Overlay.Queries.Converter.Settings);
    }

    public static class ParseItemsSerialize
    {
        public static string ToJson(this Items self) => JsonConvert.SerializeObject(self, PoE_Trade_Overlay.Queries.Converter.Settings);
    }

    internal static class ParseItemsConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                DiscConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class DiscConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Disc) || t == typeof(Disc?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "atlasofworlds":
                    return Disc.Atlasofworlds;
                case "catarina":
                    return Disc.Catarina;
                case "elreon":
                    return Disc.Elreon;
                case "haku":
                    return Disc.Haku;
                case "original":
                    return Disc.Original;
                case "theawakening":
                    return Disc.Theawakening;
                case "tora":
                    return Disc.Tora;
                case "vagan":
                    return Disc.Vagan;
                case "vorici":
                    return Disc.Vorici;
                case "warfortheatlas":
                    return Disc.Warfortheatlas;
                case "zana":
                    return Disc.Zana;
            }
            throw new Exception("Cannot unmarshal type Disc");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Disc)untypedValue;
            switch (value)
            {
                case Disc.Atlasofworlds:
                    serializer.Serialize(writer, "atlasofworlds");
                    return;
                case Disc.Catarina:
                    serializer.Serialize(writer, "catarina");
                    return;
                case Disc.Elreon:
                    serializer.Serialize(writer, "elreon");
                    return;
                case Disc.Haku:
                    serializer.Serialize(writer, "haku");
                    return;
                case Disc.Original:
                    serializer.Serialize(writer, "original");
                    return;
                case Disc.Theawakening:
                    serializer.Serialize(writer, "theawakening");
                    return;
                case Disc.Tora:
                    serializer.Serialize(writer, "tora");
                    return;
                case Disc.Vagan:
                    serializer.Serialize(writer, "vagan");
                    return;
                case Disc.Vorici:
                    serializer.Serialize(writer, "vorici");
                    return;
                case Disc.Warfortheatlas:
                    serializer.Serialize(writer, "warfortheatlas");
                    return;
                case Disc.Zana:
                    serializer.Serialize(writer, "zana");
                    return;
            }
            throw new Exception("Cannot marshal type Disc");
        }

        public static readonly DiscConverter Singleton = new DiscConverter();
    }
}
