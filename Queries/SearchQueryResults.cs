﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Globalization;

namespace PoE_Trade_Overlay.Queries
{
    public partial class SearchQueryResult
    {
        [JsonProperty("result", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Result { get; set; }

        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("total", NullValueHandling = NullValueHandling.Ignore)]
        public long Total { get; set; }

        [JsonProperty("inexact", NullValueHandling = NullValueHandling.Ignore)]
        public bool Inexact { get; set; }
    }

    public partial class SearchQueryResult
    {
        public static SearchQueryResult FromJson(string json) => JsonConvert.DeserializeObject<SearchQueryResult>(json, SearchQueryResultConverter.Settings);
    }

    public static class SearchQueryResultSerialize
    {
        public static string ToJson(this SearchQueryResult self) => JsonConvert.SerializeObject(self, SearchQueryResultConverter.Settings);
    }

    internal static class SearchQueryResultConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}