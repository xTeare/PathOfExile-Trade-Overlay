﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using PoE_Trade_Overlay.Queries;
//
//    var bulkQuery = BulkQuery.FromJson(jsonString);

namespace PoE_Trade_Overlay.Queries
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class BulkQuery
    {
        [JsonProperty("exchange", NullValueHandling = NullValueHandling.Ignore)]
        public Exchange Exchange { get; set; }
    }

    public partial class Exchange
    {
        [JsonProperty("account", NullValueHandling = NullValueHandling.Ignore)]
        public string Account { get; set; }
        
        [JsonProperty("minimum", NullValueHandling = NullValueHandling.Ignore)]
        public string Minimum { get; set; }

        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public BulkQueryStatus Status { get; set; }

        [JsonProperty("have", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Have { get; set; }

        [JsonProperty("want", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Want { get; set; }
    }

    public partial class BulkQueryStatus
    {
        [JsonProperty("option", NullValueHandling = NullValueHandling.Ignore)]
        public string Option { get; set; }
    }

    public partial class BulkQuery
    {
        public static BulkQuery FromJson(string json) => JsonConvert.DeserializeObject<BulkQuery>(json, BulkQueryConverter.Settings);
    }

    public static class BulkQuerySerialize
    {
        public static string ToJson(this BulkQuery self) => JsonConvert.SerializeObject(self, BulkQueryConverter.Settings);
    }

    internal static class BulkQueryConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
