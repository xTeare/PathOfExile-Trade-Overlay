﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace PoE_Trade_Overlay.Queries
{
    public class QueryHelper
    {
        private static List<string> lst_results = new List<string>();
        
        public static TradeData GetTradeData(string fetchURL)
        {
            if (fetchURL == string.Empty || fetchURL == null)
                return null;

            TradeData tradeData = new TradeData();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(fetchURL);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";
            httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0";

            HttpWebResponse httpResponse = null;
            try
            {
                httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch(Exception e)
            {
                Console.WriteLine("QueryHelper::GetTradeData() Exception : " + e.Message);
            }
            if(httpResponse != null)
            {
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var jayson = streamReader.ReadToEnd();
                    tradeData = TradeData.FromJson(jayson);
                }
            }
            
            return tradeData;
        }

        public static Leagues GetLeagueData()
        {
            Leagues leagues = new Leagues();
            var req = (HttpWebRequest)WebRequest.Create("http://www.pathofexile.com/api/trade/data/leagues");
            req.ContentType = "application/json";
            req.Method = "GET";

            try
            {
                var resp = (HttpWebResponse)req.GetResponse();
                using (var streamReader = new StreamReader(resp.GetResponseStream()))
                {
                    var jayson = streamReader.ReadToEnd();
                    leagues = Leagues.FromJson(jayson);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return leagues;
        }
    }
}