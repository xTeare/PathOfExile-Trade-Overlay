﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using PoE_Trade_Overlay.Queries;
//
//    var repoTags = RepoTags.FromJson(jsonString);

namespace PoE_Trade_Overlay.Queries
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class RepoTags
    {
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("message", NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        [JsonProperty("target", NullValueHandling = NullValueHandling.Ignore)]
        public string Target { get; set; }

        [JsonProperty("commit", NullValueHandling = NullValueHandling.Ignore)]
        public Commit Commit { get; set; }

        [JsonProperty("release", NullValueHandling = NullValueHandling.Ignore)]
        public Release Release { get; set; }
    }

    public partial class Commit
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("short_id", NullValueHandling = NullValueHandling.Ignore)]
        public string ShortId { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("created_at", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? CreatedAt { get; set; }

        [JsonProperty("parent_ids", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> ParentIds { get; set; }

        [JsonProperty("message", NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        [JsonProperty("author_name", NullValueHandling = NullValueHandling.Ignore)]
        public string AuthorName { get; set; }

        [JsonProperty("author_email", NullValueHandling = NullValueHandling.Ignore)]
        public string AuthorEmail { get; set; }

        [JsonProperty("authored_date", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? AuthoredDate { get; set; }

        [JsonProperty("committer_name", NullValueHandling = NullValueHandling.Ignore)]
        public string CommitterName { get; set; }

        [JsonProperty("committer_email", NullValueHandling = NullValueHandling.Ignore)]
        public string CommitterEmail { get; set; }

        [JsonProperty("committed_date", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? CommittedDate { get; set; }
    }

    public partial class Release
    {
        [JsonProperty("tag_name", NullValueHandling = NullValueHandling.Ignore)]
        public string TagName { get; set; }

        [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }
    }

    public partial class RepoTags
    {
        public static List<RepoTags> FromJson(string json) => JsonConvert.DeserializeObject<List<RepoTags>>(json, RepoTagsConverter.Settings);
    }

    public static class RepoTagsSerialize
    {
        public static string ToJson(this List<RepoTags> self) => JsonConvert.SerializeObject(self, RepoTagsConverter.Settings);
    }

    internal static class RepoTagsConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
