﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PoE_Trade_Overlay.Queries
{   

    /// <summary>
    /// Stores query, result index, results for this query
    /// </summary>
    public class QueryManager
    {
        public TabPage page;

        public string queryId;
        public int index;

        public List<string> results = new List<string>();
        public List<string> bulkResults = new List<string>();

        public bool isLoading;
        public SearchQuery sq;
        public BulkQuery bq;
        public string pseudo;

        public bool idRemoved = false;


        public string fetchURL = "https://www.pathofexile.com/api/trade/fetch/";


        public int newIndex;

        public List<string> GetBulkResults(string json)
        {
            List<string> results = new List<string>();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://www.pathofexile.com/api/trade/exchange/" + Config.league);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "Post";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            HttpWebResponse httpResponse = null;

            try
            {
                httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (WebException e)
            {
                Console.WriteLine("QueryManager::GetBulkResults Exception : " + e.Message);
                return null;
            }

            if (httpResponse != null)
            {
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var jayson = streamReader.ReadToEnd();
                    SearchQueryResult searchQueryResult = SearchQueryResult.FromJson(jayson);

                    bulkResults.Add(searchQueryResult.Id);

                    foreach (string str in searchQueryResult.Result)
                    {
                        bulkResults.Add(str);
                    }
                }
            }


            return bulkResults;
        }
        

        /// <summary>
        /// Post json to Path of Exile Trade API and get a new JSON containing the results to fetch
        /// queryResults[0] is always the QueryID !
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public List<string> GetSearchResults(string json)
        {
            List<string> queryResults = new List<string>();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://www.pathofexile.com/api/trade/search/" + Config.league);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            HttpWebResponse httpResponse = null;
            try
            {
                httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (WebException e)
            {
                Console.WriteLine("QueryManager::GetSearchResults Exception : " + e.Message);
                return null;
            }
            if (httpResponse != null)
            {
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var jayson = streamReader.ReadToEnd();
                    SearchQueryResult searchQueryResult = SearchQueryResult.FromJson(jayson);

                    queryResults.Add(searchQueryResult.Id);

                    foreach (string str in searchQueryResult.Result)
                    {
                        queryResults.Add(str);
                    }
                }
            }
            results = queryResults;
            return queryResults;
        }

        public string GetBulkFetchURL(List<string> queryResults)
        {
            if (queryResults.Count == 0 || queryResults.Count == 1)
                return string.Empty;

            // Save Queryid
            queryId = queryResults[0];

            // Remove QueryID
            queryResults.RemoveAt(0);

            index = 0;

            int to = 20;

            if (to > bulkResults.Count)
                to = bulkResults.Count;

            string url = fetchURL;

            // Add results to url
            for (int i = index; i < to; i++)
            {
                url += bulkResults[i] + ((i != to - 1) ? "," : "");
                index = i;
            }

            // Add queryid to url
            url += "?query=" + queryId + "&exchange";
            Console.WriteLine(url);
            return url;
        }

        /// <summary>
        /// Returns an url to get trade data. QueryID is removed from the result list and stored in public string queryId, Results are removed, after cycling through them
        /// </summary>
        /// <param name="queryResults"></param>
        /// <returns></returns>
        public string GetBulkURLNew(string pseudos = "")
        {

            string url = fetchURL;
            List<string> elementsToRemove = new List<string>();

            if (bulkResults.Count == 0)
                return string.Empty;

            if (!idRemoved)
            {
                queryId = bulkResults[0];
                bulkResults.RemoveAt(0);
                idRemoved = true;
            }

            int from = 0;
            int to = 10;

            if (to > bulkResults.Count)
                to = bulkResults.Count;

            for (int i = from; i < to; i++)
            {
                string add = "";
                if (i != to - 1)
                    add = ",";

                url += bulkResults[i] + add;
                elementsToRemove.Add(bulkResults[i]);
            }

            // Add queryid to url
            url += "?query=" + queryId + "&exchange";
            Console.WriteLine(url);

            foreach (string s in elementsToRemove)
                results.Remove(s);

            return url;
        }

        /// <summary>
        /// Returns an url to get trade data. QueryID is removed from the result list and stored in public string queryId, Results are removed, after cycling through them
        /// </summary>
        /// <param name="queryResults"></param>
        /// <returns></returns>
        public string GetFetchURLNew(string pseudos = "")
        {

            string url = fetchURL;
            List<string> elementsToRemove = new List<string>();

            if (results.Count == 0)
                return string.Empty;

            if (!idRemoved)
            {
                queryId = results[0];
                results.RemoveAt(0);
                idRemoved = true;
            }

            int from = 0;
            int to = 10;

            if (to > results.Count)
                to = results.Count;

            for (int i = from; i < to; i++)
            {
                string add = "";
                if (i != to - 1)
                    add = ",";

                url += results[i] + add;
                elementsToRemove.Add(results[i]);
            }

            url += "?query=" + queryId;

            url += pseudo;


            foreach (string s in elementsToRemove)
                results.Remove(s);
            
            return url;
        }



       

    }
}
