﻿using PoE_Trade_Overlay.Queries;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows;

namespace PoE_Trade_Overlay
{
    public static class Data
    {
        public static bool isLoaded;

        public static DataTable items = new DataTable();
        public static DataTable itemTypes = new DataTable();
        public static DataTable itemRarity = new DataTable();
        public static DataTable buyoutPrice = new DataTable();
        public static DataTable mods = new DataTable();
        public static DataTable socketPositions = new DataTable();
        public static DataTable currency = new DataTable();
        public static DataTable ninja = new DataTable();
        public static DataTable properties = new DataTable();

        public static List<string> modsWithReduced = new List<string>();

        public static string praiseHim = "";
        public static List<string> oldAtlasVersions = new List<string>() { "Original", "The Awakening", "Atlas of Worlds", "War for the Altas" }; 

        // Colors for Events

        public static Currency loadedCurrency; // used for bulk button generation

        private static int debugHeaderWidth = 70;

        public static void Load()
        {
            if (!isLoaded)
            {
                itemTypes = ConvertToDataTable("data/types.txt", new string[] { "id", "text" });
                itemRarity = ConvertToDataTable("data/rarity.txt", new string[] { "id", "text" });
                buyoutPrice = ConvertToDataTable("data/buyoutPrice.txt", new string[] { "id", "text" });
                socketPositions = ConvertToDataTable("data/socketspacing.txt", new string[] { "id", "x", "y" });
                properties = GeneratePropertiesDT();
                ParseCurrency();
                ParseItems();
                ParseMods();
                isLoaded = true;
            }
        }

        /// <summary>
        /// Searches the datatables rows for the search keyword in the column defined by columnName and returns the value of the selected Column Index
        /// </summary>
        /// <param name="tbl">Table to search</param>
        /// <param name="columnPos">Column content to return</param>
        /// <param name="columnName">Column to search in</param>
        /// <param name="search">Search keyword</param>
        /// <returns></returns>
        public static string GetEntryFromRow(DataTable tbl, int columnIndex, string columnToSearch, string search)
        {
            string str = string.Empty;
            try
            {
                DataRow[] rows = tbl.Select(columnToSearch + " = '" + search + "'");
                if (rows.Length != 0)
                {
                    str = rows[0][columnIndex].ToString();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Data:GetEntryFromRow Exception : " + e.Message);
                
            }
            return str;
        }

        public static DataTable GeneratePropertiesDT()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("id");
            dt.Columns.Add("text");

            dt.Rows.Add("ilvl", "Item Level");
            dt.Rows.Add("quality", "Quality");
            dt.Rows.Add("pdamage", "Physical Damage");
            dt.Rows.Add("crit", "Critical Strike Chance");
            dt.Rows.Add("aps", "Attacks per Second");
            dt.Rows.Add("es", "Energy Shield");
            dt.Rows.Add("ar", "Armour");
            dt.Rows.Add("ev", "Evasion Rating");
            dt.Rows.Add("block", "Chance to Block");
            
            return dt;
        }

        public static DataTable ConvertToDataTable(string filePath, string[] columns)
        {
            DataTable tbl = new DataTable();
            foreach (string str in columns)
            {
                tbl.Columns.Add(new DataColumn(str));
            }

            string[] lines = File.ReadAllLines(filePath);

            foreach (string line in lines)
            {
                var cols = line.Split(':');

                DataRow dr = tbl.NewRow();
                for (int cIndex = 0; cIndex < columns.Length; cIndex++)
                {
                    if (cols.Length >= cIndex + 1)
                        dr[cIndex] = cols[cIndex];

                    if (filePath == "data/items.txt")
                    {
                        if (cols[0] != cols[1])
                            dr[3] = cols[0] + " " + cols[1];
                        else
                            dr[3] = cols[0];
                    }
                }
                tbl.Rows.Add(dr);
            }

            return tbl;
        }

        public static string GetPropertyAbb(string text)
        {
            foreach(DataRow dr in properties.Rows)
            {
                if (dr[1].ToString().Contains(text))
                    return dr[0].ToString();
            }
            return "";
        }

        public static string RemoveSpecialChars(string text, bool removePlus)
        {
            string s = text;
            s = s.Replace(".", ""); // Remove point
            s = Regex.Replace(s, @"\d", "#"); // Replace all numbers with #
            s = Regex.Replace(s, @"#+", "#"); // Replace all # with just one #

            if (s.StartsWith("-"))
                s = s.Replace("-", "+");
            if(removePlus)
                s = s.Replace("+", "");
            return s;
        }

        public static string GetMod(string modText, bool isImplicit = false)
        {
            bool isPseudo = modText.Contains("(pseudo) ");  

            string newText = RemoveSpecialChars(modText, !isPseudo);


            if (isPseudo)
                newText = newText.Replace("(pseudo) ", "");
            
            /// Replace reduced with increased on some mods
            if (newText.Contains("reduced"))
                if (!modsWithReduced.Contains(newText))
                    newText = newText.Replace("reduced", "increased");
            
            foreach (DataRow dr in mods.Rows)
            {
                if (isImplicit)
                {
                    if (dr[0].ToString().Contains("implicit"))
                    {
                        if (dr[1].ToString() == newText)
                        {
                            return dr[0].ToString();
                        }
                    }
                }
                else
                {
                    if (isPseudo)
                    {
                        if (dr[0].ToString().Contains("pseudo"))
                        {
                            if (dr[1].ToString() == newText)
                            {
                                return dr[0].ToString();
                            }
                        }
                    }
                    else
                    {
                        if (!dr[0].ToString().Contains("pseudo"))
                        {
                            if (dr[1].ToString() == newText)
                            {
                                return dr[0].ToString();
                            }
                        }
                    }
                }
                
            }

            return "";
        }


        public static string GetModIdByText(string text, bool isImplicit = false)
        {
            if (text == null)
                return string.Empty;

            text = text.Replace("[pseudo] ", "").Replace("[implicit] ", "").Replace("[explicit] ", "").Replace("[crafted] ", "").Replace("[enchant] ", "");

            bool isPseudo = false;
            if (text.Contains("(pseudo)"))
            {
                isPseudo = true;
                text = text.Replace("(pseudo)", "");
            }

            foreach (DataRow dataRow in mods.Rows)
            {
                if (isImplicit)
                {
                    if (dataRow[0].ToString().Contains("implicit"))
                    {
                        if (dataRow[1].ToString() == text)
                        {
                            return dataRow[0].ToString();
                        }
                    }
                }
                else if (isPseudo)
                {
                    if (dataRow[0].ToString().Contains("pseudo"))
                    {
                        if (dataRow[1].ToString() == text)
                        {
                            return dataRow[0].ToString();
                        }
                    }
                }
                else
                {
                    if (dataRow[1].ToString() == text)
                    {
                        return dataRow[0].ToString();
                    }
                }
            }

            return string.Empty;
        }

        public static void LoadItemCategories(this System.Windows.Forms.ComboBox.ObjectCollection objectCollection)
        {
            foreach (DataRow dr in itemTypes.Rows)
            {
                objectCollection.Add(dr[1].ToString());
            }
        }

        public static string GetCurrencyURL(this string source)
        {
            string url = "";

            foreach (DataRow dataRow in currency.Rows)
            {
                if (dataRow[0].ToString() == source)
                {
                    url = dataRow[2].ToString();
                }
            }
            return url;
        }

        public static string GetFullCurrencyName(this string source)
        {
            string text = "";

            foreach (DataRow dr in currency.Rows)
            {
                if (dr[0].ToString() == source)
                    text = dr[1].ToString();
            }

            return text;
        }

        public static string GetCurrencyAbb(this string source)
        {
            string text = "";

            foreach (DataRow dr in currency.Rows)
            {
                if (dr[1].ToString() == source)
                    text = dr[0].ToString();
            }

            return text;
        }

        public static string GetChaosEquivalent(string source)
        {
            string s = string.Empty;
            foreach (DataRow dr in ninja.Rows)
            {
                if (dr[0].ToString() == source)
                    s = dr[1].ToString();
            }

            return s;
        }

        /// <summary>
        /// Loads all items from pathofexile.com/api/trade/data/items
        /// </summary>
        /// <returns></returns>
        public static Items LoadItems()
        {
            Items items = new Items();

            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://www.pathofexile.com/api/trade/data/items");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";
            httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0";
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var jayson = streamReader.ReadToEnd();
                items = Items.FromJson(jayson);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error while downloading Item Data\n" + e.Message + "\nThe Trade API might be down", "PathOfExile Trade Overlay");
                Environment.Exit(0);
            }
            return items;
        }

        public static Stats LoadMods()
        {
            Stats stats = new Stats();

            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://www.pathofexile.com/api/trade/data/stats");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";
            httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0";
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var jayson = streamReader.ReadToEnd();
                stats = Stats.FromJson(jayson);
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("Error while downloading Mod Data\n" + e.Message + "\nThe Trade API might be down", "PathOfExile Trade Overlay");
                    Environment.Exit(0);
                }

                return stats;
        }

        public static Currency LoadCurrency()
        {
            Currency currency = new Currency();

            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://www.pathofexile.com/api/trade/data/static");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";
            httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0";
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var jayson = streamReader.ReadToEnd();
                    currency = Queries.Currency.FromJson(jayson);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error while downloading Currency Data\n" + e.Message + "\nThe Trade API might be down", "PathOfExile Trade Overlay");
                Environment.Exit(0);
            }


            loadedCurrency = currency;
            return currency;
        }

        public static void ParseCurrency()
        {
            Currency currency = LoadCurrency();

            string fileContents = "";

            foreach (CurrencyElement result in currency.Result.Currency)
                fileContents += string.Format("{0}:{1}:{2}\n", result.Id, result.Text, "web.poecdn.com" + result.Image);

            foreach (CurrencyElement result in currency.Result.Fragments)
                fileContents += string.Format("{0}:{1}:{2}\n", result.Id, result.Text, "web.poecdn.com" + result.Image);

            foreach (CurrencyElement result in currency.Result.Resonators)
                fileContents += string.Format("{0}:{1}:{2}\n", result.Id, result.Text, "web.poecdn.com" + result.Image);

            foreach (CurrencyElement result in currency.Result.Fossils)
                fileContents += string.Format("{0}:{1}:{2}\n", result.Id, result.Text, "web.poecdn.com" + result.Image);

            foreach (CurrencyElement result in currency.Result.Essences)
                fileContents += string.Format("{0}:{1}:{2}\n", result.Id, result.Text, "web.poecdn.com" + result.Image);

            File.WriteAllText("data/currency.txt", fileContents);
            Data.currency = ConvertToDataTable("data/currency.txt", new string[] { "id", "text", "url" });
        }

        public static void ParseMods()
        {
            Stats stats = LoadMods();
            string fileContents = "";
            foreach (StatResult result in stats.Result)
            {
                foreach (StatEntry e in result.Entries)
                {
                    if (!e.Text.Contains("\n"))
                        fileContents += string.Format("{0}:{1}:{2}\n", e.Id, e.Text, e.Type);
                    if (e.Text.Contains("reduced"))
                        modsWithReduced.Add(e.Text);
                }
            }
            File.WriteAllText("data/mods.txt", fileContents);
            mods = ConvertToDataTable("data/mods.txt", new string[] { "id", "text", "type" });
        }


        /// <summary>
        /// Loop through the downloaded items and write them to file for parsing
        /// </summary>
        public static void ParseItems()
        {
            Items items = LoadItems();

            string fileContents = "";

            foreach (ItemResult result in items.Result)
            {
                foreach (ItemEntry e in result.Entries)
                {
                    if (result.Label == "Maps")
                    {
                        if (e.Flags != null)
                        {
                            string s2 = e.Text.Replace(e.Name + " ", "").Replace("'", "%27");
                            fileContents += string.Format("{0}:{1}\n", e.Name.Replace("'", "%27"), s2);
                        }
                        else
                        {
                            string s3 = e.Type.Replace("'", "%27"); // Academy Map
                            string s4 = e.Text.Replace("'", "%27"); // Academy Map (War for the Atlas)
                            fileContents += string.Format("{0}:{1}\n", e.Type.Replace("'", "%27"), s4.Replace(s3 + " ", ""));
                        }
                    }
                    else
                    {
                        if (e.Name != null)
                        {
                            fileContents += string.Format("{0}:{1}:{2}\n", e.Name.Replace("'", "%27"), e.Type.Replace("'", "%27"), e.Text.Replace("'", "%27"));
                        }
                        else
                        {
                            fileContents += string.Format("{0}:{1}\n", e.Type.Replace("'", "%27"), e.Text.Replace("'", "%27"));
                        }
                    }
                }
            }
            File.WriteAllText("data/items.txt", fileContents);
            Data.items = ConvertToDataTable("data/items.txt", new string[] { "name", "type", "text", "combined" });
        }

        public static void DebugTable(DataTable table)
        {
            Debug.WriteLine("--- DebugTable(" + table.TableName + ") ---");
            int zeilen = table.Rows.Count;
            int spalten = table.Columns.Count;

            // Header
            for (int i = 0; i < table.Columns.Count; i++)
            {
                string s = table.Columns[i].ToString();
                Debug.Write(String.Format("{0,-" + debugHeaderWidth + "} | ", s));
            }
            Debug.Write(Environment.NewLine);
            for (int i = 0; i < table.Columns.Count; i++)
            {
                string s = "";

                for (int j = 0; j < (debugHeaderWidth + i + 1); j++)
                {
                    s += "-";
                }
                s += "|";
                Debug.Write(s);
            }
            Debug.Write(Environment.NewLine);

            // Data
            for (int i = 0; i < zeilen; i++)
            {
                DataRow row = table.Rows[i];
                //Debug.WriteLine("{0} {1} ", row[0], row[1]);
                for (int j = 0; j < spalten; j++)
                {
                    string s = row[j].ToString();
                    if (s.Length > debugHeaderWidth) s = s.Substring(0, debugHeaderWidth - 3) + "...";
                    Debug.Write(String.Format("{0,-" + debugHeaderWidth + "} | ", s));
                }
                Debug.Write(Environment.NewLine);
            }
            for (int i = 0; i < table.Columns.Count; i++)
            {
                string s = "";

                for (int j = 0; j < (debugHeaderWidth + i + 1); j++)
                {
                    s += "-";
                }
                s += "|";
                Debug.Write(s);
            }
            Debug.Write(Environment.NewLine);
        }
    }
}