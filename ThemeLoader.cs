﻿using PoE_Trade_Overlay.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace PoE_Trade_Overlay
{
    public static class ThemeLoader
    {
        public static DataTable themeList = new DataTable("Themes");

        public static DataSet themes = new DataSet();

        public static Color textbox_Placeholder;
        public static Color textbox_Active;

        // We create blank controls to store the colors
        public static FlattenCombo fc = new FlattenCombo();
        public static ExpandCollapse ec = new ExpandCollapse();
        public static Filter f = new Filter();
        public static Toggle toggle = new Toggle();
        public static Filter_Buyout fbo = new Filter_Buyout();
        public static Filter_DropDown fd = new Filter_DropDown();
        public static Filter_Sockets fs = new Filter_Sockets();
        public static Filter_Textbox ftb = new Filter_Textbox();
        public static RichTextBox rtb = new RichTextBox();
        public static ModGroup mg = new ModGroup();
        public static ModSearch ms = new ModSearch();
        public static SearchResult_Minimalistic sr = new SearchResult_Minimalistic();
        public static ExchangeResult er = new ExchangeResult();
        public static FastBulkButton fbb = new FastBulkButton();

        public static Color ForeColor;
        public static Color ForeColor_Header;
        public static Color ForeColor_Button;
        public static Color ForeColor_Textbox;
        public static Color BackColor_Header;
        public static Color BackColor_Panel;
        public static Color BackColor_Textbox;
        public static Color BackColor;
        public static Color BackColor_Button;
        public static Image Drag_Panel;
        public static Image Layout_Small;
        public static Image Layout_Big;
        public static Image Settings;

        public static void GetAllThemes()
        {
            themeList.Columns.Clear();
            themeList.Rows.Clear();
            themeList.Columns.Add("Name");
            themeList.Columns.Add("Path");
            string[] dirs = Directory.GetDirectories("Themes");
            foreach (string d in dirs)
            {
                themeList.Rows.Add(d.After(@"\"), d);
            }
        }
        public static void ApplyTheme(List<Control> controls)
        {
            foreach (Control control in controls)
            {
                

                switch (control.GetType().Name)
                {
                    case "Toggle":
                        ((Toggle)control).CopyColors();
                        break;
                    case "NoFocusButton":
                        ((NoFocusButton)control).ForeColor = ForeColor_Button;
                        ((NoFocusButton)control).BackColor = BackColor_Button;

                        if (control.Name == "btn_settings")
                            ((NoFocusButton)control).BackgroundImage = Settings;

                        break;

                    case "Button":
                        ((Button)control).ForeColor = ForeColor_Button;
                        ((Button)control).BackColor = BackColor_Button;
                        if (control.Name == "btn_defaultLayout")
                            ((Button)control).BackgroundImage = Layout_Small;
                        if (control.Name == "btn_twoColumns")
                            ((Button)control).BackgroundImage = Layout_Big;
                        if (control.Name == "btn_settings")
                            ((Button)control).BackgroundImage = Settings;

                        break;

                    case "PictureBox":
                        if (control.Parent.Name == "panel_main") // Assign Drag Image
                            ((PictureBox)control).Image = Drag_Panel;
                        break;

                    case "Panel":
                        if (control.Name == "panel_main")
                            ((Panel)control).BackColor = BackColor_Panel;
                        break;

                    case "Label":
                        if (control.Name.Contains("header")) // Check if it's a header
                        {
                            ((Label)control).BackColor = BackColor_Header;
                            ((Label)control).ForeColor = ForeColor_Header;
                        }
                        else
                        {
                            if (control.Parent.Name.Contains("tp_")) // Just set labels outside filters etc.
                            {
                                ((Label)control).ForeColor = ForeColor;
                                ((Label)control).BackColor = BackColor;
                            }
                        }
                        break;

                    case "RichTextBox":
                        ((RichTextBox)control).CopyColors();
                        break;

                    case "FlattenCombo":
                        // Do not apply to Combos in filters
                        if (!control.Parent.GetType().Name.Contains("Filter"))
                            ((FlattenCombo)control).CopyColors();
                        break;

                    case "Filter":
                        ((Filter)control).CopyColors();
                        ((Filter)control).RefreshControls();
                        break;

                    case "Filter_Buyout":
                        ((Filter_Buyout)control).CopyColors();
                        ((Filter_Buyout)control).RefreshControls();
                        break;

                    case "Filter_DropDown":
                        ((Filter_DropDown)control).CopyColors();
                        ((Filter_DropDown)control).RefreshControls();
                        break;

                    case "Filter_Textbox":
                        ((Filter_Textbox)control).CopyColors();
                        ((Filter_Textbox)control).RefreshControls();
                        break;

                    case "Filter_Sockets":
                        ((Filter_Sockets)control).CopyColors();
                        ((Filter_Sockets)control).RefreshControls();
                        break;

                    case "ExpandCollapse":
                        ((ExpandCollapse)control).CopyColors();
                        ((ExpandCollapse)control).RefreshControls();
                        break;

                    case "ModGroup":
                        ((ModGroup)control).CopyColors();
                        ((ModGroup)control).RefreshControls();
                        break;

                    case "ModSearch":
                        ((ModSearch)control).CopyColors();
                        ((ModSearch)control).RefreshControls();
                        break;

                    case "ExchangeResult":
                        ((ExchangeResult)control).CopyColors();
                        break;

                    case "SearchResult":
                        ((SearchResult_Minimalistic)control).CopyColors();
                        break;

                    case "TextBox":

                        // Do not apply to boxes in filters or modgroups
                        if (!control.Parent.GetType().Name.Contains("Filter") && !control.Parent.GetType().Name.Contains("Mod"))
                        {
                            ((TextBox)control).ForeColor = ForeColor_Textbox;
                            ((TextBox)control).BackColor = BackColor_Textbox;
                        }
                        break;
                }
                if (control.TopLevelControl != null)
                    if (control.TopLevelControl.Name == "Form_Settings")
                        if (control.GetType().Name == "FlattenCombo")
                        {
                            Console.WriteLine(((FlattenCombo)control).ForeColor);
                            Console.WriteLine(((FlattenCombo)control).ForeColorDrop);
                        }
            }
        }

        public static void Load(string theme)
        {
            GetAllThemes();

            string path = "";

            foreach (DataRow dr in themeList.Rows)
            {
                if (dr[0].ToString() == theme)
                {
                    path += Application.StartupPath + @"\";
                    path += dr[1].ToString();
                    path += @"\theme.json";
                }
            }
            
            if (path == string.Empty | !File.Exists(path))
                return;
            

            Theme t = Theme.FromJson(File.ReadAllText(path));

            ForeColor_Header = t.Forecolor_Header.ToColor();
            ForeColor = t.Forecolor.ToColor();
            ForeColor_Button = t.Forecolor_Button.ToColor();
            ForeColor_Textbox = t.Forecolor_Textbox.ToColor();
            BackColor_Header = t.Backcolor_Header.ToColor();
            BackColor_Panel = t.Backcolor_Panel.ToColor();
            BackColor_Button = t.Backcolor_Button.ToColor();
            BackColor_Textbox = t.Backcolor_Textbox.ToColor();
            BackColor = t.Backcolor.ToColor();
            
            if (File.Exists(t.Drag_Image))
                Drag_Panel = Image.FromFile(t.Drag_Image);
            else
                Drag_Panel = Properties.Resources.hamburger_menu;

            if (File.Exists(t.Settings))
                Settings = Image.FromFile(t.Settings);
            else
                Settings = Properties.Resources.cog;

            if (File.Exists(t.Layout_Small))
                Layout_Small = Image.FromFile(t.Layout_Small);
            else
                Layout_Small = Properties.Resources.format_default_small;

            if (File.Exists(t.Layout_Big))
                Layout_Big = Image.FromFile(t.Layout_Big);
            else
                Layout_Big = Properties.Resources.format_twoColums_small;

            foreach (Ctrl control in t.Controls)
            {
                Colors c = control.Colors;
                switch (control.Type)
                {
                    case "BulkButton":
                        fbb.ForeColor = c.Forecolor.ToColor();
                        fbb.ForeColor_Selected = c.Forecolor_Selected.ToColor();
                        fbb.ForeColor_Hover = c.Forecolor_Hover.ToColor();
                        fbb.BackColor = c.Backcolor.ToColor();
                        fbb.BackColor_Selected = c.Backcolor_Selected.ToColor();
                        fbb.Border = c.Border.ToColor();
                        fbb.Border_Selected = c.Border_Hightlighted.ToColor();
                        break;

                    case "SearchResult":
                        sr.ForeColor = c.Forecolor.ToColor();
                        sr.ForeColor_Button = c.Forecolor_Button.ToColor();
                        sr.ForeColor_Online = c.Forecolor_Online.ToColor();
                        sr.ForeColor_Offline = c.Forecolor_Offline.ToColor();
                        sr.ForeColor_Afk = c.Forecolor_Afk.ToColor();
                        sr.Pseudo = c.Forecolor_Pseudo.ToColor();
                        sr.Normal = c.Forecolor_Normal.ToColor();
                        sr.Magic = c.Forecolor_Magic.ToColor();
                        sr.Rare = c.Forecolor_Rare.ToColor();
                        sr.Unique = c.Forecolor_Unique.ToColor();
                        sr.Relic = c.Forecolor_Relic.ToColor();
                        sr.Divination = c.Forecolor_Divination.ToColor();
                        sr.Gem = c.Forecolor_Gem.ToColor();
                        sr.Currency = c.Forecolor_Curreny.ToColor();
                        sr.Prophecy = c.Forecolor_Prophecy.ToColor();
                        sr.Corrupted = c.Forecolor_Corrupted.ToColor();
                        sr.Unidentified = c.Forecolor_Unidentified.ToColor();
                        sr.Crafted = c.Forecolor_Crafted.ToColor();
                        sr.Veiled = c.Forecolor_Veiled.ToColor();

                        sr.BackColor = c.Backcolor.ToColor();
                        sr.BackColor_Button = c.Backcolor_Button.ToColor();
                        sr.BackColor_Online = c.Backcolor_Online.ToColor();
                        sr.BackColor_Offline = c.Backcolor_Offline.ToColor();
                        sr.BackColor_Afk = c.Backcolor_Afk.ToColor();
                        sr.BackColor_ModHover = c.Backcolor_ModHover.ToColor();
                        sr.Separator = c.Separator.ToColor();
                        sr.Bar = c.Bar.ToColor();
                        break;

                    case "Toggle":

                        toggle.ForeColor = c.Forecolor.ToColor();
                        toggle.Bar_Off = c.Bar_Off.ToColor();
                        toggle.Bar_On = c.Bar_On.ToColor();
                        toggle.Border_Off = c.Border_Off.ToColor();
                        toggle.Border_On = c.Border_On.ToColor();
                        if (File.Exists(control.Images.Knob))
                            toggle.KnobImage = Image.FromFile(control.Images.Knob);
                        else
                            toggle.KnobImage = Properties.Resources.knob;
                        break;

                    case "ExchangeResult":
                        er.ForeColor = c.Forecolor.ToColor();
                        er.ForeColor_Exchange = c.Forecolor_Exchange.ToColor();
                        er.ForeColor_ExchangeHover = c.Forecolor_ExchangeHover.ToColor();
                        er.ForeColor_Online = c.Forecolor_Online.ToColor();
                        er.ForeColor_Offline = c.Forecolor_Offline.ToColor();
                        er.ForeColor_Afk = c.Forecolor_Afk.ToColor();
                        er.ForeColor_Warning = c.Forecolor_Warning.ToColor();
                        er.BackColor = c.Backcolor.ToColor();
                        er.BackColor_Top = c.Backcolor_Top.ToColor();
                        er.BackColor_Bottom = c.Backcolor_Bottom.ToColor();
                        er.BackColor_Online = c.Backcolor_Online.ToColor();
                        er.BackColor_Offline = c.Backcolor_Offline.ToColor();
                        er.BackColor_Afk = c.Backcolor_Afk.ToColor();
                        er.BackColor_Exchange = c.Backcolor_Exchange.ToColor();
                        er.BackColor_ExchangeHover = c.Backcolor_ExchangeHover.ToColor();
                        er.BackColor_ModHover = c.Backcolor_ModHover.ToColor();
                        break;

                    case "DropDown":
                        fc.ForeColor = c.Forecolor.ToColor();
                        fc.BackColor = c.Backcolor.ToColor();
                        fc.DropDownColor = c.Backcolor_DropDown.ToColor();
                        fc.ForeColorDrop = c.Forecolor_Dropdown.ToColor();
                        fc.ButtonColor = c.Backcolor_Button.ToColor();
                        fc.Arrow_Active = c.Arrow_Active.ToColor();
                        fc.Arrow_Inactive = c.Arrow_Inactive.ToColor();
                        fc.Border = c.Backcolor_Border.ToColor();

                        break;

                    case "ExpandCollapse":
                        ec.ForeColor_Active = c.Forecolor_Active.ToColor();
                        ec.ForeColor_Inactive = c.Forecolor_Inactive.ToColor();
                        ec.BackColor = c.Backcolor.ToColor();
                        ec.BackColor_HeaderActive = c.Backcolor_HeaderActive.ToColor();
                        ec.BackColor_HeaderInactive = c.Backcolor_HeaderInactive.ToColor();
                        if (File.Exists(control.Images.Active))
                            ec.Image_Active = Image.FromFile(control.Images.Active);
                        else
                            ec.Image_Active = Properties.Resources.chaos;

                        if (File.Exists(control.Images.Inactive))
                            ec.Image_Inactive = Image.FromFile(control.Images.Inactive);
                        else
                            ec.Image_Inactive = Properties.Resources.chaos_bw;
                        break;

                    case "Filter":
                        f.ForeColor = c.Forecolor.ToColor();
                        f.ForeColor_Placeholder = c.Forecolor_Placeholder.ToColor();
                        f.ForeColor_Textbox = c.Forecolor_Textbox.ToColor();
                        f.BackColor = c.Backcolor.ToColor();
                        f.BackColor_Label = c.Backcolor_Label.ToColor();
                        f.BackColor_Textbox = c.Backcolor_Textbox.ToColor();
                        break;

                    case "Filter_Buyout":
                        fbo.ForeColor = c.Forecolor.ToColor();
                        fbo.ForeColor_Textbox = c.Forecolor_Textbox.ToColor();
                        fbo.ForeColor_Placeholder = c.Forecolor_Placeholder.ToColor();
                        fbo.ForeColor_Dropdown = c.Forecolor_Dropdown.ToColor();
                        fbo.BackColor = c.Backcolor.ToColor();
                        fbo.BackColor_Textbox = c.Backcolor_Textbox.ToColor();
                        fbo.BackColor_Label = c.Backcolor_Label.ToColor();
                        fbo.BackColor_DropDown = c.Backcolor_DropDown.ToColor();
                        fbo.BackColor_Button = c.Backcolor_Button.ToColor();
                        fbo.Border = c.Border.ToColor();
                        fbo.Arrow_Active = c.Arrow_Active.ToColor();
                        fbo.Arrow_Inactive = c.Arrow_Inactive.ToColor();
                        break;

                    case "Filter_DropDown":
                        fd.ForeColor_Label = c.Forecolor.ToColor();
                        fd.ForeColor_Dropdown = c.Forecolor_Dropdown.ToColor();
                        fd.BackColor = c.Backcolor.ToColor();
                        fd.BackColor_Label = c.Backcolor_Label.ToColor();
                        fd.BackColor_DropDown = c.Backcolor_DropDown.ToColor();
                        fd.BackColor_Button = c.Backcolor_Button.ToColor();
                        fd.Arrow_Active = c.Arrow_Active.ToColor();
                        fd.Arrow_Inactive = c.Arrow_Inactive.ToColor();
                        fd.Border = c.Backcolor_Border.ToColor();
                        break;

                    case "Filter_Sockets":
                        fs.ForeColor = c.Forecolor.ToColor();
                        fs.ForeColor_Placeholder = c.Forecolor_Placeholder.ToColor();
                        fs.ForeColor_Textbox = c.Forecolor_Textbox.ToColor();
                        fs.BackColor = c.Backcolor.ToColor();
                        fs.BackColor_Label = c.Backcolor_Label.ToColor();
                        fs.BackColor_Textbox = c.Backcolor_Textbox.ToColor();
                        break;

                    case "Filter_Textbox":
                        ftb.ForeColor = c.Forecolor.ToColor();
                        ftb.ForeColor_Textbox = c.Forecolor_Textbox.ToColor();
                        ftb.BackColor = c.Backcolor.ToColor();
                        ftb.BackColor_Textbox = c.Backcolor_Textbox.ToColor();
                        ftb.BackColor_Label = c.Backcolor_Label.ToColor();
                        break;

                    case "ModGroup":
                        mg.ForeColor_Textbox = c.Forecolor_Textbox.ToColor();
                        mg.ForeColor_Placeholder = c.Forecolor_Placeholder.ToColor();
                        mg.ForeColor_Header = c.Forecolor_Header.ToColor();
                        mg.ForeColor_Button = c.Forecolor_Button.ToColor();
                        mg.BackColor = c.Backcolor.ToColor();
                        mg.BackColor_Header = c.Backcolor_Header.ToColor();
                        mg.BackColor_Button = c.Backcolor_Button.ToColor();
                        break;

                    case "ModSearch":
                        ms.ForeColor_Textbox = c.Forecolor_Textbox.ToColor();
                        ms.ForeColor_Button = c.Forecolor_Button.ToColor();
                        ms.ForeColor_Listbox = c.Forecolor_Listbox.ToColor();
                        ms.ForeColor_Placeholder = c.Forecolor_Placeholder.ToColor();
                        ms.BackColor = c.Backcolor.ToColor();
                        ms.BackColor_Textbox = c.Backcolor_Textbox.ToColor();
                        ms.BackColor_Button = c.Backcolor_Button.ToColor();
                        ms.BackColor_Listbox = c.Backcolor_Listbox.ToColor();
                        break;

                    case "RichTextBox":
                        rtb.ForeColor = c.Forecolor.ToColor();
                        rtb.BackColor = c.Backcolor.ToColor();
                        break;
                }
            }
        }

        public static ExpandCollapse CopyColors(this ExpandCollapse filter)
        {
            filter.ForeColor = ec.ForeColor;
            filter.ForeColor_Active = ec.ForeColor_Active;
            filter.ForeColor_Inactive = ec.ForeColor_Inactive;
            filter.BackColor = ec.BackColor;
            filter.BackColor_HeaderActive = ec.BackColor_HeaderActive;
            filter.BackColor_HeaderInactive = ec.BackColor_HeaderInactive;
            filter.Image_Active = ec.Image_Active;
            filter.Image_Inactive = ec.Image_Inactive;
            return filter;
        }

        public static RichTextBox CopyColors(this RichTextBox rich)
        {
            rich.ForeColor = rtb.ForeColor;
            rich.BackColor = rtb.BackColor;
            return rich;
        }

        public static Filter CopyColors(this Filter filter)
        {
            filter.ForeColor = f.ForeColor;
            filter.ForeColor_Placeholder = f.ForeColor_Placeholder;
            filter.ForeColor_Textbox = f.ForeColor_Textbox;
            filter.BackColor = f.BackColor;
            filter.BackColor_Label = f.BackColor_Label;
            filter.BackColor_Textbox = f.BackColor_Textbox;
            return filter;
        }

        public static Filter_Buyout CopyColors(this Filter_Buyout filter)
        {
            filter.ForeColor = fbo.ForeColor;
            filter.ForeColor_Textbox = fbo.ForeColor_Textbox;
            filter.ForeColor_Placeholder = fbo.ForeColor_Placeholder;
            filter.ForeColor_Dropdown = fbo.ForeColor_Dropdown;
            filter.BackColor = fbo.BackColor;
            filter.BackColor_Textbox = fbo.BackColor_Textbox;
            filter.BackColor_Label = fbo.BackColor_Label;
            filter.BackColor_DropDown = fbo.BackColor_DropDown;
            filter.BackColor_Button = fbo.BackColor_Button;
            filter.Border = fbo.Border;
            filter.Arrow_Active = fbo.Arrow_Active;
            filter.Arrow_Inactive = fbo.Arrow_Inactive;

            return filter;
        }

        public static Filter_DropDown CopyColors(this Filter_DropDown filter)
        {
            filter.ForeColor_Dropdown = fd.ForeColor_Dropdown;
            filter.ForeColor_Label = fd.ForeColor_Label;
            filter.BackColor = fd.BackColor;
            filter.BackColor_Label = fd.BackColor_Label;
            filter.BackColor_DropDown = fd.BackColor_DropDown;
            filter.BackColor_Button = fd.BackColor_Button;
            filter.Border = fd.Border;
            filter.Arrow_Active = fd.Arrow_Active;
            filter.Arrow_Inactive = fd.Arrow_Inactive;

            return filter;
        }

        public static Filter_Sockets CopyColors(this Filter_Sockets filter)
        {
            filter.ForeColor = fs.ForeColor;
            filter.ForeColor_Placeholder = fs.ForeColor_Placeholder;
            filter.ForeColor_Textbox = fs.ForeColor_Textbox;
            filter.BackColor = fs.BackColor;
            filter.BackColor_Label = fs.BackColor_Label;
            filter.BackColor_Textbox = fs.BackColor_Textbox;
            return filter;
        }

        public static Filter_Textbox CopyColors(this Filter_Textbox filter)
        {
            filter.ForeColor = ftb.ForeColor;
            filter.ForeColor_Label = ftb.ForeColor_Label;
            filter.ForeColor_Textbox = ftb.ForeColor_Textbox;
            filter.BackColor = ftb.BackColor;
            filter.BackColor_Label = ftb.BackColor_Label;
            filter.BackColor_Textbox = ftb.BackColor_Textbox;
            return filter;
        }

        public static FlattenCombo CopyColors(this FlattenCombo combo)
        {
            combo.ForeColor = fc.ForeColor;
            combo.BackColor = fc.BackColor;
            combo.DropDownColor = fc.DropDownColor;
            combo.ForeColorDrop = fc.ForeColorDrop;
            combo.ButtonColor = fc.ButtonColor;
            combo.Arrow_Active = fc.Arrow_Active;
            combo.Arrow_Inactive = fc.Arrow_Inactive;
            combo.Border = fc.Border;
            return combo;
        }

        public static SearchResult_Minimalistic CopyColors(this SearchResult_Minimalistic s)
        {
            s.ForeColor = sr.ForeColor;
            s.ForeColor_Button = sr.ForeColor_Button;
            s.ForeColor_Online = sr.ForeColor_Online;
            s.ForeColor_Offline = sr.ForeColor_Offline;
            s.ForeColor_Afk = sr.ForeColor_Afk;
            s.Pseudo = sr.Pseudo;
            s.Normal = sr.Normal;
            s.Magic = sr.Magic;
            s.Rare = sr.Rare;
            s.Unique = sr.Unique;
            s.Relic = sr.Relic;
            s.Divination = sr.Divination;
            s.Gem = sr.Gem;
            s.Currency = sr.Currency;
            s.Prophecy = sr.Prophecy;
            s.Corrupted = sr.Corrupted;
            s.Unidentified = sr.Unidentified;
            s.Crafted = sr.Crafted;
            s.Veiled = sr.Veiled;
            s.BackColor = sr.BackColor;
            s.BackColor_Button = sr.BackColor_Button;
            s.BackColor_Online = sr.BackColor_Online;
            s.BackColor_Offline = sr.BackColor_Offline;
            s.BackColor_Afk = sr.BackColor_Afk;
            s.BackColor_ModHover = sr.BackColor_ModHover;
            s.Separator = sr.Separator;
            s.Bar = sr.Bar;
            return s;
        }

        public static ExchangeResult CopyColors(this ExchangeResult e)
        {
            e.ForeColor = er.ForeColor;
            e.ForeColor_Exchange = er.ForeColor_Exchange;
            e.ForeColor_ExchangeHover = er.ForeColor_ExchangeHover;
            e.ForeColor_Online = er.ForeColor_Online;
            e.ForeColor_Offline = er.ForeColor_Offline;
            e.ForeColor_Afk = er.ForeColor_Afk;
            e.ForeColor_Warning = er.ForeColor_Warning;
            e.BackColor = er.BackColor;
            e.BackColor_Top = er.BackColor_Top;
            e.BackColor_Bottom = er.BackColor_Bottom;
            e.BackColor_Online = er.BackColor_Online;
            e.BackColor_Offline = er.BackColor_Offline;
            e.BackColor_Afk = er.BackColor_Afk;
            e.BackColor_Exchange = er.BackColor_Exchange;
            e.BackColor_ExchangeHover = er.BackColor_ExchangeHover;
            e.BackColor_ModHover = er.BackColor_ModHover;
            return e;
        }

        public static ModGroup CopyColors(this ModGroup grp)
        {
            grp.ForeColor_Textbox = mg.ForeColor_Textbox;
            grp.ForeColor_Placeholder = mg.ForeColor_Placeholder;
            grp.ForeColor_Header = mg.ForeColor_Header;
            grp.ForeColor_Button = mg.ForeColor_Button;
            grp.BackColor = mg.BackColor;
            grp.BackColor_Header = mg.BackColor_Header;
            grp.BackColor_Button = mg.BackColor_Button;
            return grp;
        }

        public static ModSearch CopyColors(this ModSearch grp)
        {
            grp.ForeColor_Textbox = ms.ForeColor_Textbox;
            grp.ForeColor_Button = ms.ForeColor_Button;
            grp.ForeColor_Listbox = ms.ForeColor_Listbox;
            grp.ForeColor_Placeholder = ms.ForeColor_Placeholder;
            grp.BackColor = ms.BackColor;
            grp.BackColor_Textbox = ms.BackColor_Textbox;
            grp.BackColor_Listbox = ms.BackColor_Listbox;
            grp.BackColor_Button = ms.BackColor_Button;
            return grp;
        }

        public static FastBulkButton CopyColors(this FastBulkButton b)
        {
            b.ForeColor = fbb.ForeColor;
            b.ForeColor_Selected = fbb.ForeColor_Selected;
            b.ForeColor_Hover = fbb.ForeColor_Hover;
            b.BackColor = fbb.BackColor;
            b.BackColor_Selected = fbb.BackColor_Selected;
            b.Border = fbb.Border;
            b.Border_Selected = fbb.Border_Selected;
            return b;
        }

        public static Toggle CopyColors(this Toggle to)
        {
            to.ForeColor = toggle.ForeColor;
            to.Bar_Off = toggle.Bar_Off;
            to.Bar_On = toggle.Bar_On;
            to.Border_Off = toggle.Border_Off;
            to.Border_On = toggle.Border_On;

            if (toggle.KnobImage != null)
                to.KnobImage = toggle.KnobImage;
            else
            {
                to.KnobStyle = KnobStyle.Circle;
                to.KnobColor = Color.White;
                to.KnobSize = new Size(10, 10);
            }

            return to;
        }
    }
}