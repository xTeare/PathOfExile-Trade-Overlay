﻿using PoE_Trade_Overlay.Forms;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace PoE_Trade_Overlay
{
    public static class Update
    {
        public static void Check()
        {
            bool newAviable = false;
            string file;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://gitlab.com/xTeare/PathOfExile-Trade-Overlay/raw/master/version.txt");

            Version online = null;
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    file = streamReader.ReadToEnd();

                string[] info = file.Split(',');

                online = Version.Parse(info[0]);
                Version local = Version.Parse(Config.version);
                Console.WriteLine("Online : " + online);

                Console.WriteLine("Local  : " + local);
                Console.WriteLine("Compare: " + online.CompareTo(local));
                if (online.CompareTo(local) == 1)
                    newAviable = true;
                
                
            }
            catch (Exception e)
            {
                Console.WriteLine("Execption in Update::Check() : " + e.Message);
            }

            if (newAviable)
            {
                Form_Update fu = new Form_Update();
                fu.newVersion = online.ToString();
                fu.Show();
            }
        }
        
    }
}