﻿using PoE_Trade_Overlay.Controls;
using PoE_Trade_Overlay.Queries;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Net;
using System.Windows.Forms;

namespace PoE_Trade_Overlay
{
    public static class Extensions
    {
        
        public static string Between(this string value, string a, string b)
        {
            int posA = value.IndexOf(a);
            int posB = value.LastIndexOf(b);
            if (posA == -1)
            {
                return "";
            }
            if (posB == -1)
            {
                return "";
            }
            int adjustedPosA = posA + a.Length;
            if (adjustedPosA >= posB)
            {
                return "";
            }
            return value.Substring(adjustedPosA, posB - adjustedPosA);
        }

        public static Image GetCurrencyImageByString(this string source)
        {
            Image img = null;

            return img;
        }

        /// <summary>
        /// Try to convert string to boolean Returns false if it fails
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static bool ToBoolean(this string source)
        {
            bool b = false;
            try
            {
                b = Convert.ToBoolean(source);
            }
            catch (FormatException e)
            {
                Console.WriteLine("Could not convert to boolean : " + e);
            }
            return b;
        }
        
        /// <summary>
        /// Try to convert string to float
        /// Returns -1 if it fails
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static float ToFloat(this string source)
        {
            float f = -1.0f;
            try
            {
                f = Convert.ToSingle(source);
            }
            catch (FormatException e)
            {
                Console.WriteLine("Could not convert to float : " + e);
            }
            return f;
        }

        /// <summary>
        /// Try to convert string to Int32
        /// Returns -1 if it fails
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static int ToInt(this string source)
        {
            int i = -1;
            try
            {
                i = Convert.ToInt32(source);
            }
            catch (FormatException e)
            {
                Console.WriteLine("<ToInt> : Could not convert to Int32 : " + e + "Input string : " + source);
            }
            return i;
        }

        /// <summary>
        /// Try to convert string to Long
        /// Returns -1 if it fails
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static long ToLong(this string source)
        {
            long i = 0;
            try
            {
                i = Convert.ToInt64(source);
            }
            catch (FormatException e)
            {
                Console.WriteLine("<ToLong>() : Could not convert string(" + source + ") to Int64 : " + e);
            }
            return i;
        }


        public static float LoadPriceInChaos(string origPrice, float amount)
        {
            string priceString = Data.GetChaosEquivalent(origPrice);
            try
            {
                float price = float.Parse(priceString);
                return (price * amount);
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("Extensions::LoadPriceInChaos exception : {0}, Input String {1}", e.Message, origPrice));
            }

            return 0.0f;
        }

        public static string ToShortColor(this Color color)
        {
            return string.Format("{0},{1},{2},{3}", color.A, color.R, color.G, color.B);
        }

        /// <summary>
        /// Converts a string to Color. input can be one, three or four numbers (0,0,0)
        /// a,r,g,b / r,g,b / rgb
        /// Default color is 200,200,200
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static Color ToColor(this string source)
        {
            Color c = new Color();
            c = Color.FromArgb(200, 200, 200);
            if(source != string.Empty && source != null)
            {
                if (source.Length != 0)
                {
                    string[] val = source.Split(',');
                    if (val.Length == 1)
                    {
                        c = Color.FromArgb(255, val[0].ToInt(), val[0].ToInt(), val[0].ToInt());
                    }
                    else if (val.Length == 2)
                    {
                        c = Color.FromArgb(val[0].ToInt(), val[1].ToInt(), val[1].ToInt(), val[1].ToInt());
                    }
                    else if (val.Length == 3)
                    {
                        c = Color.FromArgb(255, val[0].ToInt(), val[1].ToInt(), val[2].ToInt());
                    }
                    else if (val.Length == 4)
                    {
                        c = Color.FromArgb(val[0].ToInt(), val[1].ToInt(), val[2].ToInt(), val[3].ToInt());
                    }
                }
            }
            
            return c;
        }

        public static bool BetterContains(this string source, string toCheck, StringComparison comp)
        {
            return source != null && toCheck != null && source.IndexOf(toCheck, comp) >= 0;
        }

        public static PuneHedgehog SetDataFromFilterControl(Controls.Filter filterControl)
        {
            PuneHedgehog source = new PuneHedgehog();

            string min = filterControl.GetMin();
            string max = filterControl.GetMax();

            if (min != null)
                source.Min = min.ToLong();
            else
                source.Min = null;

            if (max != null)
                source.Max = max.ToLong();
            else
                source.Max = null;

            return source;
        }

        public static Links SetDataFromControlSocketFilter(this Filter_Sockets filterControl)
        {
            Links links = new Links();

            string min = filterControl.GetMin();
            string max = filterControl.GetMax();
            string r = filterControl.GetR();
            string g = filterControl.GetG();
            string b = filterControl.GetB();
            string w = filterControl.GetW();

            if (min != null)
                links.Min = min.ToLong();
            else
                links.Min = null;

            if (max != null)
                links.Max = max.ToLong();
            else
                links.Max = null;

            if (r != null)
                links.R = r.ToLong();
            else
                links.R = null;

            if (g != null)
                links.G = g.ToLong();
            else
                links.G = null;

            if (b != null)
                links.B = b.ToLong();
            else
                links.B = null;

            if (w != null)
                links.W = w.ToLong();
            else
                links.W = null;

            return links;
        }

        public static string After(this string value, string a)
        {
            int posA = value.LastIndexOf(a);
            if (posA == -1)
            {
                return "";
            }
            int adjustedPosA = posA + a.Length;
            if (adjustedPosA >= value.Length)
            {
                return "";
            }
            return value.Substring(adjustedPosA);
        }

        /// <summary>
        /// Removes part of a string starting by a keyword.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="keyword"></param>
        /// <param name="removeKeyword"></param>
        /// <returns></returns>
        public static string RemoveFrom(this string source, string keyword, bool removeKeyword)
        {
            int index = source.IndexOf(keyword);

            if (!removeKeyword)
                index += keyword.Length;

            return source.Remove(index, source.Length - index);
        }

        /// <summary>
        /// Removes part of the string from string:from to string:to
        /// </summary>
        /// <param name="source"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static string RemoveFromTo(this string source, string from, string to, int startOffset = 0)
        {
            if (source.Contains(from) && source.Contains(to))
            {
                int index = source.IndexOf(from) + startOffset;
                int index2 = source.IndexOf(to, index);

                source = source.Remove(index, (index2 - index) + 1);
            }

            return source;
        }

        public static bool Contains(this object[] arr, object obj)
        {
            bool contains = false;
            if (arr == null)
                return contains;

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == obj)
                    contains = true;
            }
            return contains;
        }

        public static Image GetImage(this string url)
        {
            Image img;
            var request = WebRequest.Create(url);
            using (var response = request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    img = Bitmap.FromStream(stream);
                }
            }
            return img;
        }

        public static List<Control> GetControls(this Control ctrl)
        {
            List<Control> controles = new List<Control>();
            foreach (Control c in ctrl.Controls)
            {
                if (c.GetType().Name == "TabPage" && c.Name != "tp_search")
                {
                    //Console.WriteLine(c.Name);
                }
                else
                {
                    controles.Add(c);
                }

                if (c.Controls.Count > 0)
                {
                    controles.AddRange(GetControls(c));
                }
            }
            return controles;
        }

        public static List<Control> GetControlsOfType(this Control ctrl, Type type)
        {
            List<Control> controles = new List<Control>();
            foreach (Control c in ctrl.Controls)
            {
                //Console.WriteLine(c.GetType() + " : " + type);
                if (c.GetType() == type)
                    controles.Add(c);

                if (c.Controls.Count > 0)
                    controles.AddRange(GetControls(c));
            }
            return controles;
        }

        public static bool ContainsAnyElement(this string source, string[] array)
        {
            foreach (string s in array)
                if (source.Contains(s))
                    return true;
            return false;
        }

        public static bool IsNumber(this string source)
        {
            int n;
            return int.TryParse(source, out n);
        }

        public static bool IsAfk(this Result result)
        {
            if (result.Listing.Account.Online != null)
            {
                if (result.Listing.Account.Online.Status != null)
                {
                    if (result.Listing.Account.Online.Status == "afk")
                        return true;
                }
            }

            return false;
        }

        public static bool IsUniqueMap(this string source)
        {
            string[] maps = {
                "Acton's Nightmare",
                "Caer Blaidd, Wolfpack's Den",
                "Death and Taxes",
                "Hall of Grandmasters",
                "Hallowed Ground",
                "Maelström of Chaos",
                "Mao Kun",
                "Oba's Cursed Trove",
                "Olmec's Sanctum",
                "Pillars of Arun",
                "Poorjoy's Asylum",
                "The Beachhead",
                "The Coward's Trial",
                "The Perandus Manor",
                "The Putrid Cloister",
                "The Twilight Temple",
                "The Vinktar Square",
                "Vaults of Atziri",
                "Whakawairua Tuahu"
            };
            foreach (string m in maps)
            {
                if (source.Contains(m))
                    return true;
            }
            return false;
        }

        public static string ToDiscriminator(this string source)
        {
            if (source.Contains("(War for the Atlas)"))
                return "warfortheatlas";
            else if (source.Contains("Atlas of Worlds"))
                return "atlasofworlds";
            else if (source.Contains("Original"))
                return "original";
            else if (source.Contains("The Awakening"))
                return "theawakening";

            return "";
        }
    }
}